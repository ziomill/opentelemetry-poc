#!/bin/bash

#ELASTIC_CLUSTER_BASE_URL="elasticsearch:9200"
ELASTIC_CLUSTER_USERNAME="elastic"
ELASTIC_CLUSTER_PWD="elastic"
KIBANA_SYSTEM_USERNAME="kibana_system"
KIBANA_SYSTEM_PWD="kibana_system"
APM_WRITER_ROLE="apm_writer_role"
APM_WRITER_USERNAME="apm_writer"
APM_WRITER_PWD="apm_writer"

### CURL ElasticSearch: Healtcheck   ###
checkIfElasticSearchIsAvailable() {
  http_code=$(curl -o /dev/null --write-out "%{http_code}\n" -u "${ELASTIC_CLUSTER_USERNAME}:${ELASTIC_CLUSTER_PWD}" -X GET "${ELASTIC_CLUSTER_BASE_URL}/_cluster/health?pretty")
  if [ "$http_code" -eq 200 ]
  then
     return 1
  else
     return 0
  fi
}

### CURL ElasticSearch: Reset 'kibana_system' user's Password ###
resetKibanaSystemPassword() {
  http_code=$(curl -o /dev/null --write-out "%{http_code}\n" -X POST "${ELASTIC_CLUSTER_BASE_URL}/_security/user/${KIBANA_SYSTEM_USERNAME}/_password" -H "Content-Type: application/json" -u "${ELASTIC_CLUSTER_USERNAME}:${ELASTIC_CLUSTER_PWD}" -d'
                {
                  "password" : "'$KIBANA_SYSTEM_PWD'"
                }
            ')
  if [ "$http_code" == 200 ]
  then
     return 1
  else
     return 0
  fi
}

### CURL ElasticSearch: Create 'apm_writer_role' Role ###
createApmWriterRole() {
  http_code=$(curl -o /dev/null --write-out "%{http_code}\n" -X POST "${ELASTIC_CLUSTER_BASE_URL}/_security/role/${APM_WRITER_ROLE}?pretty" -H 'Content-Type: application/json' -u "${ELASTIC_CLUSTER_USERNAME}:${ELASTIC_CLUSTER_PWD}" -d '
            {
              "cluster": ["all"],
              "indices": [
                {
                  "names": [ "traces-apm*","logs-apm*","metrics-apm*"],
                  "privileges": ["auto_configure","create_doc"]
                }
              ]
            }
           ')
  if [ "$http_code" == 200 ]
    then
       return 1
    else
       return 0
    fi
}

### CURL ElasticSearch: Create 'apm_writer' User with 'apm_writer_role' Role ###
createApmWriterUser() {
  http_code=$(curl -o /dev/null --write-out "%{http_code}\n" -X POST "${ELASTIC_CLUSTER_BASE_URL}/_security/user/${APM_WRITER_USERNAME}?pretty" -H 'Content-Type: application/json' -u "${ELASTIC_CLUSTER_USERNAME}:${ELASTIC_CLUSTER_PWD}" -d'
  {
    "password" : "'${APM_WRITER_USERNAME}'",
    "roles" : [ "'${APM_WRITER_ROLE}'"]
  }
  ')
  if [ "$http_code" == 200 ]
      then
         return 1
      else
         return 0
      fi
}

printf "###### Setup started ...\n"

### Step 1: Perform an HealtCheck on ElasticServer cluster
printf "###### STEP 1: Waiting for ElasticSearch cluster to become available\n"
current_retry=0
max_retry=50
wait_before_retry=5
checkIfElasticSearchIsAvailable
is_elasticsearch_available=$?

while [[ ( $is_elasticsearch_available == 0 ) && $current_retry -le $max_retry ]]
do
  printf "\nCurrent retry: ${current_retry} of ${max_retry} --> The ElasticSearch cluster at ${ELASTIC_CLUSTER_BASE_URL} is not yet available. A new HealthCheck will be performed in ${wait_before_retry} seconds...\n"
  ((current_retry++))
  sleep $wait_before_retry
  checkIfElasticSearchIsAvailable
  is_elasticsearch_available=$?
done

if [ "$is_elasticsearch_available" -eq 1 ]
  then
     printf "\nThe ElasticSearch cluster at ${ELASTIC_CLUSTER_BASE_URL} is now available\n"
  else
     printf "\nThe ElasticSearch cluster at ${ELASTIC_CLUSTER_BASE_URL}, after ${max_retry} retry, is not yet available. Please, check the logs. Shutting down...\n"
     exit 1
  fi

### Step 2: Reset "kibana_system" user password
printf "###### STEP 2: Resetting 'kibana_system' password...\n"
resetKibanaSystemPassword
is_kibana_system_pwd_resetted=$?

if [ $is_kibana_system_pwd_resetted == 0 ]
  then
     printf "There was an errore during 'kibana_system' password resetting. Shutting down services...\n"
     exit 1
  fi

printf "\n'kibana_system' password has been resetted with success\n"


### STEP 3: Create "apm_writer_role" role ###
printf "###### STEP 3: Creating the 'apm_writer_role' role...\n"
createApmWriterRole
is_apm_writer_role_created=$?

if [ $is_apm_writer_role_created == 0 ]
  then
     printf "There was an error during 'apm_writer_role' role creation. Shutting down services...\n"
     exit 1
  fi
printf "\n'apm_writer_role' role created\n"

### STEP 4: Create "apm_writer" user with "apm_writer_role" ###
printf "###### STEP 4: Creating the 'apm_writer' user...\n"
createApmWriterUser
is_apm_writer_user_created=$?

if [ $is_apm_writer_user_created == 0 ]
  then
     printf "\nThere was an error during 'apm_writer' user creation. Shutting down services...\n"
     exit 1
  fi
printf "\n'apm_writer' user created \n"

printf "\n############################# Setup completed ######################################\n"
printf "#                                                                                    #\n"
printf "#           ElasticSearch cluster has been configured with success.                  #\n"
printf "#                                                                                    #\n"
printf "#  Complete the configuration by installing the 'Elastic APM Integration' through    #\n"
printf "#  the Kibana console (http://localhost:5601/)                                       #\n"
printf "#                                                                                    #\n"
printf "#  See at: https://www.elastic.co/guide/en/apm/guide/current/apm-quick-start.html    #\n"
printf "#                      [STEP 3: Add the APM Integration']                            #\n"
printf "#                                                                                    #\n"
printf "######################################################################################\n"