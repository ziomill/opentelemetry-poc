#!/bin/bash

### Routine that performs the Healtcheck on Influxdb
#INFLUXDB_CLUSTER_BASE_URL=localhost:8086
INFLUXDB_CLUSTER_TOKEN=my_very_secret_influxdb_token
INFLUXDB_ORGANIZATION_NAME=millsoft

checkIfInfluxDBIsAvailable() {
  http_code=$(curl -s -o /dev/null --write-out "%{http_code}\n" -X GET "${INFLUXDB_CLUSTER_BASE_URL}/health" \
  							     --header 'Accept: application/json' \
  							     --header 'Authorization: Token '${INFLUXDB_CLUSTER_TOKEN}'')
  if [ "$http_code" -eq 200 ]
  then
     return 1
  else
     return 0
  fi
}

#################################################################################################
###    Step 1: Perform an HealtCheck on InfluxDB cluster untill the cluster became availble   ###
#################################################################################################
printf "###### STEP 1: Waiting for InfluxDB cluster to became available\n"
current_retry=0
max_retry=50
wait_before_retry=5
checkIfInfluxDBIsAvailable
is_influxdb_available=$?

while [[  ($is_influxdb_available == 0) && $current_retry -le $max_retry ]]
do
  printf "\nCurrent retry: ${current_retry} of ${max_retry} --> The InfluxDB cluster at ${INFLUXDB_CLUSTER_BASE_URL} is not yet available. A new HealthCheck will be performed in ${wait_before_retry} seconds...\n"
  ((current_retry++))
  sleep $wait_before_retry
  checkIfInfluxDBIsAvailable
  is_influxdb_available=$?
done

if [ "$is_influxdb_available" -eq 1 ]
  then
     printf "The InfluxDB cluster at ${INFLUXDB_CLUSTER_BASE_URL} is now available\n"
  else
     printf "The InfluxDB cluster at ${INFLUXDB_CLUSTER_BASE_URL}, after ${max_retry} retry, is not yet available. Please, check the logs. Shutting down...\n"
     exit 1
fi

#######################################################
### STEP 2: Get the "millsoft" organization details ###
#######################################################
printf "\n###### STEP 2: Get the "millsoft" organization details\n"
GET_ORG_RESPONSE=$(curl -s --request GET \
  --url 'http://'${INFLUXDB_CLUSTER_BASE_URL}'/api/v2/orgs?org='${INFLUXDB_ORGANIZATION_NAME}'' \
  --header 'Accept: application/json' \
  --header 'Authorization: Token '${INFLUXDB_CLUSTER_TOKEN}'')

ORG_ID=$(echo "${GET_ORG_RESPONSE}" | jq -rc  '.orgs[].id')
printf "The ORGANIZATION_ID of the MILLSOFT organization is: "$ORG_ID"\n"


####################################################################################
### STEP 3: Check if the Initial Stack has been already installed on the cluster ###
####################################################################################
printf "\n###### STEP 3: Check if the Init Stack has been already installed on the cluster\n"

INFLUXDB_INIT_STACK_NAME=opentelemetry-poc-influx-init-stack
GET_STACK_RESPONSE=$(curl -s --request GET \
  --url 'http://'${INFLUXDB_CLUSTER_BASE_URL}'/api/v2/stacks?orgID='${ORG_ID}'&name='${INFLUXDB_INIT_STACK_NAME}'' \
  --header 'Accept: application/json' \
  --header 'Authorization: Token '${INFLUXDB_CLUSTER_TOKEN}'')

IS_INIT_STACK_INSTALLED=$(echo "${GET_STACK_RESPONSE}" | jq '.stacks[].name | contains("'$INFLUXDB_INIT_STACK_NAME'")')

if [ $IS_INIT_STACK_INSTALLED ]
then
  printf "Init stack has been found. Nothing to do here ...\n"
  exit 1
else
  printf "Init stack not found. Let's install it ...\n"
fi

#######################################
### STEP 4: Install the init stack  ###
#######################################
printf "\n###### STEP 4: Install the Init Stack\n"

### Apply the template
INIT_TEMPLATE_URL=https://bitbucket.org/ziomill/opentelemetry-poc/raw/cad8e56769b841982786c62b775135f5af8e81d2/influxdb/opentelemetry-poc-influx-template.yml
APPLY_TEMPLATE_RESPONSE=$(curl -s -w "\n%{http_code}" \
  --request POST \
  --url 'http://'${INFLUXDB_CLUSTER_BASE_URL}'/api/v2/templates/apply' \
  --header 'Accept: application/json' \
  --header 'Authorization: Token '${INFLUXDB_CLUSTER_TOKEN}'' \
  --header 'Content-Type: application/json' \
  --data '{
      "orgID": "'${ORG_ID}'",
      "remotes": [
        {
          "url": "'${INIT_TEMPLATE_URL}'"
        }
      ]
    }')

APPLY_TEMPLATE_RESPONSE_HTTP_CODE=$(tail -n1 <<< "$APPLY_TEMPLATE_RESPONSE")  # The last line
APPLY_TEMPLATE_RESPONSE_BODY=$(sed '$ d' <<< "$APPLY_TEMPLATE_RESPONSE")   # Lll but the last line which contains the status code

#STACK_ID=$(echo "${APPLY_TEMPLATE_RESPONSE}" | jq -rc  '.stackID')
#echo $STACK_ID
if [ "$APPLY_TEMPLATE_RESPONSE_HTTP_CODE" -ne 201 ]
  then
    printf "There was an error during the /templates/apply. The HTTP CODE returned is: %d. Exiting..." "$APPLY_TEMPLATE_RESPONSE_HTTP_CODE"
    exit 1
fi
STACK_ID=$(echo "${APPLY_TEMPLATE_RESPONSE_BODY}" | jq -rc  '.stackID')
printf "The Template has been applied with success. The generated STACK_ID is: %s" "$STACK_ID"


### Update the generated stack with the custom INIT_STACK_NAME
UPDATE_STACK_RESPONSE_HTTP_CODE=$(curl -s -o /dev/null --write-out "%{http_code}\n" --request PATCH \
  --url 'http://'${INFLUXDB_CLUSTER_BASE_URL}'/api/v2/stacks/'${STACK_ID}'' \
  --header 'Accept: application/json' \
  --header 'Authorization: Token '${INFLUXDB_CLUSTER_TOKEN}'' \
  --header 'Content-Type: application/json' \
  --data '{
"name": "'${INFLUXDB_INIT_STACK_NAME}'"
}')

if [ "$UPDATE_STACK_RESPONSE_HTTP_CODE" -ne 200 ]
  then
    printf "\nThere was an error during the installation of the InfluxDB Init Stack...\n"
    exit 1
fi

printf "\n############################# Setup completed ######################################\n"
printf "#                                                                                    #\n"
printf "#             InfluxDB init stack has been configured with success.                  #\n"
printf "#                                                                                    #\n"
printf "#                   Check metrics on the InfluxDB console:                           #\n"
printf "#                   URL      : http://localhost:8086/                                #\n"
printf "#                   Username : influx                                                #\n"
printf "#                   Password : influx                                                #\n"
printf "#                                                                                    #\n"
printf "######################################################################################\n"