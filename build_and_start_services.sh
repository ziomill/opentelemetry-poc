#!/bin/bash

### Build "shop-api-gateway" maven project
mvn -f eshopping-app/shop-api-gateway/pom.xml clean install -DskipTests

### Build "users-api" maven project
mvn -f eshopping-app/users-api/pom.xml clean install -DskipTests

### Build "products-api" maven project
mvn -f eshopping-app/products-api/pom.xml clean install -DskipTests

### Build "orders-api" maven project
mvn -f eshopping-app/orders-api/pom.xml clean install -DskipTests

### Start services
docker compose up






