# E-Shopping: A PoC about Observability with OpenTelemetry #
***
## PoC Goal
* Demonstrate the benefits of **OpenTelemetry**'s standard and tools especially focusing on how it  **enables decoupling** between **telemetry data producers** and **Observability backend tools**, bringing down the **vendor lock-in**.

## The **OpenTelemetry** big picture
![](./docs/images/OpenTelemetry_Design.PNG)

Credits: https://opentelemetry.io/

## PoC scenario: The "eShopping" platform
The PoC implements a very simple **e-commerce platform** which leverages on **OpenTelemetry** to collect and send telemetry data to different observability backend tools.

These are the main OpenTelemetry capabilities approached in the PoC:
* Workloads **Automatic Instrumentation** through the **OpenTelemetry Java agent**.
* Telemetry data (logs, traces and Metrics) collection through the **OpenTelemetry collector**.
* Custom metrics implementation through the use of the **OpenTelemetry Metrics SDK**.
* The ability to adopt different **Observability backend tools** (like Elastic, Kibana, Jaeger, Influxdb etc.) for telemetry data analysis **_avoiding the vendor lock-in_**.

## PoC Architecture
***
The simple "**e-commerce platform**" is made of:
* Some Java based microservices with MongoDB integration:
    * **users-api**
    * **orders-api**
    * **products-api**
* An API Gateway, based on Spring Cloud Gateway:
    * **shop-api-gateway**

Each **component** is instrumented with the **OpenTelemetry** Java agent.
Agents extract telemetry data and send it to the central **OpenTelemetry** collector.
The central OpenTelemetry collector receives, eventually transforms and finally sends telemetry data to any of the configured Backend analysis tool.

![](./docs/images/poc_architecture.PNG)

## Analysis Tools
The Backend analysis tools adopted in the PoC are:

* To handle **Logs** and **Traces**
  * _**Elastic Observability**_ - **_Elastic APM + ElasticSearch + Kibana_** - (Logs and Traces): 
    * Elastic APM Server receives data from the OpenTelemetry collector, 
    transform it in the ElasticSearch format and finally send it to the ElasticSearch storage. 
    Finally, Kibana provides a powerful web console which allow to analyze data previously stored in ElasticSearch.
  * **_Loki + Grafana_** (only Logs):
    * The OpenTelemetry collector sends data to Loki, a sort of time-series db, but for logs.
    Grafana, like Kibana, provides a web console through which analyze data previously stored in Loki.
  * **_Jaeger_** (only Traces):
    * The OpenTelemetry collector send data to Jaeger, a tracing system which provide a mechanism to store traces "somewhere" and
    a powerful web console to easily analyze distributed traces. Jaeger can rely on different storage system like Cassandra or 
    ElasticSearch in order to store traces. In the PoC, for simplicity, the "in-memory" storage strategy ha been used.
* To handle **Metrics**
  * **_Prometheus + Grafana_**
  The OpenTelemetry collector expose a "Prometheus metrics endpoint" which allow the metrics scraping by Prometheus server. 
  Prometheus scrapes it at regular intervals and store metrics in it internal time-series db.
  Grafana provides a web console with specific dashboards, through which analyze the Prometheus stored metrics.
  * **_InfluxDB_**
  The OpenTelemetry collector send data to InfluxDB, which is a time-series db.
  InfluxDB also provides a web console used to create very useful dashboards for metrics analysis.

## Main use case
***
The e-shopping users, by creating orders, triggers distributed transactions which spans across several microservices.
***OpenTelemetry collects telemetry data about Logs, Traces and Metrics***  allowing the "e-commerce platform" to be monitored 
through the provided analysis tools.

## Environment setup
***
The largest part of the configurations have been automatized. 
The only manual configuration that have to be done is the **Elastic APM Integration** on Kibana.
A detailed how-to has been provided in the "**Install the Elastic APM Integration on Kibana**" paragraph.

### Deploy modules
Minimal requirements: **Maven** and **Docker** must be installed on your system. 

Run the _**build_and_start_services.sh**_ script. The process will build code, deploy components and create all needed configurations.
It takes about 3-4 minutes to complete (depending on your system properties)  

### Install the Elastic APM Integration on Kibana
In order to collect OpenTelemetry logs and traces signals on ElasticSearch and see them on Kibana, the **Elastic APM Integration** must be installed.

The Elastic APM Integration will configure all needed indexes to collect OpenTelemetry signals.
Further information at [STEP 3: Add the APM Integration](https://www.elastic.co/guide/en/apm/guide/current/apm-quick-start.html)

1. Connect to Kibana Console available at http://localhost:5601/  
   **username**: elastic  
   **password**: elastic
2. Expand the menu on the right and click on "**Add Integration**"
3. Select "**Elastic APM**"
4. Go to the "**Elastic APM in Fleet**" tab and click on <img src="./docs/images/Kibana_Install_APM_Integration_button.PNG" width="100"> button:
5. Click on "**Add Elatic APM**" and populate fields ad shown below:  
   **Integration settings** --> **Integration Name** --> apm-opentelemetry  
   **General** --> **Server Configuration** --> **Host** --> apm-es-server:8200 (The HOST of the Elastic APM Server)
   **General** --> **Server Configuration** --> **URL** --> http://apm-es-server:8200  (The URL of the Elastic APM Server)
   **Agent Authorization** --> **Anonymous Agent access** --> Enabled  
   **Agent Authorization** --> **Anonymous Agent access** --> **Allow Agentss** --> Remove all pre-selected agents (Field have to be empty)  
   **Where to add this Integration** --> **Create Agent Policy** -->  **New agent policy name** --> opentelemetry-policy  
6. Click on "**Save and Continue**" and wait the end of installation.
   When the process ends, the **Elastic APM integration added** popup appears. Select **Add Elastic Agent later**.
   
Check the **Elastic APM Integration** installation:
1. Expand the menu on the right and click on "**Management --> Integrations**"  
2. In the "**Browse Integrations**" tab, select **"Elastic APM"**
3. Go the **"Elastic APM in Fleet"** tab
4. Click on "**Check APM Server status**". You should see the message **You have correctly setup APM Server**  

Now Kibana is ready to show OpenTelemetry signals received by the Elastic APM server.  

## Usage
***

### Generate traffic on the eShopping platform

1. **Crete a new account**
   ```bash
      curl --request POST \
           --url http://localhost:9000/shops-api-gw/users-api/user \
           --header 'Content-Type: application/json' \
           --data '{
            "name": "Jhon",
            "surname": "Doe",
            "email": "jhon.doe@gmail.com"
     }'
   ```
   Take note of the **trace-id** in the response header

2. **Add some Products to the Inventory**
   ```bash
   curl --request POST \
        --url http://localhost:9000/shops-api-gw/products-api/products \
        --header 'Content-Type: application/json' \
        --data '[
        {
            "code":"PRD000001",
            "description": "Epson Printer XL1000",
            "category":"Electronics",
            "price": 150.50,
            "availableStock": 25
        },
        {
            "code":"PRD000002",
            "description": "Lenovo X1 Carbon",
            "category":"PC & Tablet",
            "price": 1800.50,
            "availableStock": 50
        },
        {
            "code":"PRD000003",
            "description": "Logictech Optical Mouse",
            "category":"Electronics",
            "price": 13.00,
            "availableStock": 5
        }
      ]'
   ```  
   Take note of the **trace-id** in the response header

3. **Submit a new (valid) Order**
   ```bash
   curl --request POST \
        --url http://localhost:9000/shops-api-gw/orders-api/order/user/luke.blue@gmail.com \
        --header 'Content-Type: application/json' \
        --data '{
          "PRD000001": 1,
          "PRD000002": 1
        }'
   ``` 
   Take note of the **trace-id** in the response header

4. **Submit an Invalid Order (insufficient stock)**
   ```bash
   curl --request POST \
        --url http://localhost:9000/shops-api-gw/orders-api/order/user/luke.blue@gmail.com \
        --header 'Content-Type: application/json' \
        --data '{
          "PRD000003": 10
        }'
   ``` 
   Take note of the **trace-id** in the response header

### Logs & Traces analysis

#### Backend Analysis tool: Elastic Observability (ElasticSearch + Kibana + Elastic APM Server)

1. Connect to the **Kibana** Console available at http://localhost:5601/  
   **username**: elastic  
   **password**: elastic

2. Through the Menu on the left, go to the **OBSERVABILITY** - **APM** - **Services** tab. You should see the **three microservices** which compose the PoC and the **Api Gateway**
   ![](./docs/images/Elastic_APM_Services.PNG)

3. Analyze **_Traces_**, **_Logs_** and **_metrics_** of the executed transactions  
   Search one of the distributed transaction by using the **trace-id** (I suggest to use the **Add User Order _trace-id_** in order to see 
   a transaction that span over the four microservices)  
   ![](./docs/images/Elastic_APM_SearchTrace.PNG)  
   ![](./docs/images/Elastic_APM_DistribuitedTrace1.PNG)  
   ![](./docs/images/Elastic_APM_DistribuitedTrace2.PNG)  
   ![](./docs/images/Elastic_APM_DistribuitedTrace3.PNG)  
   ![](./docs/images/Elastic_APM_DistribuitedTrace4.PNG)  

4. Take a look at the microservices Map with interdependencies  
   ![](./docs/images/Elastic_APM_ServiceMap.PNG)

#### Loki + Grafana (only Logs)
blablabla

#### Jaeger (only Traces)
1. Connect to the **Jaeger** Console available at http://localhost:16686/

2. 

### Metrics analysis

#### Loki + Grafana
blablabla

#### InfluxDB
blablabla

## Notes
**OpenTelemetry Logs API**, at the moment, are yet in an Experimental state.







## Next Steps
***
1. ~~* Automate the generation of Grafana auth token for InfluxDB~~  
2. ~~* Automate the configuration of Grafana~~
3. Test GRPC Services
4. Integrate more Analysis tools
5. Add Kubernetes and Istio
6. Add an Histogram for Orders created and create Dashboard into Elastic and into Grafana


## Utils

Kibana:
URL: http://localhost:5601/
Useraname: elastic
Password : elastic

Grafana:
http://localhost:3000/
Useraname: grafana
Password : grafana

InlfuxDB:
http://localhost:8086/
Useraname: influxdb
Password : influxdb

Prometheus:
http://localhost:9090/

Swagger:
http://localhost:12000/orders-api/swagger-ui/index.html


TODO:
Mapstruct for GRPC in all apis
grpc and opeani client generation in orders-api