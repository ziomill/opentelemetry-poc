print('Start MongoDB setup #################################################################');

db = db.getSiblingDB('inventory_db');
db.createUser(
    {
        user: 'test',
        pwd: 'test',
        roles: [{ role: 'readWrite', db: 'inventory_db' }],
    },
);
db.createCollection('products');

db = db.getSiblingDB('users_db');
db.createUser(
    {
        user: 'test',
        pwd: 'test',
        roles: [{ role: 'readWrite', db: 'users_db' }],
    },
);
db.createCollection('users');

db = db.getSiblingDB('orders_db');
db.createUser(
    {
        user: 'test',
        pwd: 'test',
        roles: [{ role: 'readWrite', db: 'orders_db' }],
    },
);
db.createCollection('orders');

print('END MongooDB Setup #################################################################');