<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>it.millsoft</groupId>
    <artifactId>orders-api</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <grpc.spring-boot.starter.version>2.14.0.RELEASE</grpc.spring-boot.starter.version> <!-- 2.13.1.RELEASE -->
        <org.mapstruct.version>1.5.3.Final</org.mapstruct.version>
        <entur.mapstruct.protobuf-spi-impl.version>1.41</entur.mapstruct.protobuf-spi-impl.version>
        <io.grpc.version>1.51.0</io.grpc.version> <!-- 1.49.2 -->
        <protoc.compiler.version>3.21.12</protoc.compiler.version> <!-- 3.19.2 -->
        <javax.annotation-api.version>1.3.2</javax.annotation-api.version>
        <opentelemetry.version>1.21.0</opentelemetry.version> <!-- 1.16.0 -->
        <openapi.version>2.0.2</openapi.version> <!-- 2.0.0 -->
        <jackson-databind-nullable.version>0.2.4</jackson-databind-nullable.version>
        <hibernate-validator.version>8.0.0.Final</hibernate-validator.version>
        <lombok.version>1.18.24</lombok.version>
        <lombok-mapstruct-binding.version>0.2.0</lombok-mapstruct-binding.version>
        <logstash-logback-encoder.version>7.2</logstash-logback-encoder.version> <!-- 7.1.1 -->
        <janino.version>3.1.9</janino.version> <!-- 3.1.7 -->
        <commons-io.version>2.11.0</commons-io.version>
        <jcabi.urn.version>0.9</jcabi.urn.version>
        <junit-jupiter.version>5.9.0</junit-jupiter.version> <!-- 5.7.2 -->
        <junit-piooner-extension.version>1.9.1</junit-piooner-extension.version>
        <testcotainers.version>1.17.6</testcotainers.version> <!-- 1.17.1 -->
        <okhttp3.version>4.10.0</okhttp3.version>
        <apache.commons.collection.version>4.4</apache.commons.collection.version>
        <org.apache.httpclient5.version>5.2.1</org.apache.httpclient5.version>
        <grpcmock-spring-boot.version>0.9.1</grpcmock-spring-boot.version>
    </properties>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>3.0.0</version> <!-- 2.7.2 -->
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>io.grpc</groupId>
                <artifactId>grpc-bom</artifactId>
                <version>${io.grpc.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>io.opentelemetry</groupId>
                <artifactId>opentelemetry-bom</artifactId>
                <version>${opentelemetry.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>
        <!-- Spring Boot -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-mongodb</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-actuator</artifactId>
        </dependency>
        <!-- Utils -->
        <dependency>
            <groupId>org.mapstruct</groupId>
            <artifactId>mapstruct</artifactId>
            <version>${org.mapstruct.version}</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-collections4</artifactId>
            <version>${apache.commons.collection.version}</version>
        </dependency>
        <dependency>
            <groupId>com.jcabi</groupId>
            <artifactId>jcabi-urn</artifactId>
            <version>${jcabi.urn.version}</version>
        </dependency>
        <!-- OpenAPI -->
        <dependency>
            <groupId>org.springdoc</groupId>
            <artifactId>springdoc-openapi-starter-webmvc-ui</artifactId>
            <version>${openapi.version}</version>
        </dependency>
        <dependency>
            <groupId>org.openapitools</groupId>
            <artifactId>jackson-databind-nullable</artifactId>
            <version>${jackson-databind-nullable.version}</version>
        </dependency>
        <dependency>
            <groupId>org.hibernate.validator</groupId>
            <artifactId>hibernate-validator</artifactId>
            <version>${hibernate-validator.version}</version>
        </dependency>
        <!-- Grpc -->
        <dependency>
            <groupId>net.devh</groupId>
            <artifactId>grpc-server-spring-boot-starter</artifactId> <!-- Needed for GRPC Server -->
            <version>${grpc.spring-boot.starter.version}</version>
        </dependency>
        <dependency>
            <groupId>net.devh</groupId>
            <artifactId>grpc-client-spring-boot-starter</artifactId> <!-- Needed for GRPC Clients -->
            <version>${grpc.spring-boot.starter.version}</version>
        </dependency>
        <dependency>
            <groupId>io.grpc</groupId>
            <artifactId>grpc-services</artifactId> <!-- Needed to enable Reflection -->
            <version>${io.grpc.version}</version>
        </dependency>
        <dependency>
            <groupId>io.grpc</groupId>
            <artifactId>grpc-protobuf</artifactId> <!-- Needed by the GRPC Plugin to generates stubs -->
        </dependency>
        <dependency>
            <groupId>io.grpc</groupId>
            <artifactId>grpc-stub</artifactId> <!-- Needed by the GRPC Plugin to generates stubs -->
        </dependency>
        <dependency>
            <groupId>javax.annotation</groupId>
            <artifactId>javax.annotation-api</artifactId> <!-- Needed by the GRPC Plugin to generates stubs -->
            <version>${javax.annotation-api.version}</version>
            <optional>true</optional>
        </dependency>
        <!-- OpenTelemetry API and SDK -->
        <dependency>
            <groupId>io.opentelemetry</groupId>
            <artifactId>opentelemetry-api</artifactId>
        </dependency>
        <dependency>
            <groupId>io.opentelemetry</groupId>
            <artifactId>opentelemetry-sdk</artifactId>
        </dependency>
        <dependency>
            <groupId>io.opentelemetry</groupId>
            <artifactId>opentelemetry-sdk-metrics</artifactId>
        </dependency>
        <dependency>
            <groupId>io.opentelemetry</groupId>
            <artifactId>opentelemetry-sdk-trace</artifactId>
        </dependency>
        <dependency>
            <groupId>io.opentelemetry</groupId>
            <artifactId>opentelemetry-exporter-otlp</artifactId>
        </dependency>
        <!-- Logging -->
        <dependency>
            <groupId>net.logstash.logback</groupId>
            <artifactId>logstash-logback-encoder</artifactId> <!-- JSON format extended capabilities -->
            <version>${logstash-logback-encoder.version}</version>
        </dependency>
        <dependency>
            <groupId>org.codehaus.janino</groupId>
            <artifactId>janino</artifactId> <!-- Condtional logback capabilities -->
            <version>${janino.version}</version>
        </dependency>
        <!-- Test -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit-jupiter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>commons-io</groupId>
            <artifactId>commons-io</artifactId>
            <version>${commons-io.version}</version>
            <scope>test</scope>
        </dependency>
        <!-- For integration Test with MongoDB -->
        <dependency>
            <groupId>org.testcontainers</groupId>
            <artifactId>junit-jupiter</artifactId>
            <version>${testcotainers.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.testcontainers</groupId>
            <artifactId>mongodb</artifactId>
            <version>${testcotainers.version}</version>
            <scope>test</scope>
        </dependency>
        <!-- To test Rest Clients - https://square.github.io/okhttp/ -->
        <dependency>
            <groupId>com.squareup.okhttp3</groupId>
            <artifactId>mockwebserver</artifactId>
            <version>${okhttp3.version}</version>
            <scope>test</scope>
        </dependency>
        <!--  Needed to call REST API with HTTP Patch method (PATCH requests can't be submitted due to a bug with old HttpClient version) -->
        <dependency>
            <groupId>org.apache.httpcomponents.client5</groupId>
            <artifactId>httpclient5</artifactId>
            <version>${org.apache.httpclient5.version}</version>
            <scope>test</scope>
        </dependency>
        <!-- To test GRPC Clients -->
        <dependency>
            <groupId>org.grpcmock</groupId>
            <artifactId>grpcmock-spring-boot</artifactId>
            <version>${grpcmock-spring-boot.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.grpcmock</groupId>
            <artifactId>grpcmock-junit5</artifactId>
            <version>${grpcmock-spring-boot.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <extensions>
            <!-- Required by the grpc plugin to generate stubs -->
            <extension>
                <groupId>kr.motd.maven</groupId>
                <artifactId>os-maven-plugin</artifactId>
                <version>1.7.0</version>
            </extension>
        </extensions>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
            <!-- Maven compiler plugin: needed to enable mapstruct and lombok capabilities -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.10.1</version>
                <configuration>
                    <source>${maven.compiler.source}</source>
                    <target>${maven.compiler.target}</target>
                    <annotationProcessorPaths>
                        <!-- Lombok -->
                        <path>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok</artifactId>
                            <version>${lombok.version}</version>
                        </path>
                        <!-- Needed to use Mapstruct with Lombok (on 1.18.16 and above) -->
                        <path>
                            <groupId>org.projectlombok</groupId>
                            <artifactId>lombok-mapstruct-binding</artifactId>
                            <version>${lombok-mapstruct-binding.version}</version>
                        </path>
                        <!-- Needed to use Mapstruct with GRPC protobuf -->
                        <!-- IMPORTANT: because of "https://github.com/entur/mapstruct-spi-protobuf/issues/232", this must be place before the 'mapstruct processor' in order to handle ENUMS -->
                        <path>
                            <groupId>no.entur.mapstruct.spi</groupId>
                            <artifactId>protobuf-spi-impl</artifactId>
                            <version>${entur.mapstruct.protobuf-spi-impl.version}</version>
                        </path>
                        <!-- Mapstruct -->
                        <path>
                            <groupId>org.mapstruct</groupId>
                            <artifactId>mapstruct-processor</artifactId>
                            <version>${org.mapstruct.version}</version>
                        </path>
                    </annotationProcessorPaths>
                </configuration>
            </plugin>
            <!-- Plugin to generate GRPC stubs -->
            <plugin>
                <groupId>org.xolstice.maven.plugins</groupId>
                <artifactId>protobuf-maven-plugin</artifactId>
                <version>0.6.1</version>
                <configuration>
                    <protocArtifact>com.google.protobuf:protoc:${protoc.compiler.version}:exe:${os.detected.classifier}</protocArtifact>
                    <pluginId>grpc-java</pluginId>
                    <pluginArtifact>io.grpc:protoc-gen-grpc-java:${io.grpc.version}:exe:${os.detected.classifier}</pluginArtifact>
                    <outputDirectory>${project.build.sourceDirectory}</outputDirectory>
                    <clearOutputDirectory>false</clearOutputDirectory>
                    <protoSourceRoot>${basedir}/src/main/resources/proto</protoSourceRoot>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>compile</goal>
                            <goal>compile-custom</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!-- Plugin to generate Rest (OpenAPI) stubs for the SERVER -->
            <plugin>
                <groupId>org.openapitools</groupId>
                <artifactId>openapi-generator-maven-plugin</artifactId>
                <version>6.2.1</version>
                <executions>
                    <execution>
                        <id>execution-openapi-SERVER</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <inputSpec>${basedir}/src/main/resources/openapi/orders_api__server_openapi_interfaces__v0.yaml</inputSpec>
                            <output>${basedir}</output> <!-- Needed in order to generated classes in the artifact -->
                            <generatorName>spring</generatorName>
                            <generateSupportingFiles>false</generateSupportingFiles>
                            <generateModelDocumentation>false</generateModelDocumentation>
                            <configOptions>
                                <useSpringBoot3>true</useSpringBoot3>
                                <interfaceOnly>true</interfaceOnly> <!-- Generate only needed interfaces -->
                                <skipDefaultInterface>true</skipDefaultInterface> <!-- Skip the sample -->
                                <basePackage>it.millsoft.orders.api.interfaces.rest.model</basePackage>
                                <apiPackage>it.millsoft.orders.api.interfaces.rest.model</apiPackage>
                                <modelPackage>it.millsoft.orders.api.interfaces.rest.model</modelPackage>
                                <additionalModelTypeAnnotations>@lombok.NoArgsConstructor @lombok.AllArgsConstructor</additionalModelTypeAnnotations>-->
                                <useBeanValidation>true</useBeanValidation>
                                <performBeanValidation>true</performBeanValidation>
                                <serializableModel>true</serializableModel>
                                <useTags>true</useTags>
                            </configOptions>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- Generate USERS-API rest client -->
            <plugin>
                <groupId>org.openapitools</groupId>
                <artifactId>openapi-generator-maven-plugin</artifactId>
                <version>6.2.1</version>
                <executions>
                    <execution>
                        <id>execution-openapi-USERS-API-client</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <inputSpec>${basedir}/src/main/resources/openapi/clients/users_api__client_openapi_interfaces__v0.yaml</inputSpec>
                            <output>${basedir}</output> <!-- Needed in order to generated classes in the artifcat -->
                            <generatorName>spring</generatorName>
                            <generateSupportingFiles>false</generateSupportingFiles>
                            <configOptions>
                                <useSpringBoot3>true</useSpringBoot3>
                                <interfaceOnly>true</interfaceOnly> <!-- Generate only needed interfaces -->
                                <skipDefaultInterface>true</skipDefaultInterface> <!-- Skip the sample -->
                                <basePackage>it.millsoft.orders.api.ext.http.users.api.model</basePackage>
                                <apiPackage>it.millsoft.orders.api.ext.http.users.api.model</apiPackage>
                                <modelPackage>it.millsoft.orders.api.ext.http.users.api.model</modelPackage>
                                <useBeanValidation>true</useBeanValidation>
                                <performBeanValidation>true</performBeanValidation>
                                <serializableModel>true</serializableModel>
                                <useTags>true</useTags>
                                <additionalModelTypeAnnotations>@lombok.Data @lombok.Builder @lombok.NoArgsConstructor @lombok.AllArgsConstructor</additionalModelTypeAnnotations>-->
                            </configOptions>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- Generate USERS-API rest client -->
            <plugin>
                <groupId>org.openapitools</groupId>
                <artifactId>openapi-generator-maven-plugin</artifactId>
                <version>6.2.1</version>
                <executions>
                    <execution>
                        <id>execution-openapi-PRODUCTS-API-client</id>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <inputSpec>${basedir}/src/main/resources/openapi/clients/products_api__client_openapi_interfaces__v0.yaml</inputSpec>
                            <output>${basedir}</output> <!-- Needed in order to generated classes in the artifcat -->
                            <generatorName>spring</generatorName>
                            <generateSupportingFiles>false</generateSupportingFiles>
                            <configOptions>
                                <useSpringBoot3>true</useSpringBoot3>
                                <interfaceOnly>true</interfaceOnly> <!-- Generate only needed interfaces -->
                                <skipDefaultInterface>true</skipDefaultInterface> <!-- Skip the sample -->
                                <basePackage>it.millsoft.orders.api.ext.http.products.api.model</basePackage>
                                <apiPackage>it.millsoft.orders.api.ext.http.products.api.model</apiPackage>
                                <modelPackage>it.millsoft.orders.api.ext.http.products.api.model</modelPackage>
                                <useBeanValidation>true</useBeanValidation>
                                <performBeanValidation>true</performBeanValidation>
                                <serializableModel>true</serializableModel>
                                <useTags>true</useTags>
                                <additionalModelTypeAnnotations>@lombok.Data @lombok.Builder @lombok.NoArgsConstructor @lombok.AllArgsConstructor</additionalModelTypeAnnotations>-->
                            </configOptions>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>