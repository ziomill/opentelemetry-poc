// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: orders_api_grpc_server_interfaces.proto

package it.millsoft.orders.api.interfaces.grpc.model;

/**
 * Protobuf type {@code AddOrderForUserRequest}
 */
public final class AddOrderForUserRequest extends
    com.google.protobuf.GeneratedMessageV3 implements
    // @@protoc_insertion_point(message_implements:AddOrderForUserRequest)
    AddOrderForUserRequestOrBuilder {
private static final long serialVersionUID = 0L;
  // Use AddOrderForUserRequest.newBuilder() to construct.
  private AddOrderForUserRequest(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
    super(builder);
  }
  private AddOrderForUserRequest() {
    email_ = "";
  }

  @java.lang.Override
  @SuppressWarnings({"unused"})
  protected java.lang.Object newInstance(
      UnusedPrivateParameter unused) {
    return new AddOrderForUserRequest();
  }

  @java.lang.Override
  public final com.google.protobuf.UnknownFieldSet
  getUnknownFields() {
    return this.unknownFields;
  }
  public static final com.google.protobuf.Descriptors.Descriptor
      getDescriptor() {
    return it.millsoft.orders.api.interfaces.grpc.model.OrdersApiGrpcServerInterfaces.internal_static_AddOrderForUserRequest_descriptor;
  }

  @SuppressWarnings({"rawtypes"})
  @java.lang.Override
  protected com.google.protobuf.MapField internalGetMapField(
      int number) {
    switch (number) {
      case 2:
        return internalGetProducts();
      default:
        throw new RuntimeException(
            "Invalid map field number: " + number);
    }
  }
  @java.lang.Override
  protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internalGetFieldAccessorTable() {
    return it.millsoft.orders.api.interfaces.grpc.model.OrdersApiGrpcServerInterfaces.internal_static_AddOrderForUserRequest_fieldAccessorTable
        .ensureFieldAccessorsInitialized(
            it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest.class, it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest.Builder.class);
  }

  public static final int EMAIL_FIELD_NUMBER = 1;
  @SuppressWarnings("serial")
  private volatile java.lang.Object email_ = "";
  /**
   * <code>string email = 1;</code>
   * @return The email.
   */
  @java.lang.Override
  public java.lang.String getEmail() {
    java.lang.Object ref = email_;
    if (ref instanceof java.lang.String) {
      return (java.lang.String) ref;
    } else {
      com.google.protobuf.ByteString bs = 
          (com.google.protobuf.ByteString) ref;
      java.lang.String s = bs.toStringUtf8();
      email_ = s;
      return s;
    }
  }
  /**
   * <code>string email = 1;</code>
   * @return The bytes for email.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getEmailBytes() {
    java.lang.Object ref = email_;
    if (ref instanceof java.lang.String) {
      com.google.protobuf.ByteString b = 
          com.google.protobuf.ByteString.copyFromUtf8(
              (java.lang.String) ref);
      email_ = b;
      return b;
    } else {
      return (com.google.protobuf.ByteString) ref;
    }
  }

  public static final int PRODUCTS_FIELD_NUMBER = 2;
  private static final class ProductsDefaultEntryHolder {
    static final com.google.protobuf.MapEntry<
        java.lang.String, java.lang.Integer> defaultEntry =
            com.google.protobuf.MapEntry
            .<java.lang.String, java.lang.Integer>newDefaultInstance(
                it.millsoft.orders.api.interfaces.grpc.model.OrdersApiGrpcServerInterfaces.internal_static_AddOrderForUserRequest_ProductsEntry_descriptor, 
                com.google.protobuf.WireFormat.FieldType.STRING,
                "",
                com.google.protobuf.WireFormat.FieldType.INT32,
                0);
  }
  @SuppressWarnings("serial")
  private com.google.protobuf.MapField<
      java.lang.String, java.lang.Integer> products_;
  private com.google.protobuf.MapField<java.lang.String, java.lang.Integer>
  internalGetProducts() {
    if (products_ == null) {
      return com.google.protobuf.MapField.emptyMapField(
          ProductsDefaultEntryHolder.defaultEntry);
    }
    return products_;
  }
  public int getProductsCount() {
    return internalGetProducts().getMap().size();
  }
  /**
   * <code>map&lt;string, int32&gt; products = 2;</code>
   */
  @java.lang.Override
  public boolean containsProducts(
      java.lang.String key) {
    if (key == null) { throw new NullPointerException("map key"); }
    return internalGetProducts().getMap().containsKey(key);
  }
  /**
   * Use {@link #getProductsMap()} instead.
   */
  @java.lang.Override
  @java.lang.Deprecated
  public java.util.Map<java.lang.String, java.lang.Integer> getProducts() {
    return getProductsMap();
  }
  /**
   * <code>map&lt;string, int32&gt; products = 2;</code>
   */
  @java.lang.Override
  public java.util.Map<java.lang.String, java.lang.Integer> getProductsMap() {
    return internalGetProducts().getMap();
  }
  /**
   * <code>map&lt;string, int32&gt; products = 2;</code>
   */
  @java.lang.Override
  public int getProductsOrDefault(
      java.lang.String key,
      int defaultValue) {
    if (key == null) { throw new NullPointerException("map key"); }
    java.util.Map<java.lang.String, java.lang.Integer> map =
        internalGetProducts().getMap();
    return map.containsKey(key) ? map.get(key) : defaultValue;
  }
  /**
   * <code>map&lt;string, int32&gt; products = 2;</code>
   */
  @java.lang.Override
  public int getProductsOrThrow(
      java.lang.String key) {
    if (key == null) { throw new NullPointerException("map key"); }
    java.util.Map<java.lang.String, java.lang.Integer> map =
        internalGetProducts().getMap();
    if (!map.containsKey(key)) {
      throw new java.lang.IllegalArgumentException();
    }
    return map.get(key);
  }

  private byte memoizedIsInitialized = -1;
  @java.lang.Override
  public final boolean isInitialized() {
    byte isInitialized = memoizedIsInitialized;
    if (isInitialized == 1) return true;
    if (isInitialized == 0) return false;

    memoizedIsInitialized = 1;
    return true;
  }

  @java.lang.Override
  public void writeTo(com.google.protobuf.CodedOutputStream output)
                      throws java.io.IOException {
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(email_)) {
      com.google.protobuf.GeneratedMessageV3.writeString(output, 1, email_);
    }
    com.google.protobuf.GeneratedMessageV3
      .serializeStringMapTo(
        output,
        internalGetProducts(),
        ProductsDefaultEntryHolder.defaultEntry,
        2);
    getUnknownFields().writeTo(output);
  }

  @java.lang.Override
  public int getSerializedSize() {
    int size = memoizedSize;
    if (size != -1) return size;

    size = 0;
    if (!com.google.protobuf.GeneratedMessageV3.isStringEmpty(email_)) {
      size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, email_);
    }
    for (java.util.Map.Entry<java.lang.String, java.lang.Integer> entry
         : internalGetProducts().getMap().entrySet()) {
      com.google.protobuf.MapEntry<java.lang.String, java.lang.Integer>
      products__ = ProductsDefaultEntryHolder.defaultEntry.newBuilderForType()
          .setKey(entry.getKey())
          .setValue(entry.getValue())
          .build();
      size += com.google.protobuf.CodedOutputStream
          .computeMessageSize(2, products__);
    }
    size += getUnknownFields().getSerializedSize();
    memoizedSize = size;
    return size;
  }

  @java.lang.Override
  public boolean equals(final java.lang.Object obj) {
    if (obj == this) {
     return true;
    }
    if (!(obj instanceof it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest)) {
      return super.equals(obj);
    }
    it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest other = (it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest) obj;

    if (!getEmail()
        .equals(other.getEmail())) return false;
    if (!internalGetProducts().equals(
        other.internalGetProducts())) return false;
    if (!getUnknownFields().equals(other.getUnknownFields())) return false;
    return true;
  }

  @java.lang.Override
  public int hashCode() {
    if (memoizedHashCode != 0) {
      return memoizedHashCode;
    }
    int hash = 41;
    hash = (19 * hash) + getDescriptor().hashCode();
    hash = (37 * hash) + EMAIL_FIELD_NUMBER;
    hash = (53 * hash) + getEmail().hashCode();
    if (!internalGetProducts().getMap().isEmpty()) {
      hash = (37 * hash) + PRODUCTS_FIELD_NUMBER;
      hash = (53 * hash) + internalGetProducts().hashCode();
    }
    hash = (29 * hash) + getUnknownFields().hashCode();
    memoizedHashCode = hash;
    return hash;
  }

  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data);
  }
  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return PARSER.parseFrom(data, extensionRegistry);
  }
  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }
  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input);
  }
  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
  }
  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input);
  }
  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageV3
        .parseWithIOException(PARSER, input, extensionRegistry);
  }

  @java.lang.Override
  public Builder newBuilderForType() { return newBuilder(); }
  public static Builder newBuilder() {
    return DEFAULT_INSTANCE.toBuilder();
  }
  public static Builder newBuilder(it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest prototype) {
    return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
  }
  @java.lang.Override
  public Builder toBuilder() {
    return this == DEFAULT_INSTANCE
        ? new Builder() : new Builder().mergeFrom(this);
  }

  @java.lang.Override
  protected Builder newBuilderForType(
      com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
    Builder builder = new Builder(parent);
    return builder;
  }
  /**
   * Protobuf type {@code AddOrderForUserRequest}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
      // @@protoc_insertion_point(builder_implements:AddOrderForUserRequest)
      it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequestOrBuilder {
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return it.millsoft.orders.api.interfaces.grpc.model.OrdersApiGrpcServerInterfaces.internal_static_AddOrderForUserRequest_descriptor;
    }

    @SuppressWarnings({"rawtypes"})
    protected com.google.protobuf.MapField internalGetMapField(
        int number) {
      switch (number) {
        case 2:
          return internalGetProducts();
        default:
          throw new RuntimeException(
              "Invalid map field number: " + number);
      }
    }
    @SuppressWarnings({"rawtypes"})
    protected com.google.protobuf.MapField internalGetMutableMapField(
        int number) {
      switch (number) {
        case 2:
          return internalGetMutableProducts();
        default:
          throw new RuntimeException(
              "Invalid map field number: " + number);
      }
    }
    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return it.millsoft.orders.api.interfaces.grpc.model.OrdersApiGrpcServerInterfaces.internal_static_AddOrderForUserRequest_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest.class, it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest.Builder.class);
    }

    // Construct using it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest.newBuilder()
    private Builder() {

    }

    private Builder(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      super(parent);

    }
    @java.lang.Override
    public Builder clear() {
      super.clear();
      bitField0_ = 0;
      email_ = "";
      internalGetMutableProducts().clear();
      return this;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.Descriptor
        getDescriptorForType() {
      return it.millsoft.orders.api.interfaces.grpc.model.OrdersApiGrpcServerInterfaces.internal_static_AddOrderForUserRequest_descriptor;
    }

    @java.lang.Override
    public it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest getDefaultInstanceForType() {
      return it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest.getDefaultInstance();
    }

    @java.lang.Override
    public it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest build() {
      it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest result = buildPartial();
      if (!result.isInitialized()) {
        throw newUninitializedMessageException(result);
      }
      return result;
    }

    @java.lang.Override
    public it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest buildPartial() {
      it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest result = new it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest(this);
      if (bitField0_ != 0) { buildPartial0(result); }
      onBuilt();
      return result;
    }

    private void buildPartial0(it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest result) {
      int from_bitField0_ = bitField0_;
      if (((from_bitField0_ & 0x00000001) != 0)) {
        result.email_ = email_;
      }
      if (((from_bitField0_ & 0x00000002) != 0)) {
        result.products_ = internalGetProducts();
        result.products_.makeImmutable();
      }
    }

    @java.lang.Override
    public Builder clone() {
      return super.clone();
    }
    @java.lang.Override
    public Builder setField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.setField(field, value);
    }
    @java.lang.Override
    public Builder clearField(
        com.google.protobuf.Descriptors.FieldDescriptor field) {
      return super.clearField(field);
    }
    @java.lang.Override
    public Builder clearOneof(
        com.google.protobuf.Descriptors.OneofDescriptor oneof) {
      return super.clearOneof(oneof);
    }
    @java.lang.Override
    public Builder setRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        int index, java.lang.Object value) {
      return super.setRepeatedField(field, index, value);
    }
    @java.lang.Override
    public Builder addRepeatedField(
        com.google.protobuf.Descriptors.FieldDescriptor field,
        java.lang.Object value) {
      return super.addRepeatedField(field, value);
    }
    @java.lang.Override
    public Builder mergeFrom(com.google.protobuf.Message other) {
      if (other instanceof it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest) {
        return mergeFrom((it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest)other);
      } else {
        super.mergeFrom(other);
        return this;
      }
    }

    public Builder mergeFrom(it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest other) {
      if (other == it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest.getDefaultInstance()) return this;
      if (!other.getEmail().isEmpty()) {
        email_ = other.email_;
        bitField0_ |= 0x00000001;
        onChanged();
      }
      internalGetMutableProducts().mergeFrom(
          other.internalGetProducts());
      bitField0_ |= 0x00000002;
      this.mergeUnknownFields(other.getUnknownFields());
      onChanged();
      return this;
    }

    @java.lang.Override
    public final boolean isInitialized() {
      return true;
    }

    @java.lang.Override
    public Builder mergeFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              email_ = input.readStringRequireUtf8();
              bitField0_ |= 0x00000001;
              break;
            } // case 10
            case 18: {
              com.google.protobuf.MapEntry<java.lang.String, java.lang.Integer>
              products__ = input.readMessage(
                  ProductsDefaultEntryHolder.defaultEntry.getParserForType(), extensionRegistry);
              internalGetMutableProducts().getMutableMap().put(
                  products__.getKey(), products__.getValue());
              bitField0_ |= 0x00000002;
              break;
            } // case 18
            default: {
              if (!super.parseUnknownField(input, extensionRegistry, tag)) {
                done = true; // was an endgroup tag
              }
              break;
            } // default:
          } // switch (tag)
        } // while (!done)
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.unwrapIOException();
      } finally {
        onChanged();
      } // finally
      return this;
    }
    private int bitField0_;

    private java.lang.Object email_ = "";
    /**
     * <code>string email = 1;</code>
     * @return The email.
     */
    public java.lang.String getEmail() {
      java.lang.Object ref = email_;
      if (!(ref instanceof java.lang.String)) {
        com.google.protobuf.ByteString bs =
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        email_ = s;
        return s;
      } else {
        return (java.lang.String) ref;
      }
    }
    /**
     * <code>string email = 1;</code>
     * @return The bytes for email.
     */
    public com.google.protobuf.ByteString
        getEmailBytes() {
      java.lang.Object ref = email_;
      if (ref instanceof String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        email_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }
    /**
     * <code>string email = 1;</code>
     * @param value The email to set.
     * @return This builder for chaining.
     */
    public Builder setEmail(
        java.lang.String value) {
      if (value == null) { throw new NullPointerException(); }
      email_ = value;
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }
    /**
     * <code>string email = 1;</code>
     * @return This builder for chaining.
     */
    public Builder clearEmail() {
      email_ = getDefaultInstance().getEmail();
      bitField0_ = (bitField0_ & ~0x00000001);
      onChanged();
      return this;
    }
    /**
     * <code>string email = 1;</code>
     * @param value The bytes for email to set.
     * @return This builder for chaining.
     */
    public Builder setEmailBytes(
        com.google.protobuf.ByteString value) {
      if (value == null) { throw new NullPointerException(); }
      checkByteStringIsUtf8(value);
      email_ = value;
      bitField0_ |= 0x00000001;
      onChanged();
      return this;
    }

    private com.google.protobuf.MapField<
        java.lang.String, java.lang.Integer> products_;
    private com.google.protobuf.MapField<java.lang.String, java.lang.Integer>
        internalGetProducts() {
      if (products_ == null) {
        return com.google.protobuf.MapField.emptyMapField(
            ProductsDefaultEntryHolder.defaultEntry);
      }
      return products_;
    }
    private com.google.protobuf.MapField<java.lang.String, java.lang.Integer>
        internalGetMutableProducts() {
      if (products_ == null) {
        products_ = com.google.protobuf.MapField.newMapField(
            ProductsDefaultEntryHolder.defaultEntry);
      }
      if (!products_.isMutable()) {
        products_ = products_.copy();
      }
      bitField0_ |= 0x00000002;
      onChanged();
      return products_;
    }
    public int getProductsCount() {
      return internalGetProducts().getMap().size();
    }
    /**
     * <code>map&lt;string, int32&gt; products = 2;</code>
     */
    @java.lang.Override
    public boolean containsProducts(
        java.lang.String key) {
      if (key == null) { throw new NullPointerException("map key"); }
      return internalGetProducts().getMap().containsKey(key);
    }
    /**
     * Use {@link #getProductsMap()} instead.
     */
    @java.lang.Override
    @java.lang.Deprecated
    public java.util.Map<java.lang.String, java.lang.Integer> getProducts() {
      return getProductsMap();
    }
    /**
     * <code>map&lt;string, int32&gt; products = 2;</code>
     */
    @java.lang.Override
    public java.util.Map<java.lang.String, java.lang.Integer> getProductsMap() {
      return internalGetProducts().getMap();
    }
    /**
     * <code>map&lt;string, int32&gt; products = 2;</code>
     */
    @java.lang.Override
    public int getProductsOrDefault(
        java.lang.String key,
        int defaultValue) {
      if (key == null) { throw new NullPointerException("map key"); }
      java.util.Map<java.lang.String, java.lang.Integer> map =
          internalGetProducts().getMap();
      return map.containsKey(key) ? map.get(key) : defaultValue;
    }
    /**
     * <code>map&lt;string, int32&gt; products = 2;</code>
     */
    @java.lang.Override
    public int getProductsOrThrow(
        java.lang.String key) {
      if (key == null) { throw new NullPointerException("map key"); }
      java.util.Map<java.lang.String, java.lang.Integer> map =
          internalGetProducts().getMap();
      if (!map.containsKey(key)) {
        throw new java.lang.IllegalArgumentException();
      }
      return map.get(key);
    }
    public Builder clearProducts() {
      bitField0_ = (bitField0_ & ~0x00000002);
      internalGetMutableProducts().getMutableMap()
          .clear();
      return this;
    }
    /**
     * <code>map&lt;string, int32&gt; products = 2;</code>
     */
    public Builder removeProducts(
        java.lang.String key) {
      if (key == null) { throw new NullPointerException("map key"); }
      internalGetMutableProducts().getMutableMap()
          .remove(key);
      return this;
    }
    /**
     * Use alternate mutation accessors instead.
     */
    @java.lang.Deprecated
    public java.util.Map<java.lang.String, java.lang.Integer>
        getMutableProducts() {
      bitField0_ |= 0x00000002;
      return internalGetMutableProducts().getMutableMap();
    }
    /**
     * <code>map&lt;string, int32&gt; products = 2;</code>
     */
    public Builder putProducts(
        java.lang.String key,
        int value) {
      if (key == null) { throw new NullPointerException("map key"); }
      
      internalGetMutableProducts().getMutableMap()
          .put(key, value);
      bitField0_ |= 0x00000002;
      return this;
    }
    /**
     * <code>map&lt;string, int32&gt; products = 2;</code>
     */
    public Builder putAllProducts(
        java.util.Map<java.lang.String, java.lang.Integer> values) {
      internalGetMutableProducts().getMutableMap()
          .putAll(values);
      bitField0_ |= 0x00000002;
      return this;
    }
    @java.lang.Override
    public final Builder setUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.setUnknownFields(unknownFields);
    }

    @java.lang.Override
    public final Builder mergeUnknownFields(
        final com.google.protobuf.UnknownFieldSet unknownFields) {
      return super.mergeUnknownFields(unknownFields);
    }


    // @@protoc_insertion_point(builder_scope:AddOrderForUserRequest)
  }

  // @@protoc_insertion_point(class_scope:AddOrderForUserRequest)
  private static final it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest DEFAULT_INSTANCE;
  static {
    DEFAULT_INSTANCE = new it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest();
  }

  public static it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static final com.google.protobuf.Parser<AddOrderForUserRequest>
      PARSER = new com.google.protobuf.AbstractParser<AddOrderForUserRequest>() {
    @java.lang.Override
    public AddOrderForUserRequest parsePartialFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      Builder builder = newBuilder();
      try {
        builder.mergeFrom(input, extensionRegistry);
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(builder.buildPartial());
      } catch (com.google.protobuf.UninitializedMessageException e) {
        throw e.asInvalidProtocolBufferException().setUnfinishedMessage(builder.buildPartial());
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(e)
            .setUnfinishedMessage(builder.buildPartial());
      }
      return builder.buildPartial();
    }
  };

  public static com.google.protobuf.Parser<AddOrderForUserRequest> parser() {
    return PARSER;
  }

  @java.lang.Override
  public com.google.protobuf.Parser<AddOrderForUserRequest> getParserForType() {
    return PARSER;
  }

  @java.lang.Override
  public it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest getDefaultInstanceForType() {
    return DEFAULT_INSTANCE;
  }

}

