package it.millsoft.orders.api.mappers;

import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.ext.grpc.products.api.model.Product;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.Set;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface ProductsApiClientMapper {

    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source="description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "requestedStock",ignore = true)
    ProductItemDTO productGrpcToProductItemDto(Product gProduct);

    Set<ProductItemDTO> productsGrpcToProductItemsDto(Set<Product> gProducts);

    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source="description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "requestedStock",ignore = true)
    ProductItemDTO productRestToProductItemDto(it.millsoft.orders.api.ext.http.products.api.model.Product rProduct);

    Set<ProductItemDTO> productsRestToProductItemsDto(Set<it.millsoft.orders.api.ext.http.products.api.model.Product> rProducts);

}
