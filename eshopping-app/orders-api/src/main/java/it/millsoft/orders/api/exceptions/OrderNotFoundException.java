package it.millsoft.orders.api.exceptions;

import it.millsoft.orders.api.dto.OrdersApiErrorEnum;

import java.util.UUID;

public class OrderNotFoundException extends OrdersApiException {

    public OrderNotFoundException(String message, UUID errorUUID) {
        super(message,
                OrdersApiErrorEnum.ORDER_NOT_FOUND,
                errorUUID);
    }

}
