package it.millsoft.orders.api.ext.grpc.products.api.model;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 * <pre>
 * Users Service
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.51.0)",
    comments = "Source: clients/products_api__client_grpc_interfaces__v0.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class ProductsGrpcApiGrpc {

  private ProductsGrpcApiGrpc() {}

  public static final String SERVICE_NAME = "ProductsGrpcApi";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest,
      it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse> getGetProductByCodeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getProductByCode",
      requestType = it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest.class,
      responseType = it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest,
      it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse> getGetProductByCodeMethod() {
    io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest, it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse> getGetProductByCodeMethod;
    if ((getGetProductByCodeMethod = ProductsGrpcApiGrpc.getGetProductByCodeMethod) == null) {
      synchronized (ProductsGrpcApiGrpc.class) {
        if ((getGetProductByCodeMethod = ProductsGrpcApiGrpc.getGetProductByCodeMethod) == null) {
          ProductsGrpcApiGrpc.getGetProductByCodeMethod = getGetProductByCodeMethod =
              io.grpc.MethodDescriptor.<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest, it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getProductByCode"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse.getDefaultInstance()))
              .setSchemaDescriptor(new ProductsGrpcApiMethodDescriptorSupplier("getProductByCode"))
              .build();
        }
      }
    }
    return getGetProductByCodeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest,
      it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse> getGetProductsByCodeMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getProductsByCode",
      requestType = it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest.class,
      responseType = it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest,
      it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse> getGetProductsByCodeMethod() {
    io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest, it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse> getGetProductsByCodeMethod;
    if ((getGetProductsByCodeMethod = ProductsGrpcApiGrpc.getGetProductsByCodeMethod) == null) {
      synchronized (ProductsGrpcApiGrpc.class) {
        if ((getGetProductsByCodeMethod = ProductsGrpcApiGrpc.getGetProductsByCodeMethod) == null) {
          ProductsGrpcApiGrpc.getGetProductsByCodeMethod = getGetProductsByCodeMethod =
              io.grpc.MethodDescriptor.<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest, it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getProductsByCode"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse.getDefaultInstance()))
              .setSchemaDescriptor(new ProductsGrpcApiMethodDescriptorSupplier("getProductsByCode"))
              .build();
        }
      }
    }
    return getGetProductsByCodeMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest,
      it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse> getAddProductMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "addProduct",
      requestType = it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest.class,
      responseType = it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest,
      it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse> getAddProductMethod() {
    io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest, it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse> getAddProductMethod;
    if ((getAddProductMethod = ProductsGrpcApiGrpc.getAddProductMethod) == null) {
      synchronized (ProductsGrpcApiGrpc.class) {
        if ((getAddProductMethod = ProductsGrpcApiGrpc.getAddProductMethod) == null) {
          ProductsGrpcApiGrpc.getAddProductMethod = getAddProductMethod =
              io.grpc.MethodDescriptor.<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest, it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "addProduct"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse.getDefaultInstance()))
              .setSchemaDescriptor(new ProductsGrpcApiMethodDescriptorSupplier("addProduct"))
              .build();
        }
      }
    }
    return getAddProductMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest,
      it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse> getAddProductsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "addProducts",
      requestType = it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest.class,
      responseType = it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest,
      it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse> getAddProductsMethod() {
    io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest, it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse> getAddProductsMethod;
    if ((getAddProductsMethod = ProductsGrpcApiGrpc.getAddProductsMethod) == null) {
      synchronized (ProductsGrpcApiGrpc.class) {
        if ((getAddProductsMethod = ProductsGrpcApiGrpc.getAddProductsMethod) == null) {
          ProductsGrpcApiGrpc.getAddProductsMethod = getAddProductsMethod =
              io.grpc.MethodDescriptor.<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest, it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "addProducts"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse.getDefaultInstance()))
              .setSchemaDescriptor(new ProductsGrpcApiMethodDescriptorSupplier("addProducts"))
              .build();
        }
      }
    }
    return getAddProductsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest,
      it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse> getUpdateStocksMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "updateStocks",
      requestType = it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest.class,
      responseType = it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest,
      it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse> getUpdateStocksMethod() {
    io.grpc.MethodDescriptor<it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest, it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse> getUpdateStocksMethod;
    if ((getUpdateStocksMethod = ProductsGrpcApiGrpc.getUpdateStocksMethod) == null) {
      synchronized (ProductsGrpcApiGrpc.class) {
        if ((getUpdateStocksMethod = ProductsGrpcApiGrpc.getUpdateStocksMethod) == null) {
          ProductsGrpcApiGrpc.getUpdateStocksMethod = getUpdateStocksMethod =
              io.grpc.MethodDescriptor.<it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest, it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "updateStocks"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse.getDefaultInstance()))
              .setSchemaDescriptor(new ProductsGrpcApiMethodDescriptorSupplier("updateStocks"))
              .build();
        }
      }
    }
    return getUpdateStocksMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ProductsGrpcApiStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ProductsGrpcApiStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ProductsGrpcApiStub>() {
        @java.lang.Override
        public ProductsGrpcApiStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ProductsGrpcApiStub(channel, callOptions);
        }
      };
    return ProductsGrpcApiStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ProductsGrpcApiBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ProductsGrpcApiBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ProductsGrpcApiBlockingStub>() {
        @java.lang.Override
        public ProductsGrpcApiBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ProductsGrpcApiBlockingStub(channel, callOptions);
        }
      };
    return ProductsGrpcApiBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ProductsGrpcApiFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ProductsGrpcApiFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ProductsGrpcApiFutureStub>() {
        @java.lang.Override
        public ProductsGrpcApiFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ProductsGrpcApiFutureStub(channel, callOptions);
        }
      };
    return ProductsGrpcApiFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static abstract class ProductsGrpcApiImplBase implements io.grpc.BindableService {

    /**
     */
    public void getProductByCode(it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetProductByCodeMethod(), responseObserver);
    }

    /**
     */
    public void getProductsByCode(it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetProductsByCodeMethod(), responseObserver);
    }

    /**
     */
    public void addProduct(it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getAddProductMethod(), responseObserver);
    }

    /**
     */
    public void addProducts(it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getAddProductsMethod(), responseObserver);
    }

    /**
     */
    public void updateStocks(it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getUpdateStocksMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetProductByCodeMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest,
                it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse>(
                  this, METHODID_GET_PRODUCT_BY_CODE)))
          .addMethod(
            getGetProductsByCodeMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest,
                it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse>(
                  this, METHODID_GET_PRODUCTS_BY_CODE)))
          .addMethod(
            getAddProductMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest,
                it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse>(
                  this, METHODID_ADD_PRODUCT)))
          .addMethod(
            getAddProductsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest,
                it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse>(
                  this, METHODID_ADD_PRODUCTS)))
          .addMethod(
            getUpdateStocksMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest,
                it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse>(
                  this, METHODID_UPDATE_STOCKS)))
          .build();
    }
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static final class ProductsGrpcApiStub extends io.grpc.stub.AbstractAsyncStub<ProductsGrpcApiStub> {
    private ProductsGrpcApiStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ProductsGrpcApiStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ProductsGrpcApiStub(channel, callOptions);
    }

    /**
     */
    public void getProductByCode(it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetProductByCodeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getProductsByCode(it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetProductsByCodeMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void addProduct(it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getAddProductMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void addProducts(it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getAddProductsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateStocks(it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getUpdateStocksMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static final class ProductsGrpcApiBlockingStub extends io.grpc.stub.AbstractBlockingStub<ProductsGrpcApiBlockingStub> {
    private ProductsGrpcApiBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ProductsGrpcApiBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ProductsGrpcApiBlockingStub(channel, callOptions);
    }

    /**
     */
    public it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse getProductByCode(it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetProductByCodeMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse getProductsByCode(it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetProductsByCodeMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse addProduct(it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getAddProductMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse addProducts(it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getAddProductsMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse updateStocks(it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getUpdateStocksMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static final class ProductsGrpcApiFutureStub extends io.grpc.stub.AbstractFutureStub<ProductsGrpcApiFutureStub> {
    private ProductsGrpcApiFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ProductsGrpcApiFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ProductsGrpcApiFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse> getProductByCode(
        it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetProductByCodeMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse> getProductsByCode(
        it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetProductsByCodeMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse> addProduct(
        it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getAddProductMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse> addProducts(
        it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getAddProductsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse> updateStocks(
        it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getUpdateStocksMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_PRODUCT_BY_CODE = 0;
  private static final int METHODID_GET_PRODUCTS_BY_CODE = 1;
  private static final int METHODID_ADD_PRODUCT = 2;
  private static final int METHODID_ADD_PRODUCTS = 3;
  private static final int METHODID_UPDATE_STOCKS = 4;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ProductsGrpcApiImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ProductsGrpcApiImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_PRODUCT_BY_CODE:
          serviceImpl.getProductByCode((it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductByCodeResponse>) responseObserver);
          break;
        case METHODID_GET_PRODUCTS_BY_CODE:
          serviceImpl.getProductsByCode((it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.GetProductsByCodeResponse>) responseObserver);
          break;
        case METHODID_ADD_PRODUCT:
          serviceImpl.addProduct((it.millsoft.orders.api.ext.grpc.products.api.model.AddProductRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductResponse>) responseObserver);
          break;
        case METHODID_ADD_PRODUCTS:
          serviceImpl.addProducts((it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.AddProductsResponse>) responseObserver);
          break;
        case METHODID_UPDATE_STOCKS:
          serviceImpl.updateStocks((it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.orders.api.ext.grpc.products.api.model.UpdateStocksResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ProductsGrpcApiBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ProductsGrpcApiBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return it.millsoft.orders.api.ext.grpc.products.api.model.ProductsApiClientGrpcInterfacesV0.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ProductsGrpcApi");
    }
  }

  private static final class ProductsGrpcApiFileDescriptorSupplier
      extends ProductsGrpcApiBaseDescriptorSupplier {
    ProductsGrpcApiFileDescriptorSupplier() {}
  }

  private static final class ProductsGrpcApiMethodDescriptorSupplier
      extends ProductsGrpcApiBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ProductsGrpcApiMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ProductsGrpcApiGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ProductsGrpcApiFileDescriptorSupplier())
              .addMethod(getGetProductByCodeMethod())
              .addMethod(getGetProductsByCodeMethod())
              .addMethod(getAddProductMethod())
              .addMethod(getAddProductsMethod())
              .addMethod(getUpdateStocksMethod())
              .build();
        }
      }
    }
    return result;
  }
}
