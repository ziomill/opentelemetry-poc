package it.millsoft.orders.api.ext.grpc;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.orders.api.exceptions.UsersApiClientException;
import it.millsoft.orders.api.ext.UsersApiClient;
import it.millsoft.orders.api.ext.grpc.users.api.model.CheckUserExistenceRequest;
import it.millsoft.orders.api.ext.grpc.users.api.model.UsersApiErrorResponse;
import it.millsoft.orders.api.ext.grpc.users.api.model.UsersGrpcApiGrpc;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.UUID;


@Component
@ConditionalOnProperty(
        value="commons.client.integration-strategy.users-api",
        havingValue = "GRPC",
        matchIfMissing = true)
public class UsersApiGrpcClient implements UsersApiClient {

    private static Logger LOGGER = LoggerFactory.getLogger(UsersApiGrpcClient.class);

    @GrpcClient("users-api")
    private UsersGrpcApiGrpc.UsersGrpcApiBlockingStub usersApiGrpcBlockingStub;

    @Value("${grpc.client.users-api.address}")
    private String GRPC_BASE_ADDRESS;

    @Override
    public void checkUserExistence(String email) {
        String GRPC_OP = GRPC_BASE_ADDRESS + "/CheckUserExistence";
        try {
            CheckUserExistenceRequest request = CheckUserExistenceRequest.newBuilder()
                    .setEmail(email)
                    .build();
            this.usersApiGrpcBlockingStub.checkUserExistence(request);
            LOGGER.debug("The GRPC service {} has been called with SUCCESS - An user having email {} has been found",GRPC_OP,email);
        } catch (StatusRuntimeException ex) {
            // Access to GRPC Metadata
            Metadata metadata = Status.trailersFromThrowable(ex);
            UsersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance()));

            // Raise LOCAL EXCEPTION with LOCAL ERROR CODES
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("An ERROR has occurred calling the GRPC service %s: %s - Error UUID: %s", GRPC_OP, errorResponse, errorUUID);
            LOGGER.error(msg);
            throw new UsersApiClientException(msg, errorUUID);
        } catch (Exception ex) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("An ERROR has occurred calling the GRPC service %s: %s - Error UUID: %s", GRPC_OP, ex.getMessage(), errorUUID);
            LOGGER.error(msg);
            throw new UsersApiClientException(msg,errorUUID);
        }
    }


}
