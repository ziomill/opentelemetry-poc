package it.millsoft.orders.api.interfaces.grpc.services;

import io.grpc.stub.StreamObserver;
import it.millsoft.orders.api.business.OrdersServices;
import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.dto.OrderStatus;
import it.millsoft.orders.api.interfaces.grpc.model.*;
import it.millsoft.orders.api.mappers.OrderGrpcToFromDtoMapper;
import net.devh.boot.grpc.server.service.GrpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

@GrpcService
public class OrdersGrpcApiImpl extends OrdersGrpcApiGrpc.OrdersGrpcApiImplBase {

    private static Logger LOGGER = LoggerFactory.getLogger(OrdersGrpcApiImpl.class);

    @Autowired
    private OrdersServices ordersServices;

    @Autowired
    private OrderGrpcToFromDtoMapper mapper;

    @Override
    public void getOrdersOfUser(GetOrdersOfUserRequest request, StreamObserver<GetOrdersOfUserResponse> responseObserver) {
        // Call Business
        Set<OrderDTO> dOrders = ordersServices.getOrdersOfUser(request.getEmail());

        // Build GRPC Result
        Set<Order> gOrders = mapper.ordersDtoToGrpc(dOrders);

        // Build GRPC Response
        GetOrdersOfUserResponse response = GetOrdersOfUserResponse.newBuilder()
                .addAllOrders(gOrders)
                .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void getOrder(GetOrderRequest request, StreamObserver<GetOrderResponse> responseObserver) {
        // Call Business
        OrderDTO dOrder = ordersServices.getOrder(request.getCode());

        // Build GRPC Result
        Order gOrder = mapper.orderDtoToGrpc(dOrder);

        // Build GRPC Response
        GetOrderResponse response = GetOrderResponse.newBuilder()
                .setOrder(gOrder)
                .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void addOrderForUser(AddOrderForUserRequest request, StreamObserver<AddOrderForUserResponse> responseObserver) {
        // Call Business
        OrderDTO dOrder = ordersServices.addOrderForUser(request.getEmail(), request.getProductsMap());

        // Build GRPC Result
        Order gOrder = mapper.orderDtoToGrpc(dOrder);

        // Build GRPC Response
        AddOrderForUserResponse response = AddOrderForUserResponse.newBuilder()
                .setOrder(gOrder)
                .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void deleteOrder(DeleteOrderRequest request, StreamObserver<DeleteOrderResponse> responseObserver) {
        // Call Business
        String deletedOrderId = ordersServices.deleteOrder(request.getCode());

        // Build GRPC Response
        DeleteOrderResponse response = DeleteOrderResponse.newBuilder()
                .setId(deletedOrderId)
                .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void updateOrderStatus(UpdateOrderStatusRequest request, StreamObserver<UpdateOrderStatusResponse> responseObserver) {
        // Call Business
        OrderStatus status = OrderStatus.valueOf(request.getStatus().name());
        String updatedOrderId = ordersServices.updateOrderStatus(request.getCode(), status);

        // Build GRPC Response
        UpdateOrderStatusResponse response = UpdateOrderStatusResponse.newBuilder()
                .setId(updatedOrderId)
                .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

}
