package it.millsoft.orders.api.dto;

import lombok.Getter;

@Getter
public enum OrdersApiErrorEnum {

    // ORDERS-API
    CLIENTS_INTEGRATION_STRATEGY_NOT_SUPPORTED_FAILURE("/failure/integration/clients-integration-strategy-not-supported-failure"),
    GENERIC_FAILURE("/failure/generic-failure"),
    ORDER_NOT_FOUND("/error/order-not-found"),
    INCONSISTENT_ORDER("/error/inconsistent-order"),
    ORDER_STATUS_NOT_UPDATABLE("/error/order-status-not-updatable"),

    // From USERS-API
    USERS_API_CLIENT_FAILURE("/failure/integration/users-api-failure"),

    // From PRODUCTS-API
    PRODUCTS_API_CLIENT_FAILURE("/failure/integration/products-api-failure");

    private String errorDesc;

    OrdersApiErrorEnum(String errorDesc) {
        this.errorDesc = errorDesc;
    }


}
