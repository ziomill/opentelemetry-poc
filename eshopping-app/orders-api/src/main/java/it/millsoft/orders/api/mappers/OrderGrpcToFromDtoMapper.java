package it.millsoft.orders.api.mappers;

import com.google.protobuf.Timestamp;
import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.interfaces.grpc.model.Order;
import it.millsoft.orders.api.interfaces.grpc.model.OrderStatus;
import org.mapstruct.*;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Set;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED,
        uses = ProductItemGrpcToFromDtoMapper.class)
public interface OrderGrpcToFromDtoMapper {

    @Mapping(target = "code", source = "code")
    @Mapping(target = "submittedOnUTC", source="submittedOnUTC", qualifiedByName = "toOffsetDateTime")
    @Mapping(target = "status", source = "status", qualifiedByName = "toDtoOrderStatus")
    @Mapping(target = "productItems", source = "productItems")
    @Mapping(target = "userEmail", source = "userEmail")
    OrderDTO orderGrpcToDto(Order order);

    Set<OrderDTO> ordersGrpcToDto(Set<Order> orders);

    @Mapping(target = "code", source = "code")
    @Mapping(target = "submittedOnUTC", source="submittedOnUTC", qualifiedByName = "toTimestamp")
    @Mapping(target = "status", source = "status", qualifiedByName = "toGrpcOrderStatus")
    @Mapping(target = "productItems", source = "productItems")
    @Mapping(target = "userEmail", source = "userEmail")
    Order orderDtoToGrpc(OrderDTO order);

    Set<Order> ordersDtoToGrpc(Set<OrderDTO> orders);

    @Named("toDtoOrderStatus")
    default it.millsoft.orders.api.dto.OrderStatus fromGrpcOrderStatus(OrderStatus gOrderStatus) {
        return it.millsoft.orders.api.dto.OrderStatus.valueOf(gOrderStatus.name());
    }

    @Named("toGrpcOrderStatus")
    default OrderStatus fromGrpcOrderStatus(it.millsoft.orders.api.dto.OrderStatus dOrderStatus) {
        return OrderStatus.valueOf(dOrderStatus.name());
    }

    @Named("toOffsetDateTime")
    default OffsetDateTime toOffsetDateTime(Timestamp timestamp) {
        OffsetDateTime result = Instant.ofEpochSecond(timestamp.getSeconds(), timestamp.getNanos())
                                       .atOffset(ZoneOffset.UTC);
        return result;
    }

    @Named("toTimestamp")
    default Timestamp toTimestamp(OffsetDateTime offsetDateTime) {
        Timestamp result = Timestamp.newBuilder()
                .setSeconds(offsetDateTime.toEpochSecond())
                .setNanos(offsetDateTime.getNano())
                .build();
        return result;
    }



}
