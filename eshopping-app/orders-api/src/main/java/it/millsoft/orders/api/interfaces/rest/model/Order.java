package it.millsoft.orders.api.interfaces.rest.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import it.millsoft.orders.api.interfaces.rest.model.OrderStatus;
import it.millsoft.orders.api.interfaces.rest.model.ProductItem;
import java.time.OffsetDateTime;
import java.util.LinkedHashSet;
import java.util.Set;
import org.springframework.format.annotation.DateTimeFormat;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import java.time.OffsetDateTime;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import jakarta.annotation.Generated;

/**
 * Order
 */
@lombok.NoArgsConstructor @lombok.AllArgsConstructor

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-01-03T11:16:09.840720400+01:00[Europe/Rome]")
public class Order implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("code")
  private String code;

  @JsonProperty("submittedOnUTC")
  @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
  private OffsetDateTime submittedOnUTC;

  @JsonProperty("status")
  private OrderStatus status;

  @JsonProperty("productItems")
  @Valid
  private Set<ProductItem> productItems = null;

  @JsonProperty("userEmail")
  private String userEmail;

  public Order code(String code) {
    this.code = code;
    return this;
  }

  /**
   * Get code
   * @return code
  */
  @NotNull 
  @Schema(name = "code", required = true)
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Order submittedOnUTC(OffsetDateTime submittedOnUTC) {
    this.submittedOnUTC = submittedOnUTC;
    return this;
  }

  /**
   * Get submittedOnUTC
   * @return submittedOnUTC
  */
  @NotNull @Valid 
  @Schema(name = "submittedOnUTC", required = true)
  public OffsetDateTime getSubmittedOnUTC() {
    return submittedOnUTC;
  }

  public void setSubmittedOnUTC(OffsetDateTime submittedOnUTC) {
    this.submittedOnUTC = submittedOnUTC;
  }

  public Order status(OrderStatus status) {
    this.status = status;
    return this;
  }

  /**
   * Get status
   * @return status
  */
  @NotNull @Valid 
  @Schema(name = "status", required = true)
  public OrderStatus getStatus() {
    return status;
  }

  public void setStatus(OrderStatus status) {
    this.status = status;
  }

  public Order productItems(Set<ProductItem> productItems) {
    this.productItems = productItems;
    return this;
  }

  public Order addProductItemsItem(ProductItem productItemsItem) {
    if (this.productItems == null) {
      this.productItems = new LinkedHashSet<>();
    }
    this.productItems.add(productItemsItem);
    return this;
  }

  /**
   * Get productItems
   * @return productItems
  */
  @Valid @Size(min = 1) 
  @Schema(name = "productItems", required = false)
  public Set<ProductItem> getProductItems() {
    return productItems;
  }

  @JsonDeserialize(as = LinkedHashSet.class)
  public void setProductItems(Set<ProductItem> productItems) {
    this.productItems = productItems;
  }

  public Order userEmail(String userEmail) {
    this.userEmail = userEmail;
    return this;
  }

  /**
   * Get userEmail
   * @return userEmail
  */
  @NotNull 
  @Schema(name = "userEmail", required = true)
  public String getUserEmail() {
    return userEmail;
  }

  public void setUserEmail(String userEmail) {
    this.userEmail = userEmail;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Order order = (Order) o;
    return Objects.equals(this.code, order.code) &&
        Objects.equals(this.submittedOnUTC, order.submittedOnUTC) &&
        Objects.equals(this.status, order.status) &&
        Objects.equals(this.productItems, order.productItems) &&
        Objects.equals(this.userEmail, order.userEmail);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, submittedOnUTC, status, productItems, userEmail);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Order {\n");
    sb.append("    code: ").append(toIndentedString(code)).append("\n");
    sb.append("    submittedOnUTC: ").append(toIndentedString(submittedOnUTC)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    productItems: ").append(toIndentedString(productItems)).append("\n");
    sb.append("    userEmail: ").append(toIndentedString(userEmail)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

