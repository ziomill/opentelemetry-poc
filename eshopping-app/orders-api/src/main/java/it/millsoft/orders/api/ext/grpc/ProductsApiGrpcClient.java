package it.millsoft.orders.api.ext.grpc;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.exceptions.OrdersApiException;
import it.millsoft.orders.api.exceptions.ProductsApiClientException;
import it.millsoft.orders.api.exceptions.UsersApiClientException;
import it.millsoft.orders.api.ext.ProductsApiClient;
import it.millsoft.orders.api.ext.grpc.products.api.model.*;
import it.millsoft.orders.api.mappers.ProductsApiClientMapper;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@ConditionalOnProperty(
        value="commons.client.integration-strategy.products-api",
        havingValue = "GRPC",
        matchIfMissing = true)
public class ProductsApiGrpcClient implements ProductsApiClient {

    private static Logger LOGGER = LoggerFactory.getLogger(ProductsApiGrpcClient.class);

    @GrpcClient("products-api")
    private ProductsGrpcApiGrpc.ProductsGrpcApiBlockingStub productsApiGrpcBlockingStub;

    @Value("${grpc.client.products-api.address}")
    private String GRPC_BASE_ADDRESS;

    @Autowired
    private ProductsApiClientMapper mapper;

    public Set<ProductItemDTO> getProductsByCode(Set<String> codes) throws OrdersApiException {
        String GRPC_OP = GRPC_BASE_ADDRESS + "/GetProductsByCode";
        try {
            GetProductsByCodeRequest request = GetProductsByCodeRequest.newBuilder()
                    .addAllCodes(codes)
                    .build();
            GetProductsByCodeResponse response = this.productsApiGrpcBlockingStub.getProductsByCode(request);
            LOGGER.debug("The GRPC API {} has been called with success",GRPC_OP);

            // To DTOs
            Set<ProductItemDTO> result = this.mapper.productsGrpcToProductItemsDto(new HashSet<>(response.getProductList()));
            return result;
        } catch (StatusRuntimeException ex) {
            // Access to GRPC Metadata
            Metadata metadata = Status.trailersFromThrowable(ex);
            ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));

            // Log the error
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("An error has occurred calling the GRPC API %s: %s - Error UUID: %s", GRPC_OP, errorResponse, errorUUID);
            LOGGER.error(msg);

            // Raise LOCAL EXCEPTION with LOCAL ERROR CODES
            throw new ProductsApiClientException(msg,errorUUID);
        } catch (Exception ex) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("An ERROR has occurred calling the GRPC service %s: %s - Error UUID: %s", GRPC_OP, ex.getMessage(), errorUUID);
            LOGGER.error(msg);
            throw new UsersApiClientException(msg,errorUUID);
        }
    }

    @Override
    public Map<String,Integer> updateProductsStock(Map<String, Integer> requestedStocks) throws OrdersApiException {
        String GRPC_OP = GRPC_BASE_ADDRESS + "/UpdateStocks";
        try {
            UpdateStocksRequest request = UpdateStocksRequest.newBuilder()
                    .putAllCodesToStocks(requestedStocks)
                    .build();
            UpdateStocksResponse response = this.productsApiGrpcBlockingStub.updateStocks(request);
            LOGGER.debug("The GRPC API {} has been called with success",GRPC_OP);

            return response.getCodesToStocksMap();
        } catch (StatusRuntimeException ex) {
            // Access to GRPC Metadata
            Metadata metadata = Status.trailersFromThrowable(ex);
            ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));

            // Log the error
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("An error has occurred calling the GRPC API %s: %s - Error UUID: %s", GRPC_OP, errorResponse, errorUUID);
            LOGGER.error(msg);

            // Raise LOCAL EXCEPTION with LOCAL ERROR CODES
            throw new ProductsApiClientException(msg,errorUUID);
        } catch (Exception ex) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("An ERROR has occurred calling the GRPC service %s: %s - Error UUID: %s", GRPC_OP, ex.getMessage(), errorUUID);
            LOGGER.error(msg);
            throw new UsersApiClientException(msg,errorUUID);
        }
    }

}
