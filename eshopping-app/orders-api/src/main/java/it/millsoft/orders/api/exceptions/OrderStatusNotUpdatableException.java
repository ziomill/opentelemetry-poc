package it.millsoft.orders.api.exceptions;

import it.millsoft.orders.api.dto.OrdersApiErrorEnum;

import java.util.UUID;

public class OrderStatusNotUpdatableException extends OrdersApiException {

    public OrderStatusNotUpdatableException(String message, UUID errorUUID) {
        super(message,
                OrdersApiErrorEnum.ORDER_STATUS_NOT_UPDATABLE,
                errorUUID);
    }

}
