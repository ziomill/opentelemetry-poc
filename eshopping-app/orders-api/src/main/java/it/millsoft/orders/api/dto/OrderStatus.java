package it.millsoft.orders.api.dto;

public enum OrderStatus {
    CREATED,
    PROCESSING,
    SHIPPED,
    DELIVERED;
}
