package it.millsoft.orders.api.ext.http.users.api.model;

import java.net.URI;
import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.openapitools.jackson.nullable.JsonNullable;
import java.io.Serializable;
import java.time.OffsetDateTime;
import jakarta.validation.Valid;
import jakarta.validation.constraints.*;
import org.hibernate.validator.constraints.*;
import io.swagger.v3.oas.annotations.media.Schema;


import java.util.*;
import jakarta.annotation.Generated;

/**
 * UsersApiErrorResponse
 */
@lombok.Data @lombok.Builder @lombok.NoArgsConstructor @lombok.AllArgsConstructor

@Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2023-01-03T11:16:10.516269500+01:00[Europe/Rome]")
public class UsersApiErrorResponse implements Serializable {

  private static final long serialVersionUID = 1L;

  @JsonProperty("type")
  private String type;

  @JsonProperty("instance")
  private String instance;

  @JsonProperty("title")
  private String title;

  @JsonProperty("details")
  private String details;

  /**
   * Gets or Sets applicativeError
   */
  public enum ApplicativeErrorEnum {
    USER_NOT_FOUND("USER_NOT_FOUND"),
    
    USER_ALREADY_EXISTS("USER_ALREADY_EXISTS"),
    
    GENERIC_FAILURE("GENERIC_FAILURE");

    private String value;

    ApplicativeErrorEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static ApplicativeErrorEnum fromValue(String value) {
      for (ApplicativeErrorEnum b : ApplicativeErrorEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("applicativeError")
  private ApplicativeErrorEnum applicativeError;

  @JsonProperty("status")
  private Integer status;

  public UsersApiErrorResponse type(String type) {
    this.type = type;
    return this;
  }

  /**
   * Get type
   * @return type
  */
  @NotNull 
  @Schema(name = "type", required = true)
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public UsersApiErrorResponse instance(String instance) {
    this.instance = instance;
    return this;
  }

  /**
   * Get instance
   * @return instance
  */
  @NotNull 
  @Schema(name = "instance", required = true)
  public String getInstance() {
    return instance;
  }

  public void setInstance(String instance) {
    this.instance = instance;
  }

  public UsersApiErrorResponse title(String title) {
    this.title = title;
    return this;
  }

  /**
   * Get title
   * @return title
  */
  @NotNull 
  @Schema(name = "title", required = true)
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public UsersApiErrorResponse details(String details) {
    this.details = details;
    return this;
  }

  /**
   * Get details
   * @return details
  */
  @NotNull 
  @Schema(name = "details", required = true)
  public String getDetails() {
    return details;
  }

  public void setDetails(String details) {
    this.details = details;
  }

  public UsersApiErrorResponse applicativeError(ApplicativeErrorEnum applicativeError) {
    this.applicativeError = applicativeError;
    return this;
  }

  /**
   * Get applicativeError
   * @return applicativeError
  */
  @NotNull 
  @Schema(name = "applicativeError", required = true)
  public ApplicativeErrorEnum getApplicativeError() {
    return applicativeError;
  }

  public void setApplicativeError(ApplicativeErrorEnum applicativeError) {
    this.applicativeError = applicativeError;
  }

  public UsersApiErrorResponse status(Integer status) {
    this.status = status;
    return this;
  }

  /**
   * Get status
   * @return status
  */
  @NotNull 
  @Schema(name = "status", required = true)
  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UsersApiErrorResponse usersApiErrorResponse = (UsersApiErrorResponse) o;
    return Objects.equals(this.type, usersApiErrorResponse.type) &&
        Objects.equals(this.instance, usersApiErrorResponse.instance) &&
        Objects.equals(this.title, usersApiErrorResponse.title) &&
        Objects.equals(this.details, usersApiErrorResponse.details) &&
        Objects.equals(this.applicativeError, usersApiErrorResponse.applicativeError) &&
        Objects.equals(this.status, usersApiErrorResponse.status);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, instance, title, details, applicativeError, status);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class UsersApiErrorResponse {\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    instance: ").append(toIndentedString(instance)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    details: ").append(toIndentedString(details)).append("\n");
    sb.append("    applicativeError: ").append(toIndentedString(applicativeError)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

