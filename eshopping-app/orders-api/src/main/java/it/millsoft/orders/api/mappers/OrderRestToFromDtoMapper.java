package it.millsoft.orders.api.mappers;

import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.interfaces.rest.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.Set;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        uses = ProductItemRestToFromDtoMapper.class)
public interface OrderRestToFromDtoMapper {

    @Mapping(target = "code", source = "code")
    @Mapping(target = "submittedOnUTC", source = "submittedOnUTC")
    @Mapping(target = "status", source = "status")
    @Mapping(target = "productItems", source = "productItems")
    @Mapping(target = "userEmail", source = "userEmail")
    OrderDTO orderRestToDto(Order order);

    Set<OrderDTO> ordersRestToDto(Set<Order> orders);

    @Mapping(target = "code", source = "code")
    @Mapping(target = "submittedOnUTC", source = "submittedOnUTC")
    @Mapping(target = "status", source = "status")
    @Mapping(target = "productItems", source = "productItems")
    @Mapping(target = "userEmail", source = "userEmail")
    Order orderDtoToRest(OrderDTO order);

    Set<Order> ordersDtoToRest(Set<OrderDTO> orders);

}
