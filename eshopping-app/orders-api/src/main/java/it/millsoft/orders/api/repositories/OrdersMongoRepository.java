package it.millsoft.orders.api.repositories;

import it.millsoft.orders.api.model.Order;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.Optional;
import java.util.Set;

@Repository
public interface OrdersMongoRepository extends MongoRepository<Order,String> {

    @Query("{ 'userEmail' : ?0 }")
    Set<Order> findOrdersOfUser(String email);

    @Query("{ 'code' :  ?0 }")
    Optional<Order> findOrderByCode(String code);

    @Query(value = "{" +
                   "    'submittedOnUTC' : {" +
                   "        $gte: ?0," +
                   "        $lte: ?1" +
                   "    }" +
                   "}", count = true)
    long countOrdersInPeriod(OffsetDateTime from,
                             OffsetDateTime to);

}
