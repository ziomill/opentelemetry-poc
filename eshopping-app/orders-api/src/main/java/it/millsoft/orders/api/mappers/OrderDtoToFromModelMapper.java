package it.millsoft.orders.api.mappers;


import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.model.Order;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface OrderDtoToFromModelMapper {

    @Mapping(target = "code", source = "order.code")
    @Mapping(target = "submittedOnUTC", source = "order.submittedOnUTC")
    @Mapping(target = "status", source = "order.status")
    @Mapping(target = "productItems", expression = "java(mapFullProducts(order.getProductItems(), productItems))")
    @Mapping(target = "userEmail", source = "order.userEmail")
    OrderDTO orderModelToDto(Order order, Set<ProductItemDTO> productItems);

    /**
     * Add, for each product, the requested stock.
     *
     * @param productsWithRequestedStockMap the Map where keys are product's code and values are the requested stock for that product
     * @param productItems the Collection of the items in the order with full info (except
     * @return the Collection of products with all needed info.
     */
    default Set<ProductItemDTO> mapFullProducts(Map<String,Integer> productsWithRequestedStockMap,
                                                Set<ProductItemDTO> productItems) {
        Set<ProductItemDTO> fullProducts = productItems.stream()
                .map(product -> {
                    int requestedStock = productsWithRequestedStockMap.get(product.getCode());
                    product.setRequestedStock(requestedStock);
                    return product;
                })
                .collect(Collectors.toSet());
        return fullProducts;
    }



}
