package it.millsoft.orders.api.ext;

import it.millsoft.orders.api.dto.ProductItemDTO;

import java.util.Map;
import java.util.Set;

public interface ProductsApiClient {

    Set<ProductItemDTO> getProductsByCode(Set<String> codes);
    Map<String,Integer> updateProductsStock(Map<String,Integer> requestedStock);

}
