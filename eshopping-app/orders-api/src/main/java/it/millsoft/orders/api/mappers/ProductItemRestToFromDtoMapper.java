package it.millsoft.orders.api.mappers;

import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.interfaces.rest.model.ProductItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.Set;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ProductItemRestToFromDtoMapper {

    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "requestedStock", source = "requestedStock")
    ProductItemDTO productItemRestToDto(ProductItem productItem);

    Set<ProductItemDTO> productItemsRestToDto(Set<ProductItem> productItems);

    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "requestedStock", source = "requestedStock")
    ProductItem productItemDtoToRest(ProductItemDTO productItem);

    Set<ProductItem> productItemsDtoToRest(Set<ProductItemDTO> productItems);

}
