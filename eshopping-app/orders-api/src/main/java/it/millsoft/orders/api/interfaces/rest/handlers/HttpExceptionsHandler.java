package it.millsoft.orders.api.interfaces.rest.handlers;


import it.millsoft.orders.api.commons.Utils;
import it.millsoft.orders.api.dto.OrdersApiErrorEnum;
import it.millsoft.orders.api.exceptions.*;
import it.millsoft.orders.api.interfaces.rest.model.OrdersApiErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.UUID;

@ControllerAdvice
public class HttpExceptionsHandler extends ResponseEntityExceptionHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(HttpExceptionsHandler.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleGenericError(Exception ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", OrdersApiErrorEnum.GENERIC_FAILURE.name());
        OrdersApiErrorResponse errorBody = this.buildGenericError(ex);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    @ExceptionHandler(ClientsIntegrationStrategyNotSupportedException.class)
    public ResponseEntity<Object> handleApplicativeError(ClientsIntegrationStrategyNotSupportedException ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", ex.getError().name());
        OrdersApiErrorResponse errorBody = this.buildApplicativeError(ex,HttpStatus.INTERNAL_SERVER_ERROR);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<Object> handleApplicativeError(OrderNotFoundException ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", ex.getError().name());
        OrdersApiErrorResponse errorBody = this.buildApplicativeError(ex,HttpStatus.NOT_FOUND);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    @ExceptionHandler(OrderStatusNotUpdatableException.class)
    public ResponseEntity<Object> handleApplicativeError(OrderStatusNotUpdatableException ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", ex.getError().name());
        OrdersApiErrorResponse errorBody = this.buildApplicativeError(ex,HttpStatus.CONFLICT);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    @ExceptionHandler(InconsitentOrderException.class)
    public ResponseEntity<Object> handleApplicativeError(InconsitentOrderException ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", ex.getError().name());
        OrdersApiErrorResponse errorBody = this.buildApplicativeError(ex,HttpStatus.INTERNAL_SERVER_ERROR);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    @ExceptionHandler({ProductsApiClientException.class,
                       UsersApiClientException.class})
    public ResponseEntity<Object> handleApplicativeError(OrdersApiException ex, WebRequest request) { // TODO: Da controllare
        LOGGER.debug("REST - Intercepted error --> {}", ex.getError().name());
        //    BAD GATEWAY 502 – The server while acting as a gateway or a proxy,
        //    received an invalid response from the upstream server it accessed
        //    in attempting to fulfill the request.
        OrdersApiErrorResponse errorBody = this.buildApplicativeError(ex,HttpStatus.BAD_GATEWAY);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    private OrdersApiErrorResponse buildApplicativeError(OrdersApiException ex, HttpStatus status) {
        String type = ex.getError().getErrorDesc();
        String instance = Utils.errorInstanceAsURN(ex.getErrorUUID());
        String title = ex.getError().name();
        String details = ex.getMessage();
        OrdersApiErrorEnum error = ex.getError();
        OrdersApiErrorResponse result = new OrdersApiErrorResponse(type,
                instance,
                title,
                details,
                OrdersApiErrorResponse.ApplicativeErrorEnum.fromValue(error.name()),
                status.value());
        return result;
    }

    private OrdersApiErrorResponse buildGenericError(Exception ex) {
        UUID errorUUID = UUID.randomUUID();
        String instance = Utils.errorInstanceAsURN(errorUUID);
        String type = OrdersApiErrorEnum.GENERIC_FAILURE.getErrorDesc();
        String title = OrdersApiErrorEnum.GENERIC_FAILURE.name();
        String details = ex.getMessage();
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        OrdersApiErrorResponse result = new OrdersApiErrorResponse(type,
                instance,
                title,
                details,
                OrdersApiErrorResponse.ApplicativeErrorEnum.GENERIC_FAILURE,
                status.value());
        String msg = String.format(ex.getMessage() + " - Error UUID: %s", errorUUID);
        LOGGER.error(msg);
        return result;
    }

}
