package it.millsoft.orders.api.config;

import it.millsoft.orders.api.interfaces.grpc.handlers.GrpcExceptionsHandler;
import net.devh.boot.grpc.client.autoconfigure.GrpcClientAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcAdviceAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcServerAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcServerFactoryAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ImportAutoConfiguration(value = {GrpcServerAutoConfiguration.class,
        GrpcClientAutoConfiguration.class,
        GrpcServerFactoryAutoConfiguration.class,
        GrpcAdviceAutoConfiguration.class})
@Import(GrpcExceptionsHandler.class)
public class GrpcConfig {
}
