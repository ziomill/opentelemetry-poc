package it.millsoft.orders.api.interfaces.grpc.handlers;


import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.orders.api.dto.OrdersApiErrorEnum;
import it.millsoft.orders.api.exceptions.*;
import it.millsoft.orders.api.interfaces.grpc.model.OrdersApiErrorResponse;
import net.devh.boot.grpc.server.advice.GrpcAdvice;
import net.devh.boot.grpc.server.advice.GrpcExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

@GrpcAdvice
public class GrpcExceptionsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(GrpcExceptionsHandler.class);

    @GrpcExceptionHandler(Exception.class)
    public StatusException handleGenericError(Exception ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", OrdersApiErrorEnum.GENERIC_FAILURE.name());
        return this.buildGenericStatusException(ex);
    }

    @GrpcExceptionHandler(ClientsIntegrationStrategyNotSupportedException.class)
    public StatusException handleApplicativeError(ClientsIntegrationStrategyNotSupportedException ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", ex.getError().name());
        return this.buildApplicativeStatusException(ex, Status.INTERNAL);
    }

    @GrpcExceptionHandler(OrderNotFoundException.class)
    public StatusException handleApplicativeError(OrderNotFoundException ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", ex.getError().name());
        return this.buildApplicativeStatusException(ex, Status.NOT_FOUND);
    }

    @GrpcExceptionHandler(OrderStatusNotUpdatableException.class)
    public StatusException handleApplicativeError(OrderStatusNotUpdatableException ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", ex.getError().name());
        return this.buildApplicativeStatusException(ex, Status.INVALID_ARGUMENT);
    }

    @GrpcExceptionHandler(InconsitentOrderException.class)
    public StatusException handleApplicativeError(InconsitentOrderException ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", ex.getError().name());
        return this.buildApplicativeStatusException(ex, Status.INTERNAL);
    }

    @GrpcExceptionHandler({ProductsApiClientException.class,
                           UsersApiClientException.class})
    public StatusException handleApplicativeError(OrdersApiException ex) { // TODO: Da controllare
        LOGGER.debug("GRPC - Intercepted error --> {}", ex.getError().name());
        switch (ex.getError()) {
            case PRODUCTS_API_CLIENT_FAILURE,
                    USERS_API_CLIENT_FAILURE -> {
                // DEADLINE_EXCEEDED 4: The deadline expired before the operation could complete.
                return this.buildApplicativeStatusException(ex, Status.DEADLINE_EXCEEDED);
            }
            default -> {
                // FAILED_PRECONDITION 9: The operation was rejected because the system is not in a state required for the operation's execution
                return this.buildApplicativeStatusException(ex,Status.UNAVAILABLE);
            }
        }
    }

    private StatusException buildApplicativeStatusException(OrdersApiException ex, Status status) {
        String type = ex.getError().getErrorDesc();
        String instance = "urn:uuid:" + ex.getErrorUUID();
        String title = ex.getError().name();
        String details = ex.getMessage();
        OrdersApiErrorEnum error = ex.getError();

//        Any applicativeError = Any.newBuilder().setValue(ByteString.copyFrom(error.toString().getBytes())).build();
        OrdersApiErrorResponse.ApplicativeError applicativeError = OrdersApiErrorResponse.ApplicativeError.valueOf(error.name());
        OrdersApiErrorResponse errorResponse = OrdersApiErrorResponse.newBuilder()
                .setType(type)
                .setInstance(instance)
                .setTitle(title)
                .setDetails(details)
                .setApplicativeError(applicativeError)
                .setStatus(status.getCode().value())
                .build();
        Metadata.Key<OrdersApiErrorResponse> shopErrorResponseKey = ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance());
        Metadata metadata = new Metadata();
        metadata.put(shopErrorResponseKey, errorResponse);
        return status.asException(metadata);
    }

    private StatusException buildGenericStatusException(Exception ex) {
        UUID errorUUID = UUID.randomUUID();
        String type = OrdersApiErrorEnum.GENERIC_FAILURE.getErrorDesc();
        String instance = "urn:uuid:" + errorUUID;
        String title = OrdersApiErrorEnum.GENERIC_FAILURE.name();
        String details = ex.getMessage();
        OrdersApiErrorEnum error = OrdersApiErrorEnum.GENERIC_FAILURE;
        Status status = Status.INTERNAL;

        OrdersApiErrorResponse.ApplicativeError applicativeError = OrdersApiErrorResponse.ApplicativeError.valueOf(error.name());
        String msg = String.format(ex.getMessage() + " - Error UUID: %s", errorUUID);
        LOGGER.error(msg);
        OrdersApiErrorResponse errorResponse = OrdersApiErrorResponse.newBuilder()
                .setType(type)
                .setInstance(instance)
                .setTitle(title)
                .setDetails(details)
                .setApplicativeError(applicativeError)
                .setStatus(status.getCode().value())
                .build();
        Metadata.Key<OrdersApiErrorResponse> shopErrorResponseKey = ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance());
        Metadata metadata = new Metadata();
        metadata.put(shopErrorResponseKey, errorResponse);
        return status.asException(metadata);
    }

}
