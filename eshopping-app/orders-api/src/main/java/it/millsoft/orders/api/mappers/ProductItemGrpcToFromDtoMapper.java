package it.millsoft.orders.api.mappers;

import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.interfaces.grpc.model.ProductItem;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.Set;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface ProductItemGrpcToFromDtoMapper {

    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "requestedStock", source = "requestedStock")
    ProductItemDTO productItemGrpcToDto(ProductItem productItem);

    Set<ProductItemDTO> productItemsGrpcToDto(Set<ProductItem> productItems);

    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "requestedStock", source = "requestedStock")
    ProductItem productItemDtoToGrpc(ProductItemDTO productItem);

    Set<ProductItem> productItemsDtoToGrpc(Set<ProductItemDTO> productItems);

}
