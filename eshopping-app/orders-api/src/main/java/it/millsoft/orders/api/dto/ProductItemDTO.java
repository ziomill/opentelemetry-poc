package it.millsoft.orders.api.dto;

import lombok.*;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ProductItemDTO {

    @NonNull
    @EqualsAndHashCode.Include
    private String code;
    @NonNull
    private String description;
    @NonNull
    private String category;
    @NonNull
    private Double price;
    private Integer requestedStock;

}
