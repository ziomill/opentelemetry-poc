package it.millsoft.orders.api.ext.http;

import it.millsoft.orders.api.commons.Utils;
import it.millsoft.orders.api.exceptions.UsersApiClientException;
import it.millsoft.orders.api.ext.UsersApiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;
import java.util.UUID;

@Component
@ConditionalOnProperty(
        value = "commons.client.integration-strategy.users-api",
        havingValue = "HTTP")
public class UsersApiHttpClient implements UsersApiClient {

    private static Logger LOGGER = LoggerFactory.getLogger(UsersApiHttpClient.class);

    @Value("${rest.client.users-api.address}")
    private String USERS_API_HTTP_ENDPOINT;

    @Override
    public void checkUserExistence(String email) {
        String uriTemplate = "{users_api_http_endpoint}/user/{user_email}";
        Map<String,String> uriParams = Map.of(
                "users_api_http_endpoint",USERS_API_HTTP_ENDPOINT,
                "user_email",email);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        try {
            // Build Http Client
            HttpClient client = HttpClient.newBuilder().build();

            // Build request
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(uri)
                    .method("HEAD", HttpRequest.BodyPublishers.noBody())
                    .build();

            // Call the REST API
            HttpResponse response = client.send(request,
                    HttpResponse.BodyHandlers.discarding());

            // Handle response --> Warning: The HEAD http method doesn't return a body in the response.
            switch (HttpStatus.valueOf(response.statusCode())) {
                // User Exists
                case NO_CONTENT -> {
                    LOGGER.debug("The REST service {} has been called with SUCCESS - An user having email {} has been found",uri,email);
                }
                // User doesn't exist
                case NOT_FOUND -> {
                    UUID errorUUID = UUID.randomUUID();
                    String msg = String.format("The REST service %s has been called with SUCCESS - No user having email %s has been found",uri,email);
                    LOGGER.error(msg);
                    throw new UsersApiClientException(msg,errorUUID);
                }
                // Service Failure or Unexpected result
                default -> {
                    UUID errorUUID = UUID.randomUUID();
                    String msg = String.format("An ERROR has occurred calling the REST service %s: %s - Error UUID: %s", uri, "No response from the client because of HEAD method)", errorUUID);
                    LOGGER.error(msg);
                    throw new UsersApiClientException(msg,errorUUID);
                }
            }
        } catch (Exception ex) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("An ERROR has occurred calling the REST service %s: %s - Error UUID: %s", uri, ex.getMessage(), errorUUID);
            LOGGER.error(msg);
            throw new UsersApiClientException(msg,errorUUID);
        }
    }

}
