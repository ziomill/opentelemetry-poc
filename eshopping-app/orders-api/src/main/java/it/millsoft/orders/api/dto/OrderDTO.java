package it.millsoft.orders.api.dto;

import lombok.*;

import java.time.OffsetDateTime;
import java.util.Set;

@Data
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class OrderDTO {

    @EqualsAndHashCode.Include
    @NonNull
    private String code;
    @NonNull
    private OffsetDateTime submittedOnUTC;
    @NonNull
    private OrderStatus status;
    @NonNull
    private Set<ProductItemDTO> productItems;
    @NonNull
    private String userEmail;

}
