package it.millsoft.orders.api.business;


import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.dto.OrderStatus;

import java.util.Map;
import java.util.Set;

public interface OrdersServices {

    Set<OrderDTO> getOrdersOfUser(String email);
    OrderDTO getOrder(String code);
    OrderDTO addOrderForUser(String email,
                            Map<String,Integer> requestedStock) ;
    String deleteOrder(String code);
    String updateOrderStatus(String code, OrderStatus status);

}
