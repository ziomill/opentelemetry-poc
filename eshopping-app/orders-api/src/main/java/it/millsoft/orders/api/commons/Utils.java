package it.millsoft.orders.api.commons;

import com.jcabi.urn.URN;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.SpanContext;
import org.apache.commons.collections4.MapUtils;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Map;
import java.util.UUID;

public class Utils {

    public static class Constants {
        public static final String W3C_TRACE_CONTEXT_SPEC_VERSION = "00";
        public static final String W3C_SERVER_TIMING_HEADER = "Server-Timing";
    }

    public static String errorInstanceAsURN(UUID errorUUID) {
        String nid = "uuid";
        String nss = errorUUID.toString();
        URN urn = new URN(nid,nss);
        return urn.toString();
    }

    public static URI toURI(String uriTemplate,
                      Map<String,String> uriParams,
                      Map<String,Object[]> queryParams
    ) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(uriTemplate);
        if(MapUtils.isNotEmpty(queryParams)) {
            for(String queryParamKey : queryParams.keySet()) {
                builder.queryParam(queryParamKey,queryParams.get(queryParamKey));
            }
        }
        if(MapUtils.isNotEmpty(uriParams)) {
            return builder.buildAndExpand(uriParams).toUri();
        } else {
            return builder.build().toUri();
        }
    }

    public static String composeServerTimingHeader() {
        String result = "traceparent;desc=\"" + getCurrentTraceparent() + "\"";
        return result;
    }

    private static String getCurrentTraceparent() {
        SpanContext spanContext = Span.current().getSpanContext();
        String traceId = spanContext.getTraceId();
        String parentId = spanContext.getSpanId();
        String traceFlags = spanContext.getTraceFlags().asHex();
        String result = Utils.Constants.W3C_TRACE_CONTEXT_SPEC_VERSION + "-" + traceId + "-" + parentId + "-" + traceFlags;
        return result;
    }

}
