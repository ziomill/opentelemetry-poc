package it.millsoft.orders.api.ext.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import it.millsoft.orders.api.commons.Utils;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.exceptions.ProductsApiClientException;
import it.millsoft.orders.api.ext.ProductsApiClient;
import it.millsoft.orders.api.ext.http.products.api.model.Product;
import it.millsoft.orders.api.ext.http.products.api.model.ProductsApiErrorResponse;
import it.millsoft.orders.api.mappers.ProductsApiClientMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@ConditionalOnProperty(
        value = "commons.client.integration-strategy.products-api",
        havingValue = "HTTP")
public class ProductsApiHttpClient implements ProductsApiClient {

    private static Logger LOGGER = LoggerFactory.getLogger(ProductsApiHttpClient.class);

    @Value("${rest.client.products-api.address}")
    private String PRODUCTS_API_HTTP_ENDPOINT;

    @Autowired
    private ProductsApiClientMapper mapper;


    @Override
    public Set<ProductItemDTO> getProductsByCode(Set<String> codes) {
        String productsCode = CollectionUtils.isEmpty(codes) ? "*" : codes.stream().map(Object::toString).collect(Collectors.joining(","));

        String uriTemplate = "{products_api_endpoint}/products/{products_code}";
        Map<String,String> uriParams = Map.of(
                "products_api_endpoint",PRODUCTS_API_HTTP_ENDPOINT,
                "products_code",productsCode);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        try {
            // Build Http Client
            HttpClient client = HttpClient.newBuilder().build();

            // Build request
            HttpRequest request = HttpRequest.newBuilder()
                    .uri(uri)
                    .GET()
                    .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                    .build();

            // Call the REST API
            HttpResponse<String> response = client.send(request,
                    HttpResponse.BodyHandlers.ofString());

            // Handle response
            switch (HttpStatus.valueOf(response.statusCode())) {
                case OK -> {
                    ObjectMapper mapper = new ObjectMapper();
                    CollectionType setType = mapper.getTypeFactory().constructCollectionType(Set.class, Product.class);
                    Set<Product> productsList = mapper.readValue(response.body(), setType);
                    LOGGER.debug("The REST service {} has been called with SUCCESS - {} products has been found for codes {}",uri,productsList.size(),productsCode);
                    Set<ProductItemDTO> result = this.mapper.productsRestToProductItemsDto(productsList);
                    return result;
                }
                // Service Failure or Unexpected result
                default -> {
                    UUID errorUUID = UUID.randomUUID();
                    ObjectMapper mapper = new ObjectMapper();
                    ProductsApiErrorResponse errorResponse = mapper.readValue(response.body(), ProductsApiErrorResponse.class);
                    String msg = String.format("An ERROR has occurred calling the REST service %s: %s - Error UUID: %s", uri,errorResponse.toString(), errorUUID);
                    LOGGER.error(msg);
                    throw new ProductsApiClientException(msg,errorUUID);
                }
            }
        } catch (Exception ex) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("An ERROR has occurred calling the REST service %s: %s - Error UUID: %s", uri,ex.getMessage(), errorUUID);
            LOGGER.error(msg);
            throw new ProductsApiClientException(msg,errorUUID);
        }
    }

    @Override
    public Map<String, Integer> updateProductsStock(Map<String, Integer> requestedStock) {
        String uriTemplate = "{products_api_endpoint}/products/stock";
        Map<String,String> uriParams = Map.of("products_api_endpoint",PRODUCTS_API_HTTP_ENDPOINT);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        try {
            // Build Http Client
            HttpClient client = HttpClient.newBuilder().build();

            // Build request
            ObjectMapper objectMapper = new ObjectMapper();
            String requestBody = objectMapper.writerWithDefaultPrettyPrinter()
                                             .writeValueAsString(requestedStock);

            HttpRequest request = HttpRequest.newBuilder()
                                             .uri(uri)
                                             .method("PATCH",HttpRequest.BodyPublishers.ofString(requestBody))
                                             .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                                             .build();

            // Call the REST API
            HttpResponse<String> response = client.send(request,
                                                        HttpResponse.BodyHandlers.ofString());

            // Handle response
            switch (HttpStatus.valueOf(response.statusCode())) {
                case OK -> {
                    ObjectMapper mapper = new ObjectMapper();
                    Map<String,Integer> newAvailableStock = mapper.readValue(response.body(), Map.class);
                    LOGGER.debug("The REST service {} has been called with SUCCESS - The available requestedStock for products having codes {} has been updated",uri,requestedStock.keySet());
                    return newAvailableStock;
                }
                default -> {
                    UUID errorUUID = UUID.randomUUID();
                    ObjectMapper mapper = new ObjectMapper();
                    ProductsApiErrorResponse errorResponse = mapper.readValue(response.body(), ProductsApiErrorResponse.class);
                    String msg = String.format("An ERROR has occurred calling the REST service %s: %s - Error UUID: %s", uri,errorResponse.toString(), errorUUID);
                    LOGGER.error(msg);
                    throw new ProductsApiClientException(msg,errorUUID);
                }
            }
        } catch (Exception ex) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("An ERROR has occurred calling the REST service %s: %s - Error UUID: %s", uri, ex.getMessage(), errorUUID);
            LOGGER.error(msg);
            throw new ProductsApiClientException(msg,errorUUID);
        }
    }


}
