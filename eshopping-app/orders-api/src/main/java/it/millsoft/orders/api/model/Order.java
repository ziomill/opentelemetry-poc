package it.millsoft.orders.api.model;

import it.millsoft.orders.api.dto.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nonapi.io.github.classgraph.json.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.OffsetDateTime;
import java.util.Map;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Document("orders")
public class Order {

    @Id
    private String id;

    private String code;
    private OffsetDateTime submittedOnUTC;
    private OrderStatus status;
    private Map<String,Integer> productItems;
    private String userEmail;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return getCode().equals(order.getCode());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCode());
    }
}
