package it.millsoft.orders.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class OrdersApiStarter {

    public static void main(String[] args)
    {
        SpringApplication.run(OrdersApiStarter.class, args);
    }

}
