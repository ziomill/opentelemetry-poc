package it.millsoft.orders.api.interfaces.rest.controllers;

import it.millsoft.orders.api.business.OrdersServices;
import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.interfaces.rest.model.Order;
import it.millsoft.orders.api.interfaces.rest.model.OrderStatus;
import it.millsoft.orders.api.interfaces.rest.model.OrdersRestApi;
import it.millsoft.orders.api.mappers.OrderRestToFromDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@RestController
public class OrdersRestApiImpl implements OrdersRestApi {

    @Autowired
    private OrdersServices ordersServices;

    @Autowired
    private OrderRestToFromDtoMapper mapper;

    @Override
    public ResponseEntity<Order> getUserOrders(String email) {
        Set<OrderDTO> dOrders = ordersServices.getOrdersOfUser(email);
        Set<Order> rOrders = mapper.ordersDtoToRest(dOrders);
        return new ResponseEntity(rOrders,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Order> getOrder(String code) {
        OrderDTO dOrder = ordersServices.getOrder(code);
        Order rOrder = mapper.orderDtoToRest(dOrder);
        return new ResponseEntity(rOrder,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Order> addOrderForUser(String email,
                                                 Map<String, Integer> productsQuantity) {
        OrderDTO dOrder = ordersServices.addOrderForUser(email, productsQuantity);
        Order rOrder = mapper.orderDtoToRest(dOrder);
        return new ResponseEntity(rOrder,HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<String> deleteOrder(String code) {
        String deletedOrderId = ordersServices.deleteOrder(code);
        return new ResponseEntity(deletedOrderId,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> updateOrderStatus(String code,
                                                    OrderStatus status) {
        String updatedOrderId = ordersServices.updateOrderStatus(code,
                                                                 it.millsoft.orders.api.dto.OrderStatus.valueOf(status.name()));
        return new ResponseEntity(updatedOrderId,HttpStatus.OK);
    }

}
