// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: orders_api_grpc_server_interfaces.proto

package it.millsoft.orders.api.interfaces.grpc.model;

public interface AddOrderForUserResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:AddOrderForUserResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>.Order order = 1;</code>
   * @return Whether the order field is set.
   */
  boolean hasOrder();
  /**
   * <code>.Order order = 1;</code>
   * @return The order.
   */
  it.millsoft.orders.api.interfaces.grpc.model.Order getOrder();
  /**
   * <code>.Order order = 1;</code>
   */
  it.millsoft.orders.api.interfaces.grpc.model.OrderOrBuilder getOrderOrBuilder();
}
