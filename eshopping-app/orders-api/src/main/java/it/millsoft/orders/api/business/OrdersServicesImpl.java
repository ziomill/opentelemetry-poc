package it.millsoft.orders.api.business;

import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.dto.OrderStatus;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.exceptions.InconsitentOrderException;
import it.millsoft.orders.api.exceptions.OrderNotFoundException;
import it.millsoft.orders.api.exceptions.OrderStatusNotUpdatableException;
import it.millsoft.orders.api.ext.ProductsApiClient;
import it.millsoft.orders.api.ext.UsersApiClient;
import it.millsoft.orders.api.mappers.OrderDtoToFromModelMapper;
import it.millsoft.orders.api.model.Order;
import it.millsoft.orders.api.repositories.OrdersMongoRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OrdersServicesImpl implements OrdersServices {

    private static Logger LOGGER = LoggerFactory.getLogger(OrdersServicesImpl.class);

    @Autowired
    private UsersApiClient usersApiClient;

    @Autowired
    private ProductsApiClient productApiClient;

    @Autowired
    private OrdersMongoRepository ordersMongoRepository;

    @Autowired
    private OrderDtoToFromModelMapper mapper;

    @Override
    public Set<OrderDTO> getOrdersOfUser(String email) {
        LOGGER.info("Retrieving orders of USER {}", email);

        // Check if user have orders
        Set<Order> foundOrders = ordersMongoRepository.findOrdersOfUser(email);
        if(CollectionUtils.isEmpty(foundOrders)) {
            LOGGER.warn("No orders has been found for USER having email {}",email);
            return new HashSet<>();
        }

        // User have orders
        LOGGER.info("Found {} orders for USER {}",foundOrders.size(),email);

        Set<OrderDTO> result = new HashSet<>();
        for(Order foundOrder : foundOrders) {
            String productsCode = foundOrder.getProductItems().keySet().stream().map(Object::toString).collect(Collectors.joining(","));
            LOGGER.info("An ORDER with code {} has been found. It includes products with codes: {}. Retrieving products info ...",foundOrder.getCode(),productsCode);

            // Call the Products API in order to retrieve products order data
            Set<ProductItemDTO> productItems = productApiClient.getProductsByCode(foundOrder.getProductItems().keySet());

            // Integrity Check on retrieved products
            checkOrderConsistency(foundOrder,productItems);

            LOGGER.info("Products info retrieved with success for Order having code {}",foundOrder.getCode());

            OrderDTO dOrder = mapper.orderModelToDto(foundOrder,productItems);
            result.add(dOrder);
        }
        LOGGER.info("Retrieved {} orders for USER {}",result.size(),email);
        return result;
    }

    @Override
    public OrderDTO getOrder(String code) {
        Optional<Order> optOrder = ordersMongoRepository.findOrderByCode(code);
        if(optOrder.isEmpty()) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("No ORDER with the CODE %s has been found - Error UUID: %s",code,errorUUID);
            LOGGER.warn(msg);
            throw new OrderNotFoundException(msg,errorUUID);
        }

        // Order exists
        Order mOrder = optOrder.get();
        LOGGER.info("An ORDER with ID {} has been found.",mOrder.getId());

        // Call the Products API in order to retrieve products order data
        String productsCode = mOrder.getProductItems().keySet().stream().map(Object::toString).collect(Collectors.joining(","));
        LOGGER.info("The ORDER with the code {} and ID {} includes products with codes: {}. Retrieving products info ...",mOrder.getCode(), mOrder.getId(),productsCode);
        Set<ProductItemDTO> products = productApiClient.getProductsByCode(mOrder.getProductItems().keySet());

        // Integrity Check on retrieved products
        checkOrderConsistency(mOrder,products);

        // Build result
        OrderDTO result = mapper.orderModelToDto(mOrder,products);
        return result;
    }

    private void checkOrderConsistency(Order foundOrder,
                                       Set<ProductItemDTO> products) {
        if(products.size() != foundOrder.getProductItems().size()) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("The ORDER with code %s should contains %d products but the PRODUCTS-SERVICE returned %d - Error UUID: %s",foundOrder.getCode(),
                                                                                                                                                  foundOrder.getProductItems().size(),
                                                                                                                                                  products.size(),
                                                                                                                                                  errorUUID);
            LOGGER.warn(msg);
            throw new InconsitentOrderException(msg,errorUUID);
        }
    }

    @Override
    public OrderDTO addOrderForUser(String email,
                                    Map<String,Integer> requestedStock) {

        // Check if User who made the Order exists
        usersApiClient.checkUserExistence(email);

        // Update products stock
        productApiClient.updateProductsStock(requestedStock);
        LOGGER.info("PRODUCTS stock updated with success");

        // Get Product's info
        Set<ProductItemDTO> productItems = productApiClient.getProductsByCode(requestedStock.keySet());

        Order mOrder = createNewOrder(email, requestedStock);
        mOrder = ordersMongoRepository.save(mOrder);

        LOGGER.info("Added ORDER with ID {} and CODE {} for USER {}",mOrder.getId(),mOrder.getCode(),mOrder.getUserEmail());
        OrderDTO result = mapper.orderModelToDto(mOrder,productItems);
        return result;
    }

    @Override
    public String deleteOrder(String code) {
        // Order doesn't exist
        Optional<Order> optOrder = ordersMongoRepository.findOrderByCode(code);
        if(optOrder.isEmpty()) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("No ORDER with the CODE %s has been found - Error UUID: %s",code,errorUUID);
            LOGGER.info(msg);
            throw new OrderNotFoundException(msg,errorUUID);
        }

        // Order exists
        Order mOrder = optOrder.get();
        ordersMongoRepository.delete(mOrder);

        LOGGER.info("The Order with CODE {} and ID {} has been deleted.",code,mOrder.getId());
        return mOrder.getId();
    }

    @Override
    public String updateOrderStatus(String code, OrderStatus status) {
        // Order doesn't exist
        Optional<Order> optOrder = ordersMongoRepository.findOrderByCode(code);
        if(optOrder.isEmpty()) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("No Order with the CODE %s has been found - Error UUID: %s",code,errorUUID);
            LOGGER.info(msg);
            throw new OrderNotFoundException(msg,errorUUID);
        }

        // Order exists
        Order mOrder = optOrder.get();

        // Check Actual Status
        if(mOrder.getStatus().equals(status)) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("The Order with the CODE %s is already in the %s status  - Error UUID: %s",code,status.name(),errorUUID);
            LOGGER.info(msg);
            throw new OrderStatusNotUpdatableException(msg,errorUUID);
        }

        // New Status is Consistent
        mOrder.setStatus(status);
        mOrder = ordersMongoRepository.save(mOrder);

        LOGGER.info("The Order's Status with CODE {} and ID {} has been updated to {}",code,mOrder.getId(),status.name());
        return mOrder.getId();
    }

    private static Order createNewOrder(String email,Map<String,Integer> productsStock) {
        Order result = new Order();
        result.setCode(generateOrderCode());
        result.setStatus(OrderStatus.CREATED);
        result.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        result.setUserEmail(email);
        result.setProductItems(productsStock);
        return result;
    }

    public static String generateOrderCode(){
        return "ORD" + UUID.randomUUID();
    }

}
