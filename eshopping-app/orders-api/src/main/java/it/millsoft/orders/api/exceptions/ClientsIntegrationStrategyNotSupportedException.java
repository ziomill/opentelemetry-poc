package it.millsoft.orders.api.exceptions;

import it.millsoft.orders.api.dto.OrdersApiErrorEnum;

import java.util.UUID;

public class ClientsIntegrationStrategyNotSupportedException extends OrdersApiException {

    public ClientsIntegrationStrategyNotSupportedException(String message, UUID errorUUID) {
        super(message,
                OrdersApiErrorEnum.CLIENTS_INTEGRATION_STRATEGY_NOT_SUPPORTED_FAILURE,
                errorUUID);
    }

}
