package it.millsoft.orders.api.metrics;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.metrics.Meter;
import io.opentelemetry.api.metrics.ObservableLongMeasurement;
import it.millsoft.orders.api.repositories.OrdersMongoRepository;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;

@Component
public class OrdersAsynchMetrics {

    private static Logger LOGGER = LoggerFactory.getLogger(OrdersAsynchMetrics.class);

    @Value("${OTEL_METRIC_EXPORT_INTERVAL}")
    private Long OTEL_METRIC_EXPORT_INTERVAL;

    @Autowired
    private OpenTelemetry openTelemetry;

    @Autowired
    private OrdersMongoRepository ordersMongoRepository;

    @PostConstruct
    public void init() {
        // Obtain the OpenTelemetry Meter instance
        Meter meter = openTelemetry.getMeter(this.getClass().getName());

        // Instantiate an asynch counter that counts submitted orders in the configured period of time
        meter.counterBuilder("submitted-orders-in-period-asynch-counter")
                .setDescription("Counts how many orders has been submitted in the configured period of time")
                .buildWithCallback(measurement -> countSubmittedOrdersAsynchMetrics(measurement));
    }

    // The callback function that PeriodMetricReader will trigger periodically, every <<metricExportIntervalInMillis>>
    public void countSubmittedOrdersAsynchMetrics(ObservableLongMeasurement measurement) {
        OffsetDateTime to = OffsetDateTime.now(ZoneOffset.UTC);
        Long periodInSecs = OTEL_METRIC_EXPORT_INTERVAL / 1000;
        OffsetDateTime from = to.minusSeconds(periodInSecs);
        Long ordersCount = ordersMongoRepository.countOrdersInPeriod(from,to);
        measurement.record(ordersCount);
        LOGGER.info("A total of " + ordersCount + " orders has been submitted in the last: " + periodInSecs + " seconds.");
    }

}
