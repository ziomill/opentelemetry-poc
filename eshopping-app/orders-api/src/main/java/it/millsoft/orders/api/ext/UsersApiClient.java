package it.millsoft.orders.api.ext;

public interface UsersApiClient {

    void checkUserExistence(String email);

}
