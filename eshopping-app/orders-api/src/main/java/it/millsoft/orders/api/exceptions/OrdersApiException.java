package it.millsoft.orders.api.exceptions;

import it.millsoft.orders.api.dto.OrdersApiErrorEnum;
import lombok.Getter;

import java.util.UUID;

@Getter
public abstract class OrdersApiException extends RuntimeException {

    private OrdersApiErrorEnum error;
    private UUID errorUUID;

    public OrdersApiException(String message,
                              OrdersApiErrorEnum error,
                              UUID errorUUID) {
        super(message);
        this.error = error;
        this.errorUUID = errorUUID;
    }

}
