package it.millsoft.orders.api.exceptions;

import it.millsoft.orders.api.dto.OrdersApiErrorEnum;

import java.util.UUID;

public class UsersApiClientException extends OrdersApiException {

    public UsersApiClientException(String message,
                                   UUID errorUUID) {
        super(message,
              OrdersApiErrorEnum.USERS_API_CLIENT_FAILURE,
              errorUUID);
    }

}
