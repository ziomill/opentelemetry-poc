package it.millsoft.orders.api.interfaces.grpc.handlers;

import io.grpc.*;
import it.millsoft.orders.api.commons.Utils;
import net.devh.boot.grpc.server.interceptor.GrpcGlobalServerInterceptor;
import org.springframework.http.HttpHeaders;

@GrpcGlobalServerInterceptor
public class GrpcTracesHandler implements ServerInterceptor {

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call,
                                                                 final Metadata requestHeaders,
                                                                 ServerCallHandler<ReqT, RespT> next) {
        return next.startCall(new ForwardingServerCall.SimpleForwardingServerCall<ReqT, RespT>(call) {
            @Override
            public void sendHeaders(Metadata responseHeaders) {
                // Add the "Access-Control-Expose-Headers" to enable clients to see the "Server-Timing" header
                Metadata.Key<String> ACCESS_CONTROL_EXPOSE_HEADERS_HEADER_KEY = Metadata.Key.of(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, Metadata.ASCII_STRING_MARSHALLER);
                responseHeaders.put(ACCESS_CONTROL_EXPOSE_HEADERS_HEADER_KEY, Utils.Constants.W3C_SERVER_TIMING_HEADER);
                // Add the "Server-Timing" header with the traceparent
                Metadata.Key<String> W3C_SERVER_TIMING_HEADER_KEY = Metadata.Key.of(Utils.Constants.W3C_SERVER_TIMING_HEADER, Metadata.ASCII_STRING_MARSHALLER);
                responseHeaders.put(W3C_SERVER_TIMING_HEADER_KEY, Utils.composeServerTimingHeader());
                super.sendHeaders(responseHeaders);
            }
        }, requestHeaders);

    }
}
