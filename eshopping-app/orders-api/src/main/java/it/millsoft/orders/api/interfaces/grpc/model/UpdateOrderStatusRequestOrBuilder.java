// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: orders_api_grpc_server_interfaces.proto

package it.millsoft.orders.api.interfaces.grpc.model;

public interface UpdateOrderStatusRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:UpdateOrderStatusRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string code = 1;</code>
   * @return The code.
   */
  java.lang.String getCode();
  /**
   * <code>string code = 1;</code>
   * @return The bytes for code.
   */
  com.google.protobuf.ByteString
      getCodeBytes();

  /**
   * <code>.OrderStatus status = 2;</code>
   * @return The enum numeric value on the wire for status.
   */
  int getStatusValue();
  /**
   * <code>.OrderStatus status = 2;</code>
   * @return The status.
   */
  it.millsoft.orders.api.interfaces.grpc.model.OrderStatus getStatus();
}
