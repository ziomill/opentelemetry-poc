package it.millsoft.orders.api.exceptions;

import it.millsoft.orders.api.dto.OrdersApiErrorEnum;

import java.util.UUID;

public class ProductsApiClientException extends OrdersApiException {

    public ProductsApiClientException(String message,
                                      UUID errorUUID) {
        super(message,
              OrdersApiErrorEnum.PRODUCTS_API_CLIENT_FAILURE,
              errorUUID);
    }

}
