package it.millsoft.orders.api.interfaces.grpc.model;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 * <pre>
 * Users Service
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.51.0)",
    comments = "Source: orders_api_grpc_server_interfaces.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class OrdersGrpcApiGrpc {

  private OrdersGrpcApiGrpc() {}

  public static final String SERVICE_NAME = "OrdersGrpcApi";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest,
      it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse> getGetOrdersOfUserMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getOrdersOfUser",
      requestType = it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest.class,
      responseType = it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest,
      it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse> getGetOrdersOfUserMethod() {
    io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest, it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse> getGetOrdersOfUserMethod;
    if ((getGetOrdersOfUserMethod = OrdersGrpcApiGrpc.getGetOrdersOfUserMethod) == null) {
      synchronized (OrdersGrpcApiGrpc.class) {
        if ((getGetOrdersOfUserMethod = OrdersGrpcApiGrpc.getGetOrdersOfUserMethod) == null) {
          OrdersGrpcApiGrpc.getGetOrdersOfUserMethod = getGetOrdersOfUserMethod =
              io.grpc.MethodDescriptor.<it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest, it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getOrdersOfUser"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse.getDefaultInstance()))
              .setSchemaDescriptor(new OrdersGrpcApiMethodDescriptorSupplier("getOrdersOfUser"))
              .build();
        }
      }
    }
    return getGetOrdersOfUserMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest,
      it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse> getGetOrderMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getOrder",
      requestType = it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest.class,
      responseType = it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest,
      it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse> getGetOrderMethod() {
    io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest, it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse> getGetOrderMethod;
    if ((getGetOrderMethod = OrdersGrpcApiGrpc.getGetOrderMethod) == null) {
      synchronized (OrdersGrpcApiGrpc.class) {
        if ((getGetOrderMethod = OrdersGrpcApiGrpc.getGetOrderMethod) == null) {
          OrdersGrpcApiGrpc.getGetOrderMethod = getGetOrderMethod =
              io.grpc.MethodDescriptor.<it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest, it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getOrder"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse.getDefaultInstance()))
              .setSchemaDescriptor(new OrdersGrpcApiMethodDescriptorSupplier("getOrder"))
              .build();
        }
      }
    }
    return getGetOrderMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest,
      it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse> getAddOrderForUserMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "addOrderForUser",
      requestType = it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest.class,
      responseType = it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest,
      it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse> getAddOrderForUserMethod() {
    io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest, it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse> getAddOrderForUserMethod;
    if ((getAddOrderForUserMethod = OrdersGrpcApiGrpc.getAddOrderForUserMethod) == null) {
      synchronized (OrdersGrpcApiGrpc.class) {
        if ((getAddOrderForUserMethod = OrdersGrpcApiGrpc.getAddOrderForUserMethod) == null) {
          OrdersGrpcApiGrpc.getAddOrderForUserMethod = getAddOrderForUserMethod =
              io.grpc.MethodDescriptor.<it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest, it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "addOrderForUser"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse.getDefaultInstance()))
              .setSchemaDescriptor(new OrdersGrpcApiMethodDescriptorSupplier("addOrderForUser"))
              .build();
        }
      }
    }
    return getAddOrderForUserMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest,
      it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse> getDeleteOrderMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "deleteOrder",
      requestType = it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest.class,
      responseType = it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest,
      it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse> getDeleteOrderMethod() {
    io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest, it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse> getDeleteOrderMethod;
    if ((getDeleteOrderMethod = OrdersGrpcApiGrpc.getDeleteOrderMethod) == null) {
      synchronized (OrdersGrpcApiGrpc.class) {
        if ((getDeleteOrderMethod = OrdersGrpcApiGrpc.getDeleteOrderMethod) == null) {
          OrdersGrpcApiGrpc.getDeleteOrderMethod = getDeleteOrderMethod =
              io.grpc.MethodDescriptor.<it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest, it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "deleteOrder"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse.getDefaultInstance()))
              .setSchemaDescriptor(new OrdersGrpcApiMethodDescriptorSupplier("deleteOrder"))
              .build();
        }
      }
    }
    return getDeleteOrderMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest,
      it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse> getUpdateOrderStatusMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "updateOrderStatus",
      requestType = it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest.class,
      responseType = it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest,
      it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse> getUpdateOrderStatusMethod() {
    io.grpc.MethodDescriptor<it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest, it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse> getUpdateOrderStatusMethod;
    if ((getUpdateOrderStatusMethod = OrdersGrpcApiGrpc.getUpdateOrderStatusMethod) == null) {
      synchronized (OrdersGrpcApiGrpc.class) {
        if ((getUpdateOrderStatusMethod = OrdersGrpcApiGrpc.getUpdateOrderStatusMethod) == null) {
          OrdersGrpcApiGrpc.getUpdateOrderStatusMethod = getUpdateOrderStatusMethod =
              io.grpc.MethodDescriptor.<it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest, it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "updateOrderStatus"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse.getDefaultInstance()))
              .setSchemaDescriptor(new OrdersGrpcApiMethodDescriptorSupplier("updateOrderStatus"))
              .build();
        }
      }
    }
    return getUpdateOrderStatusMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static OrdersGrpcApiStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<OrdersGrpcApiStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<OrdersGrpcApiStub>() {
        @java.lang.Override
        public OrdersGrpcApiStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new OrdersGrpcApiStub(channel, callOptions);
        }
      };
    return OrdersGrpcApiStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static OrdersGrpcApiBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<OrdersGrpcApiBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<OrdersGrpcApiBlockingStub>() {
        @java.lang.Override
        public OrdersGrpcApiBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new OrdersGrpcApiBlockingStub(channel, callOptions);
        }
      };
    return OrdersGrpcApiBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static OrdersGrpcApiFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<OrdersGrpcApiFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<OrdersGrpcApiFutureStub>() {
        @java.lang.Override
        public OrdersGrpcApiFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new OrdersGrpcApiFutureStub(channel, callOptions);
        }
      };
    return OrdersGrpcApiFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static abstract class OrdersGrpcApiImplBase implements io.grpc.BindableService {

    /**
     */
    public void getOrdersOfUser(it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetOrdersOfUserMethod(), responseObserver);
    }

    /**
     */
    public void getOrder(it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetOrderMethod(), responseObserver);
    }

    /**
     */
    public void addOrderForUser(it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getAddOrderForUserMethod(), responseObserver);
    }

    /**
     */
    public void deleteOrder(it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getDeleteOrderMethod(), responseObserver);
    }

    /**
     */
    public void updateOrderStatus(it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getUpdateOrderStatusMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetOrdersOfUserMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest,
                it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse>(
                  this, METHODID_GET_ORDERS_OF_USER)))
          .addMethod(
            getGetOrderMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest,
                it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse>(
                  this, METHODID_GET_ORDER)))
          .addMethod(
            getAddOrderForUserMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest,
                it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse>(
                  this, METHODID_ADD_ORDER_FOR_USER)))
          .addMethod(
            getDeleteOrderMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest,
                it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse>(
                  this, METHODID_DELETE_ORDER)))
          .addMethod(
            getUpdateOrderStatusMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest,
                it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse>(
                  this, METHODID_UPDATE_ORDER_STATUS)))
          .build();
    }
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static final class OrdersGrpcApiStub extends io.grpc.stub.AbstractAsyncStub<OrdersGrpcApiStub> {
    private OrdersGrpcApiStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OrdersGrpcApiStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new OrdersGrpcApiStub(channel, callOptions);
    }

    /**
     */
    public void getOrdersOfUser(it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetOrdersOfUserMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getOrder(it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetOrderMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void addOrderForUser(it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getAddOrderForUserMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void deleteOrder(it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getDeleteOrderMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateOrderStatus(it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getUpdateOrderStatusMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static final class OrdersGrpcApiBlockingStub extends io.grpc.stub.AbstractBlockingStub<OrdersGrpcApiBlockingStub> {
    private OrdersGrpcApiBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OrdersGrpcApiBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new OrdersGrpcApiBlockingStub(channel, callOptions);
    }

    /**
     */
    public it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse getOrdersOfUser(it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetOrdersOfUserMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse getOrder(it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetOrderMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse addOrderForUser(it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getAddOrderForUserMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse deleteOrder(it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getDeleteOrderMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse updateOrderStatus(it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getUpdateOrderStatusMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static final class OrdersGrpcApiFutureStub extends io.grpc.stub.AbstractFutureStub<OrdersGrpcApiFutureStub> {
    private OrdersGrpcApiFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected OrdersGrpcApiFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new OrdersGrpcApiFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse> getOrdersOfUser(
        it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetOrdersOfUserMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse> getOrder(
        it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetOrderMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse> addOrderForUser(
        it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getAddOrderForUserMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse> deleteOrder(
        it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getDeleteOrderMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse> updateOrderStatus(
        it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getUpdateOrderStatusMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_ORDERS_OF_USER = 0;
  private static final int METHODID_GET_ORDER = 1;
  private static final int METHODID_ADD_ORDER_FOR_USER = 2;
  private static final int METHODID_DELETE_ORDER = 3;
  private static final int METHODID_UPDATE_ORDER_STATUS = 4;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final OrdersGrpcApiImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(OrdersGrpcApiImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ORDERS_OF_USER:
          serviceImpl.getOrdersOfUser((it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.GetOrdersOfUserResponse>) responseObserver);
          break;
        case METHODID_GET_ORDER:
          serviceImpl.getOrder((it.millsoft.orders.api.interfaces.grpc.model.GetOrderRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.GetOrderResponse>) responseObserver);
          break;
        case METHODID_ADD_ORDER_FOR_USER:
          serviceImpl.addOrderForUser((it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.AddOrderForUserResponse>) responseObserver);
          break;
        case METHODID_DELETE_ORDER:
          serviceImpl.deleteOrder((it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.DeleteOrderResponse>) responseObserver);
          break;
        case METHODID_UPDATE_ORDER_STATUS:
          serviceImpl.updateOrderStatus((it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.orders.api.interfaces.grpc.model.UpdateOrderStatusResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class OrdersGrpcApiBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    OrdersGrpcApiBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return it.millsoft.orders.api.interfaces.grpc.model.OrdersApiGrpcServerInterfaces.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("OrdersGrpcApi");
    }
  }

  private static final class OrdersGrpcApiFileDescriptorSupplier
      extends OrdersGrpcApiBaseDescriptorSupplier {
    OrdersGrpcApiFileDescriptorSupplier() {}
  }

  private static final class OrdersGrpcApiMethodDescriptorSupplier
      extends OrdersGrpcApiBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    OrdersGrpcApiMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (OrdersGrpcApiGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new OrdersGrpcApiFileDescriptorSupplier())
              .addMethod(getGetOrdersOfUserMethod())
              .addMethod(getGetOrderMethod())
              .addMethod(getAddOrderForUserMethod())
              .addMethod(getDeleteOrderMethod())
              .addMethod(getUpdateOrderStatusMethod())
              .build();
        }
      }
    }
    return result;
  }
}
