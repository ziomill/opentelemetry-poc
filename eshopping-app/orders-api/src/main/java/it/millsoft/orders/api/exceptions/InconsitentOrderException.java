package it.millsoft.orders.api.exceptions;

import it.millsoft.orders.api.dto.OrdersApiErrorEnum;

import java.util.UUID;

public class InconsitentOrderException extends OrdersApiException {

    public InconsitentOrderException(String message, UUID errorUUID) {
        super(message,
                OrdersApiErrorEnum.INCONSISTENT_ORDER,
                errorUUID);
    }

}
