// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: orders_api_grpc_server_interfaces.proto

package it.millsoft.orders.api.interfaces.grpc.model;

public interface ProductItemOrBuilder extends
    // @@protoc_insertion_point(interface_extends:ProductItem)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string code = 1;</code>
   * @return The code.
   */
  java.lang.String getCode();
  /**
   * <code>string code = 1;</code>
   * @return The bytes for code.
   */
  com.google.protobuf.ByteString
      getCodeBytes();

  /**
   * <code>optional string description = 2;</code>
   * @return Whether the description field is set.
   */
  boolean hasDescription();
  /**
   * <code>optional string description = 2;</code>
   * @return The description.
   */
  java.lang.String getDescription();
  /**
   * <code>optional string description = 2;</code>
   * @return The bytes for description.
   */
  com.google.protobuf.ByteString
      getDescriptionBytes();

  /**
   * <code>optional string category = 3;</code>
   * @return Whether the category field is set.
   */
  boolean hasCategory();
  /**
   * <code>optional string category = 3;</code>
   * @return The category.
   */
  java.lang.String getCategory();
  /**
   * <code>optional string category = 3;</code>
   * @return The bytes for category.
   */
  com.google.protobuf.ByteString
      getCategoryBytes();

  /**
   * <code>optional double price = 4;</code>
   * @return Whether the price field is set.
   */
  boolean hasPrice();
  /**
   * <code>optional double price = 4;</code>
   * @return The price.
   */
  double getPrice();

  /**
   * <code>int32 requestedStock = 5;</code>
   * @return The requestedStock.
   */
  int getRequestedStock();
}
