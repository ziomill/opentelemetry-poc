openapi: 3.0.3
info:
  title: Orders-API OpenAPI definition
  version: v0
servers:
  - url: http://localhost:12000/orders-api
    description: Generated server url
tags:
  - name: OrdersRest
    description: The orders-api REST resources
paths:
  /order/user/{email}:
    post:
      tags:
        - OrdersRest
      summary: Add a new Order for User
      operationId: addOrderForUser
      parameters:
        - name: email
          in: path
          required: true
          schema:
            type: string
            format: email
      requestBody:
        content:
          application/json:
            schema:
              type: object
              additionalProperties:
                type: integer
                format: int32
        required: true
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Order'
        '502':
          description: EXTERNAL API CLIENT ERROR
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrdersApiErrorResponse'
        '5XX':
          description: INTERNAL SERVER ERROR
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrdersApiErrorResponse'
  /order/{code}/status:
    patch:
      tags:
        - OrdersRest
      summary: Update an Order's Status
      operationId: updateOrderStatus
      parameters:
        - name: code
          in: path
          required: true
          schema:
            type: string
        - name: status
          in: query
          required: true
          schema:
            $ref: '#/components/schemas/OrderStatus'
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: string
        '404':
          description: ORDER NOT FOUND
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrdersApiErrorResponse'
        '409':
          description: ORDER STATUS NOT UPDATABLE
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrdersApiErrorResponse'
        '5XX':
          description: INTERNAL SERVER ERROR
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrdersApiErrorResponse'
  /orders/user/{email}:
    get:
      tags:
        - OrdersRest
      summary: Search user's orders
      operationId: getUserOrders
      parameters:
        - name: email
          in: path
          required: true
          schema:
            type: string
            format: email
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Order'
        '5XX':
          description: INTERNAL SERVER ERROR
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrdersApiErrorResponse'
  /order/{code}:
    get:
      tags:
        - OrdersRest
      summary: Search an order by the code
      operationId: getOrder
      parameters:
        - name: code
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Order'
        '404':
          description: ORDER NOT FOUND
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrdersApiErrorResponse'
        '5XX':
          description: INTERNAL SERVER ERROR
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrdersApiErrorResponse'
    delete:
      tags:
        - OrdersRest
      summary: Delete an Order
      operationId: deleteOrder
      parameters:
        - name: code
          in: path
          required: true
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: string
        '404':
          description: ORDER NOT FOUND
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrdersApiErrorResponse'
        '5XX':
          description: INTERNAL SERVER ERROR
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/OrdersApiErrorResponse'
components:
  schemas:
    Order:
      type: object
      properties:
        code:
          type: string
        submittedOnUTC:
          type: string
          format: date-time
        status:
          $ref: '#/components/schemas/OrderStatus'
        productItems:
          minItems: 1
          uniqueItems: true
          type: array
          items:
            $ref: '#/components/schemas/ProductItem'
        userEmail:
          type: string
      required:
        - code
        - submittedOnUTC
        - status
        - userEmail
        - products
    ProductItem:
      type: object
      properties:
        code:
          type: string
        description:
          type: string
        category:
          type: string
        price:
          type: number
          format: double
        requestedStock:
          minimum: 1
          type: integer
          format: int32
      required:
        - code
        - description
        - category
        - price
        - requestedStock
    OrderStatus:
      type: string
      enum:
        - CREATED
        - PROCESSING
        - SHIPPED
        - DELIVERED
    OrdersApiErrorResponse:
      type: object
      properties:
        type:
          type: string
        instance:
          type: string
        title:
          type: string
        details:
          type: string
        applicativeError:
          type: string
          enum:
            - "ORDER_NOT_FOUND"
            - "CLIENTS_INTEGRATION_STRATEGY_NOT_SUPPORTED_FAILURE"
            - "INCONSISTENT_ORDER"
            - "ORDER_STATUS_NOT_UPDATABLE"
            - "PRODUCTS_API_CLIENT_FAILURE"
            - "USERS_API_CLIENT_FAILURE"
            - "GENERIC_FAILURE"
        status:
          type: integer
      required:
        - type
        - instance
        - title
        - details
        - applicativeError
        - status