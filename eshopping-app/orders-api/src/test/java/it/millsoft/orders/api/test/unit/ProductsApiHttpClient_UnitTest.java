package it.millsoft.orders.api.test.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.millsoft.orders.api.dto.OrdersApiErrorEnum;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.exceptions.ProductsApiClientException;
import it.millsoft.orders.api.ext.ProductsApiClient;
import it.millsoft.orders.api.ext.http.products.api.model.Product;
import it.millsoft.orders.api.ext.http.products.api.model.ProductsApiErrorResponse;
import it.millsoft.orders.api.test.config.ExtHttpClientsUnitTestConfig;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@SpringJUnitConfig(ExtHttpClientsUnitTestConfig.class)
public class ProductsApiHttpClient_UnitTest {

    public static MockWebServer mockBackEnd;

    @BeforeAll
    static void setUp() throws IOException {
        mockBackEnd = new MockWebServer();
        mockBackEnd.start();
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockBackEnd.shutdown();
    }

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        String uri = "http://" + mockBackEnd.getHostName() + ":" + mockBackEnd.getPort() + "/products-api";
        registry.add("rest.client.products-api.address", () -> uri);
    }

    @Autowired
    private ProductsApiClient productsApiClient;

    @Test
    public void getProductsByCodes_RequestIsSuccessfull_ShouldReturnRequestedProducts() throws Exception {

        // Given
        Product mProduct1 = new Product("PRD001", "Lenovo X1 Carbon", "PC & Tablet", 1500.00, 1);
        Product mProduct2 = new Product("PRD002", "Monitor G100 Samsung", "Electronics", 160.00, 1);
        Set<Product> mockProducts = Set.of(mProduct1, mProduct2);

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonResponse = objectMapper.writeValueAsString(mockProducts);

        MockResponse mockResponse = new MockResponse().setBody(jsonResponse)
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setResponseCode(HttpStatus.OK.value());
        mockBackEnd.enqueue(mockResponse);

        // When
        Set<String> productsCode = Set.of(mProduct1.getCode(), mProduct2.getCode());
        Set<ProductItemDTO> products = productsApiClient.getProductsByCode(productsCode);

        // then
        Assertions.assertAll(
                () -> Assertions.assertEquals(2, products.size()),
                () -> {
                    Optional<ProductItemDTO> optActualProduct1 = products.stream().filter(product -> product.getCode().equals(mProduct1.getCode())).findFirst();
                    Assertions.assertTrue(optActualProduct1.isPresent());
                    ProductItemDTO actualProduct1 = optActualProduct1.get();
                    Assertions.assertEquals(mProduct1.getCategory(), actualProduct1.getCategory());
                    Assertions.assertEquals(mProduct1.getPrice(), actualProduct1.getPrice());
                    Assertions.assertEquals(mProduct1.getDescription(), actualProduct1.getDescription());

                    Optional<ProductItemDTO> optActualProduct2 = products.stream().filter(product -> product.getCode().equals(mProduct2.getCode())).findFirst();
                    Assertions.assertTrue(optActualProduct2.isPresent());
                    ProductItemDTO actualProduct2 = optActualProduct2.get();
                    Assertions.assertEquals(mProduct2.getCategory(), actualProduct2.getCategory());
                    Assertions.assertEquals(mProduct2.getPrice(), actualProduct2.getPrice());
                    Assertions.assertEquals(mProduct2.getDescription(), actualProduct2.getDescription());
                }
        );
    }

    @Test
    public void getProductsByCodes_RequestUnsuccessfull_ShouldThrowAnProductsApiClientException() throws Exception {
        // Given
        ProductsApiErrorResponse mError = new ProductsApiErrorResponse("An error",
                UUID.randomUUID().toString(),
                "error-title",
                "error-details",
                ProductsApiErrorResponse.ApplicativeErrorEnum.GENERIC_FAILURE,
                HttpStatus.BAD_REQUEST.value());

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonResponse = objectMapper.writeValueAsString(mError);

        MockResponse mockResponse = new MockResponse()
                .setBody(jsonResponse)
                .setResponseCode(HttpStatus.BAD_REQUEST.value());
        mockBackEnd.enqueue(mockResponse);

        // Then
        ProductsApiClientException ex = Assertions.assertThrows(
                ProductsApiClientException.class,
                () -> {
                    // When
                    Set<String> productsCode = Set.of("PRD000001", "PRD000002");
                    productsApiClient.getProductsByCode(productsCode);
                },
                "It should have thrown the " + ProductsApiClientException.class.getName()
        );
        Assertions.assertAll(
                () -> Assertions.assertNotNull(ex.getErrorUUID()),
                () -> Assertions.assertNotNull(ex.getMessage()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.PRODUCTS_API_CLIENT_FAILURE, ex.getError())
        );
    }

    @Test
    public void updateProductsStock_RequestIsSuccessfull_ShouldReturnTheUpdatedStocks() throws Exception {
        // Given
        Map<String, Integer> mUpdatedStock = Map.of("PRD000001", 10,
                "PRD000002", 3);

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonResponse = objectMapper.writeValueAsString(mUpdatedStock);

        MockResponse mockResponse = new MockResponse()
                .setBody(jsonResponse)
                .addHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .setResponseCode(HttpStatus.OK.value());
        mockBackEnd.enqueue(mockResponse);

        // When
        Map<String, Integer> requestedStock = Map.of("PRD000001", 1,
                                                     "PRD000002", 2);
        Map<String, Integer> updatedStock = productsApiClient.updateProductsStock(requestedStock);

        // then
        Assertions.assertEquals(updatedStock.size(), mUpdatedStock.size());
    }

    @Test
    public void updateProductsStock_RequestIsUsuccessfull_ShouldThrowAnProductsApiClientException() throws Exception {
        // Given
        ProductsApiErrorResponse mError = new ProductsApiErrorResponse("An error",
                UUID.randomUUID().toString(),
                "error-title",
                "error-details",
                ProductsApiErrorResponse.ApplicativeErrorEnum.GENERIC_FAILURE,
                HttpStatus.BAD_REQUEST.value());

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonResponse = objectMapper.writeValueAsString(mError);

        MockResponse mockResponse = new MockResponse()
                .setBody(jsonResponse)
                .setResponseCode(HttpStatus.BAD_REQUEST.value());
        mockBackEnd.enqueue(mockResponse);

        // Then
        ProductsApiClientException ex = Assertions.assertThrows(
                ProductsApiClientException.class,
                () -> {
                    // When
                    Map<String, Integer> requestedStock = Map.of("PRD000001", 1,
                                                                 "PRD000002", 2);
                    productsApiClient.updateProductsStock(requestedStock);
                },
                "It should have thrown the " + ProductsApiClientException.class.getName()
        );
        Assertions.assertAll(
                () -> Assertions.assertNotNull(ex.getErrorUUID()),
                () -> Assertions.assertNotNull(ex.getMessage()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.PRODUCTS_API_CLIENT_FAILURE, ex.getError())
        );
    }

}


