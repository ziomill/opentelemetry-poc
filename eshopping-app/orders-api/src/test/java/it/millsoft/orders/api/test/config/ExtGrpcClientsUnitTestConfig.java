package it.millsoft.orders.api.test.config;

import it.millsoft.orders.api.ext.ProductsApiClient;
import it.millsoft.orders.api.ext.UsersApiClient;
import it.millsoft.orders.api.ext.grpc.ProductsApiGrpcClient;
import it.millsoft.orders.api.ext.grpc.UsersApiGrpcClient;
import it.millsoft.orders.api.mappers.ProductsApiClientMapper;
import it.millsoft.orders.api.mappers.ProductsApiClientMapperImpl;
import net.devh.boot.grpc.client.autoconfigure.GrpcClientAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcAdviceAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.context.annotation.Bean;

// Load Only Grpc Client AutoConfiguration in order to inject with @GrpcClient
@ImportAutoConfiguration(value = {
        GrpcClientAutoConfiguration.class,
        GrpcAdviceAutoConfiguration.class})
public class ExtGrpcClientsUnitTestConfig {

    @Bean
    public UsersApiClient createUsersApiGrpcClient() {
        return new UsersApiGrpcClient();
    }

    @Bean
    public ProductsApiClient createProductsApiGrpcClient() {
        return new ProductsApiGrpcClient();
    }

    @Bean
    public ProductsApiClientMapper createProductsApiClientGrpcMapper() {
        return new ProductsApiClientMapperImpl();
    }


}
