package it.millsoft.orders.api.test.unit;

import io.grpc.*;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.orders.api.dto.OrdersApiErrorEnum;
import it.millsoft.orders.api.exceptions.UsersApiClientException;
import it.millsoft.orders.api.ext.UsersApiClient;
import it.millsoft.orders.api.ext.grpc.users.api.model.CheckUserExistenceRequest;
import it.millsoft.orders.api.ext.grpc.users.api.model.CheckUserExistenceResponse;
import it.millsoft.orders.api.ext.grpc.users.api.model.UsersApiErrorResponse;
import it.millsoft.orders.api.ext.grpc.users.api.model.UsersGrpcApiGrpc;
import it.millsoft.orders.api.test.config.ExtGrpcClientsUnitTestConfig;
import org.grpcmock.GrpcMock;
import org.grpcmock.junit5.GrpcMockExtension;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Optional;

import static org.grpcmock.GrpcMock.*;


@SpringJUnitConfig(ExtGrpcClientsUnitTestConfig.class)
@ExtendWith(GrpcMockExtension.class)
@TestPropertySource(
        properties = {
                "grpc.client.users-api.negotiation-type=plaintext"
        }
)
public class UsersApiGrpcClient_UnitTest {

    private ManagedChannel channel;

    @BeforeEach
    void setupChannel() {
        channel = ManagedChannelBuilder.forAddress("localhost", GrpcMock.getGlobalPort())
                .usePlaintext()
                .build();
    }

    @AfterEach
    void shutdownChannel() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        String uri = "static://localhost:" + GrpcMock.getGlobalPort();
        registry.add("grpc.client.users-api.address", () -> uri);
    }

    @Autowired
    private UsersApiClient usersApiClient;

    @Test
    public void checkUserExistence_RequestSuccessfull_ShouldNotThrowAnException() {
        // Given
        String existingUserEmail = "jhon.doe@gmail.com";
        CheckUserExistenceResponse mResponse = CheckUserExistenceResponse.newBuilder()
                .setExists(true)
                .build();
        stubFor(unaryMethod(UsersGrpcApiGrpc.getCheckUserExistenceMethod())
                .willReturn(mResponse));

        // Then
        Assertions.assertDoesNotThrow(
                // When
                () -> {
                    CheckUserExistenceRequest.newBuilder()
                            .setEmail(existingUserEmail)
                            .build();
                    usersApiClient.checkUserExistence(existingUserEmail);
                }
        );
    }

    @Test
    public void checkUserExistence_RequestUnsuccessfull_ShouldThrowAnException() {
        // Given
       UsersApiErrorResponse.ApplicativeError applicativeError = UsersApiErrorResponse.ApplicativeError.USER_NOT_FOUND;
        UsersApiErrorResponse errorResponse = UsersApiErrorResponse.newBuilder()
                .setType("error-type")
                .setInstance("error-instance")
                .setTitle("error-title")
                .setDetails("error-details")
                .setApplicativeError(applicativeError)
                .setStatus(Status.NOT_FOUND.getCode().value())
                .build();
        Metadata.Key<UsersApiErrorResponse> shopErrorResponseKey = ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance());
        Metadata metadata = new Metadata();
        metadata.put(shopErrorResponseKey, errorResponse);
        Status status = Status.NOT_FOUND;
        StatusException statusException = status.asException(metadata);

        String notExistingUserEmail = "jhon.doe@gmail.com";
        stubFor(unaryMethod(UsersGrpcApiGrpc.getCheckUserExistenceMethod())
                .willReturn(exception(statusException)));

        // Then
        UsersApiClientException ex = Assertions.assertThrows(
                UsersApiClientException.class,
                () -> usersApiClient.checkUserExistence(notExistingUserEmail), // When
                "It should have thrown the " + UsersApiClientException.class.getName()
        );
        Assertions.assertAll(
                () -> Assertions.assertNotNull(ex.getErrorUUID()),
                () -> Assertions.assertNotNull(ex.getMessage()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.USERS_API_CLIENT_FAILURE, ex.getError())
        );
    }

}
