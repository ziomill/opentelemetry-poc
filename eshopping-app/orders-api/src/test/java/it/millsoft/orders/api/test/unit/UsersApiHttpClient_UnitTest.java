package it.millsoft.orders.api.test.unit;

import it.millsoft.orders.api.dto.OrdersApiErrorEnum;
import it.millsoft.orders.api.exceptions.UsersApiClientException;
import it.millsoft.orders.api.ext.UsersApiClient;
import it.millsoft.orders.api.test.config.ExtHttpClientsUnitTestConfig;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.io.IOException;


@SpringJUnitConfig(ExtHttpClientsUnitTestConfig.class)
public class UsersApiHttpClient_UnitTest {

    public static MockWebServer mockBackEnd;

    @BeforeAll
    static void setUp() throws IOException {
        mockBackEnd = new MockWebServer();
        mockBackEnd.start();
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockBackEnd.shutdown();
    }

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        String uri = "http://" + mockBackEnd.getHostName() + ":" + mockBackEnd.getPort() + "/users-api";
        registry.add("rest.client.users-api.address", () -> uri);
    }

    @Autowired
    private UsersApiClient usersApiClient;

    @Test
    public void checkUserExistence_UserExists_ShouldNotThrowAnException() {
        // Given
        String existingUserEmail = "jhon.doe@gmail.com";
        MockResponse response = new MockResponse().setResponseCode(HttpStatus.NO_CONTENT.value());
        mockBackEnd.enqueue(response);

        // Then
        Assertions.assertDoesNotThrow(
                // When
                () -> usersApiClient.checkUserExistence(existingUserEmail)
        );
    }

    @Test
    public void checkUserExistence_UserDoesntExists_ShouldThrowAnException() {
        // Given
        String existingUserEmail = "jhon.doe.junior@gmail.com";
        MockResponse response = new MockResponse().setResponseCode(HttpStatus.NOT_FOUND.value());
        mockBackEnd.enqueue(response);

        // Then
        UsersApiClientException ex = Assertions.assertThrows(
                UsersApiClientException.class,
                () -> usersApiClient.checkUserExistence(existingUserEmail), // When
                "It should have thrown the " + UsersApiClientException.class.getName()
        );
        Assertions.assertAll(
                () -> Assertions.assertNotNull(ex.getErrorUUID()),
                () -> Assertions.assertNotNull(ex.getMessage()),
                () ->  Assertions.assertEquals(OrdersApiErrorEnum.USERS_API_CLIENT_FAILURE,ex.getError())
        );
    }

}
