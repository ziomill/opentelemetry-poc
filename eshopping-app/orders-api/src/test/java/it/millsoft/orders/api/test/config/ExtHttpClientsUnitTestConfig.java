package it.millsoft.orders.api.test.config;

import it.millsoft.orders.api.ext.ProductsApiClient;
import it.millsoft.orders.api.ext.UsersApiClient;
import it.millsoft.orders.api.ext.http.ProductsApiHttpClient;
import it.millsoft.orders.api.ext.http.UsersApiHttpClient;
import it.millsoft.orders.api.mappers.ProductsApiClientMapper;
import it.millsoft.orders.api.mappers.ProductsApiClientMapperImpl;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class ExtHttpClientsUnitTestConfig {

    @Bean
    public UsersApiClient createUsersApiHttpClient() {
        return new UsersApiHttpClient();
    }

    @Bean
    public ProductsApiClient createProductsApiHttpClient() {
        return new ProductsApiHttpClient();
    }

    @Bean
    public ProductsApiClientMapper createProductsApiClientGrpcMapper() {
        return new ProductsApiClientMapperImpl();
    }
}
