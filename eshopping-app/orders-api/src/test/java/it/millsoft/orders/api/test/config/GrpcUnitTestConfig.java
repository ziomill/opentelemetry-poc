package it.millsoft.orders.api.test.config;

import it.millsoft.orders.api.mappers.OrderGrpcToFromDtoMapper;
import it.millsoft.orders.api.mappers.OrderGrpcToFromDtoMapperImpl;
import it.millsoft.orders.api.mappers.ProductItemGrpcToFromDtoMapper;
import it.millsoft.orders.api.mappers.ProductItemGrpcToFromDtoMapperImpl;
import it.millsoft.orders.api.interfaces.grpc.handlers.GrpcExceptionsHandler;
import it.millsoft.orders.api.interfaces.grpc.services.OrdersGrpcApiImpl;
import net.devh.boot.grpc.client.autoconfigure.GrpcClientAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcAdviceAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcServerAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcServerFactoryAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@TestConfiguration
@ImportAutoConfiguration(value = {GrpcServerAutoConfiguration.class,
                          GrpcServerFactoryAutoConfiguration.class,
                          GrpcClientAutoConfiguration.class,
                          GrpcAdviceAutoConfiguration.class})
@Import(GrpcExceptionsHandler.class)
public class GrpcUnitTestConfig {

    @Bean
    public OrdersGrpcApiImpl createGrpcService() {
        return new OrdersGrpcApiImpl();
    }

    @Bean
    public OrderGrpcToFromDtoMapper createOrderGrpcToFromDtoMapper() {
        return new OrderGrpcToFromDtoMapperImpl();
    }
    @Bean
    public ProductItemGrpcToFromDtoMapper createProductItemGrpcToFromDtoMapper() {
        return new ProductItemGrpcToFromDtoMapperImpl();
    }

}
