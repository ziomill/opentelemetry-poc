package it.millsoft.orders.api.test.integration;

import it.millsoft.orders.api.dto.OrderStatus;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.exceptions.ProductsApiClientException;
import it.millsoft.orders.api.exceptions.UsersApiClientException;
import it.millsoft.orders.api.ext.ProductsApiClient;
import it.millsoft.orders.api.ext.UsersApiClient;
import it.millsoft.orders.api.interfaces.rest.model.Order;
import it.millsoft.orders.api.interfaces.rest.model.OrdersApiErrorResponse;
import it.millsoft.orders.api.interfaces.rest.model.ProductItem;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.*;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.net.URI;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static it.millsoft.orders.api.commons.Utils.toURI;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
                // Applicative config
                "USERS_API_CLIENT_INTGR_STRATEGY=GRPC",
                "USERS_API_GRPC_ENDPOINT=users-api:10001",
                "USERS_API_HTTP_ENDPOINT=http://localhost:10000/users-api",
                "PRODUCTS_API_CLIENT_INTGR_STRATEGY=GRPC",
                "PRODUCTS_API_GRPC_ENDPOINT=products-api:11001",
                "PRODUCTS_API_HTTP_ENDPOINT=http://localhost:11000/products-api",
                // OpenTelemetry config
                "OTEL_EXPORTER_OTLP_ENDPOINT=http://otel-collector:4317",
                "OTEL_SERVICE_NAME=orders-api",
                "OTEL_METRIC_EXPORT_INTERVAL=30000"
        })
@Testcontainers
public class OrdersRestApi_IntegrationTest {

    private static Logger LOGGER = LoggerFactory.getLogger(OrdersRestApi_IntegrationTest.class);

    @Container
    private static MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:5.0.6"));

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private MongoTemplate mongoTemplate;

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        Objects.requireNonNull(mongoDBContainer);
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    @MockBean
    private UsersApiClient usersApiClient;

    @MockBean
    private ProductsApiClient productsApiClient;

    private it.millsoft.orders.api.model.Order orderOfJhonDoe;
    private it.millsoft.orders.api.model.Order orderOfLukeGreen;
    private it.millsoft.orders.api.model.Order order1OfJackMarshall;
    private it.millsoft.orders.api.model.Order order2OfJackMarshall;
    private it.millsoft.orders.api.model.Order order3OfJackMarshall;
    private it.millsoft.orders.api.model.Order inconsitentOrderOfColinDuff;

    @BeforeEach
    public void setupDatabase() {
        // Mock products
        String productCode101 = "PRD000101";
        Integer productQuantity101 = 1;
        ProductItemDTO mProduct101 = new ProductItemDTO(productCode101,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                productQuantity101);

        String productCode102 = "PRD000102";
        Integer productQuantity102 = 2;
        ProductItemDTO mProduct102 = new ProductItemDTO(productCode102,
                "Logitech Optical Mouse",
                "Electronic",
                14.00,
                productQuantity102);

        String productCode103 = "PRD000103";
        Integer productQuantity103 = 1;
        ProductItemDTO mProduct103 = new ProductItemDTO(productCode103,
                "IPhone 11",
                "Electronic",
                1099.00,
                productQuantity103);

        String productCode104 = "PRD000104";
        Integer productQuantity104 = 3;
        ProductItemDTO mProduct104 = new ProductItemDTO(productCode104,
                "SDD 512GB Samsung",
                "Electronic",
                215.00,
                productQuantity104);

        String productCode105 = "PRD000105";
        Integer productQuantity105 = 2;
        ProductItemDTO mProduct105 = new ProductItemDTO(productCode105,
                "USB 3.0 Switch",
                "Electronic",
                8.99,
                productQuantity105);

        // Order of jhon.doe@gmail.com (1 Order)
        String jhonDoeEmail = "jhon.doe@gmail.com";
        orderOfJhonDoe = new it.millsoft.orders.api.model.Order();
        orderOfJhonDoe.setCode("ORD000001");
        orderOfJhonDoe.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        orderOfJhonDoe.setStatus(OrderStatus.CREATED);
        orderOfJhonDoe.setProductItems(Map.of(productCode101, productQuantity101));
        orderOfJhonDoe.setUserEmail(jhonDoeEmail);
        orderOfJhonDoe = mongoTemplate.save(orderOfJhonDoe);
        LOGGER.debug("Order {} saved with success", orderOfJhonDoe);
        // Mock PRODUCTS-API client for Product in the Order of0 'jhon.doe@gmail.com'
        when(productsApiClient.getProductsByCode(Set.of(productCode101))).thenReturn(Set.of(mProduct101));

        // Order of luke.green@gmail.com (1 Order)
        String lukeGreenEmail = "luke.green@gmail.com";
        orderOfLukeGreen = new it.millsoft.orders.api.model.Order();
        orderOfLukeGreen.setCode("ORD000002");
        orderOfLukeGreen.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        orderOfLukeGreen.setStatus(OrderStatus.SHIPPED);
        orderOfLukeGreen.setProductItems(Map.of(productCode101, productQuantity101,
                productCode102, productQuantity102));
        orderOfLukeGreen.setUserEmail(lukeGreenEmail);
        orderOfLukeGreen = mongoTemplate.save(orderOfLukeGreen);
        LOGGER.debug("Order {} saved with success", orderOfLukeGreen);
        // Mock PRODUCTS-API client for Product in the Order of 'luke.green@gmail.com'
        when(productsApiClient.getProductsByCode(Set.of(productCode101, productCode102))).thenReturn(Set.of(mProduct101, mProduct102));

        // Jack Marshall orders (3 orders)
        String jackMarshallEmail = "jack.marshall@aol.com";
        order1OfJackMarshall = new it.millsoft.orders.api.model.Order();
        order1OfJackMarshall.setCode("ORD000003");
        order1OfJackMarshall.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        order1OfJackMarshall.setStatus(OrderStatus.DELIVERED);
        order1OfJackMarshall.setProductItems(Map.of(productCode103, productQuantity103,
                productCode104, productQuantity104,
                productCode105, productQuantity105));
        order1OfJackMarshall.setUserEmail(jackMarshallEmail);
        order1OfJackMarshall = mongoTemplate.save(order1OfJackMarshall);
        LOGGER.debug("Order {} saved with success", order1OfJackMarshall);
        // Mock PRODUCTS-API client for Product in the Order 1 of 'jack.marshall@aol.com'
        when(productsApiClient.getProductsByCode(Set.of(productCode103, productCode104, productCode105))).thenReturn(Set.of(mProduct103, mProduct104, mProduct105));

        order2OfJackMarshall = new it.millsoft.orders.api.model.Order();
        order2OfJackMarshall.setCode("ORD000004");
        order2OfJackMarshall.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        order2OfJackMarshall.setStatus(OrderStatus.SHIPPED);
        order2OfJackMarshall.setProductItems(Map.of(productCode104, productQuantity104,
                productCode105, productQuantity105));
        order2OfJackMarshall.setUserEmail(jackMarshallEmail);
        order2OfJackMarshall = mongoTemplate.save(order2OfJackMarshall);
        LOGGER.debug("Order {} saved with success", order2OfJackMarshall);
        // Mock PRODUCTS-API client for Product in the Order 2 of 'jack.marshall@aol.com'
        when(productsApiClient.getProductsByCode(Set.of(productCode104, productCode105))).thenReturn(Set.of(mProduct104, mProduct105));

        order3OfJackMarshall = new it.millsoft.orders.api.model.Order();
        order3OfJackMarshall.setCode("ORD000005");
        order3OfJackMarshall.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        order3OfJackMarshall.setStatus(OrderStatus.CREATED);
        order3OfJackMarshall.setProductItems(Map.of(productCode102, productQuantity102));
        order3OfJackMarshall.setUserEmail(jackMarshallEmail);
        order3OfJackMarshall = mongoTemplate.save(order3OfJackMarshall);
        LOGGER.debug("Order {} saved with success", order3OfJackMarshall);
        // Mock PRODUCTS-API client for Product in the Order 3 of 'jack.marshall@aol.com'
        when(productsApiClient.getProductsByCode(Set.of(productCode102))).thenReturn(Set.of(mProduct102));

        // Inconsitent Order of Colin Duff
        String colinDuffEmail = "colin.duff@aol.com";
        String inconsisentProductCode = "PRD000071";
        inconsitentOrderOfColinDuff = new it.millsoft.orders.api.model.Order();
        inconsitentOrderOfColinDuff.setCode("ORD000071");
        inconsitentOrderOfColinDuff.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        inconsitentOrderOfColinDuff.setStatus(OrderStatus.CREATED);
        inconsitentOrderOfColinDuff.setProductItems(Map.of(inconsisentProductCode, 10));
        inconsitentOrderOfColinDuff.setUserEmail(colinDuffEmail);
        inconsitentOrderOfColinDuff = mongoTemplate.save(inconsitentOrderOfColinDuff);
        LOGGER.debug("Order {} saved with success", inconsitentOrderOfColinDuff);
        // Mock PRODUCTS-API client for Product in the inconsitent Order of 'colin.duff@aol.com'
        when(productsApiClient.getProductsByCode(Set.of(inconsisentProductCode))).thenReturn(Set.of()); // the client doesn't return any product
    }

    @AfterEach
    void cleanupDatabase() {
        mongoTemplate.getDb().drop();
    }

    @Test
    public void getOrdersOfUser_UserDoesntHaveSubmittedAnyOrders_ShouldReturnAnEmptyListAndHttpCode200OK() {
        // Given: An user who doesn't have submitted any order
        String email = "rod.been@gmail.com";

        // When
        String uriTemplate = "/orders/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);
        ResponseEntity<Set> response = this.restTemplate.exchange(uri, HttpMethod.GET, requestEntity, Set.class);

        // Then
        Set ordersOfUser = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertTrue(ordersOfUser.isEmpty())
        );
    }

    @Test
    public void getOrdersOfUser_UserHasSubmittedSingleOrder_ShouldReturnAnOrderAndHttpCode200OK() {
        // Given: An user who has submitted a single order
        String email = orderOfJhonDoe.getUserEmail();

        // When
        String uriTemplate = "/orders/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);

        ResponseEntity<Set> response = this.restTemplate.exchange(uri, HttpMethod.GET, requestEntity, Set.class);

        // Then
        Set<Order> ordersOfUser = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(1,ordersOfUser.size())
        );
    }

    @Test
    public void getOrdersOfUser_UserHasSubmittedManyOrder_ShouldReturnThreeOrdersAndHttpCode200OK() {
        // Given: An user who has submitted multiple order
        String email = order1OfJackMarshall.getUserEmail();

        // When
        String uriTemplate = "/orders/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);
        ResponseEntity<Set> response = this.restTemplate.exchange(uri, HttpMethod.GET, requestEntity, Set.class);

        // Then
        Set<Order> ordersOfUser = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(3,ordersOfUser.size())
        );
    }

    @Test
    public void getOrdersOfUser_UserHasAnInconsistenOrder_ShouldReturnHttpCode500InternalServerError() {
        // Given: An user who has submitted an order which results inconsistent (The product in the order has not been found in the inventory)
        String email = inconsitentOrderOfColinDuff.getUserEmail();

        // When
        String uriTemplate = "/orders/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);
        ResponseEntity<OrdersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.GET, requestEntity, OrdersApiErrorResponse.class);

        // Then
        OrdersApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.INCONSISTENT_ORDER, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void getOrder_OrderExists_ShouldReturnTheOrderAndHttpCode200Ok() {
        // Given: an existent order
        String existentOrderCode = order1OfJackMarshall.getCode();

        // When
        String uriTemplate = "/order/{code}";
        Map<String, String> uriParams = Map.of("code",existentOrderCode);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);
        ResponseEntity<Order> response = this.restTemplate.exchange(uri, HttpMethod.GET, requestEntity, Order.class);

        // Then
        Order order = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                () -> Assertions.assertNotNull(order),
                () -> Assertions.assertEquals(order1OfJackMarshall.getCode(), order.getCode()),
                () -> Assertions.assertEquals(order1OfJackMarshall.getStatus().name(), order.getStatus().name()),
                () -> Assertions.assertEquals(order1OfJackMarshall.getUserEmail(), order.getUserEmail()),
                () -> {
                    OffsetDateTime expectedSubmittedOnUTC = order1OfJackMarshall.getSubmittedOnUTC().withNano(0);
                    OffsetDateTime actualSubmittedOnUTC = order.getSubmittedOnUTC().withNano(0);
                    Assertions.assertEquals(expectedSubmittedOnUTC, actualSubmittedOnUTC);
                },
                () -> {
                    Map<String,Integer> expectedProducts = order1OfJackMarshall.getProductItems();
                    Set<ProductItem> actualProducts = order.getProductItems();
                    Assertions.assertEquals(expectedProducts.size(), actualProducts.size());

                    Set<String> expectedProductsCode = order1OfJackMarshall.getProductItems().keySet().stream().collect(Collectors.toSet());
                    Set<String> actualProductsCode = order.getProductItems().stream().map(product -> product.getCode()).collect(Collectors.toSet());
                    Assertions.assertEquals(expectedProductsCode, actualProductsCode);
                }
        );
    }

    @Test
    public void getOrder_OrderExistsButIsInconsistent_ShouldReturnHttpCode500InternalServerError() {
        // Given: an existent but inconsistent order  (The product in the order has not been found in the inventory)
        String existentButInconsistentOrderCode = inconsitentOrderOfColinDuff.getCode();

        // When
        String uriTemplate = "/order/{code}";
        Map<String, String> uriParams = Map.of("code",existentButInconsistentOrderCode);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);
        ResponseEntity<OrdersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.GET, requestEntity, OrdersApiErrorResponse.class);

        // Then
        OrdersApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.INCONSISTENT_ORDER, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void getOrder_OrderDoesntExist_ShouldReturnHttpCode404NotFound() {
        // Given: an inexistent order
        String inexistentOrderCode = "ORDXY00004110";

        // When
        String uriTemplate = "/order/{code}";
        Map<String, String> uriParams = Map.of("code",inexistentOrderCode);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);
        ResponseEntity<OrdersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.GET, requestEntity, OrdersApiErrorResponse.class);

        // Then
        OrdersApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.ORDER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void addOrderForUser_ErrorOnUsersApiClient_ShouldReturnHttpCode504BadGateway() {
        // Given: An error from checkUserExistence of the users-api client
        String inexistentUserEmail = "olivier.black@google.com";
        Map<String, Integer> requestedStock = Map.of("PRD00001", 1,
                                                     "PRD00002", 3);

        UsersApiClientException userApiClientException = new UsersApiClientException("User doesn't exist", UUID.randomUUID());
        doThrow(userApiClientException).when(usersApiClient).checkUserExistence(inexistentUserEmail);

        // When
        String uriTemplate = "/order/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",inexistentUserEmail);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(requestedStock,headers);
        ResponseEntity<OrdersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.POST, requestEntity, OrdersApiErrorResponse.class);

        // Then
        OrdersApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.BAD_GATEWAY, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.USERS_API_CLIENT_FAILURE, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void addOrderForUser_ErrorOnProductsApiClient_ShouldReturnHttpCode504BadGateway() {
        // Given: An error from updateProductsStock of the products-api client
        String existentUserEmail = orderOfJhonDoe.getUserEmail();
        Map<String, Integer> requestedStock = Map.of("PRD00001", 1,
                                                     "PRD00002", 3);

        ProductsApiClientException productsApiClientException = new ProductsApiClientException("Can't update products stocks", UUID.randomUUID());
        doThrow(productsApiClientException).when(productsApiClient).updateProductsStock(requestedStock);

        // When
        String uriTemplate = "/order/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",existentUserEmail);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(requestedStock,headers);
        ResponseEntity<OrdersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.POST, requestEntity, OrdersApiErrorResponse.class);

        // Then
        OrdersApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.BAD_GATEWAY, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.PRODUCTS_API_CLIENT_FAILURE, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void addOrderForUser_AllPreconditionsSatisfiedAndTheOrderCanBeAdded_ShouldReturnTheOrderAndHttpCode201Created() {
        // Mock preparation for specific use-case
        String productCode124 = "PRD000124";
        Integer productQuantity124 = 3;
        ProductItemDTO mProduct124 = new ProductItemDTO(productCode124,
                "SDD 216GB WSD",
                "Electronic",
                160.0,
                productQuantity124);

        String productCode125 = "PRD000125";
        Integer productQuantity125 = 2;
        ProductItemDTO mProduct125 = new ProductItemDTO(productCode125,
                "USB Cable",
                "Electronic",
                4.99,
                productQuantity125);
        when(productsApiClient.getProductsByCode(Set.of(productCode124, productCode125))).thenReturn(Set.of(mProduct124, mProduct125));

        // Given: All conditions are satisfied --> User Exists, Products of order Exists, Products of orders are available in stock
        String userEmail = "jhon.doe@google.com";
        Map<String, Integer> requestedStock = Map.of(productCode124, 1,
                                                     productCode125, 2);
        when(productsApiClient.updateProductsStock(requestedStock)).thenReturn(Map.of(productCode124, 6,
                                                                                      productCode125, 7));

        // When
        String uriTemplate = "/order/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",userEmail);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(requestedStock,headers);
        ResponseEntity<Order> response = this.restTemplate.exchange(uri, HttpMethod.POST, requestEntity, Order.class);

        // Then
        Order order = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertNotNull(order),
                () -> Assertions.assertEquals(userEmail, order.getUserEmail()),
                () -> Assertions.assertNotNull(order.getSubmittedOnUTC()),
                () -> Assertions.assertEquals(OrderStatus.CREATED.name(), order.getStatus().name()),
                () -> {
                    // Check that the result contains exactly the two requested products
                    Assertions.assertEquals(requestedStock.size(), order.getProductItems().size());

                    // Check that the result contains the two requested products
                    Optional<ProductItem> optProduct124 = order.getProductItems().stream().filter(product -> product.getCode().equals(productCode124)).findFirst();
                    Assertions.assertTrue(optProduct124.isPresent());
                    Optional<ProductItem> optProduct125 = order.getProductItems().stream().filter(product -> product.getCode().equals(productCode125)).findFirst();
                    Assertions.assertTrue(optProduct125.isPresent());
                }
        );
    }

    @Test
    public void deleteOrder_OrderExists_ShouldReturnTheDeletedOrderIdAndHttpCode200OK() {
        // Given
        String existentOrderCode = orderOfJhonDoe.getCode();

        // When
        String uriTemplate = "/order/{code}";
        Map<String, String> uriParams = Map.of("code",existentOrderCode);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);
        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.DELETE, requestEntity, String.class);

        // Then
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(orderOfJhonDoe.getId(),response.getBody())
        );
    }

    @Test
    public void deleteOrder_OrderDoesntExist_ShouldReturnHttpCode404NotFound() {
        // Given
        String notExistentOrderCode = "ORDXYX0000231";

        // When
        String uriTemplate = "/order/{code}";
        Map<String, String> uriParams = Map.of("code",notExistentOrderCode);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);
        ResponseEntity<OrdersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.DELETE, requestEntity, OrdersApiErrorResponse.class);

        // Then
        OrdersApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.ORDER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateOrderStatus_OrderDoesntExist_ShouldReturnHttpCode404NotFound() {
        // Given
        String notExistentOrderCode = "ORDXYX0000231";

        // When
        String uriTemplate = "/order/{code}/status";
        Map<String, String> uriParams = Map.of("code",notExistentOrderCode);
        Map<String, Object[]> queryParams = Map.of("status",List.of(it.millsoft.orders.api.interfaces.rest.model.OrderStatus.SHIPPED.name()).toArray());
        URI uri = toURI(uriTemplate,uriParams,queryParams);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);
        ResponseEntity<OrdersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, OrdersApiErrorResponse.class);

        // Then
        OrdersApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.ORDER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateOrderStatus_WantUpdateWithTheSameStatus_ShouldReturnHttpCode400BadRequest() throws Exception {
        // Given
        String existentOrderCode = orderOfLukeGreen.getCode();

        // When
        String uriTemplate = "/order/{code}/status";
        Map<String, String> uriParams = Map.of("code",existentOrderCode);
        Map<String, Object[]> queryParams = Map.of("status",List.of(it.millsoft.orders.api.interfaces.rest.model.OrderStatus.SHIPPED.name()).toArray());
        URI uri = toURI(uriTemplate,uriParams,queryParams);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);
        ResponseEntity<OrdersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, OrdersApiErrorResponse.class);

        // Then
        OrdersApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.CONFLICT, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.ORDER_STATUS_NOT_UPDATABLE, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateOrderStatus_WantUpdateWithAValidStatus_ShouldReturnTheIdOfTheUpdatedOrderAndHttpCode200Ok() {
        // Given
        String existentOrderCode = orderOfLukeGreen.getCode();

        // When
        String uriTemplate = "/order/{code}/status";
        Map<String, String> uriParams = Map.of("code",existentOrderCode);
        Map<String, Object[]> queryParams = Map.of("status",List.of(it.millsoft.orders.api.interfaces.rest.model.OrderStatus.DELIVERED.name()).toArray());
        URI uri = toURI(uriTemplate,uriParams,queryParams);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);
        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, String.class);

        // Then
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(orderOfLukeGreen.getId(),response.getBody())
        );
    }


}