package it.millsoft.orders.api.test.integration;

import com.google.protobuf.Timestamp;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.orders.api.dto.OrderStatus;
import it.millsoft.orders.api.dto.OrdersApiErrorEnum;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.exceptions.ProductsApiClientException;
import it.millsoft.orders.api.exceptions.UsersApiClientException;
import it.millsoft.orders.api.ext.ProductsApiClient;
import it.millsoft.orders.api.ext.UsersApiClient;
import it.millsoft.orders.api.interfaces.grpc.model.*;
import it.millsoft.orders.api.test.config.GrpcIntegrationTestConfig;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SpringBootTest(
        properties = {
                // Start an internal GRPC server
                "grpc.server.inProcessName=test",
                "grpc.server.port=-1",
                "grpc.client.inProcess.address=in-process:test",
                // Applicative config
                "USERS_API_CLIENT_INTGR_STRATEGY=GRPC",
                "USERS_API_GRPC_ENDPOINT=users-api:10001",
                "USERS_API_HTTP_ENDPOINT=http://localhost:10000/users-api",
                "PRODUCTS_API_CLIENT_INTGR_STRATEGY=GRPC",
                "PRODUCTS_API_GRPC_ENDPOINT=products-api:11001",
                "PRODUCTS_API_HTTP_ENDPOINT=http://localhost:11000/products-api",
                // OpenTelemetry config
                "OTEL_EXPORTER_OTLP_ENDPOINT=http://otel-collector:4317",
                "OTEL_SERVICE_NAME=orders-api",
                "OTEL_METRIC_EXPORT_INTERVAL=30000"
        }
)
@SpringJUnitConfig(classes = GrpcIntegrationTestConfig.class)
@DirtiesContext // Properly shutdown the GRPC Server after each test
@Testcontainers
public class OrdersGrpcApi_IntegrationTest {

    private static Logger LOGGER = LoggerFactory.getLogger(OrdersGrpcApi_IntegrationTest.class);

    @Container
    private static MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:5.0.6"));

    @GrpcClient("inProcess")
    private OrdersGrpcApiGrpc.OrdersGrpcApiBlockingStub ordersGrpcApi;

    @Autowired
    private MongoTemplate mongoTemplate;

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        Objects.requireNonNull(mongoDBContainer);
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    @MockBean
    private UsersApiClient usersApiClient;

    @MockBean
    private ProductsApiClient productsApiClient;

    private it.millsoft.orders.api.model.Order orderOfJhonDoe;
    private it.millsoft.orders.api.model.Order orderOfLukeGreen;
    private it.millsoft.orders.api.model.Order order1OfJackMarshall;
    private it.millsoft.orders.api.model.Order order2OfJackMarshall;
    private it.millsoft.orders.api.model.Order order3OfJackMarshall;
    private it.millsoft.orders.api.model.Order inconsitentOrderOfColinDuff;

    @BeforeEach
    public void setupDatabase() {
        // Mock products
        String productCode101 = "PRD000101";
        Integer productQuantity101 = 1;
        ProductItemDTO mProduct101 = new ProductItemDTO(productCode101,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                productQuantity101);

        String productCode102 = "PRD000102";
        Integer productQuantity102 = 2;
        ProductItemDTO mProduct102 = new ProductItemDTO(productCode102,
                "Logitech Optical Mouse",
                "Electronic",
                14.00,
                productQuantity102);

        String productCode103 = "PRD000103";
        Integer productQuantity103 = 1;
        ProductItemDTO mProduct103 = new ProductItemDTO(productCode103,
                "IPhone 11",
                "Electronic",
                1099.00,
                productQuantity103);

        String productCode104 = "PRD000104";
        Integer productQuantity104 = 3;
        ProductItemDTO mProduct104 = new ProductItemDTO(productCode104,
                "SDD 512GB Samsung",
                "Electronic",
                215.00,
                productQuantity104);

        String productCode105 = "PRD000105";
        Integer productQuantity105 = 2;
        ProductItemDTO mProduct105 = new ProductItemDTO(productCode105,
                "USB 3.0 Switch",
                "Electronic",
                8.99,
                productQuantity105);

        // Order of jhon.doe@gmail.com (1 Order)
        String jhonDoeEmail = "jhon.doe@gmail.com";
        orderOfJhonDoe = new it.millsoft.orders.api.model.Order();
        orderOfJhonDoe.setCode("ORD000001");
        orderOfJhonDoe.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        orderOfJhonDoe.setStatus(OrderStatus.CREATED);
        orderOfJhonDoe.setProductItems(Map.of(productCode101, productQuantity101));
        orderOfJhonDoe.setUserEmail(jhonDoeEmail);
        orderOfJhonDoe = mongoTemplate.save(orderOfJhonDoe);
        LOGGER.debug("Order {} saved with success", orderOfJhonDoe);
        // Mock PRODUCTS-API client for Product in the Order of0 'jhon.doe@gmail.com'
        when(productsApiClient.getProductsByCode(Set.of(productCode101))).thenReturn(Set.of(mProduct101));

        // Order of luke.green@gmail.com (1 Order)
        String lukeGreenEmail = "luke.green@gmail.com";
        orderOfLukeGreen = new it.millsoft.orders.api.model.Order();
        orderOfLukeGreen.setCode("ORD000002");
        orderOfLukeGreen.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        orderOfLukeGreen.setStatus(OrderStatus.SHIPPED);
        orderOfLukeGreen.setProductItems(Map.of(productCode101, productQuantity101,
                productCode102, productQuantity102));
        orderOfLukeGreen.setUserEmail(lukeGreenEmail);
        orderOfLukeGreen = mongoTemplate.save(orderOfLukeGreen);
        LOGGER.debug("Order {} saved with success", orderOfLukeGreen);
        // Mock PRODUCTS-API client for Product in the Order of 'luke.green@gmail.com'
        when(productsApiClient.getProductsByCode(Set.of(productCode101, productCode102))).thenReturn(Set.of(mProduct101, mProduct102));

        // Jack Marshall orders (3 orders)
        String jackMarshallEmail = "jack.marshall@aol.com";
        order1OfJackMarshall = new it.millsoft.orders.api.model.Order();
        order1OfJackMarshall.setCode("ORD000003");
        order1OfJackMarshall.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        order1OfJackMarshall.setStatus(OrderStatus.DELIVERED);
        order1OfJackMarshall.setProductItems(Map.of(productCode103, productQuantity103,
                productCode104, productQuantity104,
                productCode105, productQuantity105));
        order1OfJackMarshall.setUserEmail(jackMarshallEmail);
        order1OfJackMarshall = mongoTemplate.save(order1OfJackMarshall);
        LOGGER.debug("Order {} saved with success", order1OfJackMarshall);
        // Mock PRODUCTS-API client for Product in the Order 1 of 'jack.marshall@aol.com'
        when(productsApiClient.getProductsByCode(Set.of(productCode103, productCode104, productCode105))).thenReturn(Set.of(mProduct103, mProduct104, mProduct105));

        order2OfJackMarshall = new it.millsoft.orders.api.model.Order();
        order2OfJackMarshall.setCode("ORD000004");
        order2OfJackMarshall.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        order2OfJackMarshall.setStatus(OrderStatus.SHIPPED);
        order2OfJackMarshall.setProductItems(Map.of(productCode104, productQuantity104,
                productCode105, productQuantity105));
        order2OfJackMarshall.setUserEmail(jackMarshallEmail);
        order2OfJackMarshall = mongoTemplate.save(order2OfJackMarshall);
        LOGGER.debug("Order {} saved with success", order2OfJackMarshall);
        // Mock PRODUCTS-API client for Product in the Order 2 of 'jack.marshall@aol.com'
        when(productsApiClient.getProductsByCode(Set.of(productCode104, productCode105))).thenReturn(Set.of(mProduct104, mProduct105));

        order3OfJackMarshall = new it.millsoft.orders.api.model.Order();
        order3OfJackMarshall.setCode("ORD000005");
        order3OfJackMarshall.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        order3OfJackMarshall.setStatus(OrderStatus.CREATED);
        order3OfJackMarshall.setProductItems(Map.of(productCode102, productQuantity102));
        order3OfJackMarshall.setUserEmail(jackMarshallEmail);
        order3OfJackMarshall = mongoTemplate.save(order3OfJackMarshall);
        LOGGER.debug("Order {} saved with success", order3OfJackMarshall);
        // Mock PRODUCTS-API client for Product in the Order 3 of 'jack.marshall@aol.com'
        when(productsApiClient.getProductsByCode(Set.of(productCode102))).thenReturn(Set.of(mProduct102));

        // Inconsitent Order of Colin Duff
        String colinDuffEmail = "colin.duff@aol.com";
        String inconsisentProductCode = "PRD000071";
        inconsitentOrderOfColinDuff = new it.millsoft.orders.api.model.Order();
        inconsitentOrderOfColinDuff.setCode("ORD000071");
        inconsitentOrderOfColinDuff.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        inconsitentOrderOfColinDuff.setStatus(OrderStatus.CREATED);
        inconsitentOrderOfColinDuff.setProductItems(Map.of(inconsisentProductCode, 10));
        inconsitentOrderOfColinDuff.setUserEmail(colinDuffEmail);
        inconsitentOrderOfColinDuff = mongoTemplate.save(inconsitentOrderOfColinDuff);
        LOGGER.debug("Order {} saved with success", inconsitentOrderOfColinDuff);
        // Mock PRODUCTS-API client for Product in the inconsitent Order of 'colin.duff@aol.com'
        when(productsApiClient.getProductsByCode(Set.of(inconsisentProductCode))).thenReturn(Set.of()); // the client doesn't return any product
    }

    @AfterEach
    void cleanupDatabase() {
        mongoTemplate.getDb().drop();
    }

    @Test
    public void getOrdersOfUser_UserDoesntHaveSubmittedAnyOrders_ShouldReturnAnEmptyList() {
        // Given: An user who doesn't have submitted any order
        String email = "paul.grillish@gmail.com";

        // When
        GetOrdersOfUserRequest request = GetOrdersOfUserRequest.newBuilder()
                .setEmail(email)
                .build();
        GetOrdersOfUserResponse response = ordersGrpcApi.getOrdersOfUser(request);
        List<Order> ordersOfUser = response.getOrdersList();

        // Then
        Assertions.assertEquals(0, ordersOfUser.size());
    }

    @Test
    public void getOrdersOfUser_UserHasSubmittedSingleOrder_ShouldReturnAnOrder() {
        // Given: An user who has submitted a single order
        String email = orderOfJhonDoe.getUserEmail();

        // When
        GetOrdersOfUserRequest request = GetOrdersOfUserRequest.newBuilder()
                .setEmail(email)
                .build();
        GetOrdersOfUserResponse response = ordersGrpcApi.getOrdersOfUser(request);
        List<Order> ordersOfUser = response.getOrdersList();

        // Then
        Assertions.assertEquals(1, ordersOfUser.size());
    }

    @Test
    public void getOrdersOfUser_UserHasSubmittedManyOrder_ShouldReturnThreeOrders() {
        // Given: An user who has submitted multiple order
        String email = order1OfJackMarshall.getUserEmail();

        // When
        GetOrdersOfUserRequest request = GetOrdersOfUserRequest.newBuilder()
                .setEmail(email)
                .build();
        GetOrdersOfUserResponse response = ordersGrpcApi.getOrdersOfUser(request);
        List<Order> ordersOfUser = response.getOrdersList();

        // Then
        Assertions.assertEquals(3, ordersOfUser.size());
    }

    @Test
    public void getOrdersOfUser_UserHasAnInconsistenOrder_ShouldReturnGrpcCode13Internal() {
        // Given: An user who has submitted an order which results inconsistent (The product in the order has not been found in the inventory)
        String email = inconsitentOrderOfColinDuff.getUserEmail();

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    GetOrdersOfUserRequest request = GetOrdersOfUserRequest.newBuilder()
                            .setEmail(email)
                            .build();
                    ordersGrpcApi.getOrdersOfUser(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
//         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.INTERNAL, ex.getStatus()),
                () -> Assertions.assertEquals(Status.INTERNAL.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.INCONSISTENT_ORDER, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void getOrder_OrderExists_ShouldReturnTheOrder() {
        // Given: an existent order
        String existentOrderCode = order1OfJackMarshall.getCode();

        // When
        GetOrderRequest request = GetOrderRequest.newBuilder()
                .setCode(existentOrderCode)
                .build();
        GetOrderResponse response = ordersGrpcApi.getOrder(request);
        Order order = response.getOrder();

        // Then
        Assertions.assertAll(
                () -> Assertions.assertNotNull(order),
                () -> Assertions.assertEquals(order1OfJackMarshall.getCode(), order.getCode()),
                () -> Assertions.assertEquals(order1OfJackMarshall.getStatus().name(), order.getStatus().name()),
                () -> Assertions.assertEquals(order1OfJackMarshall.getUserEmail(), order.getUserEmail()),
                () -> {
                    OffsetDateTime expectedSubmittedOnUTC = order1OfJackMarshall.getSubmittedOnUTC().withNano(0);
                    OffsetDateTime actualSubmittedOnUTC = utcDateTimeFromTimezone(order.getSubmittedOnUTC()).withNano(0);
                    Assertions.assertEquals(expectedSubmittedOnUTC, actualSubmittedOnUTC);
                },
                () -> {
                    Map<String, Integer> expectedProducts = order1OfJackMarshall.getProductItems();
                    List<ProductItem> actualProducts = order.getProductItemsList();
                    Assertions.assertEquals(expectedProducts.size(), actualProducts.size());

                    Set<String> expectedProductsCode = order1OfJackMarshall.getProductItems().keySet().stream().collect(Collectors.toSet());
                    Set<String> actualProductsCode = order.getProductItemsList().stream().map(product -> product.getCode()).collect(Collectors.toSet());
                    Assertions.assertEquals(expectedProductsCode, actualProductsCode);
                }
        );
    }

    @Test
    public void getOrder_OrderExistsButIsInconsistent_ShouldReturnGrpcCode13Internal() {
        // Given: an existent but inconsistent order  (The product in the order has not been found in the inventory)
        String existentButInconsistentOrderCode = inconsitentOrderOfColinDuff.getCode();

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    GetOrderRequest request = GetOrderRequest.newBuilder()
                            .setCode(existentButInconsistentOrderCode)
                            .build();
                    ordersGrpcApi.getOrder(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.INTERNAL, ex.getStatus()),
                () -> Assertions.assertEquals(Status.INTERNAL.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.INCONSISTENT_ORDER, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void getOrder_OrderDoesntExist_ShouldReturnGrpcCode5NotFound() {
        // Given: an inexistent order
        String inexistentOrderCode = "ORDXY0031";

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    GetOrderRequest request = GetOrderRequest.newBuilder()
                            .setCode(inexistentOrderCode)
                            .build();
                    ordersGrpcApi.getOrder(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.ORDER_NOT_FOUND, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void addOrderForUser_ErrorOnUsersApiClient_ShouldReturnGrpcCode4DeadLineExceeded() {
        // Given: An error from checkUserExistence of the users-api client
        String inexistentUserEmail = "jhon.doe@google.com";
        Map<String, Integer> requestedStock = Map.of("PRD00001", 1,
                "PRD00002", 3);
        UsersApiClientException userApiClientException = new UsersApiClientException("User doesn't exist", UUID.randomUUID());
        doThrow(userApiClientException).when(usersApiClient).checkUserExistence(inexistentUserEmail);

        // When
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    AddOrderForUserRequest request = AddOrderForUserRequest.newBuilder()
                            .setEmail(inexistentUserEmail)
                            .putAllProducts(requestedStock)
                            .build();
                    ordersGrpcApi.addOrderForUser(request);
                },
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.DEADLINE_EXCEEDED, ex.getStatus()),
                () -> Assertions.assertEquals(Status.DEADLINE_EXCEEDED.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.USERS_API_CLIENT_FAILURE, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void addOrderForUser_ErrorOnProductsApiClient_ShouldReturnGrpcCode4DeadLineExceeded() {
        // Given: An error from updateProductsStock of the products-api client
        String existentUserEmail = "jhon.doe@google.com";
        Map<String, Integer> requestedStock = Map.of("PRD00001", 1,
                "PRD00002", 3);
        ProductsApiClientException productsApiClientException = new ProductsApiClientException("Can't update products stocks", UUID.randomUUID());
        doThrow(productsApiClientException).when(productsApiClient).updateProductsStock(requestedStock);

        // When
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    AddOrderForUserRequest request = AddOrderForUserRequest.newBuilder()
                            .setEmail(existentUserEmail)
                            .putAllProducts(requestedStock)
                            .build();
                    ordersGrpcApi.addOrderForUser(request);
                },
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.DEADLINE_EXCEEDED, ex.getStatus()),
                () -> Assertions.assertEquals(Status.DEADLINE_EXCEEDED.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.PRODUCTS_API_CLIENT_FAILURE, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void addOrderForUser_AllPreconditionsSatisfiedAndTheOrderCanBeAdded_ShouldReturnTheOrder() {
        String productCode124 = "PRD000124";
        Integer productQuantity124 = 3;
        ProductItemDTO mProduct124 = new ProductItemDTO(productCode124,
                "SDD 216GB WSD",
                "Electronic",
                160.0,
                productQuantity124);

        String productCode125 = "PRD000125";
        Integer productQuantity125 = 2;
        ProductItemDTO mProduct125 = new ProductItemDTO(productCode125,
                "USB Cable",
                "Electronic",
                4.99,
                productQuantity125);
        when(productsApiClient.getProductsByCode(Set.of(productCode124, productCode125))).thenReturn(Set.of(mProduct124, mProduct125));

        // Given: All conditions are satisfied --> User Exists, Products of order Exists, Products of orders are available in stock
        String userEmail = "jhon.doe@google.com";
        Map<String, Integer> requestedStock = Map.of(productCode124, 1,
                productCode125, 2);
        when(productsApiClient.updateProductsStock(requestedStock)).thenReturn(Map.of(productCode124, 6,
                productCode125, 7));

        // When
        AddOrderForUserRequest request = AddOrderForUserRequest.newBuilder()
                .setEmail(userEmail)
                .putAllProducts(requestedStock)
                .build();
        AddOrderForUserResponse response = ordersGrpcApi.addOrderForUser(request);
        Order order = response.getOrder();

        // Then
        Assertions.assertAll(
                () -> Assertions.assertNotNull(order),
                () -> Assertions.assertEquals(userEmail, order.getUserEmail()),
                () -> Assertions.assertNotNull(order.getSubmittedOnUTC()),
                () -> Assertions.assertEquals(OrderStatus.CREATED.name(), order.getStatus().name()),
                () -> {
                    // Check that the result contains exactly the two requested products
                    Assertions.assertEquals(requestedStock.size(), order.getProductItemsList().size());

                    // Check that the result contains the two requested products
                    Optional<ProductItem> optProduct124 = order.getProductItemsList().stream().filter(product -> product.getCode().equals(productCode124)).findFirst();
                    Assertions.assertTrue(optProduct124.isPresent());
                    Optional<ProductItem> optProduct125 = order.getProductItemsList().stream().filter(product -> product.getCode().equals(productCode125)).findFirst();
                    Assertions.assertTrue(optProduct125.isPresent());
                }
        );
    }

    @Test
    public void deleteOrder_OrderExists_ShouldReturnTheDeletedOrderId() {
        // Given
        String existentOrderCode = order1OfJackMarshall.getCode();

        // When
        DeleteOrderRequest request = DeleteOrderRequest.newBuilder()
                .setCode(existentOrderCode)
                .build();
        DeleteOrderResponse response = ordersGrpcApi.deleteOrder(request);

        // Then
        Assertions.assertEquals(order1OfJackMarshall.getId(), response.getId());
    }

    @Test
    public void deleteOrder_OrderDoesntExist_ShouldReturnGrpcCode5NotFound() {
        // Given
        String notExistentOrderCode = "ORDXYZ00091";

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    DeleteOrderRequest request = DeleteOrderRequest.newBuilder()
                            .setCode(notExistentOrderCode)
                            .build();
                    ordersGrpcApi.deleteOrder(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.ORDER_NOT_FOUND, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void updateOrderStatus_OrderDoesntExist_ShouldReturnGrpcCode5NotFound() {
        // Given
        String notExistentOrderCode = "ORDXYZ00091";

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    UpdateOrderStatusRequest request = UpdateOrderStatusRequest.newBuilder()
                            .setCode(notExistentOrderCode)
                            .setStatus(it.millsoft.orders.api.interfaces.grpc.model.OrderStatus.SHIPPED)
                            .build();
                    ordersGrpcApi.updateOrderStatus(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.ORDER_NOT_FOUND, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void updateOrderStatus_WantUpdateWithTheSameStatus_ShouldReturnGrpcCode3InvalidArgument() {
        // Given
        String existentOrderCode = order1OfJackMarshall.getCode();

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    UpdateOrderStatusRequest request = UpdateOrderStatusRequest.newBuilder()
                            .setCode(existentOrderCode)
                            .setStatus(it.millsoft.orders.api.interfaces.grpc.model.OrderStatus.valueOf(order1OfJackMarshall.getStatus().name()))
                            .build();
                    ordersGrpcApi.updateOrderStatus(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT, ex.getStatus()),
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.ORDER_STATUS_NOT_UPDATABLE, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void updateOrderStatus_WantUpdateWithAValidStatus_ShouldReturnTheIdOfTheUpdatedOrder() {
        // Given
        String existentOrderCode = order1OfJackMarshall.getCode();

        // When
        UpdateOrderStatusRequest request = UpdateOrderStatusRequest.newBuilder()
                .setCode(existentOrderCode)
                .setStatus(it.millsoft.orders.api.interfaces.grpc.model.OrderStatus.SHIPPED)
                .build();
        UpdateOrderStatusResponse response = ordersGrpcApi.updateOrderStatus(request);

        // Then
        Assertions.assertEquals(response.getId(), order1OfJackMarshall.getId());
    }

    private OffsetDateTime utcDateTimeFromTimezone(Timestamp timestamp) {
        return OffsetDateTime.ofInstant(Instant.ofEpochSecond(timestamp.getSeconds(), timestamp.getNanos()),
                ZoneOffset.UTC);
    }

}