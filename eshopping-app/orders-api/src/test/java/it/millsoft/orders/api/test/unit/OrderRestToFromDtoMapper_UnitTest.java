package it.millsoft.orders.api.test.unit;

import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.interfaces.rest.model.Order;
import it.millsoft.orders.api.interfaces.rest.model.OrderStatus;
import it.millsoft.orders.api.interfaces.rest.model.ProductItem;
import it.millsoft.orders.api.mappers.OrderRestToFromDtoMapper;
import it.millsoft.orders.api.mappers.OrderRestToFromDtoMapperImpl;
import it.millsoft.orders.api.mappers.ProductItemRestToFromDtoMapper;
import it.millsoft.orders.api.mappers.ProductItemRestToFromDtoMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.Set;


@SpringJUnitConfig
public class OrderRestToFromDtoMapper_UnitTest {

    @TestConfiguration
    public static class MapperTestConfiguration {

        @Bean
        public OrderRestToFromDtoMapper createOrderRestToFromDtoMapper() {
            return new OrderRestToFromDtoMapperImpl();
        }

        @Bean
        public ProductItemRestToFromDtoMapper createProductItemRestToFromDtoMapper() {
            return new ProductItemRestToFromDtoMapperImpl();
        }
    }

    @Autowired
    private OrderRestToFromDtoMapper mapper;

    @Test
    public void orderRestToDto_createTheOrderDTO_ShouldReturnTheOrderDTO() {
        // Given
        String productItemCode1 = "PRD000001";
        ProductItem rProductItem1 = new ProductItem();
        rProductItem1.setCode(productItemCode1);
        rProductItem1.setDescription("Lenovo X1 Carbon");
        rProductItem1.setCategory("Electronic");
        rProductItem1.setPrice(1899.00);
        rProductItem1.setRequestedStock(2);

        String productItemCode2 = "PRD000002";
        ProductItem rProductItem2 = new ProductItem();
        rProductItem2.setCode(productItemCode2);
        rProductItem2.setDescription("IPhone 8");
        rProductItem2.setCategory("Electronic");
        rProductItem2.setPrice(899.00);
        rProductItem2.setRequestedStock(1);
        Set<ProductItem> rProductItems = Set.of(rProductItem1, rProductItem2);

        Order rOrder = new Order();
        rOrder.setCode("ORD000001");
        rOrder.setUserEmail("jhon.doe@gmail.com");
        rOrder.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        rOrder.setStatus(OrderStatus.CREATED);
        rOrder.setProductItems(rProductItems);

        // When
        OrderDTO dOrder = mapper.orderRestToDto(rOrder);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(rOrder.getCode(), dOrder.getCode()),
                () -> Assertions.assertEquals(rOrder.getStatus().name(), dOrder.getStatus().name()),
                () -> Assertions.assertEquals(rOrder.getSubmittedOnUTC(), dOrder.getSubmittedOnUTC()),
                () -> Assertions.assertEquals(rOrder.getUserEmail(), dOrder.getUserEmail()),
                () -> Assertions.assertEquals(rOrder.getProductItems().size(), dOrder.getProductItems().size()),
                () -> {
                    Optional<ProductItemDTO> optProductItem1 = dOrder.getProductItems().stream()
                            .filter(dProductItem -> dProductItem.getCode().equals(productItemCode1))
                            .findFirst();
                    Assertions.assertTrue(optProductItem1.isPresent());
                    ProductItemDTO dProductItem1 = optProductItem1.get();

                    Assertions.assertEquals(rProductItem1.getCode(), dProductItem1.getCode());
                    Assertions.assertEquals(rProductItem1.getCategory(), dProductItem1.getCategory());
                    Assertions.assertEquals(rProductItem1.getPrice(), dProductItem1.getPrice());
                    Assertions.assertEquals(rProductItem1.getDescription(), dProductItem1.getDescription());
                    Assertions.assertEquals(rProductItem1.getRequestedStock(), dProductItem1.getRequestedStock());

                    Optional<ProductItemDTO> optProductItem2 = dOrder.getProductItems().stream()
                            .filter(dProductItem -> dProductItem.getCode().equals(productItemCode2))
                            .findFirst();
                    Assertions.assertTrue(optProductItem2.isPresent());
                    ProductItemDTO dProductItem2 = optProductItem2.get();

                    Assertions.assertEquals(rProductItem2.getCode(), dProductItem2.getCode());
                    Assertions.assertEquals(rProductItem2.getCategory(), dProductItem2.getCategory());
                    Assertions.assertEquals(rProductItem2.getPrice(), dProductItem2.getPrice());
                    Assertions.assertEquals(rProductItem2.getDescription(), dProductItem2.getDescription());
                    Assertions.assertEquals(rProductItem2.getRequestedStock(), dProductItem2.getRequestedStock());
                }
        );
    }

    @Test
    public void orderDtoToRest_createTheOrderRest_ShouldReturnTheOrderRest() {
        // Given
        String productItemCode1 = "PRD000001";
        ProductItemDTO dProductItem1 = new ProductItemDTO();
        dProductItem1.setCode(productItemCode1);
        dProductItem1.setDescription("Lenovo X1 Carbon");
        dProductItem1.setCategory("Electronic");
        dProductItem1.setPrice(1899.00);
        dProductItem1.setRequestedStock(2);

        String productItemCode2 = "PRD000002";
        ProductItemDTO dProductItem2 = new ProductItemDTO();
        dProductItem2.setCode(productItemCode2);
        dProductItem2.setDescription("IPhone 8");
        dProductItem2.setCategory("Electronic");
        dProductItem2.setPrice(899.00);
        dProductItem2.setRequestedStock(1);
        Set<ProductItemDTO> dProductItems = Set.of(dProductItem1, dProductItem2);

        OrderDTO dOrder = new OrderDTO("ORD000001",
                OffsetDateTime.now(ZoneOffset.UTC),
                it.millsoft.orders.api.dto.OrderStatus.CREATED,
                dProductItems,
                "jhon.doe@gmail.com");

        // When
        Order rOrder = mapper.orderDtoToRest(dOrder);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(dOrder.getCode(), rOrder.getCode()),
                () -> Assertions.assertEquals(dOrder.getStatus().name(), rOrder.getStatus().name()),
                () -> Assertions.assertEquals(dOrder.getSubmittedOnUTC(), rOrder.getSubmittedOnUTC()),
                () -> Assertions.assertEquals(dOrder.getUserEmail(), rOrder.getUserEmail()),
                () -> Assertions.assertEquals(dOrder.getProductItems().size(), rOrder.getProductItems().size()),
                () -> {
                    Optional<ProductItem> optProductItem1 = rOrder.getProductItems().stream()
                            .filter(rProductItem -> rProductItem.getCode().equals(productItemCode1))
                            .findFirst();
                    Assertions.assertTrue(optProductItem1.isPresent());
                    ProductItem rProductItem1 = optProductItem1.get();

                    Assertions.assertEquals(dProductItem1.getCode(), rProductItem1.getCode());
                    Assertions.assertEquals(dProductItem1.getCategory(), rProductItem1.getCategory());
                    Assertions.assertEquals(dProductItem1.getPrice(), rProductItem1.getPrice());
                    Assertions.assertEquals(dProductItem1.getDescription(), rProductItem1.getDescription());
                    Assertions.assertEquals(dProductItem1.getRequestedStock(), rProductItem1.getRequestedStock());

                    Optional<ProductItem> optProductItem2 = rOrder.getProductItems().stream()
                            .filter(gProductItem -> gProductItem.getCode().equals(productItemCode2))
                            .findFirst();
                    Assertions.assertTrue(optProductItem2.isPresent());
                    ProductItem rProductItem2 = optProductItem2.get();

                    Assertions.assertEquals(dProductItem2.getCode(), rProductItem2.getCode());
                    Assertions.assertEquals(dProductItem2.getCategory(), rProductItem2.getCategory());
                    Assertions.assertEquals(dProductItem2.getPrice(), rProductItem2.getPrice());
                    Assertions.assertEquals(dProductItem2.getDescription(), rProductItem2.getDescription());
                    Assertions.assertEquals(dProductItem2.getRequestedStock(), rProductItem2.getRequestedStock());
                }
        );
    }

    @Test
    public void ordersRestToDto_createAListOfOrderDto_ShouldReturnTheListOfOrderDto() {
        // Given
        String productItemCode1 = "PRD000001";
        ProductItem rProductItem1 = new ProductItem();
        rProductItem1.setCode(productItemCode1);
        rProductItem1.setDescription("Lenovo X1 Carbon");
        rProductItem1.setCategory("Electronic");
        rProductItem1.setPrice(1899.00);
        rProductItem1.setRequestedStock(2);

        String productItemCode2 = "PRD000002";
        ProductItem rProductItem2 = new ProductItem();
        rProductItem2.setCode(productItemCode2);
        rProductItem2.setDescription("IPhone 8");
        rProductItem2.setCategory("Electronic");
        rProductItem2.setPrice(899.00);
        rProductItem2.setRequestedStock(1);

        Set<ProductItem> rProductItems = Set.of(rProductItem1, rProductItem2);

        Order rOrder1 = new Order();
        rOrder1.setCode("ORD000001");
        rOrder1.setUserEmail("jhon.doe@gmail.com");
        rOrder1.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        rOrder1.setStatus(OrderStatus.CREATED);
        rOrder1.setProductItems(rProductItems);

        Order rOrder2 = new Order();
        rOrder2.setCode("ORD000002");
        rOrder2.setUserEmail("andy.green@gmail.com");
        rOrder2.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        rOrder2.setStatus(OrderStatus.CREATED);
        rOrder2.setProductItems(rProductItems);

        Set<Order> rOrders = Set.of(rOrder1, rOrder2);

        // When
        Set<OrderDTO> dOrders = mapper.ordersRestToDto(rOrders);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(rOrders.size(), dOrders.size()),
                () -> {
                    Optional<OrderDTO> optOrder1 = dOrders.stream().filter(dOrder -> dOrder.getCode().equals(rOrder1.getCode())).findFirst();
                    Assertions.assertTrue(optOrder1.isPresent());
                    OrderDTO dOrder1 = optOrder1.get();
                    Assertions.assertEquals(rOrder1.getCode(), dOrder1.getCode());
                    Assertions.assertEquals(rOrder1.getStatus().name(), dOrder1.getStatus().name());
                    Assertions.assertEquals(rOrder1.getSubmittedOnUTC(), dOrder1.getSubmittedOnUTC());
                    Assertions.assertEquals(rOrder1.getUserEmail(), dOrder1.getUserEmail());
                    Assertions.assertEquals(rOrder1.getProductItems().size(), dOrder1.getProductItems().size());

                    Optional<OrderDTO> optOrder2 = dOrders.stream().filter(dOrder -> dOrder.getCode().equals(rOrder2.getCode())).findFirst();
                    Assertions.assertTrue(optOrder2.isPresent());
                    OrderDTO dOrder2 = optOrder2.get();
                    Assertions.assertEquals(rOrder2.getCode(), dOrder2.getCode());
                    Assertions.assertEquals(rOrder2.getStatus().name(), dOrder2.getStatus().name());
                    Assertions.assertEquals(rOrder2.getSubmittedOnUTC(), dOrder2.getSubmittedOnUTC());
                    Assertions.assertEquals(rOrder2.getUserEmail(), dOrder2.getUserEmail());
                    Assertions.assertEquals(rOrder2.getProductItems().size(), dOrder2.getProductItems().size());
                }
        );
    }

    @Test
    public void OrderDtoToRest_createAListOfOrderRest_ShouldReturnTheListOfOrderRest() {
        // Given
        String productItemCode1 = "PRD000001";
        ProductItemDTO dProductItem1 = new ProductItemDTO();
        dProductItem1.setCode(productItemCode1);
        dProductItem1.setDescription("Lenovo X1 Carbon");
        dProductItem1.setCategory("Electronic");
        dProductItem1.setPrice(1899.00);
        dProductItem1.setRequestedStock(2);

        String productItemCode2 = "PRD000002";
        ProductItemDTO dProductItem2 = new ProductItemDTO();
        dProductItem2.setCode(productItemCode2);
        dProductItem2.setDescription("IPhone 8");
        dProductItem2.setCategory("Electronic");
        dProductItem2.setPrice(899.00);
        dProductItem2.setRequestedStock(1);
        Set<ProductItemDTO> dProductItems = Set.of(dProductItem1, dProductItem2);

        OrderDTO dOrder1 = new OrderDTO("ORD000001",
                OffsetDateTime.now(ZoneOffset.UTC),
                it.millsoft.orders.api.dto.OrderStatus.CREATED,
                dProductItems,
                "jhon.doe@gmail.com");

        OrderDTO dOrder2 = new OrderDTO("ORD000002",
                OffsetDateTime.now(ZoneOffset.UTC),
                it.millsoft.orders.api.dto.OrderStatus.CREATED,
                dProductItems,
                "andy.green@gmail.com");

        Set<OrderDTO> dOrders = Set.of(dOrder1, dOrder2);

        // When
        Set<Order> rOrders = mapper.ordersDtoToRest(dOrders);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(dOrders.size(), rOrders.size()),
                () -> {
                    Optional<Order> optOrder1 = rOrders.stream().filter(gOrder -> gOrder.getCode().equals(dOrder1.getCode())).findFirst();
                    Assertions.assertTrue(optOrder1.isPresent());
                    Order rOrder1 = optOrder1.get();
                    Assertions.assertEquals(dOrder1.getCode(), rOrder1.getCode());
                    Assertions.assertEquals(dOrder1.getStatus().name(), rOrder1.getStatus().name());
                    Assertions.assertEquals(dOrder1.getSubmittedOnUTC(), rOrder1.getSubmittedOnUTC());
                    Assertions.assertEquals(dOrder1.getUserEmail(), rOrder1.getUserEmail());
                    Assertions.assertEquals(dOrder1.getProductItems().size(), rOrder1.getProductItems().size());

                    Optional<Order> optOrder2 = rOrders.stream().filter(rOrder -> rOrder.getCode().equals(dOrder2.getCode())).findFirst();
                    Assertions.assertTrue(optOrder2.isPresent());
                    Order rOrder2 = optOrder2.get();
                    Assertions.assertEquals(dOrder2.getCode(), rOrder2.getCode());
                    Assertions.assertEquals(dOrder2.getStatus().name(), rOrder2.getStatus().name());
                    Assertions.assertEquals(dOrder2.getSubmittedOnUTC(), rOrder2.getSubmittedOnUTC());
                    Assertions.assertEquals(dOrder2.getUserEmail(), rOrder2.getUserEmail());
                    Assertions.assertEquals(dOrder2.getProductItems().size(), rOrder2.getProductItems().size());
                }
        );
    }

}
