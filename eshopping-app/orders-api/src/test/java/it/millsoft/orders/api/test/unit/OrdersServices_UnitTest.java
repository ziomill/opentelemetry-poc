package it.millsoft.orders.api.test.unit;

import it.millsoft.orders.api.business.OrdersServices;
import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.dto.OrderStatus;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.exceptions.*;
import it.millsoft.orders.api.ext.ProductsApiClient;
import it.millsoft.orders.api.ext.UsersApiClient;
import it.millsoft.orders.api.model.Order;
import it.millsoft.orders.api.repositories.OrdersMongoRepository;
import it.millsoft.orders.api.test.config.UnitTestConfig;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;

@SpringJUnitConfig(classes = UnitTestConfig.class)
public class OrdersServices_UnitTest {

    @Autowired
    private OrdersServices ordersServices;

    @MockBean
    private OrdersMongoRepository ordersMongoRepository;

    @MockBean
    private ProductsApiClient productsApiClient;

    @MockBean
    private UsersApiClient usersApiClient;

    @Test
    public void getOrdersOfUser_UserDoesntHaveSubmittedAnyOrders_ShouldReturnAnEmptyList() {
        // Given: An user who doesn't have submitted any order
        String email = "jhon.doe@gmail.com"; 
        when(ordersMongoRepository.findOrdersOfUser(email)).thenReturn(new HashSet<>());
        
        // When
        Set<OrderDTO> ordersOfUser = ordersServices.getOrdersOfUser(email);

        // Then
        Assertions.assertEquals(0,ordersOfUser.size());
    }

    @Test
    public void getOrdersOfUser_UserHasSubmittedSingleOrder_ShouldReturnAnOrder() {
        // Given: An user who has submitted a single order
        String email = "jhon.doe@gmail.com";

        String productCode = "PRD000001";
        Order mOrder = new Order();
        mOrder.setCode("ORD000001");
        mOrder.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        mOrder.setStatus(OrderStatus.CREATED);
        mOrder.setProductItems(Map.of(productCode,1));
        mOrder.setUserEmail(email);
        when(ordersMongoRepository.findOrdersOfUser(email)).thenReturn(Set.of(mOrder));

        ProductItemDTO mProduct = new ProductItemDTO(productCode,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                1);
        when(productsApiClient.getProductsByCode(mOrder.getProductItems().keySet())).thenReturn(Set.of(mProduct));

        // When
        Set<OrderDTO> ordersOfUser = ordersServices.getOrdersOfUser(email);

        // Then
        Assertions.assertEquals(1,ordersOfUser.size());
    }

    @Test
    public void getOrdersOfUser_UserHasSubmittedManyOrder_ShouldReturnThreeOrders() {
        // Given: An user who has submitted multiple order
        String email = "jhon.doe@gmail.com";

        String productCode1 = "PRD000001";
        Order mOrder1 = new Order();
        mOrder1.setCode("ORD000001");
        mOrder1.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        mOrder1.setStatus(OrderStatus.CREATED);
        mOrder1.setProductItems(Map.of(productCode1,1));
        mOrder1.setUserEmail(email);

        String productCode2 = "PRD000002";
        Order mOrder2 = new Order();
        mOrder2.setCode("ORD000002");
        mOrder2.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        mOrder2.setStatus(OrderStatus.CREATED);
        mOrder2.setProductItems(Map.of(productCode2,1));
        mOrder2.setUserEmail(email);

        String productCode3 = "PRD000003";
        Order mOrder3 = new Order();
        mOrder3.setCode("ORD000003");
        mOrder3.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        mOrder3.setStatus(OrderStatus.CREATED);
        mOrder3.setProductItems(Map.of(productCode3,1));
        mOrder3.setUserEmail(email);
        when(ordersMongoRepository.findOrdersOfUser(email)).thenReturn(Set.of(mOrder1,mOrder2,mOrder3));

        ProductItemDTO mProduct1 = new ProductItemDTO(productCode1,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                1);
        when(productsApiClient.getProductsByCode(mOrder1.getProductItems().keySet())).thenReturn(Set.of(mProduct1));

        ProductItemDTO mProduct2 = new ProductItemDTO(productCode2,
                "Logitech Optical Mouse",
                "Electronic",
                14.00,
                2);
        when(productsApiClient.getProductsByCode(mOrder2.getProductItems().keySet())).thenReturn(Set.of(mProduct2));

        ProductItemDTO mProduct3 = new ProductItemDTO(productCode3,
                "IPhone 11",
                "Electronic",
                1099.00,
                1);
        when(productsApiClient.getProductsByCode(mOrder3.getProductItems().keySet())).thenReturn(Set.of(mProduct3));

        // When
        Set<OrderDTO> ordersOfUser = ordersServices.getOrdersOfUser(email);

        // Then
        Assertions.assertEquals(3,ordersOfUser.size());
    }

    @Test
    public void getOrdersOfUser_UserHasAnInconsistenOrder_ShouldThrowAnInconsistentOrderException() {
        // Given: An user who has submitted an order which results inconsistent (The product in the order has not been found in the inventory)
        String email = "jhon.doe@gmail.com";

        String productCode = "PRD000001";
        Order mOrder = new Order();
        mOrder.setCode("ORD000001");
        mOrder.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        mOrder.setStatus(OrderStatus.CREATED);
        mOrder.setProductItems(Map.of(productCode,1));
        mOrder.setUserEmail(email);
        when(ordersMongoRepository.findOrdersOfUser(email)).thenReturn(Set.of(mOrder));

        // Then
        Assertions.assertThrows(
                InconsitentOrderException.class,
                () -> ordersServices.getOrdersOfUser(email), // When
                "It should have raised the " + InconsitentOrderException.class.getName()
        );
    }

    @Test
    public void getOrder_OrderExists_ShouldReturnTheOrder() {
        // Given: an existent order
        String existentOrderCode = "ORD000001";

        String productCode = "PRD000001";
        Order mOrder = new Order();
        mOrder.setCode(existentOrderCode);
        mOrder.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        mOrder.setStatus(OrderStatus.CREATED);
        mOrder.setProductItems(Map.of(productCode,1));
        mOrder.setUserEmail("jhon.doe@gmail.com");
        when(ordersMongoRepository.findOrderByCode(existentOrderCode)).thenReturn(Optional.of(mOrder));

        ProductItemDTO mProduct = new ProductItemDTO(productCode,
                "IPhone 11",
                "Electronic",
                1099.00,
                1);
        when(productsApiClient.getProductsByCode(mOrder.getProductItems().keySet())).thenReturn(Set.of(mProduct));

        // When
        OrderDTO order = ordersServices.getOrder(existentOrderCode);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertNotNull(order),
                () -> Assertions.assertEquals(mOrder.getCode(),order.getCode()),
                () -> Assertions.assertEquals(mOrder.getSubmittedOnUTC(),order.getSubmittedOnUTC()),
                () -> Assertions.assertEquals(mOrder.getStatus(),order.getStatus()),
                () -> {
                    Set<String> expectedProducts = mOrder.getProductItems().keySet();
                    Set<String> actualProducts = order.getProductItems().stream().map(product -> product.getCode()).collect(Collectors.toSet());
                    Assertions.assertEquals(expectedProducts.size(),actualProducts.size());
                    Assertions.assertEquals(expectedProducts,actualProducts);
                },
                () -> Assertions.assertEquals(mOrder.getUserEmail(),order.getUserEmail())
        );
    }

    @Test
    public void getOrder_OrderExistsButIsInconsistent_ShouldThrowAnInconsistentOrderException() {
        // Given: an existent but inconsistent order  (The product in the order has not been found in the inventory)
        String existentButInconsistentOrderCode = "ORD000001";

        String productCode = "PRD000001";
        Order mOrder = new Order();
        mOrder.setCode(existentButInconsistentOrderCode);
        mOrder.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        mOrder.setStatus(OrderStatus.CREATED);
        mOrder.setProductItems(Map.of(productCode,1));
        mOrder.setUserEmail("jhon.doe@gmail.com");
        when(ordersMongoRepository.findOrderByCode(existentButInconsistentOrderCode)).thenReturn(Optional.of(mOrder));

        // Then
        Assertions.assertThrows(
                InconsitentOrderException.class,
                () -> ordersServices.getOrder(existentButInconsistentOrderCode), // When
                "It should have raised the " + InconsitentOrderException.class.getName()
        );
    }

    @Test
    public void getOrder_OrderDoesntExist_ShouldThrowAnOrderNotFoundException() {
        // Given: an inexistent order
        String inexistentOrderCode = "ORD000031";
        when(ordersMongoRepository.findOrderByCode(inexistentOrderCode)).thenReturn(Optional.empty());

        // Then
        Assertions.assertThrows(
                OrderNotFoundException.class,
                () -> ordersServices.getOrder(inexistentOrderCode), // When
                "It should have raised the " + OrderNotFoundException.class.getName()
        );
    }

    @Test
    public void addOrderForUser_ErrorOnCheckUserExistence_ShouldThrowAnUsersApiException() {
        // Given: An error from checkUserExistence of the users-api client
        String inexistentUserEmail = "jhon.doe@google.com";
        UsersApiClientException userApiClientException = new UsersApiClientException("User doesn't exist", UUID.randomUUID());
        doThrow(userApiClientException).when(usersApiClient).checkUserExistence(inexistentUserEmail);

        // When
        Assertions.assertThrows(
                UsersApiClientException.class,
                () -> {
                    // When
                    Map<String,Integer> requestedStock = Map.of("PRD00001",1,
                                                                "PRD00002",3);
                    ordersServices.addOrderForUser(inexistentUserEmail,requestedStock);
                },
                "It should have raised the " + UsersApiClientException.class.getName()
        );
    }

    @Test
    public void addOrderForUser_ErrorOnUpdateProductsStock_ShouldThrowAnProductsApiException() {
        // Given: An error from getProductsByCode of the products-api client
        String productCode1 = "PRD00001";
        String productCode2 = "PRD00002";
        ProductsApiClientException productsApiClientException = new ProductsApiClientException("Insufficient Stock",UUID.randomUUID());
        Map<String,Integer> requestedStock = Map.of(productCode1,1,
                                                    productCode2,3);
        doThrow(productsApiClientException).when(productsApiClient).updateProductsStock(requestedStock);

        ProductItemDTO mProduct1 = new ProductItemDTO(productCode1,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                1);
        ProductItemDTO mProduct2 = new ProductItemDTO(productCode2,
                "Logitech Optical Mouse",
                "Electronic",
                14.00,
                2);
        Set<ProductItemDTO> mProducts = Set.of(mProduct1,mProduct2);
        when(productsApiClient.getProductsByCode(Set.of(productCode1,productCode2))).thenReturn(mProducts);

        String existentUserEmail = "jhon.doe@google.com";
        doNothing().when(usersApiClient).checkUserExistence(existentUserEmail);

        // When
        Assertions.assertThrows(
                ProductsApiClientException.class,
                () -> {
                    // When
                    ordersServices.addOrderForUser(existentUserEmail,requestedStock);
                },
                "It should have raised the " + ProductsApiClientException.class.getName()
        );
    }

    @Test
    public void addOrderForUser_ErrorOnGetProductsByCode_ShouldThrowAnProductsApiException() {
        // Given: An error from getProductsByCode of the products-api client
        ProductsApiClientException productsApiClientException = new ProductsApiClientException("Product not found",UUID.randomUUID());
        doThrow(productsApiClientException).when(productsApiClient).getProductsByCode(Set.of("PRD00001","PRD00002"));

        String existentUserEmail = "jhon.doe@google.com";
        doNothing().when(usersApiClient).checkUserExistence(existentUserEmail);

        // When
        Assertions.assertThrows(
                ProductsApiClientException.class,
                () -> {
                    // When
                    Map<String,Integer> requestedStock = Map.of("PRD00001",1,
                                                                "PRD00002",3);
                    ordersServices.addOrderForUser(existentUserEmail,requestedStock);
                },
                "It should have raised the " + ProductsApiClientException.class.getName()
        );
    }

    @Test
    public void addOrderForUser_AllPreconditionsSatisfiedAndTheOrderCanBeAdded_ShouldReturnTheOrder() {
        // Given: All conditions are satisfied --> User Exists, Products of order Exists, Products of orders are available in stock
        String productCode1 = "PRD00001";
        String productCode2 = "PRD00002";
        Map<String,Integer> requestedStock = Map.of(productCode1,1,
                                                    productCode2,3);
        Map<String,Integer> newAvailableStock = Map.of(productCode1,5,
                                                       productCode2,4);
        when(productsApiClient.updateProductsStock(requestedStock)).thenReturn(newAvailableStock);

        ProductItemDTO mProduct1 = new ProductItemDTO(productCode1,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                1);
        ProductItemDTO mProduct2 = new ProductItemDTO(productCode2,
                "Logitech Optical Mouse",
                "Electronic",
                14.00,
                3);
        Set<ProductItemDTO> mProducts = Set.of(mProduct1,mProduct2);
        when(productsApiClient.getProductsByCode(Set.of(productCode1,productCode2))).thenReturn(mProducts);

        String existentUserEmail = "jhon.doe@google.com";
        doNothing().when(usersApiClient).checkUserExistence(existentUserEmail);

        Order mOrder = new Order();
        mOrder.setCode("ORD000001");
        mOrder.setUserEmail(existentUserEmail);
        mOrder.setStatus(OrderStatus.CREATED);
        when(ordersMongoRepository.save(any(Order.class))).thenAnswer(i -> {
            Order order = i.getArgument(0);
            order.setId(new ObjectId().toString());
            return order;
        });

        // When
        OrderDTO order = ordersServices.addOrderForUser(existentUserEmail,requestedStock);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertNotNull(order),
                () -> Assertions.assertEquals(existentUserEmail,order.getUserEmail()),
                () -> Assertions.assertNotNull(order.getSubmittedOnUTC()),
                () -> Assertions.assertEquals(OrderStatus.CREATED,order.getStatus()),
                () -> {
                    // Check that the result contains exactly the two requested products
                    Assertions.assertEquals(requestedStock.size(),order.getProductItems().size());

                    // Check that the result contains the two requested products
                    List<ProductItemDTO> productsOfOrder = order.getProductItems().stream().toList();
                    ProductItemDTO p1 = productsOfOrder.get(productsOfOrder.indexOf(mProduct1));
                    Assertions.assertEquals(productCode1,p1.getCode());
                    ProductItemDTO p2 = productsOfOrder.get(productsOfOrder.indexOf(mProduct2));
                    Assertions.assertEquals(productCode2,p2.getCode());
                }
        );
    }

    @Test
    public void deleteOrder_OrderExists_ShouldReturnTheDeletedOrderId() {
        // Given
        String existentOrderCode = "ORD00001";
        Order mOrder = new Order();
        mOrder.setId(new ObjectId().toString());
        mOrder.setCode(existentOrderCode);
        mOrder.setUserEmail("jhon.doe@gmail.com");
        mOrder.setProductItems(Map.of("PRD00001",3));
        mOrder.setStatus(OrderStatus.CREATED);
        mOrder.setSubmittedOnUTC(OffsetDateTime.now());

        Optional<Order> optOrder = Optional.of(mOrder);
        when(ordersMongoRepository.findOrderByCode(existentOrderCode)).thenReturn(optOrder);

        doNothing().when(ordersMongoRepository).delete(mOrder);

        // When
        String deletedOrderId = ordersServices.deleteOrder(existentOrderCode);

        // Then
        Assertions.assertEquals(mOrder.getId(),deletedOrderId);
    }

    @Test
    public void deleteOrder_OrderDoesntExist_ShouldThrowAnOrderNotFoundException() {
        // Given
        String notExistentOrderCode = "ORD00001";
        Optional<Order> optOrder = Optional.empty();
        when(ordersMongoRepository.findOrderByCode(notExistentOrderCode)).thenReturn(optOrder);

        // Then
        Assertions.assertThrows(
                OrderNotFoundException.class,
                () -> ordersServices.deleteOrder(notExistentOrderCode), // When
                "It should have raised the " + OrderNotFoundException.class.getName()
        );
    }

    @Test
    public void updateOrderStatus_OrderDoesntExist_ShouldThrowAnOrderNotFoundException()  {
        // Given
        String notExistentOrderCode = "ORD00001";
        Optional<Order> optOrder = Optional.empty();
        when(ordersMongoRepository.findOrderByCode(notExistentOrderCode)).thenReturn(optOrder);

        // Then
        Assertions.assertThrows(
                OrderNotFoundException.class,
                () -> ordersServices.updateOrderStatus(notExistentOrderCode,OrderStatus.DELIVERED), // When
                "It should have raised the " + OrderNotFoundException.class.getName()
        );
    }

    @Test
    public void updateOrderStatus_WantUpdateWithTheSameStatus_ShouldThrowAnOrderStatusNotUpdatableException()  {
        // Given
        String existentOrderCode = "ORD00001";
        Order mOrder = new Order();
        mOrder.setId(new ObjectId().toString());
        mOrder.setCode(existentOrderCode);
        mOrder.setUserEmail("jhon.doe@gmail.com");
        mOrder.setProductItems(Map.of("PRD00001",3));
        mOrder.setStatus(OrderStatus.CREATED);
        mOrder.setSubmittedOnUTC(OffsetDateTime.now());

        Optional<Order> optOrder = Optional.of(mOrder);
        when(ordersMongoRepository.findOrderByCode(existentOrderCode)).thenReturn(optOrder);

        // Then
        Assertions.assertThrows(
                OrderStatusNotUpdatableException.class,
                () -> ordersServices.updateOrderStatus(existentOrderCode,OrderStatus.CREATED), // When
                "It should have raised the " + OrderStatusNotUpdatableException.class.getName()
        );
    }

    @Test
    public void updateOrderStatus_WantUpdateWithAValidStatus_ShouldReturnTheIdOfTheUpdatedOrder()  {
        // Given
        String existentOrderCode = "ORD00001";
        Order mOrder = new Order();
        mOrder.setId(new ObjectId().toString());
        mOrder.setCode(existentOrderCode);
        mOrder.setUserEmail("jhon.doe@gmail.com");
        mOrder.setProductItems(Map.of("PRD00001",3));
        mOrder.setStatus(OrderStatus.CREATED);
        mOrder.setSubmittedOnUTC(OffsetDateTime.now());

        Optional<Order> optOrder = Optional.of(mOrder);
        when(ordersMongoRepository.findOrderByCode(existentOrderCode)).thenReturn(optOrder);

        when(ordersMongoRepository.save(any(Order.class))).thenAnswer(i -> i.getArgument(0));

        // When
        String updatedOrderId = ordersServices.updateOrderStatus(existentOrderCode,OrderStatus.SHIPPED);

        // Then
        Assertions.assertEquals(mOrder.getId(),updatedOrderId);
    }

}
