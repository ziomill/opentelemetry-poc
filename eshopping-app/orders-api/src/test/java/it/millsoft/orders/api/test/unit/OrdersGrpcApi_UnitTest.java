package it.millsoft.orders.api.test.unit;

import com.google.protobuf.Timestamp;
import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.orders.api.business.OrdersServices;
import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.dto.OrderStatus;
import it.millsoft.orders.api.dto.OrdersApiErrorEnum;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.exceptions.*;
import it.millsoft.orders.api.interfaces.grpc.model.*;
import it.millsoft.orders.api.test.config.GrpcUnitTestConfig;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@SpringJUnitConfig(classes = GrpcUnitTestConfig.class)
@TestPropertySource(
        properties = {
                "grpc.server.inProcessName=test",
                "grpc.server.port=-1",
                "grpc.client.inProcess.address=in-process:test"
        }
)
@DirtiesContext  // Properly shutdown the GRPC Server after each test
public class OrdersGrpcApi_UnitTest {
    @GrpcClient("inProcess")
    private OrdersGrpcApiGrpc.OrdersGrpcApiBlockingStub ordersGrpcApi;

    @MockBean
    private OrdersServices ordersServices;

    @Test
    public void getOrdersOfUser_UserDoesntHaveSubmittedAnyOrders_ShouldReturnAnEmptyList() {
        // Given: An user who doesn't have submitted any order
        String email = "jhon.doe@gmail.com";
        when(ordersServices.getOrdersOfUser(email)).thenReturn(new HashSet<>());

        // When
        GetOrdersOfUserRequest request = GetOrdersOfUserRequest.newBuilder()
                .setEmail(email)
                .build();
        GetOrdersOfUserResponse response = ordersGrpcApi.getOrdersOfUser(request);
        List<Order> ordersOfUser = response.getOrdersList();

        // Then
        Assertions.assertEquals(0, ordersOfUser.size());
    }

    @Test
    public void getOrdersOfUser_UserHasSubmittedSingleOrder_ShouldReturnAnOrder() {
        // Given: An user who has submitted a single order
        String email = "jhon.doe@gmail.com";

        String productCode = "PRD000001";
        ProductItemDTO mProduct = new ProductItemDTO(productCode,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                1);
        Set<ProductItemDTO> mProducts = Set.of(mProduct);

        OrderDTO mOrder = new OrderDTO("ORD000001",
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                mProducts,
                email);
        when(ordersServices.getOrdersOfUser(email)).thenReturn(Set.of(mOrder));

        // When
        GetOrdersOfUserRequest request = GetOrdersOfUserRequest.newBuilder()
                .setEmail(email)
                .build();
        GetOrdersOfUserResponse response = ordersGrpcApi.getOrdersOfUser(request);
        List<Order> ordersOfUser = response.getOrdersList();

        // Then
        Assertions.assertEquals(1, ordersOfUser.size());
    }

    @Test
    public void getOrdersOfUser_UserHasSubmittedManyOrder_ShouldReturnThreeOrders() {
        // Given: An user who has submitted multiple order
        String email = "jhon.doe@gmail.com";

        // ORDER 1
        String productCode1 = "PRD000001";
        ProductItemDTO mProduct1 = new ProductItemDTO(productCode1,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                1);
        Set<ProductItemDTO> mProductsOfOrder1 = Set.of(mProduct1);
        OrderDTO mOrder1 = new OrderDTO("ORD000001",
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                mProductsOfOrder1,
                email);

        // ORDER 2
        String productCode2 = "PRD000002";
        ProductItemDTO mProduct2 = new ProductItemDTO(productCode2,
                "Logitech Optical Mouse",
                "Electronic",
                14.00,
                2);
        Set<ProductItemDTO> mProductsOfOrder2 = Set.of(mProduct2);
        OrderDTO mOrder2 = new OrderDTO("ORD000002",
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                mProductsOfOrder2,
                email);

        // ORDER 3
        String productCode3 = "PRD000003";
        ProductItemDTO mProduct3 = new ProductItemDTO(productCode3,
                "IPhone 11",
                "Electronic",
                1099.00,
                1);
        Set<ProductItemDTO> mProductsOfOrder3 = Set.of(mProduct3);
        OrderDTO mOrder3 = new OrderDTO("ORD000003",
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                mProductsOfOrder3,
                email);
        when(ordersServices.getOrdersOfUser(email)).thenReturn(Set.of(mOrder1, mOrder2, mOrder3));

        // When
        GetOrdersOfUserRequest request = GetOrdersOfUserRequest.newBuilder()
                .setEmail(email)
                .build();
        GetOrdersOfUserResponse response = ordersGrpcApi.getOrdersOfUser(request);
        List<Order> ordersOfUser = response.getOrdersList();

        // Then
        Assertions.assertEquals(3, ordersOfUser.size());
    }

    @Test
    public void getOrdersOfUser_UserHasAnInconsistenOrder_ShouldReturnGrpcCode13Internal() {
        // Given: An user who has submitted an order which results inconsistent (The product in the order has not been found in the inventory)
        String email = "jhon.doe@gmail.com";
        InconsitentOrderException ioe = new InconsitentOrderException("Inconsistent Order", UUID.randomUUID());
        doThrow(ioe).when(ordersServices).getOrdersOfUser(email);

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    GetOrdersOfUserRequest request = GetOrdersOfUserRequest.newBuilder()
                            .setEmail(email)
                            .build();
                    ordersGrpcApi.getOrdersOfUser(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.INTERNAL, ex.getStatus()),
                () -> Assertions.assertEquals(Status.INTERNAL.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.INCONSISTENT_ORDER, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void getOrder_OrderExists_ShouldReturnTheOrder() {
        // Given: an existent order
        String existentOrderCode = "ORD000001";

        String productCode = "PRD000001";
        ProductItemDTO mProduct = new ProductItemDTO(productCode,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                1);
        Set<ProductItemDTO> mProductsOfOrder1 = Set.of(mProduct);
        OrderDTO mOrder = new OrderDTO(existentOrderCode,
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                mProductsOfOrder1,
                "jhon.doe@gmail.com");
        when(ordersServices.getOrder(existentOrderCode)).thenReturn(mOrder);

        // When
        GetOrderRequest request = GetOrderRequest.newBuilder()
                .setCode(existentOrderCode)
                .build();
        GetOrderResponse response = ordersGrpcApi.getOrder(request);
        Order order = response.getOrder();

        // Then
        Assertions.assertAll(
                () -> Assertions.assertNotNull(order),
                () -> Assertions.assertEquals(mOrder.getCode(), order.getCode()),
                () -> Assertions.assertEquals(mOrder.getStatus().name(), order.getStatus().name()),
                () -> Assertions.assertEquals(mOrder.getUserEmail(), order.getUserEmail()),
                () -> {
                    OffsetDateTime expectedSubmittedOnUTC = mOrder.getSubmittedOnUTC();
                    OffsetDateTime actualSubmittedOnUTC = utcDateTimeFromTimezone(order.getSubmittedOnUTC());
                    Assertions.assertEquals(expectedSubmittedOnUTC, actualSubmittedOnUTC);
                },
                () -> {
                    Set<ProductItemDTO> expectedProducts = mOrder.getProductItems();
                    List<ProductItem> actualProducts = order.getProductItemsList();
                    Assertions.assertEquals(expectedProducts.size(), actualProducts.size());

                    Set<String> expectedProductsCode = mOrder.getProductItems().stream().map(product -> product.getCode()).collect(Collectors.toSet());
                    Set<String> actualProductsCode = order.getProductItemsList().stream().map(product -> product.getCode()).collect(Collectors.toSet());
                    Assertions.assertEquals(expectedProductsCode, actualProductsCode);
                }
        );
    }

    @Test
    public void getOrder_OrderExistsButIsInconsistent_ShouldReturnGrpcCode13Internal() {
        // Given: an existent but inconsistent order  (The product in the order has not been found in the inventory)
        String existentButInconsistentOrderCode = "ORD000001";
        InconsitentOrderException ioe = new InconsitentOrderException("Inconsistent Order", UUID.randomUUID());
        doThrow(ioe).when(ordersServices).getOrder(existentButInconsistentOrderCode);

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    GetOrderRequest request = GetOrderRequest.newBuilder()
                            .setCode(existentButInconsistentOrderCode)
                            .build();
                    ordersGrpcApi.getOrder(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.INTERNAL, ex.getStatus()),
                () -> Assertions.assertEquals(Status.INTERNAL.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.INCONSISTENT_ORDER, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void getOrder_OrderDoesntExist_ShouldReturnGrpcCode5NotFound() {
        // Given: an inexistent order
        String inexistentOrderCode = "ORD000031";
        OrderNotFoundException onf = new OrderNotFoundException("Order not found", UUID.randomUUID());
        doThrow(onf).when(ordersServices).getOrder(inexistentOrderCode);

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    GetOrderRequest request = GetOrderRequest.newBuilder()
                            .setCode(inexistentOrderCode)
                            .build();
                    ordersGrpcApi.getOrder(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.ORDER_NOT_FOUND, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void addOrderForUser_ErrorOnUsersApiClient_ShouldReturnGrpcCode4DeadLineExceeded() {
        // Given: An error from checkUserExistence of the users-api client
        String inexistentUserEmail = "jhon.doe@google.com";
        Map<String, Integer> requestedStock = Map.of("PRD00001", 1,
                "PRD00002", 3);
        UsersApiClientException userApiClientException = new UsersApiClientException("User doesn't exist", UUID.randomUUID());
        doThrow(userApiClientException).when(ordersServices).addOrderForUser(inexistentUserEmail, requestedStock);

        // When
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    AddOrderForUserRequest request = AddOrderForUserRequest.newBuilder()
                            .setEmail(inexistentUserEmail)
                            .putAllProducts(requestedStock)
                            .build();
                    ordersGrpcApi.addOrderForUser(request);
                },
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.DEADLINE_EXCEEDED, ex.getStatus()),
                () -> Assertions.assertEquals(Status.DEADLINE_EXCEEDED.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.USERS_API_CLIENT_FAILURE, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void addOrderForUser_ErrorOnProductsApiClient_ShouldReturnGrpcCode4DeadLineExceeded() {
        // Given: An error from updateProductsStock of the products-api client
        String existentUserEmail = "jhon.doe@google.com";
        Map<String, Integer> requestedStock = Map.of("PRD00001", 1,
                "PRD00002", 3);
        ProductsApiClientException productsApiClientException = new ProductsApiClientException("Can't update products stocks", UUID.randomUUID());
        doThrow(productsApiClientException).when(ordersServices).addOrderForUser(existentUserEmail, requestedStock);

        // When
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    AddOrderForUserRequest request = AddOrderForUserRequest.newBuilder()
                            .setEmail(existentUserEmail)
                            .putAllProducts(requestedStock)
                            .build();
                    ordersGrpcApi.addOrderForUser(request);
                },
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.DEADLINE_EXCEEDED, ex.getStatus()),
                () -> Assertions.assertEquals(Status.DEADLINE_EXCEEDED.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.PRODUCTS_API_CLIENT_FAILURE, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void addOrderForUser_AllPreconditionsSatisfiedAndTheOrderCanBeAdded_ShouldReturnTheOrder() {
        // Given: All conditions are satisfied --> User Exists, Products of order Exists, Products of orders are available in stock
        String userEmail = "jhon.doe@google.com";

        // PRODUCT 1
        String productCode1 = "PRD00001";
        int product1RequestedStock = 2;
        ProductItemDTO mProduct1 = new ProductItemDTO(productCode1,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                product1RequestedStock);
        String productCode2 = "PRD00002";

        // PRODUCT 2
        int product2RequestedStock = 1;
        ProductItemDTO mProduct2 = new ProductItemDTO(productCode2,
                "Logitech Optical Mouse",
                "Electronic",
                14.00,
                product2RequestedStock);

        // ORDER WITH TWO PRODUCTS
        OrderDTO mOrder = new OrderDTO("ORD000001",
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                Set.of(mProduct1, mProduct2),
                userEmail);

        Map<String, Integer> requestedStock = Map.of(productCode1, product1RequestedStock,
                productCode2, product2RequestedStock);
        when(ordersServices.addOrderForUser(userEmail, requestedStock)).thenReturn(mOrder);

        // When
        AddOrderForUserRequest request = AddOrderForUserRequest.newBuilder()
                .setEmail(userEmail)
                .putAllProducts(requestedStock)
                .build();
        AddOrderForUserResponse response = ordersGrpcApi.addOrderForUser(request);
        Order order = response.getOrder();

        // Then
        Assertions.assertAll(
                () -> Assertions.assertNotNull(order),
                () -> Assertions.assertEquals(userEmail, order.getUserEmail()),
                () -> Assertions.assertNotNull(order.getSubmittedOnUTC()),
                () -> Assertions.assertEquals(OrderStatus.CREATED.name(), order.getStatus().name()),
                () -> {
                    // Check that the result contains exactly the two requested products
                    Assertions.assertEquals(requestedStock.size(), order.getProductItemsList().size());

                    // Check that the result contains the two requested products
                    Optional<ProductItem> optProduct1 = order.getProductItemsList().stream().filter(product -> product.getCode().equals(productCode1)).findFirst();
                    Assertions.assertTrue(optProduct1.isPresent());
                    Optional<ProductItem> optProduct2 = order.getProductItemsList().stream().filter(product -> product.getCode().equals(productCode2)).findFirst();
                    Assertions.assertTrue(optProduct2.isPresent());
                }
        );
    }

    @Test
    public void deleteOrder_OrderExists_ShouldReturnTheDeletedOrderId() {
        // Given
        String existentOrderCode = "ORD00001";
        String existingOrderId = new ObjectId().toString();
        when(ordersServices.deleteOrder(existentOrderCode)).thenReturn(existingOrderId);

        // When
        DeleteOrderRequest request = DeleteOrderRequest.newBuilder()
                .setCode(existentOrderCode)
                .build();
        DeleteOrderResponse response = ordersGrpcApi.deleteOrder(request);

        // Then
        Assertions.assertEquals(existingOrderId, response.getId());
    }

    @Test
    public void deleteOrder_OrderDoesntExist_ShouldReturnGrpcCode5NotFound() {
        // Given
        String notExistentOrderCode = "ORD00001";
        OrderNotFoundException onf = new OrderNotFoundException("Order not found", UUID.randomUUID());
        doThrow(onf).when(ordersServices).deleteOrder(notExistentOrderCode);

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    DeleteOrderRequest request = DeleteOrderRequest.newBuilder()
                            .setCode(notExistentOrderCode)
                            .build();
                    ordersGrpcApi.deleteOrder(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.ORDER_NOT_FOUND, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void updateOrderStatus_OrderDoesntExist_ShouldReturnGrpcCode5NotFound() {
        // Given
        String notExistentOrderCode = "ORD00001";
        OrderNotFoundException onf = new OrderNotFoundException("Order not found", UUID.randomUUID());
        doThrow(onf).when(ordersServices).updateOrderStatus(notExistentOrderCode, OrderStatus.SHIPPED);

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    UpdateOrderStatusRequest request = UpdateOrderStatusRequest.newBuilder()
                            .setCode(notExistentOrderCode)
                            .setStatus(it.millsoft.orders.api.interfaces.grpc.model.OrderStatus.SHIPPED)
                            .build();
                    ordersGrpcApi.updateOrderStatus(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.ORDER_NOT_FOUND, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void updateOrderStatus_WantUpdateWithTheSameStatus_ShouldReturnGrpcCode3InvalidArgument() {
        // Given
        String notExistentOrderCode = "ORD00001";
        OrderStatusNotUpdatableException osnu = new OrderStatusNotUpdatableException("Order's Status not updatable", UUID.randomUUID());
        doThrow(osnu).when(ordersServices).updateOrderStatus(notExistentOrderCode, OrderStatus.SHIPPED);

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    UpdateOrderStatusRequest request = UpdateOrderStatusRequest.newBuilder()
                            .setCode(notExistentOrderCode)
                            .setStatus(it.millsoft.orders.api.interfaces.grpc.model.OrderStatus.SHIPPED)
                            .build();
                    ordersGrpcApi.updateOrderStatus(request);
                }, // When
                "It should have raised the " + StatusRuntimeException.class.getName()
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        OrdersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(OrdersApiErrorResponse.getDefaultInstance()));
         String ordersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT, ex.getStatus()),
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.ORDER_STATUS_NOT_UPDATABLE, OrdersApiErrorEnum.valueOf(ordersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

        @Test
        public void updateOrderStatus_WantUpdateWithAValidStatus_ShouldReturnTheIdOfTheUpdatedOrder() {
            // Given
            String existentOrderCode = "ORD00001";
            String updatedOrderId = new ObjectId().toString();
            when(ordersServices.updateOrderStatus(existentOrderCode,OrderStatus.SHIPPED)).thenReturn(updatedOrderId);

            // When
            UpdateOrderStatusRequest request = UpdateOrderStatusRequest.newBuilder()
                    .setCode(existentOrderCode)
                    .setStatus(it.millsoft.orders.api.interfaces.grpc.model.OrderStatus.SHIPPED)
                    .build();
            UpdateOrderStatusResponse response = ordersGrpcApi.updateOrderStatus(request);

            // Then
            Assertions.assertEquals(response.getId(), updatedOrderId);
        }

    private OffsetDateTime utcDateTimeFromTimezone(Timestamp timestamp) {
        return OffsetDateTime.ofInstant(Instant.ofEpochSecond(timestamp.getSeconds(), timestamp.getNanos()),
                ZoneOffset.UTC);
    }

}