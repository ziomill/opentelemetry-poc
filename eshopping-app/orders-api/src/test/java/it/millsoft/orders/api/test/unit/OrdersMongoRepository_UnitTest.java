package it.millsoft.orders.api.test.unit;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import it.millsoft.orders.api.config.MongoConfig;
import it.millsoft.orders.api.dto.OrderStatus;
import it.millsoft.orders.api.model.Order;
import it.millsoft.orders.api.repositories.OrdersMongoRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@DataMongoTest
@Import(MongoConfig.class)
@DirtiesContext
@Testcontainers
public class OrdersMongoRepository_UnitTest {

    @Container
    private static MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:5.0.6"));

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        Objects.requireNonNull(mongoDBContainer);
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    @Autowired
    private OrdersMongoRepository ordersMongoRepository;
    @Autowired
    private MongoTemplate mongoTemplate;

    private static final String USER_WITH_SINGLE_ORDER_EMAIL = "jack.green@gmail.com";
    private static final String USER_WITH_SOME_ORDERS_EMAIL = "jhon.doe@gmail.com";

    private DBObject order1;
    private DBObject order2;
    private DBObject order3;

    @BeforeEach
    void initDatabase() {
        DBObject orderToSave1 = BasicDBObjectBuilder.start()
                .add("code", "ORD00001")
                .add("submittedOnUTC", OffsetDateTime.now(ZoneOffset.UTC))
                .add("status", OrderStatus.CREATED)
                .add("productItems", Map.of("PRD00001",1,
                                                "PRD00002",3))
                .add("userEmail", USER_WITH_SOME_ORDERS_EMAIL)
                .get();
        order1 = this.mongoTemplate.save(orderToSave1, "orders");

        DBObject orderToSave2 = BasicDBObjectBuilder.start()
                .add("code", "ORD00002")
                .add("submittedOnUTC", OffsetDateTime.now(ZoneOffset.UTC))
                .add("status", OrderStatus.DELIVERED)
                .add("productItems", Map.of("PRD00003",2,
                                             "PRD00004",2))
                .add("userEmail", USER_WITH_SOME_ORDERS_EMAIL)
                .get();
        order2 = this.mongoTemplate.save(orderToSave2, "orders");

        DBObject orderToSave3 = BasicDBObjectBuilder.start()
                .add("code", "ORD00003")
                .add("submittedOnUTC", OffsetDateTime.now(ZoneOffset.UTC))
                .add("status", OrderStatus.SHIPPED)
                .add("productItems", Map.of("PRD00001",4,
                                            "PRD00002",3))
                .add("userEmail", USER_WITH_SINGLE_ORDER_EMAIL)
                .get();
        order3 = this.mongoTemplate.save(orderToSave3, "orders");

    }

    @AfterEach
    void cleanUpDatabase() {
        mongoTemplate.getDb().drop();
    }

    @Test
    public void findOrdersOfUser_UserHaveExactlyOneOrder_ShouldReturnTheOrderOfTheUser() {
        // Given
        String email = USER_WITH_SINGLE_ORDER_EMAIL;

        // When
        Set<Order> orders = ordersMongoRepository.findOrdersOfUser(email);

        // Then
        Assertions.assertEquals(1,orders.size());
    }

    @Test
    public void findOrdersOfUser_UserHaveMultipleOrders_ShouldReturnTwoUserOrders() {
        // Given
        String email = USER_WITH_SOME_ORDERS_EMAIL;

        // When
        Set<Order> orders = ordersMongoRepository.findOrdersOfUser(email);

        // Then
        Assertions.assertEquals(2,orders.size());
    }

    @Test
    public void findOrdersOfUser_UserDoesntHaveOrders_ShouldReturnAnEmptyList() {
        // Given
        String email = "jasmes.red@gmail.com";

        // When
        Set<Order> orders = ordersMongoRepository.findOrdersOfUser(email);

        // Then
        Assertions.assertEquals(0,orders.size());
    }

    @Test
    public void findOrderByCode_OrderExists_ShouldTheOrderBePresent() {
        // Given
        String existingOrderCode = (String) order1.get("code");

        // When
        Optional<Order> order = ordersMongoRepository.findOrderByCode(existingOrderCode);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertTrue(order.isPresent()),
                () -> Assertions.assertEquals(order1.get("code"),order.get().getCode()),
                () -> {
                    OffsetDateTime submittedOnExpected = ((OffsetDateTime)order1.get("submittedOnUTC")).withNano(0);
                    OffsetDateTime submittedOnActual = order.get().getSubmittedOnUTC().withNano(0);
                    Assertions.assertEquals(submittedOnExpected,submittedOnActual);
                }, // Mongo truncates nano
                () -> Assertions.assertEquals(order1.get("status"),order.get().getStatus()),
                () -> Assertions.assertEquals(order1.get("productItems"),order.get().getProductItems()),
                () -> Assertions.assertEquals(order1.get("userEmail"),order.get().getUserEmail())
        );
    }

    @Test
    public void findOrderByCode_OrderDoesntExists_ShouldReturnAnEmptyOptional() {
        // Given
        String notExistingOrderCode = "ORD00022";

        // When
        Optional<Order> order = ordersMongoRepository.findOrderByCode(notExistingOrderCode);

        // Then
        Assertions.assertFalse(order.isPresent());
    }

    @Test
    public void countOrdersInPeriod_ThreeOrdersHasBeenCreatedInTheGivenPeriod_ShouldReturnThree(){
        // Given
        OffsetDateTime from = (OffsetDateTime) order1.get("submittedOnUTC");
        OffsetDateTime to = (OffsetDateTime) order3.get("submittedOnUTC");

        // When
        long createdOrders = ordersMongoRepository.countOrdersInPeriod(from,to);

        // Then
        Assertions.assertEquals(3,createdOrders);
    }

    @Test
    public void countOrdersInPeriod_TwoOrdersHasBeenCreatedInTheGivenPeriod_ShouldReturnTwo(){
        // Given
        OffsetDateTime from = (OffsetDateTime) order1.get("submittedOnUTC");
        OffsetDateTime to = (OffsetDateTime) order2.get("submittedOnUTC");

        // When
        long createdOrders = ordersMongoRepository.countOrdersInPeriod(from,to);

        // Then
        Assertions.assertEquals(2,createdOrders);
    }

    @Test
    public void countOrdersInPeriod_OneOrdersHasBeenCreatedInTheGivenPeriod_ShouldReturnOne(){
        // Given
        OffsetDateTime from = (OffsetDateTime) order1.get("submittedOnUTC");
        OffsetDateTime to = (OffsetDateTime) order1.get("submittedOnUTC");

        // When
        long createdOrders = ordersMongoRepository.countOrdersInPeriod(from,to);

        // Then
        Assertions.assertEquals(1,createdOrders);
    }

    @Test
    public void countOrdersInPeriod_NoOrdersHasBeenCreatedInTheGivenPeriod_ShouldReturnZero(){
        // Given
        OffsetDateTime from = OffsetDateTime.now(ZoneOffset.UTC).minusHours(1);
        OffsetDateTime to = ((OffsetDateTime) order1.get("submittedOnUTC")).minusSeconds(1);

        // When
        long createdOrders = ordersMongoRepository.countOrdersInPeriod(from,to);

        // Then
        Assertions.assertEquals(0,createdOrders);
    }

}
