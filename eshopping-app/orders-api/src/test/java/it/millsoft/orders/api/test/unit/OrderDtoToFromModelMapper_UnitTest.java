package it.millsoft.orders.api.test.unit;

import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.dto.OrderStatus;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.mappers.OrderDtoToFromModelMapper;
import it.millsoft.orders.api.model.Order;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class OrderDtoToFromModelMapper_UnitTest {

    @Test
    public void orderModelToDto_createTheOrderDTOWithFullProductItem_ShouldReturnTheOrderDTO() {
        // Given
        OrderDtoToFromModelMapper mapper = Mappers.getMapper( OrderDtoToFromModelMapper.class );

        String productCode1 = "PRD00001";
        Integer product1RequestedStock = 2;
        String productCode2 = "PRD00002";
        Integer product2RequestedStock = 3;

        Order mOrder = new Order();
        mOrder.setCode("ORD000001");
        mOrder.setUserEmail("jhon.doe@aol.com");
        mOrder.setStatus(OrderStatus.CREATED);
        mOrder.setSubmittedOnUTC(OffsetDateTime.now(ZoneOffset.UTC));
        mOrder.setProductItems(Map.of(productCode1,product1RequestedStock,
                                      productCode2,product2RequestedStock));
        ProductItemDTO pi1 = new ProductItemDTO(productCode1,"Lenovo X1 Carbon","Electronic",1899.00);
        ProductItemDTO pi2 = new ProductItemDTO(productCode2,"IPhone 8","Electronic",799.00);
        Set<ProductItemDTO> fullProducts = Set.of(pi1,pi2);

        // When
        OrderDTO dOrder = mapper.orderModelToDto(mOrder,fullProducts);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(mOrder.getCode(),dOrder.getCode()),
                () -> Assertions.assertEquals(mOrder.getUserEmail(),dOrder.getUserEmail()),
                () -> Assertions.assertEquals(mOrder.getStatus(),dOrder.getStatus()),
                () -> Assertions.assertEquals(mOrder.getSubmittedOnUTC(),dOrder.getSubmittedOnUTC()),
                () -> Assertions.assertEquals(mOrder.getProductItems().size(),dOrder.getProductItems().size()),
                () -> {
                    Set<ProductItemDTO> productItems = dOrder.getProductItems();

                    Optional<ProductItemDTO> optProductItem1 = productItems.stream().filter(productItem -> productItem.getCode().equals(productCode1)).findFirst();
                    Assertions.assertTrue(optProductItem1.isPresent());
                    ProductItemDTO productItem1 = optProductItem1.get();
                    Assertions.assertEquals(product1RequestedStock,productItem1.getRequestedStock());

                    Optional<ProductItemDTO> optProductItem2 = productItems.stream().filter(productItem -> productItem.getCode().equals(productCode2)).findFirst();
                    Assertions.assertTrue(optProductItem2.isPresent());
                    ProductItemDTO productItem2 = optProductItem2.get();
                    Assertions.assertEquals(product2RequestedStock,productItem2.getRequestedStock());
                }
        );
    }

}
