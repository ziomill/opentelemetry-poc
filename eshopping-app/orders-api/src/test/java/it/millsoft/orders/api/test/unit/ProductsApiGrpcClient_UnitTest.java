package it.millsoft.orders.api.test.unit;

import io.grpc.*;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.orders.api.dto.OrdersApiErrorEnum;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.exceptions.ProductsApiClientException;
import it.millsoft.orders.api.ext.ProductsApiClient;
import it.millsoft.orders.api.ext.grpc.products.api.model.*;
import it.millsoft.orders.api.test.config.ExtGrpcClientsUnitTestConfig;
import org.grpcmock.GrpcMock;
import org.grpcmock.junit5.GrpcMockExtension;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.grpcmock.GrpcMock.*;

@SpringJUnitConfig(ExtGrpcClientsUnitTestConfig.class)
@ExtendWith(GrpcMockExtension.class)
@TestPropertySource(
        properties = {
                "grpc.client.products-api.negotiation-type=plaintext"
        }
)
public class ProductsApiGrpcClient_UnitTest {

    private ManagedChannel channel;

    @BeforeEach
    void setupChannel() {
        channel = ManagedChannelBuilder.forAddress("localhost", GrpcMock.getGlobalPort())
                .usePlaintext()
                .build();
    }

    @AfterEach
    void shutdownChannel() {
        Optional.ofNullable(channel).ifPresent(ManagedChannel::shutdownNow);
    }

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        String uri = "static://localhost:" + GrpcMock.getGlobalPort();
        registry.add("grpc.client.products-api.address", () -> uri);
    }

    @Autowired
    private ProductsApiClient productsApiClient;

    @Test
    public void getProductsByCodes_RequestIsSuccessfull_ShouldReturnRequestedProducts() {
        // Given
        Product mProduct1 = Product.newBuilder()
                .setCode("PRD000001")
                .setCategory("Electronic")
                .setDescription("Lenovo X1 Carbon")
                .setPrice(1500.00)
                .setAvailableStock(3)
                .build();
        Product mProduct2 = Product.newBuilder()
                .setCode("PRD000002")
                .setCategory("Electronic")
                .setDescription("Monitor G100 Samsung")
                .setPrice(200.00)
                .setAvailableStock(1)
                .build();
        Set<Product> mProducts = Set.of(mProduct1,mProduct2);

        GetProductsByCodeResponse mResponse = GetProductsByCodeResponse.newBuilder()
                .addAllProduct(mProducts)
                .build();
        stubFor(unaryMethod(ProductsGrpcApiGrpc.getGetProductsByCodeMethod())
                .willReturn(mResponse));

        // Then
        Set<ProductItemDTO> requestedProducts = Assertions.assertDoesNotThrow(
                // When
                () -> {
                    Set<String> productsCode = Set.of(mProduct1.getCode(),mProduct2.getCode());
                    return productsApiClient.getProductsByCode(productsCode);
                }
        );

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(mProducts.size(), requestedProducts.size()),
                () -> {
                    Optional<ProductItemDTO> optActualProduct1 = requestedProducts.stream().filter(product -> product.getCode().equals(mProduct1.getCode())).findFirst();
                    Assertions.assertTrue(optActualProduct1.isPresent());
                    ProductItemDTO actualProduct1 = optActualProduct1.get();
                    Assertions.assertEquals(mProduct1.getCategory(), actualProduct1.getCategory());
                    Assertions.assertEquals(mProduct1.getPrice(), actualProduct1.getPrice());
                    Assertions.assertEquals(mProduct1.getDescription(), actualProduct1.getDescription());

                    Optional<ProductItemDTO> optActualProduct2 = requestedProducts.stream().filter(product -> product.getCode().equals(mProduct2.getCode())).findFirst();
                    Assertions.assertTrue(optActualProduct2.isPresent());
                    ProductItemDTO actualProduct2 = optActualProduct2.get();
                    Assertions.assertEquals(mProduct2.getCategory(), actualProduct2.getCategory());
                    Assertions.assertEquals(mProduct2.getPrice(), actualProduct2.getPrice());
                    Assertions.assertEquals(mProduct2.getDescription(), actualProduct2.getDescription());
                }
        );
    }

    @Test
    public void getProductsByCodes_RequestUnsuccessfull_ShouldThrowAnProductsApiClientException() {
        // Given
        ProductsApiErrorResponse.ApplicativeError applicativeError = ProductsApiErrorResponse.ApplicativeError.GENERIC_FAILURE;
        ProductsApiErrorResponse errorResponse = ProductsApiErrorResponse.newBuilder()
                .setType("error-type")
                .setInstance("error-instance")
                .setTitle("error-title")
                .setDetails("error-details")
                .setApplicativeError(applicativeError)
                .setStatus(Status.NOT_FOUND.getCode().value())
                .build();
        Metadata.Key<ProductsApiErrorResponse> shopErrorResponseKey = ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance());
        Metadata metadata = new Metadata();
        metadata.put(shopErrorResponseKey, errorResponse);
        Status status = Status.INTERNAL;
        StatusException statusException = status.asException(metadata);

        stubFor(unaryMethod(ProductsGrpcApiGrpc.getGetProductsByCodeMethod())
                .willReturn(exception(statusException)));

        // Then
        ProductsApiClientException ex = Assertions.assertThrows(
                ProductsApiClientException.class,
                () -> {
                    Set<String> productsCode = Set.of("PRD00001","PRD00002");
                    productsApiClient.getProductsByCode(productsCode);
                }, // When
                "It should have thrown the " + ProductsApiClientException.class.getName()
        );
        Assertions.assertAll(
                () -> Assertions.assertNotNull(ex.getErrorUUID()),
                () -> Assertions.assertNotNull(ex.getMessage()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.PRODUCTS_API_CLIENT_FAILURE, ex.getError())
        );
    }

    @Test
    public void updateProductsStock_RequestIsSuccessfull_ShouldReturnTheUpdatedStocks() {
        // Given
        String mProductCode1 = "PRD000001";
        String mProductCode2 = "PRD000002";
        Map<String, Integer> mUpdatedStock = Map.of(mProductCode1, 10,
                                                    mProductCode2, 3);
        UpdateStocksResponse mResponse = UpdateStocksResponse.newBuilder()
                .putAllCodesToStocks(mUpdatedStock)
                .build();

        stubFor(unaryMethod(ProductsGrpcApiGrpc.getUpdateStocksMethod())
                .willReturn(mResponse));

        // then
        Map<String, Integer> updatedStock = Assertions.assertDoesNotThrow(
                // When
                () -> {
                    Map<String, Integer> requestedStock = Map.of(mProductCode1, 1,
                                                                 mProductCode2, 2);
                    return productsApiClient.updateProductsStock(requestedStock);
                }
        );
        Assertions.assertEquals(mResponse.getCodesToStocksMap().size(),updatedStock.size());
        Assertions.assertEquals(mResponse.getCodesToStocksMap().get(mProductCode1), mUpdatedStock.get(mProductCode1));
        Assertions.assertEquals(mResponse.getCodesToStocksMap().get(mProductCode2), mUpdatedStock.get(mProductCode2));
    }

    @Test
    public void updateProductsStock_RequestIsUsuccessfull_ShouldThrowAnProductsApiClientException() {
        // Given
        ProductsApiErrorResponse.ApplicativeError applicativeError = ProductsApiErrorResponse.ApplicativeError.GENERIC_FAILURE;
        ProductsApiErrorResponse errorResponse = ProductsApiErrorResponse.newBuilder()
                .setType("error-type")
                .setInstance("error-instance")
                .setTitle("error-title")
                .setDetails("error-details")
                .setApplicativeError(applicativeError)
                .setStatus(Status.NOT_FOUND.getCode().value())
                .build();
        Metadata.Key<ProductsApiErrorResponse> shopErrorResponseKey = ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance());
        Metadata metadata = new Metadata();
        metadata.put(shopErrorResponseKey, errorResponse);
        Status status = Status.INTERNAL;
        StatusException statusException = status.asException(metadata);

        stubFor(unaryMethod(ProductsGrpcApiGrpc.getUpdateStocksMethod())
                .willReturn(exception(statusException)));

        // Then
        ProductsApiClientException ex = Assertions.assertThrows(
                ProductsApiClientException.class,
                () -> {
                    Map<String, Integer> requestedStock = Map.of("PRD000001", 1,
                                                                 "PRD000002", 2);
                    productsApiClient.updateProductsStock(requestedStock);
                }, // When
                "It should have thrown the " + ProductsApiClientException.class.getName()
        );
        Assertions.assertAll(
                () -> Assertions.assertNotNull(ex.getErrorUUID()),
                () -> Assertions.assertNotNull(ex.getMessage()),
                () -> Assertions.assertEquals(OrdersApiErrorEnum.PRODUCTS_API_CLIENT_FAILURE, ex.getError())
        );
    }

}


