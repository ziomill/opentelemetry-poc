package it.millsoft.orders.api.test.unit;

import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.ext.grpc.products.api.model.Product;
import it.millsoft.orders.api.mappers.ProductsApiClientMapper;
import it.millsoft.orders.api.mappers.ProductsApiClientMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.Optional;
import java.util.Set;


@SpringJUnitConfig
public class ProductsApiClientMapper_UnitTest {

    @TestConfiguration
    public static class MapperTestConfiguration {

        @Bean
        public ProductsApiClientMapper createProductsApiClientGrpcMapper() {
            return new ProductsApiClientMapperImpl();
        }
    }

    @Autowired
    private ProductsApiClientMapper mapper;

    @Test
    public void productGrpcToProductItemDto_createTheProductItemDTO_ShouldReturnTheProductItemDTO() {
        // Given
        Product gProduct = Product.newBuilder()
                .setCode("PRD000001")
                .setDescription("Lenovo X1 Carbon")
                .setCategory("Electronic")
                .setPrice(1899.00)
                .build();

        // When
        ProductItemDTO dProductItem = mapper.productGrpcToProductItemDto(gProduct);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(gProduct.getCode(), dProductItem.getCode()),
                () -> Assertions.assertEquals(gProduct.getDescription(), dProductItem.getDescription()),
                () -> Assertions.assertEquals(gProduct.getCategory(), dProductItem.getCategory()),
                () -> Assertions.assertEquals(gProduct.getPrice(), dProductItem.getPrice())
        );
    }

    @Test
    public void productsGrpcToProductItemsDto_createAListOfProductItemDTO_ShouldReturnTheListOfProductItemDTO() {
        // Given
        Product gProduct1 = Product.newBuilder()
                .setCode("PRD000001")
                .setDescription("Lenovo X1 Carbon")
                .setCategory("Electronic")
                .setPrice(1899.00)
                .build();

        Product gProduct2 = Product.newBuilder()
                .setCode("PRD000002")
                .setDescription("IPhone 8")
                .setCategory("Electronic")
                .setPrice(899.00)
                .build();

        Set<Product> gProducts = Set.of(gProduct1, gProduct2);

        // When
        Set<ProductItemDTO> dProductItems = mapper.productsGrpcToProductItemsDto(gProducts);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(gProducts.size(), dProductItems.size()),
                () -> {
                    Optional<ProductItemDTO> optProductItem1 = dProductItems.stream().filter(dProduct -> dProduct.getCode().equals(gProduct1.getCode())).findFirst();
                    Assertions.assertTrue(optProductItem1.isPresent());
                    ProductItemDTO productItem1 = optProductItem1.get();
                    Assertions.assertEquals(gProduct1.getCode(), productItem1.getCode());
                    Assertions.assertEquals(gProduct1.getDescription(), productItem1.getDescription());
                    Assertions.assertEquals(gProduct1.getCategory(), productItem1.getCategory());
                    Assertions.assertEquals(gProduct1.getPrice(), productItem1.getPrice());

                    Optional<ProductItemDTO> optProductItem2 = dProductItems.stream().filter(dProduct -> dProduct.getCode().equals(gProduct2.getCode())).findFirst();
                    Assertions.assertTrue(optProductItem2.isPresent());
                    ProductItemDTO productItem2 = optProductItem2.get();
                    Assertions.assertEquals(gProduct2.getCode(), productItem2.getCode());
                    Assertions.assertEquals(gProduct2.getDescription(), productItem2.getDescription());
                    Assertions.assertEquals(gProduct2.getCategory(), productItem2.getCategory());
                    Assertions.assertEquals(gProduct2.getPrice(), productItem2.getPrice());
                }
        );
    }

    @Test
    public void productRestToProductItemDto_createTheProductItemDTO_ShouldReturnTheProductItemDTO() {
        // Given
        it.millsoft.orders.api.ext.http.products.api.model.Product rProduct = new it.millsoft.orders.api.ext.http.products.api.model.Product();
        rProduct.setCode("PRD000001");
        rProduct.setDescription("Lenovo X1 Carbon");
        rProduct.setCategory("Electronic");
        rProduct.setPrice(1899.00);

        // When
        ProductItemDTO dProductItem = mapper.productRestToProductItemDto(rProduct);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(rProduct.getCode(), dProductItem.getCode()),
                () -> Assertions.assertEquals(rProduct.getDescription(), dProductItem.getDescription()),
                () -> Assertions.assertEquals(rProduct.getCategory(), dProductItem.getCategory()),
                () -> Assertions.assertEquals(rProduct.getPrice(), dProductItem.getPrice())
        );
    }

    @Test
    public void productsRestToProductItemsDto_createAListOfProductItemDTO_ShouldReturnTheListOfProductItemDTO() {
        // Given
        it.millsoft.orders.api.ext.http.products.api.model.Product rProduct1 = new it.millsoft.orders.api.ext.http.products.api.model.Product();
        rProduct1.setCode("PRD000001");
        rProduct1.setDescription("Lenovo X1 Carbon");
        rProduct1.setCategory("Electronic");
        rProduct1.setPrice(1899.00);

        it.millsoft.orders.api.ext.http.products.api.model.Product rProduct2 = new it.millsoft.orders.api.ext.http.products.api.model.Product();
        rProduct2.setCode("PRD000002");
        rProduct2.setDescription("IPhone 8");
        rProduct2.setCategory("Electronic");
        rProduct2.setPrice(899.00);

        Set<it.millsoft.orders.api.ext.http.products.api.model.Product> rProducts = Set.of(rProduct1, rProduct2);

        // When
        Set<ProductItemDTO> dProductItems = mapper.productsRestToProductItemsDto(rProducts);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(rProducts.size(), dProductItems.size()),
                () -> {
                    Optional<ProductItemDTO> optProductItem1 = dProductItems.stream().filter(dProduct -> dProduct.getCode().equals(rProduct1.getCode())).findFirst();
                    Assertions.assertTrue(optProductItem1.isPresent());
                    ProductItemDTO productItem1 = optProductItem1.get();
                    Assertions.assertEquals(rProduct1.getCode(), productItem1.getCode());
                    Assertions.assertEquals(rProduct1.getDescription(), productItem1.getDescription());
                    Assertions.assertEquals(rProduct1.getCategory(), productItem1.getCategory());
                    Assertions.assertEquals(rProduct1.getPrice(), productItem1.getPrice());

                    Optional<ProductItemDTO> optProductItem2 = dProductItems.stream().filter(dProduct -> dProduct.getCode().equals(rProduct2.getCode())).findFirst();
                    Assertions.assertTrue(optProductItem2.isPresent());
                    ProductItemDTO productItem2 = optProductItem2.get();
                    Assertions.assertEquals(rProduct2.getCode(), productItem2.getCode());
                    Assertions.assertEquals(rProduct2.getDescription(), productItem2.getDescription());
                    Assertions.assertEquals(rProduct2.getCategory(), productItem2.getCategory());
                    Assertions.assertEquals(rProduct2.getPrice(), productItem2.getPrice());
                }
        );
    }


}
