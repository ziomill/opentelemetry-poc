package it.millsoft.orders.api.test.unit;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import it.millsoft.orders.api.business.OrdersServices;
import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.dto.OrderStatus;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.exceptions.*;
import it.millsoft.orders.api.interfaces.rest.model.Order;
import it.millsoft.orders.api.interfaces.rest.model.OrdersApiErrorResponse;
import it.millsoft.orders.api.interfaces.rest.model.ProductItem;
import it.millsoft.orders.api.mappers.OrderRestToFromDtoMapper;
import it.millsoft.orders.api.mappers.ProductItemRestToFromDtoMapper;
import it.millsoft.orders.api.interfaces.rest.handlers.HttpExceptionsHandler;
import org.bson.types.ObjectId;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@WebMvcTest
@Import(HttpExceptionsHandler.class)
public class OrdersRestApi_UnitTest {

    @TestConfiguration
    static class ProductsRestApiConfiguration {

        @Bean
        OrderRestToFromDtoMapper createOrderRestToDtoMapper(){
            OrderRestToFromDtoMapper mapper = Mappers.getMapper( OrderRestToFromDtoMapper.class );
            return mapper;
        }

        @Bean
        ProductItemRestToFromDtoMapper productItemOrderRestToDtoMapper(){
            ProductItemRestToFromDtoMapper mapper = Mappers.getMapper( ProductItemRestToFromDtoMapper.class );
            return mapper;
        }
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private OrdersServices ordersServices;

    @Test
    public void getOrdersOfUser_UserDoesntHaveSubmittedAnyOrders_ShouldReturnAnEmptyListAndHttpCode200OK() throws Exception {
        // Given: An user who doesn't have submitted any order
        String email = "jhon.doe@gmail.com";
        when(ordersServices.getOrdersOfUser(email)).thenReturn(new HashSet<>());

        // When
        String uriTemplate = "/orders/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Set responseBody = mapper.readValue(responseBodyAsJson, Set.class);
        Assertions.assertTrue(responseBody.isEmpty());
    }

    @Test
    public void getOrdersOfUser_UserHasSubmittedSingleOrder_ShouldReturnAnOrderAndHttpCode200OK() throws Exception {
        // Given: An user who has submitted a single order
        String email = "jhon.doe@gmail.com";

        String productCode = "PRD000001";
        ProductItemDTO mProduct = new ProductItemDTO(productCode,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                1);
        Set<ProductItemDTO> mProducts = Set.of(mProduct);

        OrderDTO mOrder = new OrderDTO("ORD000001",
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                mProducts,
                email);
        when(ordersServices.getOrdersOfUser(email)).thenReturn(Set.of(mOrder));

        // When
        String uriTemplate = "/orders/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
//        Set<ProductItemDTO> responseBody = mapper.readValue(responseBodyAsJson, Set.class); // TODO: dovrbbero essere Order e non PRductItem
        Set<Order> responseBody = mapper.readValue(responseBodyAsJson, Set.class);
        Assertions.assertEquals(1, responseBody.size());
    }

    @Test
    public void getOrdersOfUser_UserHasSubmittedManyOrder_ShouldReturnThreeOrdersAndHttpCode200OK() throws Exception {
        // Given: An user who has submitted multiple order
        String email = "jhon.doe@gmail.com";

        // ORDER 1
        String productCode1 = "PRD000001";
        ProductItemDTO mProduct1 = new ProductItemDTO(productCode1,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                1);
        Set<ProductItemDTO> mProductsOfOrder1 = Set.of(mProduct1);
        OrderDTO mOrder1 = new OrderDTO("ORD000001",
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                mProductsOfOrder1,
                email);

        // ORDER 2
        String productCode2 = "PRD000002";
        ProductItemDTO mProduct2 = new ProductItemDTO(productCode2,
                "Logitech Optical Mouse",
                "Electronic",
                14.00,
                2);
        Set<ProductItemDTO> mProductsOfOrder2 = Set.of(mProduct2);
        OrderDTO mOrder2 = new OrderDTO("ORD000002",
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                mProductsOfOrder2,
                email);

        // ORDER 3
        String productCode3 = "PRD000003";
        ProductItemDTO mProduct3 = new ProductItemDTO(productCode3,
                "IPhone 11",
                "Electronic",
                1099.00,
                1);
        Set<ProductItemDTO> mProductsOfOrder3 = Set.of(mProduct3);
        OrderDTO mOrder3 = new OrderDTO("ORD000003",
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                mProductsOfOrder3,
                email);
        when(ordersServices.getOrdersOfUser(email)).thenReturn(Set.of(mOrder1, mOrder2, mOrder3));

        // When
        String uriTemplate = "/orders/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Set<Order> responseBody = mapper.readValue(responseBodyAsJson, Set.class);
        Assertions.assertEquals(3, responseBody.size());
    }

    @Test
    public void getOrdersOfUser_UserHasAnInconsistenOrder_ShouldReturnHttpCode500InternalServerError() throws Exception {
        // Given: An user who has submitted an order which results inconsistent (The product in the order has not been found in the inventory)
        String email = "jhon.doe@gmail.com";
        InconsitentOrderException ioe = new InconsitentOrderException("Inconsistent Order", UUID.randomUUID());
        doThrow(ioe).when(ordersServices).getOrdersOfUser(email);

        // When
        String uriTemplate = "/orders/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        OrdersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, OrdersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.INCONSISTENT_ORDER, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void getOrder_OrderExists_ShouldReturnTheOrderAndHttpCode200Ok() throws Exception {
        // Given: an existent order
        String existentOrderCode = "ORD000001";

        String productCode = "PRD000001";
        ProductItemDTO mProduct = new ProductItemDTO(productCode,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                1);
        Set<ProductItemDTO> mProductsOfOrder1 = Set.of(mProduct);
        OrderDTO mOrder = new OrderDTO(existentOrderCode,
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                mProductsOfOrder1,
                "jhon.doe@gmail.com");
        when(ordersServices.getOrder(existentOrderCode)).thenReturn(mOrder);

        // When
        String uriTemplate = "/order/{code}";
        Map<String, String> uriParams = Map.of("code",existentOrderCode);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Order order = mapper.readValue(responseBodyAsJson, Order.class);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertNotNull(order),
                () -> Assertions.assertEquals(mOrder.getCode(), order.getCode()),
                () -> Assertions.assertEquals(mOrder.getStatus().name(), order.getStatus().name()),
                () -> Assertions.assertEquals(mOrder.getUserEmail(), order.getUserEmail()),
                () -> {
                    OffsetDateTime expectedSubmittedOnUTC = mOrder.getSubmittedOnUTC();
                    OffsetDateTime actualSubmittedOnUTC = order.getSubmittedOnUTC();
                    Assertions.assertEquals(expectedSubmittedOnUTC, actualSubmittedOnUTC);
                },
                () -> {
                    Set<ProductItemDTO> expectedProducts = mOrder.getProductItems();
                    Set<ProductItem> actualProducts = order.getProductItems();
                    Assertions.assertEquals(expectedProducts.size(), actualProducts.size());

                    Set<String> expectedProductsCode = mOrder.getProductItems().stream().map(product -> product.getCode()).collect(Collectors.toSet());
                    Set<String> actualProductsCode = order.getProductItems().stream().map(product -> product.getCode()).collect(Collectors.toSet());
                    Assertions.assertEquals(expectedProductsCode, actualProductsCode);
                }
        );
    }

    @Test
    public void getOrder_OrderExistsButIsInconsistent_ShouldReturnHttpCode500InternalServerError() throws Exception {
        // Given: an existent but inconsistent order  (The product in the order has not been found in the inventory)
        String existentButInconsistentOrderCode = "ORD000001";
        InconsitentOrderException ioe = new InconsitentOrderException("Inconsistent Order", UUID.randomUUID());
        doThrow(ioe).when(ordersServices).getOrder(existentButInconsistentOrderCode);

        // When
        String uriTemplate = "/order/{code}";
        Map<String, String> uriParams = Map.of("code",existentButInconsistentOrderCode);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        OrdersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, OrdersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.INCONSISTENT_ORDER, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void getOrder_OrderDoesntExist_ShouldReturnHttpCode404NotFound() throws Exception {
        // Given: an inexistent order
        String inexistentOrderCode = "ORD000031";
        OrderNotFoundException onf = new OrderNotFoundException("Order not found", UUID.randomUUID());
        doThrow(onf).when(ordersServices).getOrder(inexistentOrderCode);

        // When
        String uriTemplate = "/order/{code}";
        Map<String, String> uriParams = Map.of("code",inexistentOrderCode);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        OrdersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, OrdersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.ORDER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void addOrderForUser_ErrorOnUsersApiClient_ShouldReturnHttpCode504BadGateway() throws Exception {
        // Given: An error from checkUserExistence of the users-api client
        String inexistentUserEmail = "jhon.doe@google.com";
        Map<String, Integer> requestedStock = Map.of("PRD00001", 1,
                "PRD00002", 3);
        UsersApiClientException userApiClientException = new UsersApiClientException("User doesn't exist", UUID.randomUUID());
        doThrow(userApiClientException).when(ordersServices).addOrderForUser(inexistentUserEmail, requestedStock);

        // When
        ObjectMapper mapper = new ObjectMapper();
        String bodyAsJson = mapper.writeValueAsString(requestedStock);

        String uriTemplate = "/order/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",inexistentUserEmail);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isBadGateway())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        OrdersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, OrdersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.BAD_GATEWAY.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.USERS_API_CLIENT_FAILURE, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void addOrderForUser_ErrorOnProductsApiClient_ShouldReturnHttpCode504BadGateway() throws Exception {
        // Given: An error from updateProductsStock of the products-api client
        String existentUserEmail = "jhon.doe@google.com";
        Map<String, Integer> requestedStock = Map.of("PRD00001", 1,
                "PRD00002", 3);
        ProductsApiClientException productsApiClientException = new ProductsApiClientException("User doesn't exist", UUID.randomUUID());
        doThrow(productsApiClientException).when(ordersServices).addOrderForUser(existentUserEmail, requestedStock);

        // When
        ObjectMapper mapper = new ObjectMapper();
        String bodyAsJson = mapper.writeValueAsString(requestedStock);

        String uriTemplate = "/order/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",existentUserEmail);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isBadGateway())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        OrdersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, OrdersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.BAD_GATEWAY.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.PRODUCTS_API_CLIENT_FAILURE, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void addOrderForUser_AllPreconditionsSatisfiedAndTheOrderCanBeAdded_ShouldReturnTheOrderAndHttpCode201Created() throws Exception {
        // Given: All conditions are satisfied --> User Exists, Products of order Exists, Products of orders are available in stock
        String userEmail = "jhon.doe@google.com";

        // PRODUCT 1
        String productCode1 = "PRD00001";
        int product1RequestedStock = 2;
        ProductItemDTO mProduct1 = new ProductItemDTO(productCode1,
                "Lenovo X1 Carbon",
                "Electronic",
                1799.00,
                product1RequestedStock);
        String productCode2 = "PRD00002";

        // PRODUCT 2
        int product2RequestedStock = 1;
        ProductItemDTO mProduct2 = new ProductItemDTO(productCode2,
                "Logitech Optical Mouse",
                "Electronic",
                14.00,
                product2RequestedStock);

        // ORDER WITH TWO PRODUCTS
        OrderDTO mOrder = new OrderDTO("ORD000001",
                OffsetDateTime.now(ZoneOffset.UTC),
                OrderStatus.CREATED,
                Set.of(mProduct1, mProduct2),
                userEmail);

        Map<String, Integer> requestedStock = Map.of(productCode1, product1RequestedStock,
                productCode2, product2RequestedStock);
        when(ordersServices.addOrderForUser(userEmail, requestedStock)).thenReturn(mOrder);

        // When
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);
        String bodyAsJson = mapper.writeValueAsString(requestedStock);

        String uriTemplate = "/order/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",userEmail);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Order order = mapper.readValue(responseBodyAsJson, Order.class);
        Assertions.assertAll(
                () -> Assertions.assertNotNull(order),
                () -> Assertions.assertEquals(userEmail, order.getUserEmail()),
                () -> Assertions.assertNotNull(order.getSubmittedOnUTC()),
                () -> Assertions.assertEquals(OrderStatus.CREATED.name(), order.getStatus().name()),
                () -> {
                    // Check that the result contains exactly the two requested products
                    Assertions.assertEquals(requestedStock.size(), order.getProductItems().size());

                    // Check that the result contains the two requested products
                    Optional<ProductItem> optProduct1 = order.getProductItems().stream().filter(product -> product.getCode().equals(productCode1)).findFirst();
                    Assertions.assertTrue(optProduct1.isPresent());
                    Optional<ProductItem> optProduct2 = order.getProductItems().stream().filter(product -> product.getCode().equals(productCode2)).findFirst();
                    Assertions.assertTrue(optProduct2.isPresent());
                }
        );
    }

    @Test
    public void deleteOrder_OrderExists_ShouldReturnTheDeletedOrderIdAndHttpCode200OK() throws Exception {
        // Given
        String existentOrderCode = "ORD00001";
        String existingOrderId = new ObjectId().toString();
        when(ordersServices.deleteOrder(existentOrderCode)).thenReturn(existingOrderId);

        // When
        String uriTemplate = "/order/{code}";
        Map<String, String> uriParams = Map.of("code",existentOrderCode);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        this.mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is(existingOrderId)));
    }

    @Test
    public void deleteOrder_OrderDoesntExist_ShouldReturnHttpCode404NotFound() throws Exception {
        // Given
        String notExistentOrderCode = "ORD00001";
        OrderNotFoundException onf = new OrderNotFoundException("Order not found", UUID.randomUUID());
        doThrow(onf).when(ordersServices).deleteOrder(notExistentOrderCode);

        // When
        String uriTemplate = "/order/{code}";
        Map<String, String> uriParams = Map.of("code",notExistentOrderCode);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        OrdersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, OrdersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.ORDER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateOrderStatus_OrderDoesntExist_ShouldReturnHttpCode404NotFound() throws Exception {
        // Given
        String notExistentOrderCode = "ORD00001";
        OrderNotFoundException onf = new OrderNotFoundException("Order not found", UUID.randomUUID());
        doThrow(onf).when(ordersServices).updateOrderStatus(notExistentOrderCode, OrderStatus.SHIPPED);

        // When
        String uriTemplate = "/order/{code}/status";
        Map<String, String> uriParams = Map.of("code",notExistentOrderCode);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.patch(uri)
                        .queryParam("status", it.millsoft.orders.api.interfaces.rest.model.OrderStatus.SHIPPED.name())
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        OrdersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, OrdersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.ORDER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateOrderStatus_WantUpdateWithTheSameStatus_ShouldReturnHttpCode400BadRequest() throws Exception {
        // Given
        String existentOrderCode = "ORD00001";
        OrderStatusNotUpdatableException osnu = new OrderStatusNotUpdatableException("Order's Status not updatable", UUID.randomUUID());
        doThrow(osnu).when(ordersServices).updateOrderStatus(existentOrderCode, OrderStatus.SHIPPED);

        // When
        String uriTemplate = "/order/{code}/status";
        Map<String, String> uriParams = Map.of("code",existentOrderCode);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.patch(uri)
                        .queryParam("status", it.millsoft.orders.api.interfaces.rest.model.OrderStatus.SHIPPED.name())
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        OrdersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, OrdersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.CONFLICT.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(OrdersApiErrorResponse.ApplicativeErrorEnum.ORDER_STATUS_NOT_UPDATABLE, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateOrderStatus_WantUpdateWithAValidStatus_ShouldReturnTheIdOfTheUpdatedOrderAndHttpCode200Ok() throws Exception {
        // Given
        String existentOrderCode = "ORD00001";
        String updatedOrderId = new ObjectId().toString();
        when(ordersServices.updateOrderStatus(existentOrderCode, OrderStatus.SHIPPED)).thenReturn(updatedOrderId);

        // When
        String uriTemplate = "/order/{code}/status";
        Map<String, String> uriParams = Map.of("code",existentOrderCode);
        URI uri = UriComponentsBuilder.fromUriString(uriTemplate).buildAndExpand(uriParams).toUri();

        this.mockMvc.perform(MockMvcRequestBuilders.patch(uri)
                        .queryParam("status", it.millsoft.orders.api.interfaces.rest.model.OrderStatus.SHIPPED.name())
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is(updatedOrderId)));
    }

}