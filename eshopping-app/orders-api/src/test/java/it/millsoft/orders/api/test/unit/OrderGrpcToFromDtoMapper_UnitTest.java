package it.millsoft.orders.api.test.unit;

import com.google.protobuf.Timestamp;
import it.millsoft.orders.api.dto.OrderDTO;
import it.millsoft.orders.api.dto.ProductItemDTO;
import it.millsoft.orders.api.interfaces.grpc.model.Order;
import it.millsoft.orders.api.interfaces.grpc.model.OrderStatus;
import it.millsoft.orders.api.interfaces.grpc.model.ProductItem;
import it.millsoft.orders.api.mappers.OrderGrpcToFromDtoMapper;
import it.millsoft.orders.api.mappers.OrderGrpcToFromDtoMapperImpl;
import it.millsoft.orders.api.mappers.ProductItemGrpcToFromDtoMapper;
import it.millsoft.orders.api.mappers.ProductItemGrpcToFromDtoMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Optional;
import java.util.Set;


@SpringJUnitConfig
public class OrderGrpcToFromDtoMapper_UnitTest {

    @TestConfiguration
    public static class MapperTestConfiguration {

        @Bean
        public OrderGrpcToFromDtoMapper createOrderGrpcToFromDtoMapper() {
            return new OrderGrpcToFromDtoMapperImpl();
        }
        @Bean
        public ProductItemGrpcToFromDtoMapper createProductItemGrpcToFromDtoMapper() {
            return new ProductItemGrpcToFromDtoMapperImpl();
        }
    }

    @Autowired
    private OrderGrpcToFromDtoMapper mapper;

    @Test
    public void orderGrpcToDto_createTheOrderDTO_ShouldReturnTheOrderDTO() {
        // Given
        String productItemCode1 = "PRD000001";
        ProductItem gProductItem1 = ProductItem.newBuilder()
                .setCode(productItemCode1)
                .setDescription("Lenovo X1 Carbon")
                .setCategory("Electronic")
                .setPrice(1899.00)
                .setRequestedStock(2)
                .build();
        String productItemCode2 = "PRD000002";
        ProductItem gProductItem2 = ProductItem.newBuilder()
                .setCode(productItemCode2)
                .setDescription("IPhone 8")
                .setCategory("Electronic")
                .setPrice(899.00)
                .setRequestedStock(1)
                .build();
        Set<ProductItem> gProductItems = Set.of(gProductItem1,gProductItem2);

        Order gOrder = Order.newBuilder()
                .setCode("ORD000001")
                .setUserEmail("jhon.doe@gmail.com")
                .setSubmittedOnUTC(generateTimestamp())
                .setStatus(OrderStatus.CREATED)
                .addAllProductItems(gProductItems)
                .build();

        // When
        OrderDTO dOrder = mapper.orderGrpcToDto(gOrder);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(gOrder.getCode(),dOrder.getCode()),
                () -> Assertions.assertEquals(gOrder.getStatus().name(),dOrder.getStatus().name()),
                () -> Assertions.assertEquals(gOrder.getSubmittedOnUTC(),toTimestamp(dOrder.getSubmittedOnUTC())),
                () -> Assertions.assertEquals(gOrder.getUserEmail(),dOrder.getUserEmail()),
                () -> Assertions.assertEquals(gOrder.getProductItemsList().size(),dOrder.getProductItems().size()),
                () -> {
                    Optional<ProductItemDTO> optProductItem1 = dOrder.getProductItems().stream()
                            .filter(dProductItem -> dProductItem.getCode().equals(productItemCode1))
                            .findFirst();
                    Assertions.assertTrue(optProductItem1.isPresent());
                    ProductItemDTO dProductItem1 = optProductItem1.get();

                    Assertions.assertEquals(gProductItem1.getCode(),dProductItem1.getCode());
                    Assertions.assertEquals(gProductItem1.getCategory(),dProductItem1.getCategory());
                    Assertions.assertEquals(gProductItem1.getPrice(),dProductItem1.getPrice());
                    Assertions.assertEquals(gProductItem1.getDescription(),dProductItem1.getDescription());
                    Assertions.assertEquals(gProductItem1.getRequestedStock(),dProductItem1.getRequestedStock());

                    Optional<ProductItemDTO> optProductItem2 = dOrder.getProductItems().stream()
                            .filter(dProductItem -> dProductItem.getCode().equals(productItemCode2))
                            .findFirst();
                    Assertions.assertTrue(optProductItem2.isPresent());
                    ProductItemDTO dProductItem2 = optProductItem2.get();

                    Assertions.assertEquals(gProductItem2.getCode(),dProductItem2.getCode());
                    Assertions.assertEquals(gProductItem2.getCategory(),dProductItem2.getCategory());
                    Assertions.assertEquals(gProductItem2.getPrice(),dProductItem2.getPrice());
                    Assertions.assertEquals(gProductItem2.getDescription(),dProductItem2.getDescription());
                    Assertions.assertEquals(gProductItem2.getRequestedStock(),dProductItem2.getRequestedStock());
                }
        );
    }

    @Test
    public void prderDtoToGrpc_createTheOrderGrpc_ShouldReturnTheOrderGrpc() {
        // Given
        String productItemCode1 = "PRD000001";
        ProductItemDTO dProductItem1 = new ProductItemDTO();
        dProductItem1.setCode(productItemCode1);
        dProductItem1.setDescription("Lenovo X1 Carbon");
        dProductItem1.setCategory("Electronic");
        dProductItem1.setPrice(1899.00);
        dProductItem1.setRequestedStock(2);

        String productItemCode2 = "PRD000002";
        ProductItemDTO dProductItem2 = new ProductItemDTO();
        dProductItem2.setCode(productItemCode2);
        dProductItem2.setDescription("IPhone 8");
        dProductItem2.setCategory("Electronic");
        dProductItem2.setPrice(899.00);
        dProductItem2.setRequestedStock(1);
        Set<ProductItemDTO> dProductItems = Set.of(dProductItem1,dProductItem2);

        OrderDTO dOrder = new OrderDTO("ORD000001",
                OffsetDateTime.now(ZoneOffset.UTC),
                it.millsoft.orders.api.dto.OrderStatus.CREATED,
                dProductItems,
                "jhon.doe@gmail.com");

        // When
        Order gOrder = mapper.orderDtoToGrpc(dOrder);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(dOrder.getCode(),gOrder.getCode()),
                () -> Assertions.assertEquals(dOrder.getStatus().name(),gOrder.getStatus().name()),
                () -> Assertions.assertEquals(dOrder.getSubmittedOnUTC(),toOffsetDateTime(gOrder.getSubmittedOnUTC())),
                () -> Assertions.assertEquals(dOrder.getUserEmail(),gOrder.getUserEmail()),
                () -> Assertions.assertEquals(dOrder.getProductItems().size(),gOrder.getProductItemsList().size()),
                () -> {
                    Optional<ProductItem> optProductItem1 = gOrder.getProductItemsList().stream()
                            .filter(gProductItem -> gProductItem.getCode().equals(productItemCode1))
                            .findFirst();
                    Assertions.assertTrue(optProductItem1.isPresent());
                    ProductItem gProductItem1 = optProductItem1.get();

                    Assertions.assertEquals(dProductItem1.getCode(),gProductItem1.getCode());
                    Assertions.assertEquals(dProductItem1.getCategory(),gProductItem1.getCategory());
                    Assertions.assertEquals(dProductItem1.getPrice(),gProductItem1.getPrice());
                    Assertions.assertEquals(dProductItem1.getDescription(),gProductItem1.getDescription());
                    Assertions.assertEquals(dProductItem1.getRequestedStock(),gProductItem1.getRequestedStock());

                    Optional<ProductItem> optProductItem2 = gOrder.getProductItemsList().stream()
                            .filter(gProductItem -> gProductItem.getCode().equals(productItemCode2))
                            .findFirst();
                    Assertions.assertTrue(optProductItem2.isPresent());
                    ProductItem gProductItem2 = optProductItem2.get();

                    Assertions.assertEquals(dProductItem2.getCode(),gProductItem2.getCode());
                    Assertions.assertEquals(dProductItem2.getCategory(),gProductItem2.getCategory());
                    Assertions.assertEquals(dProductItem2.getPrice(),gProductItem2.getPrice());
                    Assertions.assertEquals(dProductItem2.getDescription(),gProductItem2.getDescription());
                    Assertions.assertEquals(dProductItem2.getRequestedStock(),gProductItem2.getRequestedStock());
                }
        );
    }

    @Test
    public void ordersGrpcToDto_createAListOfOrderDto_ShouldReturnTheListOfOrderDto() {
        // Given
        String productItemCode1 = "PRD000001";
        ProductItem gProductItem1 = ProductItem.newBuilder()
                .setCode(productItemCode1)
                .setDescription("Lenovo X1 Carbon")
                .setCategory("Electronic")
                .setPrice(1899.00)
                .setRequestedStock(2)
                .build();
        String productItemCode2 = "PRD000002";
        ProductItem gProductItem2 = ProductItem.newBuilder()
                .setCode(productItemCode2)
                .setDescription("IPhone 8")
                .setCategory("Electronic")
                .setPrice(899.00)
                .setRequestedStock(1)
                .build();
        Set<ProductItem> gProductItems = Set.of(gProductItem1,gProductItem2);

        Order gOrder1 = Order.newBuilder()
                .setCode("ORD000001")
                .setUserEmail("jhon.doe@gmail.com")
                .setSubmittedOnUTC(generateTimestamp())
                .setStatus(OrderStatus.CREATED)
                .addAllProductItems(gProductItems)
                .build();

        Order gOrder2 = Order.newBuilder()
                .setCode("ORD000002")
                .setUserEmail("andy.green@gmail.com")
                .setSubmittedOnUTC(generateTimestamp())
                .setStatus(OrderStatus.CREATED)
                .addAllProductItems(gProductItems)
                .build();

        Set<Order> gOrders = Set.of(gOrder1,gOrder2);

        // When
        Set<OrderDTO> dOrders = mapper.ordersGrpcToDto(gOrders);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(gOrders.size(),dOrders.size()),
                () -> {
                    Optional<OrderDTO> optOrder1 = dOrders.stream().filter(dOrder -> dOrder.getCode().equals(gOrder1.getCode())).findFirst();
                    Assertions.assertTrue(optOrder1.isPresent());
                    OrderDTO dOrder1 = optOrder1.get();
                    Assertions.assertEquals(gOrder1.getCode(),dOrder1.getCode());
                    Assertions.assertEquals(gOrder1.getStatus().name(),dOrder1.getStatus().name());
                    Assertions.assertEquals(gOrder1.getSubmittedOnUTC(),toTimestamp(dOrder1.getSubmittedOnUTC()));
                    Assertions.assertEquals(gOrder1.getUserEmail(),dOrder1.getUserEmail());
                    Assertions.assertEquals(gOrder1.getProductItemsList().size(),dOrder1.getProductItems().size());

                    Optional<OrderDTO> optOrder2 = dOrders.stream().filter(dOrder -> dOrder.getCode().equals(gOrder2.getCode())).findFirst();
                    Assertions.assertTrue(optOrder2.isPresent());
                    OrderDTO dOrder2 = optOrder2.get();
                    Assertions.assertEquals(gOrder2.getCode(),dOrder2.getCode());
                    Assertions.assertEquals(gOrder2.getStatus().name(),dOrder2.getStatus().name());
                    Assertions.assertEquals(gOrder2.getSubmittedOnUTC(),toTimestamp(dOrder2.getSubmittedOnUTC()));
                    Assertions.assertEquals(gOrder2.getUserEmail(),dOrder2.getUserEmail());
                    Assertions.assertEquals(gOrder2.getProductItemsList().size(),dOrder2.getProductItems().size());
                }
        );
    }

    @Test
    public void ordersDtoToGrpc_createAListOfOrderGrpc_ShouldReturnTheListOfOrderGrpc() {
        // Given
        String productItemCode1 = "PRD000001";
        ProductItemDTO dProductItem1 = new ProductItemDTO();
        dProductItem1.setCode(productItemCode1);
        dProductItem1.setDescription("Lenovo X1 Carbon");
        dProductItem1.setCategory("Electronic");
        dProductItem1.setPrice(1899.00);
        dProductItem1.setRequestedStock(2);

        String productItemCode2 = "PRD000002";
        ProductItemDTO dProductItem2 = new ProductItemDTO();
        dProductItem2.setCode(productItemCode2);
        dProductItem2.setDescription("IPhone 8");
        dProductItem2.setCategory("Electronic");
        dProductItem2.setPrice(899.00);
        dProductItem2.setRequestedStock(1);
        Set<ProductItemDTO> dProductItems = Set.of(dProductItem1,dProductItem2);

        OrderDTO dOrder1 = new OrderDTO("ORD000001",
                OffsetDateTime.now(ZoneOffset.UTC),
                it.millsoft.orders.api.dto.OrderStatus.CREATED,
                dProductItems,
                "jhon.doe@gmail.com");

        OrderDTO dOrder2 = new OrderDTO("ORD000002",
                OffsetDateTime.now(ZoneOffset.UTC),
                it.millsoft.orders.api.dto.OrderStatus.CREATED,
                dProductItems,
                "andy.green@gmail.com");

        Set<OrderDTO> dOrders = Set.of(dOrder1,dOrder2);

        // When
        Set<Order> gOrders = mapper.ordersDtoToGrpc(dOrders);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(dOrders.size(),gOrders.size()),
                () -> {
                    Optional<Order> optOrder1 = gOrders.stream().filter(gOrder -> gOrder.getCode().equals(dOrder1.getCode())).findFirst();
                    Assertions.assertTrue(optOrder1.isPresent());
                    Order gOrder1 = optOrder1.get();
                    Assertions.assertEquals(dOrder1.getCode(),gOrder1.getCode());
                    Assertions.assertEquals(dOrder1.getStatus().name(),gOrder1.getStatus().name());
                    Assertions.assertEquals(dOrder1.getSubmittedOnUTC(),toOffsetDateTime(gOrder1.getSubmittedOnUTC()));
                    Assertions.assertEquals(dOrder1.getUserEmail(),gOrder1.getUserEmail());
                    Assertions.assertEquals(dOrder1.getProductItems().size(),gOrder1.getProductItemsList().size());

                    Optional<Order> optOrder2 = gOrders.stream().filter(gOrder -> gOrder.getCode().equals(dOrder2.getCode())).findFirst();
                    Assertions.assertTrue(optOrder2.isPresent());
                    Order gOrder2 = optOrder2.get();
                    Assertions.assertEquals(dOrder2.getCode(),gOrder2.getCode());
                    Assertions.assertEquals(dOrder2.getStatus().name(),gOrder2.getStatus().name());
                    Assertions.assertEquals(dOrder2.getSubmittedOnUTC(),toOffsetDateTime(gOrder2.getSubmittedOnUTC()));
                    Assertions.assertEquals(dOrder2.getUserEmail(),gOrder2.getUserEmail());
                    Assertions.assertEquals(dOrder2.getProductItems().size(),gOrder2.getProductItemsList().size());
                }
        );
    }

    private Timestamp generateTimestamp() {
        Instant submittedOnUTCAsInstant = OffsetDateTime.now(ZoneOffset.UTC).toInstant();
        Timestamp result = Timestamp.newBuilder()
                .setSeconds(submittedOnUTCAsInstant.getEpochSecond())
                .setNanos(submittedOnUTCAsInstant.getNano())
                .build();
        return result;
    }

    private Timestamp toTimestamp(OffsetDateTime offsetDateTime) {
        Timestamp result = Timestamp.newBuilder()
                .setSeconds(offsetDateTime.toEpochSecond())
                .setNanos(offsetDateTime.getNano())
                .build();
        return result;
    }

    private OffsetDateTime toOffsetDateTime(Timestamp timestamp) {
        OffsetDateTime result = Instant.ofEpochSecond(timestamp.getSeconds(), timestamp.getNanos())
                .atOffset(ZoneOffset.UTC);
        return result;
    }

}
