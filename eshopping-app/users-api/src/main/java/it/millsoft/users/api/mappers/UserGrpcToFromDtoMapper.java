package it.millsoft.users.api.mappers;

import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.interfaces.grpc.model.User;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.Set;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface UserGrpcToFromDtoMapper {

    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "email", source = "email")
    UserDTO userGrpcToDto(User user);

    Set<UserDTO> usersGrpcToDto(Set<User> users);

    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "email", source = "email")
    User userDtoToGrpc(UserDTO user);

    Set<User> usersDtoToGrpc(Set<UserDTO> users);



}
