package it.millsoft.users.api.mappers;

import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.interfaces.rest.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.Set;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserRestToFromDtoMapper {

    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "email", source = "email")
    UserDTO userRestToDto(User user);

    Set<UserDTO> usersRestToDto(Set<User> users);

    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "email", source = "email")
    User userDtoToRest(UserDTO person);

    Set<User> usersDtoToRest(Set<UserDTO> users);



}
