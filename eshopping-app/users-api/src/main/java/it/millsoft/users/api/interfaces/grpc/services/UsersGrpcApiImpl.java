package it.millsoft.users.api.interfaces.grpc.services;

import io.grpc.stub.StreamObserver;
import it.millsoft.users.api.business.UsersServices;
import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.interfaces.grpc.model.*;
import it.millsoft.users.api.mappers.UserGrpcToFromDtoMapper;
import net.devh.boot.grpc.server.service.GrpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

@GrpcService
public class UsersGrpcApiImpl extends UsersGrpcApiGrpc.UsersGrpcApiImplBase {

    private static Logger LOGGER = LoggerFactory.getLogger(UsersGrpcApiImpl.class);

    @Autowired
    private UsersServices usersServices;

    @Autowired
    private UserGrpcToFromDtoMapper mapper;

    @Override
    public void getUserByEmail(GetUserByEmailRequest request, StreamObserver<GetUserByEmailResponse> responseObserver) {
        // Call Business
        UserDTO dUser = usersServices.getUserByEmail(request.getEmail());

        // Build GRPC Result
        User gResult = mapper.userDtoToGrpc(dUser);

        // Build GRPC Response
        GetUserByEmailResponse response = GetUserByEmailResponse.newBuilder()
                                                                .setUser(gResult)
                                                                .build();
        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void getUsers(GetUsersRequest request, StreamObserver<GetUsersResponse> responseObserver) {
        // Call Business
        Set<UserDTO> dUsers = usersServices.getUsers();

        // Build GRPC Result
        Set<User> gUsers = mapper.usersDtoToGrpc(dUsers);

        // Build GRPC Response
        GetUsersResponse response = GetUsersResponse.newBuilder()
                                                    .addAllUsers(gUsers)
                                                    .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void addUser(AddUserRequest request, StreamObserver<AddUserResponse> responseObserver) {
        // Call Business
        String createdUserId = usersServices.addUser(request.getUser().getName(),
                                               request.getUser().getSurname(),
                                               request.getUser().getEmail());

        // Build GRPC Response
        AddUserResponse response = AddUserResponse.newBuilder()
                .setId(createdUserId)
                .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void updateUser(UpdateUserRequest request, StreamObserver<UpdateUserResponse> responseObserver) {
        // Call Business
        UserDTO dUser = mapper.userGrpcToDto(request.getUser());
        String updatedUserId = usersServices.updateUser(request.getEmail(),
                                                  dUser);

        // Build GRPC Response
        UpdateUserResponse response = UpdateUserResponse.newBuilder()
                .setId(updatedUserId)
                .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void deleteUser(DeleteUserRequest request, StreamObserver<DeleteUserResponse> responseObserver) {
        // Call Business
        String deleteUserId = usersServices.deleteUser(request.getEmail());

        // Build GRPC Response
        DeleteUserResponse response = DeleteUserResponse.newBuilder()
                .setId(deleteUserId)
                .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void checkUserExistence(CheckUserExistenceRequest request, StreamObserver<CheckUserExistenceResponse> responseObserver) {
        usersServices.checkUserExistence(request.getEmail());

        // Build GRPC Response
        CheckUserExistenceResponse response = CheckUserExistenceResponse.newBuilder()
                                                                        .setExists(true)
                                                                        .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

}
