package it.millsoft.users.api.exceptions;

import it.millsoft.users.api.dto.UsersApiErrorEnum;
import lombok.Getter;

import java.util.UUID;

@Getter
public abstract class UsersApiException extends RuntimeException {
    private UsersApiErrorEnum error;
    private UUID errorUUID;

    public UsersApiException(String message, UsersApiErrorEnum error, UUID errorUUID) {
        super(message);
        this.error = error;
        this.errorUUID = errorUUID;
    }

}
