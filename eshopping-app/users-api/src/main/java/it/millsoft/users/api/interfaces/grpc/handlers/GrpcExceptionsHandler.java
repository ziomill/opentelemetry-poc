package it.millsoft.users.api.interfaces.grpc.handlers;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.users.api.commons.Utils;
import it.millsoft.users.api.dto.UsersApiErrorEnum;
import it.millsoft.users.api.exceptions.UserAlreadyExistsException;
import it.millsoft.users.api.exceptions.UserNotFoundException;
import it.millsoft.users.api.exceptions.UsersApiException;
import it.millsoft.users.api.interfaces.grpc.model.UsersApiErrorResponse;
import net.devh.boot.grpc.server.advice.GrpcAdvice;
import net.devh.boot.grpc.server.advice.GrpcExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

@GrpcAdvice
public class GrpcExceptionsHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(GrpcExceptionsHandler.class);

    @GrpcExceptionHandler({Exception.class})
    public StatusException handleGenericError(Exception ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", UsersApiErrorEnum.GENERIC_FAILURE.name());
        StatusException result = this.buildGenericStatusException(ex);
        return result;
    }

    @GrpcExceptionHandler({UserNotFoundException.class})
    public StatusException handleApplicativeError(UserNotFoundException ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", ex.getError().name());
        StatusException result = this.buildApplicativeStatusException(ex, Status.NOT_FOUND);
        return result;
    }

    @GrpcExceptionHandler({UserAlreadyExistsException.class})
    public StatusException handleApplicativeError(UserAlreadyExistsException ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", ex.getError().name());
        StatusException result = this.buildApplicativeStatusException(ex, Status.ALREADY_EXISTS);
        return result;
    }

    private StatusException buildApplicativeStatusException(UsersApiException ex, Status status) {
        String type = ex.getError().getErrorDesc();
        String instance = Utils.errorInstanceAsURN(ex.getErrorUUID());
        String title = ex.getError().name();
        String details = ex.getMessage();
        UsersApiErrorEnum error = ex.getError();

//        Any applicativeError = Any.newBuilder().setValue(ByteString.copyFrom(error.toString().getBytes())).build();
        UsersApiErrorResponse.ApplicativeError applicativeError = UsersApiErrorResponse.ApplicativeError.valueOf(error.name());
        UsersApiErrorResponse errorResponse = UsersApiErrorResponse.newBuilder()
                .setType(type)
                .setInstance(instance)
                .setTitle(title)
                .setDetails(details)
                .setApplicativeError(applicativeError)
                .setStatus(status.getCode().value())
                .build();
        Metadata.Key<UsersApiErrorResponse> shopErrorResponseKey = ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance());
        Metadata metadata = new Metadata();
        metadata.put(shopErrorResponseKey, errorResponse);
        return status.asException(metadata);
    }

    private StatusException buildGenericStatusException(Exception ex) {
        UUID errorUUID = UUID.randomUUID();
        String type = UsersApiErrorEnum.GENERIC_FAILURE.getErrorDesc();
        String instance = Utils.errorInstanceAsURN(errorUUID);
        String title = UsersApiErrorEnum.GENERIC_FAILURE.name();
        String details = ex.getMessage();
        UsersApiErrorEnum error = UsersApiErrorEnum.GENERIC_FAILURE;
        Status status = Status.INTERNAL;

//        Any applicativeError = Any.newBuilder().setValue(ByteString.copyFrom(error.toString().getBytes())).build();
        UsersApiErrorResponse.ApplicativeError applicativeError = UsersApiErrorResponse.ApplicativeError.valueOf(error.name());
        String msg = String.format(ex.getMessage() + " - Error UUID: %s", errorUUID);
        LOGGER.error(msg);
        UsersApiErrorResponse errorResponse = UsersApiErrorResponse.newBuilder()
                .setType(type)
                .setInstance(instance)
                .setTitle(title)
                .setDetails(details)
                .setApplicativeError(applicativeError)
                .setStatus(status.getCode().value())
                .build();
        Metadata.Key<UsersApiErrorResponse> shopErrorResponseKey = ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance());
        Metadata metadata = new Metadata();
        metadata.put(shopErrorResponseKey, errorResponse);
        return status.asException(metadata);
    }
}
