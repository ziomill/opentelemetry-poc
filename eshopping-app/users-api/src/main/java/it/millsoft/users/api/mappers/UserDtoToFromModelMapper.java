package it.millsoft.users.api.mappers;


import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.Set;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserDtoToFromModelMapper {

    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "email", source = "email")
    UserDTO userModelToDto(User user);

    Set<UserDTO> usersModelToDto(Set<User> users);

    @Mapping(target = "name", source = "name")
    @Mapping(target = "surname", source = "surname")
    @Mapping(target = "email", source = "email")
    User userDtoToModel(UserDTO person);

    Set<User> usersDtoToModel(Set<UserDTO> users);



}
