package it.millsoft.users.api.dto;

import lombok.Getter;
import lombok.NonNull;

@Getter
public enum UsersApiErrorEnum {
    USER_NOT_FOUND("/error/user-not-found"),
    USER_ALREADY_EXISTS("/error/user-already-exists"),
    GENERIC_FAILURE("/failure/generic-failure");

    @NonNull
    private String errorDesc;

    UsersApiErrorEnum(String errorDesc) {
        this.errorDesc = errorDesc;
    }

}
