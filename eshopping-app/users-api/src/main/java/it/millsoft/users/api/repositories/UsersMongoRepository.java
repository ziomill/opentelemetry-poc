package it.millsoft.users.api.repositories;

import it.millsoft.users.api.model.User;
import org.springframework.data.mongodb.repository.ExistsQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersMongoRepository extends MongoRepository<User,String> {

    @Query("{ 'email' : ?0}")
    Optional<User> findUserByEmail(String email);

    @ExistsQuery("{ 'email': ?0}")
    boolean existByEmail(String email);

}
