package it.millsoft.users.api.interfaces.grpc.model;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 * <pre>
 * Users Service
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.51.0)",
    comments = "Source: users_api__server_grpc_interfaces__v0.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class UsersGrpcApiGrpc {

  private UsersGrpcApiGrpc() {}

  public static final String SERVICE_NAME = "UsersGrpcApi";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest,
      it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse> getGetUserByEmailMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getUserByEmail",
      requestType = it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest.class,
      responseType = it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest,
      it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse> getGetUserByEmailMethod() {
    io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest, it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse> getGetUserByEmailMethod;
    if ((getGetUserByEmailMethod = UsersGrpcApiGrpc.getGetUserByEmailMethod) == null) {
      synchronized (UsersGrpcApiGrpc.class) {
        if ((getGetUserByEmailMethod = UsersGrpcApiGrpc.getGetUserByEmailMethod) == null) {
          UsersGrpcApiGrpc.getGetUserByEmailMethod = getGetUserByEmailMethod =
              io.grpc.MethodDescriptor.<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest, it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getUserByEmail"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse.getDefaultInstance()))
              .setSchemaDescriptor(new UsersGrpcApiMethodDescriptorSupplier("getUserByEmail"))
              .build();
        }
      }
    }
    return getGetUserByEmailMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest,
      it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse> getGetUsersMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getUsers",
      requestType = it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest.class,
      responseType = it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest,
      it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse> getGetUsersMethod() {
    io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest, it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse> getGetUsersMethod;
    if ((getGetUsersMethod = UsersGrpcApiGrpc.getGetUsersMethod) == null) {
      synchronized (UsersGrpcApiGrpc.class) {
        if ((getGetUsersMethod = UsersGrpcApiGrpc.getGetUsersMethod) == null) {
          UsersGrpcApiGrpc.getGetUsersMethod = getGetUsersMethod =
              io.grpc.MethodDescriptor.<it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest, it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getUsers"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse.getDefaultInstance()))
              .setSchemaDescriptor(new UsersGrpcApiMethodDescriptorSupplier("getUsers"))
              .build();
        }
      }
    }
    return getGetUsersMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.AddUserRequest,
      it.millsoft.users.api.interfaces.grpc.model.AddUserResponse> getAddUserMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "addUser",
      requestType = it.millsoft.users.api.interfaces.grpc.model.AddUserRequest.class,
      responseType = it.millsoft.users.api.interfaces.grpc.model.AddUserResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.AddUserRequest,
      it.millsoft.users.api.interfaces.grpc.model.AddUserResponse> getAddUserMethod() {
    io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.AddUserRequest, it.millsoft.users.api.interfaces.grpc.model.AddUserResponse> getAddUserMethod;
    if ((getAddUserMethod = UsersGrpcApiGrpc.getAddUserMethod) == null) {
      synchronized (UsersGrpcApiGrpc.class) {
        if ((getAddUserMethod = UsersGrpcApiGrpc.getAddUserMethod) == null) {
          UsersGrpcApiGrpc.getAddUserMethod = getAddUserMethod =
              io.grpc.MethodDescriptor.<it.millsoft.users.api.interfaces.grpc.model.AddUserRequest, it.millsoft.users.api.interfaces.grpc.model.AddUserResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "addUser"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.AddUserRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.AddUserResponse.getDefaultInstance()))
              .setSchemaDescriptor(new UsersGrpcApiMethodDescriptorSupplier("addUser"))
              .build();
        }
      }
    }
    return getAddUserMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest,
      it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse> getUpdateUserMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "updateUser",
      requestType = it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest.class,
      responseType = it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest,
      it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse> getUpdateUserMethod() {
    io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest, it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse> getUpdateUserMethod;
    if ((getUpdateUserMethod = UsersGrpcApiGrpc.getUpdateUserMethod) == null) {
      synchronized (UsersGrpcApiGrpc.class) {
        if ((getUpdateUserMethod = UsersGrpcApiGrpc.getUpdateUserMethod) == null) {
          UsersGrpcApiGrpc.getUpdateUserMethod = getUpdateUserMethod =
              io.grpc.MethodDescriptor.<it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest, it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "updateUser"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse.getDefaultInstance()))
              .setSchemaDescriptor(new UsersGrpcApiMethodDescriptorSupplier("updateUser"))
              .build();
        }
      }
    }
    return getUpdateUserMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest,
      it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse> getDeleteUserMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "deleteUser",
      requestType = it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest.class,
      responseType = it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest,
      it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse> getDeleteUserMethod() {
    io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest, it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse> getDeleteUserMethod;
    if ((getDeleteUserMethod = UsersGrpcApiGrpc.getDeleteUserMethod) == null) {
      synchronized (UsersGrpcApiGrpc.class) {
        if ((getDeleteUserMethod = UsersGrpcApiGrpc.getDeleteUserMethod) == null) {
          UsersGrpcApiGrpc.getDeleteUserMethod = getDeleteUserMethod =
              io.grpc.MethodDescriptor.<it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest, it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "deleteUser"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse.getDefaultInstance()))
              .setSchemaDescriptor(new UsersGrpcApiMethodDescriptorSupplier("deleteUser"))
              .build();
        }
      }
    }
    return getDeleteUserMethod;
  }

  private static volatile io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest,
      it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse> getCheckUserExistenceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "checkUserExistence",
      requestType = it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest.class,
      responseType = it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest,
      it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse> getCheckUserExistenceMethod() {
    io.grpc.MethodDescriptor<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest, it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse> getCheckUserExistenceMethod;
    if ((getCheckUserExistenceMethod = UsersGrpcApiGrpc.getCheckUserExistenceMethod) == null) {
      synchronized (UsersGrpcApiGrpc.class) {
        if ((getCheckUserExistenceMethod = UsersGrpcApiGrpc.getCheckUserExistenceMethod) == null) {
          UsersGrpcApiGrpc.getCheckUserExistenceMethod = getCheckUserExistenceMethod =
              io.grpc.MethodDescriptor.<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest, it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "checkUserExistence"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse.getDefaultInstance()))
              .setSchemaDescriptor(new UsersGrpcApiMethodDescriptorSupplier("checkUserExistence"))
              .build();
        }
      }
    }
    return getCheckUserExistenceMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static UsersGrpcApiStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<UsersGrpcApiStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<UsersGrpcApiStub>() {
        @java.lang.Override
        public UsersGrpcApiStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new UsersGrpcApiStub(channel, callOptions);
        }
      };
    return UsersGrpcApiStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static UsersGrpcApiBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<UsersGrpcApiBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<UsersGrpcApiBlockingStub>() {
        @java.lang.Override
        public UsersGrpcApiBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new UsersGrpcApiBlockingStub(channel, callOptions);
        }
      };
    return UsersGrpcApiBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static UsersGrpcApiFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<UsersGrpcApiFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<UsersGrpcApiFutureStub>() {
        @java.lang.Override
        public UsersGrpcApiFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new UsersGrpcApiFutureStub(channel, callOptions);
        }
      };
    return UsersGrpcApiFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static abstract class UsersGrpcApiImplBase implements io.grpc.BindableService {

    /**
     */
    public void getUserByEmail(it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetUserByEmailMethod(), responseObserver);
    }

    /**
     */
    public void getUsers(it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetUsersMethod(), responseObserver);
    }

    /**
     */
    public void addUser(it.millsoft.users.api.interfaces.grpc.model.AddUserRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.AddUserResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getAddUserMethod(), responseObserver);
    }

    /**
     */
    public void updateUser(it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getUpdateUserMethod(), responseObserver);
    }

    /**
     */
    public void deleteUser(it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getDeleteUserMethod(), responseObserver);
    }

    /**
     */
    public void checkUserExistence(it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getCheckUserExistenceMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetUserByEmailMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest,
                it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse>(
                  this, METHODID_GET_USER_BY_EMAIL)))
          .addMethod(
            getGetUsersMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest,
                it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse>(
                  this, METHODID_GET_USERS)))
          .addMethod(
            getAddUserMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.users.api.interfaces.grpc.model.AddUserRequest,
                it.millsoft.users.api.interfaces.grpc.model.AddUserResponse>(
                  this, METHODID_ADD_USER)))
          .addMethod(
            getUpdateUserMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest,
                it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse>(
                  this, METHODID_UPDATE_USER)))
          .addMethod(
            getDeleteUserMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest,
                it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse>(
                  this, METHODID_DELETE_USER)))
          .addMethod(
            getCheckUserExistenceMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest,
                it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse>(
                  this, METHODID_CHECK_USER_EXISTENCE)))
          .build();
    }
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static final class UsersGrpcApiStub extends io.grpc.stub.AbstractAsyncStub<UsersGrpcApiStub> {
    private UsersGrpcApiStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected UsersGrpcApiStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new UsersGrpcApiStub(channel, callOptions);
    }

    /**
     */
    public void getUserByEmail(it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetUserByEmailMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getUsers(it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetUsersMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void addUser(it.millsoft.users.api.interfaces.grpc.model.AddUserRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.AddUserResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getAddUserMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateUser(it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getUpdateUserMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void deleteUser(it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getDeleteUserMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void checkUserExistence(it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest request,
        io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getCheckUserExistenceMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static final class UsersGrpcApiBlockingStub extends io.grpc.stub.AbstractBlockingStub<UsersGrpcApiBlockingStub> {
    private UsersGrpcApiBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected UsersGrpcApiBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new UsersGrpcApiBlockingStub(channel, callOptions);
    }

    /**
     */
    public it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse getUserByEmail(it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetUserByEmailMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse getUsers(it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetUsersMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.users.api.interfaces.grpc.model.AddUserResponse addUser(it.millsoft.users.api.interfaces.grpc.model.AddUserRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getAddUserMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse updateUser(it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getUpdateUserMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse deleteUser(it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getDeleteUserMethod(), getCallOptions(), request);
    }

    /**
     */
    public it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse checkUserExistence(it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getCheckUserExistenceMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * Users Service
   * </pre>
   */
  public static final class UsersGrpcApiFutureStub extends io.grpc.stub.AbstractFutureStub<UsersGrpcApiFutureStub> {
    private UsersGrpcApiFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected UsersGrpcApiFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new UsersGrpcApiFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse> getUserByEmail(
        it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetUserByEmailMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse> getUsers(
        it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetUsersMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.users.api.interfaces.grpc.model.AddUserResponse> addUser(
        it.millsoft.users.api.interfaces.grpc.model.AddUserRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getAddUserMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse> updateUser(
        it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getUpdateUserMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse> deleteUser(
        it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getDeleteUserMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse> checkUserExistence(
        it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getCheckUserExistenceMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_USER_BY_EMAIL = 0;
  private static final int METHODID_GET_USERS = 1;
  private static final int METHODID_ADD_USER = 2;
  private static final int METHODID_UPDATE_USER = 3;
  private static final int METHODID_DELETE_USER = 4;
  private static final int METHODID_CHECK_USER_EXISTENCE = 5;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final UsersGrpcApiImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(UsersGrpcApiImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_USER_BY_EMAIL:
          serviceImpl.getUserByEmail((it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse>) responseObserver);
          break;
        case METHODID_GET_USERS:
          serviceImpl.getUsers((it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse>) responseObserver);
          break;
        case METHODID_ADD_USER:
          serviceImpl.addUser((it.millsoft.users.api.interfaces.grpc.model.AddUserRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.AddUserResponse>) responseObserver);
          break;
        case METHODID_UPDATE_USER:
          serviceImpl.updateUser((it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse>) responseObserver);
          break;
        case METHODID_DELETE_USER:
          serviceImpl.deleteUser((it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse>) responseObserver);
          break;
        case METHODID_CHECK_USER_EXISTENCE:
          serviceImpl.checkUserExistence((it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest) request,
              (io.grpc.stub.StreamObserver<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class UsersGrpcApiBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    UsersGrpcApiBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return it.millsoft.users.api.interfaces.grpc.model.UsersApiServerGrpcInterfacesV0.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("UsersGrpcApi");
    }
  }

  private static final class UsersGrpcApiFileDescriptorSupplier
      extends UsersGrpcApiBaseDescriptorSupplier {
    UsersGrpcApiFileDescriptorSupplier() {}
  }

  private static final class UsersGrpcApiMethodDescriptorSupplier
      extends UsersGrpcApiBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    UsersGrpcApiMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (UsersGrpcApiGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new UsersGrpcApiFileDescriptorSupplier())
              .addMethod(getGetUserByEmailMethod())
              .addMethod(getGetUsersMethod())
              .addMethod(getAddUserMethod())
              .addMethod(getUpdateUserMethod())
              .addMethod(getDeleteUserMethod())
              .addMethod(getCheckUserExistenceMethod())
              .build();
        }
      }
    }
    return result;
  }
}
