
package it.millsoft.users.api.business;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.metrics.LongCounter;
import io.opentelemetry.api.metrics.Meter;
import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.exceptions.UserAlreadyExistsException;
import it.millsoft.users.api.exceptions.UserNotFoundException;
import it.millsoft.users.api.mappers.UserDtoToFromModelMapper;
import it.millsoft.users.api.model.User;
import it.millsoft.users.api.repositories.UsersMongoRepository;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UsersServicesImpl implements UsersServices {

    private static Logger LOGGER = LoggerFactory.getLogger(UsersServicesImpl.class);

    @Autowired
    private UsersMongoRepository usersMongoRepository;

    @Autowired
    private OpenTelemetry openTelemetry;

    @Autowired
    private UserDtoToFromModelMapper mapper;

    private LongCounter USER_ACCOUNTS_CREATED_COUNTER;

    @PostConstruct
    public void init() {
        Meter meter = this.openTelemetry.getMeter(this.getClass().getName());
        this.USER_ACCOUNTS_CREATED_COUNTER = meter.counterBuilder("user-accounts-created-counter")
                .setDescription("A global counter to count how many accounts has been created globally")
                .build();
    }

    public UserDTO getUserByEmail(String email) {
        LOGGER.info("Searching for User having EMAIL {}", email);
        Optional<User> optUser = this.usersMongoRepository.findUserByEmail(email);
        if (optUser.isEmpty()) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("No User with the EMAIL %s has been found - Error UUID: %s", email, errorUUID);
            LOGGER.warn(msg);
            throw new UserNotFoundException(msg, errorUUID);
        } else {
            User dUser = optUser.get();
            LOGGER.info("User with EMAIL {} was found with the ID {}", email, dUser.getId());
            UserDTO result = mapper.userModelToDto(dUser);
            return result;
        }
    }

    public String addUser(String name, String surname, String email) {
        if (this.usersMongoRepository.existByEmail(email)) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("An User with EMAIL %s already exists - Error UUID: %s", email, errorUUID);
            LOGGER.warn(msg);
            throw new UserAlreadyExistsException(msg, errorUUID);
        } else {
            User dUser = new User();
            dUser.setName(name);
            dUser.setSurname(surname);
            dUser.setEmail(email);
            dUser = this.usersMongoRepository.save(dUser);
            this.USER_ACCOUNTS_CREATED_COUNTER.add(1, Attributes.of(AttributeKey.stringKey("method"), "addUser"));
            LOGGER.info("User with EMAIL {} added with ID.", email, dUser.getId());
            return dUser.getId();
        }
    }

    public String updateUser(String email, UserDTO user) {
        Optional<User> optUser = this.usersMongoRepository.findUserByEmail(email);
        if (optUser.isEmpty()) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("No User with the EMAIL %s has been found - Error UUID: %s", email, errorUUID);
            LOGGER.warn(msg);
            throw new UserNotFoundException(msg, errorUUID);
        } else {
            User dUser = optUser.get();
            dUser.setName(user.getName());
            dUser.setSurname(user.getSurname());
            dUser.setEmail(user.getEmail());
            dUser = this.usersMongoRepository.save(dUser);
            LOGGER.info("The User with EMAIL {} has been updated.", email);
            return dUser.getId();
        }
    }

    public String deleteUser(String email) {
        Optional<User> optUser = this.usersMongoRepository.findUserByEmail(email);
        if (optUser.isEmpty()) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("No User with the EMAIL %s has been found - Error UUID: %s", email, errorUUID);
            LOGGER.warn(msg);
            throw new UserNotFoundException(msg, errorUUID);
        } else {
            User dUser = optUser.get();
            this.usersMongoRepository.delete(dUser);
            LOGGER.info("User with email {} has been deleted.", email);
            return dUser.getId();
        }
    }

    public void checkUserExistence(String email) {
        boolean exists = this.usersMongoRepository.existByEmail(email);
        if (!exists) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("No User with the EMAIL %s has been found - Error UUID: %s", email, errorUUID);
            LOGGER.warn(msg);
            throw new UserNotFoundException(msg, errorUUID);
        } else {
            LOGGER.info("User with email {} has been found.", email);
        }
    }

    public Set<UserDTO> getUsers() {
        List<User> dUsers = this.usersMongoRepository.findAll();
        LOGGER.info("{} users has been found", dUsers.size());
        Set<UserDTO> result = mapper.usersModelToDto(new HashSet<>(dUsers));
        return result;
    }

}
