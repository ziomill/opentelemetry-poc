// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: users_api__server_grpc_interfaces__v0.proto

package it.millsoft.users.api.interfaces.grpc.model;

public interface UserOrBuilder extends
    // @@protoc_insertion_point(interface_extends:User)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string name = 1;</code>
   * @return The name.
   */
  java.lang.String getName();
  /**
   * <code>string name = 1;</code>
   * @return The bytes for name.
   */
  com.google.protobuf.ByteString
      getNameBytes();

  /**
   * <code>string surname = 2;</code>
   * @return The surname.
   */
  java.lang.String getSurname();
  /**
   * <code>string surname = 2;</code>
   * @return The bytes for surname.
   */
  com.google.protobuf.ByteString
      getSurnameBytes();

  /**
   * <code>string email = 3;</code>
   * @return The email.
   */
  java.lang.String getEmail();
  /**
   * <code>string email = 3;</code>
   * @return The bytes for email.
   */
  com.google.protobuf.ByteString
      getEmailBytes();
}
