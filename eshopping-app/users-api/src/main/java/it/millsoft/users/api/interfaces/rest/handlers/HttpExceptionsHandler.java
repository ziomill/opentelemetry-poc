
package it.millsoft.users.api.interfaces.rest.handlers;

import it.millsoft.users.api.commons.Utils;
import it.millsoft.users.api.dto.UsersApiErrorEnum;
import it.millsoft.users.api.exceptions.UserAlreadyExistsException;
import it.millsoft.users.api.exceptions.UserNotFoundException;
import it.millsoft.users.api.exceptions.UsersApiException;
import it.millsoft.users.api.interfaces.rest.model.UsersApiErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.UUID;

@ControllerAdvice
public class HttpExceptionsHandler extends ResponseEntityExceptionHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(HttpExceptionsHandler.class);

    public HttpExceptionsHandler() {
    }

    @ExceptionHandler({Exception.class})
    protected ResponseEntity<Object> handleGenericError(Exception ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", UsersApiErrorEnum.GENERIC_FAILURE.name());
        UsersApiErrorResponse errorBody = this.buildGenericError(ex);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    @ExceptionHandler({UserNotFoundException.class})
    protected ResponseEntity<Object> handleApplcativeError(UserNotFoundException ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", ex.getError().name());
        UsersApiErrorResponse errorBody = this.buildApplicativeError(ex, HttpStatus.NOT_FOUND);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    @ExceptionHandler({UserAlreadyExistsException.class})
    protected ResponseEntity<Object> handleApplcativeError(UserAlreadyExistsException ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", ex.getError().name());
        UsersApiErrorResponse errorBody = this.buildApplicativeError(ex, HttpStatus.CONFLICT);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    private UsersApiErrorResponse buildApplicativeError(UsersApiException ex, HttpStatus status) {
        String type = ex.getError().getErrorDesc();
        String instance = Utils.errorInstanceAsURN(ex.getErrorUUID());
        String title = ex.getError().name();
        String details = ex.getMessage();
        UsersApiErrorEnum error = ex.getError();
        UsersApiErrorResponse result = new UsersApiErrorResponse(type,
                instance,
                title,
                details,
                UsersApiErrorResponse.ApplicativeErrorEnum.fromValue(error.name()),
                status.value());
        return result;
    }

    private UsersApiErrorResponse buildGenericError(Exception ex) {
        UUID errorUUID = UUID.randomUUID();
        String instance = Utils.errorInstanceAsURN(errorUUID);
        String type = UsersApiErrorEnum.GENERIC_FAILURE.getErrorDesc();
        String title = UsersApiErrorEnum.GENERIC_FAILURE.name();
        String details = ex.getMessage();
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        UsersApiErrorResponse result = new UsersApiErrorResponse(type,
                instance,
                title,
                details,
                UsersApiErrorResponse.ApplicativeErrorEnum.fromValue(UsersApiErrorEnum.GENERIC_FAILURE.name()),
                status.value());
        String msg = String.format(ex.getMessage() + " - Error UUID: %s", errorUUID);
        LOGGER.error(msg);
        return result;
    }

}
