package it.millsoft.users.api.exceptions;

import it.millsoft.users.api.dto.UsersApiErrorEnum;

import java.util.UUID;

public class UserNotFoundException extends UsersApiException {
    public UserNotFoundException(String message, UUID errorUUID) {
        super(message,
              UsersApiErrorEnum.USER_NOT_FOUND,
              errorUUID);
    }
}
