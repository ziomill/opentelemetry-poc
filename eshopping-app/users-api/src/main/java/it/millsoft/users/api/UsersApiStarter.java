package it.millsoft.users.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UsersApiStarter {

    public static void main(String[] args)
    {
        SpringApplication.run(UsersApiStarter.class, args);
    }

}
