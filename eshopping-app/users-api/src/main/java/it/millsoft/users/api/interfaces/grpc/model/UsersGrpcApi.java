// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: users_api__server_grpc_interfaces__v0.proto

package it.millsoft.users.api.interfaces.grpc.model;

/**
 * <pre>
 * Users Service
 * </pre>
 *
 * Protobuf service {@code UsersGrpcApi}
 */
public  abstract class UsersGrpcApi
    implements com.google.protobuf.Service {
  protected UsersGrpcApi() {}

  public interface Interface {
    /**
     * <code>rpc getUserByEmail(.GetUserByEmailRequest) returns (.GetUserByEmailResponse);</code>
     */
    public abstract void getUserByEmail(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse> done);

    /**
     * <code>rpc getUsers(.GetUsersRequest) returns (.GetUsersResponse);</code>
     */
    public abstract void getUsers(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse> done);

    /**
     * <code>rpc addUser(.AddUserRequest) returns (.AddUserResponse);</code>
     */
    public abstract void addUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.AddUserRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.AddUserResponse> done);

    /**
     * <code>rpc updateUser(.UpdateUserRequest) returns (.UpdateUserResponse);</code>
     */
    public abstract void updateUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse> done);

    /**
     * <code>rpc deleteUser(.DeleteUserRequest) returns (.DeleteUserResponse);</code>
     */
    public abstract void deleteUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse> done);

    /**
     * <code>rpc checkUserExistence(.CheckUserExistenceRequest) returns (.CheckUserExistenceResponse);</code>
     */
    public abstract void checkUserExistence(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse> done);

  }

  public static com.google.protobuf.Service newReflectiveService(
      final Interface impl) {
    return new UsersGrpcApi() {
      @java.lang.Override
      public  void getUserByEmail(
          com.google.protobuf.RpcController controller,
          it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest request,
          com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse> done) {
        impl.getUserByEmail(controller, request, done);
      }

      @java.lang.Override
      public  void getUsers(
          com.google.protobuf.RpcController controller,
          it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest request,
          com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse> done) {
        impl.getUsers(controller, request, done);
      }

      @java.lang.Override
      public  void addUser(
          com.google.protobuf.RpcController controller,
          it.millsoft.users.api.interfaces.grpc.model.AddUserRequest request,
          com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.AddUserResponse> done) {
        impl.addUser(controller, request, done);
      }

      @java.lang.Override
      public  void updateUser(
          com.google.protobuf.RpcController controller,
          it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest request,
          com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse> done) {
        impl.updateUser(controller, request, done);
      }

      @java.lang.Override
      public  void deleteUser(
          com.google.protobuf.RpcController controller,
          it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest request,
          com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse> done) {
        impl.deleteUser(controller, request, done);
      }

      @java.lang.Override
      public  void checkUserExistence(
          com.google.protobuf.RpcController controller,
          it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest request,
          com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse> done) {
        impl.checkUserExistence(controller, request, done);
      }

    };
  }

  public static com.google.protobuf.BlockingService
      newReflectiveBlockingService(final BlockingInterface impl) {
    return new com.google.protobuf.BlockingService() {
      public final com.google.protobuf.Descriptors.ServiceDescriptor
          getDescriptorForType() {
        return getDescriptor();
      }

      public final com.google.protobuf.Message callBlockingMethod(
          com.google.protobuf.Descriptors.MethodDescriptor method,
          com.google.protobuf.RpcController controller,
          com.google.protobuf.Message request)
          throws com.google.protobuf.ServiceException {
        if (method.getService() != getDescriptor()) {
          throw new java.lang.IllegalArgumentException(
            "Service.callBlockingMethod() given method descriptor for " +
            "wrong service type.");
        }
        switch(method.getIndex()) {
          case 0:
            return impl.getUserByEmail(controller, (it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest)request);
          case 1:
            return impl.getUsers(controller, (it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest)request);
          case 2:
            return impl.addUser(controller, (it.millsoft.users.api.interfaces.grpc.model.AddUserRequest)request);
          case 3:
            return impl.updateUser(controller, (it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest)request);
          case 4:
            return impl.deleteUser(controller, (it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest)request);
          case 5:
            return impl.checkUserExistence(controller, (it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest)request);
          default:
            throw new java.lang.AssertionError("Can't get here.");
        }
      }

      public final com.google.protobuf.Message
          getRequestPrototype(
          com.google.protobuf.Descriptors.MethodDescriptor method) {
        if (method.getService() != getDescriptor()) {
          throw new java.lang.IllegalArgumentException(
            "Service.getRequestPrototype() given method " +
            "descriptor for wrong service type.");
        }
        switch(method.getIndex()) {
          case 0:
            return it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest.getDefaultInstance();
          case 1:
            return it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest.getDefaultInstance();
          case 2:
            return it.millsoft.users.api.interfaces.grpc.model.AddUserRequest.getDefaultInstance();
          case 3:
            return it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest.getDefaultInstance();
          case 4:
            return it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest.getDefaultInstance();
          case 5:
            return it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest.getDefaultInstance();
          default:
            throw new java.lang.AssertionError("Can't get here.");
        }
      }

      public final com.google.protobuf.Message
          getResponsePrototype(
          com.google.protobuf.Descriptors.MethodDescriptor method) {
        if (method.getService() != getDescriptor()) {
          throw new java.lang.IllegalArgumentException(
            "Service.getResponsePrototype() given method " +
            "descriptor for wrong service type.");
        }
        switch(method.getIndex()) {
          case 0:
            return it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse.getDefaultInstance();
          case 1:
            return it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse.getDefaultInstance();
          case 2:
            return it.millsoft.users.api.interfaces.grpc.model.AddUserResponse.getDefaultInstance();
          case 3:
            return it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse.getDefaultInstance();
          case 4:
            return it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse.getDefaultInstance();
          case 5:
            return it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse.getDefaultInstance();
          default:
            throw new java.lang.AssertionError("Can't get here.");
        }
      }

    };
  }

  /**
   * <code>rpc getUserByEmail(.GetUserByEmailRequest) returns (.GetUserByEmailResponse);</code>
   */
  public abstract void getUserByEmail(
      com.google.protobuf.RpcController controller,
      it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest request,
      com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse> done);

  /**
   * <code>rpc getUsers(.GetUsersRequest) returns (.GetUsersResponse);</code>
   */
  public abstract void getUsers(
      com.google.protobuf.RpcController controller,
      it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest request,
      com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse> done);

  /**
   * <code>rpc addUser(.AddUserRequest) returns (.AddUserResponse);</code>
   */
  public abstract void addUser(
      com.google.protobuf.RpcController controller,
      it.millsoft.users.api.interfaces.grpc.model.AddUserRequest request,
      com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.AddUserResponse> done);

  /**
   * <code>rpc updateUser(.UpdateUserRequest) returns (.UpdateUserResponse);</code>
   */
  public abstract void updateUser(
      com.google.protobuf.RpcController controller,
      it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest request,
      com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse> done);

  /**
   * <code>rpc deleteUser(.DeleteUserRequest) returns (.DeleteUserResponse);</code>
   */
  public abstract void deleteUser(
      com.google.protobuf.RpcController controller,
      it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest request,
      com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse> done);

  /**
   * <code>rpc checkUserExistence(.CheckUserExistenceRequest) returns (.CheckUserExistenceResponse);</code>
   */
  public abstract void checkUserExistence(
      com.google.protobuf.RpcController controller,
      it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest request,
      com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse> done);

  public static final
      com.google.protobuf.Descriptors.ServiceDescriptor
      getDescriptor() {
    return it.millsoft.users.api.interfaces.grpc.model.UsersApiServerGrpcInterfacesV0.getDescriptor().getServices().get(0);
  }
  public final com.google.protobuf.Descriptors.ServiceDescriptor
      getDescriptorForType() {
    return getDescriptor();
  }

  public final void callMethod(
      com.google.protobuf.Descriptors.MethodDescriptor method,
      com.google.protobuf.RpcController controller,
      com.google.protobuf.Message request,
      com.google.protobuf.RpcCallback<
        com.google.protobuf.Message> done) {
    if (method.getService() != getDescriptor()) {
      throw new java.lang.IllegalArgumentException(
        "Service.callMethod() given method descriptor for wrong " +
        "service type.");
    }
    switch(method.getIndex()) {
      case 0:
        this.getUserByEmail(controller, (it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest)request,
          com.google.protobuf.RpcUtil.<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse>specializeCallback(
            done));
        return;
      case 1:
        this.getUsers(controller, (it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest)request,
          com.google.protobuf.RpcUtil.<it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse>specializeCallback(
            done));
        return;
      case 2:
        this.addUser(controller, (it.millsoft.users.api.interfaces.grpc.model.AddUserRequest)request,
          com.google.protobuf.RpcUtil.<it.millsoft.users.api.interfaces.grpc.model.AddUserResponse>specializeCallback(
            done));
        return;
      case 3:
        this.updateUser(controller, (it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest)request,
          com.google.protobuf.RpcUtil.<it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse>specializeCallback(
            done));
        return;
      case 4:
        this.deleteUser(controller, (it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest)request,
          com.google.protobuf.RpcUtil.<it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse>specializeCallback(
            done));
        return;
      case 5:
        this.checkUserExistence(controller, (it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest)request,
          com.google.protobuf.RpcUtil.<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse>specializeCallback(
            done));
        return;
      default:
        throw new java.lang.AssertionError("Can't get here.");
    }
  }

  public final com.google.protobuf.Message
      getRequestPrototype(
      com.google.protobuf.Descriptors.MethodDescriptor method) {
    if (method.getService() != getDescriptor()) {
      throw new java.lang.IllegalArgumentException(
        "Service.getRequestPrototype() given method " +
        "descriptor for wrong service type.");
    }
    switch(method.getIndex()) {
      case 0:
        return it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest.getDefaultInstance();
      case 1:
        return it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest.getDefaultInstance();
      case 2:
        return it.millsoft.users.api.interfaces.grpc.model.AddUserRequest.getDefaultInstance();
      case 3:
        return it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest.getDefaultInstance();
      case 4:
        return it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest.getDefaultInstance();
      case 5:
        return it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest.getDefaultInstance();
      default:
        throw new java.lang.AssertionError("Can't get here.");
    }
  }

  public final com.google.protobuf.Message
      getResponsePrototype(
      com.google.protobuf.Descriptors.MethodDescriptor method) {
    if (method.getService() != getDescriptor()) {
      throw new java.lang.IllegalArgumentException(
        "Service.getResponsePrototype() given method " +
        "descriptor for wrong service type.");
    }
    switch(method.getIndex()) {
      case 0:
        return it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse.getDefaultInstance();
      case 1:
        return it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse.getDefaultInstance();
      case 2:
        return it.millsoft.users.api.interfaces.grpc.model.AddUserResponse.getDefaultInstance();
      case 3:
        return it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse.getDefaultInstance();
      case 4:
        return it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse.getDefaultInstance();
      case 5:
        return it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse.getDefaultInstance();
      default:
        throw new java.lang.AssertionError("Can't get here.");
    }
  }

  public static Stub newStub(
      com.google.protobuf.RpcChannel channel) {
    return new Stub(channel);
  }

  public static final class Stub extends it.millsoft.users.api.interfaces.grpc.model.UsersGrpcApi implements Interface {
    private Stub(com.google.protobuf.RpcChannel channel) {
      this.channel = channel;
    }

    private final com.google.protobuf.RpcChannel channel;

    public com.google.protobuf.RpcChannel getChannel() {
      return channel;
    }

    public  void getUserByEmail(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse> done) {
      channel.callMethod(
        getDescriptor().getMethods().get(0),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse.getDefaultInstance(),
        com.google.protobuf.RpcUtil.generalizeCallback(
          done,
          it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse.class,
          it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse.getDefaultInstance()));
    }

    public  void getUsers(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse> done) {
      channel.callMethod(
        getDescriptor().getMethods().get(1),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse.getDefaultInstance(),
        com.google.protobuf.RpcUtil.generalizeCallback(
          done,
          it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse.class,
          it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse.getDefaultInstance()));
    }

    public  void addUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.AddUserRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.AddUserResponse> done) {
      channel.callMethod(
        getDescriptor().getMethods().get(2),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.AddUserResponse.getDefaultInstance(),
        com.google.protobuf.RpcUtil.generalizeCallback(
          done,
          it.millsoft.users.api.interfaces.grpc.model.AddUserResponse.class,
          it.millsoft.users.api.interfaces.grpc.model.AddUserResponse.getDefaultInstance()));
    }

    public  void updateUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse> done) {
      channel.callMethod(
        getDescriptor().getMethods().get(3),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse.getDefaultInstance(),
        com.google.protobuf.RpcUtil.generalizeCallback(
          done,
          it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse.class,
          it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse.getDefaultInstance()));
    }

    public  void deleteUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse> done) {
      channel.callMethod(
        getDescriptor().getMethods().get(4),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse.getDefaultInstance(),
        com.google.protobuf.RpcUtil.generalizeCallback(
          done,
          it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse.class,
          it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse.getDefaultInstance()));
    }

    public  void checkUserExistence(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest request,
        com.google.protobuf.RpcCallback<it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse> done) {
      channel.callMethod(
        getDescriptor().getMethods().get(5),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse.getDefaultInstance(),
        com.google.protobuf.RpcUtil.generalizeCallback(
          done,
          it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse.class,
          it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse.getDefaultInstance()));
    }
  }

  public static BlockingInterface newBlockingStub(
      com.google.protobuf.BlockingRpcChannel channel) {
    return new BlockingStub(channel);
  }

  public interface BlockingInterface {
    public it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse getUserByEmail(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest request)
        throws com.google.protobuf.ServiceException;

    public it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse getUsers(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest request)
        throws com.google.protobuf.ServiceException;

    public it.millsoft.users.api.interfaces.grpc.model.AddUserResponse addUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.AddUserRequest request)
        throws com.google.protobuf.ServiceException;

    public it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse updateUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest request)
        throws com.google.protobuf.ServiceException;

    public it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse deleteUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest request)
        throws com.google.protobuf.ServiceException;

    public it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse checkUserExistence(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest request)
        throws com.google.protobuf.ServiceException;
  }

  private static final class BlockingStub implements BlockingInterface {
    private BlockingStub(com.google.protobuf.BlockingRpcChannel channel) {
      this.channel = channel;
    }

    private final com.google.protobuf.BlockingRpcChannel channel;

    public it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse getUserByEmail(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailRequest request)
        throws com.google.protobuf.ServiceException {
      return (it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse) channel.callBlockingMethod(
        getDescriptor().getMethods().get(0),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.GetUserByEmailResponse.getDefaultInstance());
    }


    public it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse getUsers(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.GetUsersRequest request)
        throws com.google.protobuf.ServiceException {
      return (it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse) channel.callBlockingMethod(
        getDescriptor().getMethods().get(1),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.GetUsersResponse.getDefaultInstance());
    }


    public it.millsoft.users.api.interfaces.grpc.model.AddUserResponse addUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.AddUserRequest request)
        throws com.google.protobuf.ServiceException {
      return (it.millsoft.users.api.interfaces.grpc.model.AddUserResponse) channel.callBlockingMethod(
        getDescriptor().getMethods().get(2),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.AddUserResponse.getDefaultInstance());
    }


    public it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse updateUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.UpdateUserRequest request)
        throws com.google.protobuf.ServiceException {
      return (it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse) channel.callBlockingMethod(
        getDescriptor().getMethods().get(3),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.UpdateUserResponse.getDefaultInstance());
    }


    public it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse deleteUser(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.DeleteUserRequest request)
        throws com.google.protobuf.ServiceException {
      return (it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse) channel.callBlockingMethod(
        getDescriptor().getMethods().get(4),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.DeleteUserResponse.getDefaultInstance());
    }


    public it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse checkUserExistence(
        com.google.protobuf.RpcController controller,
        it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceRequest request)
        throws com.google.protobuf.ServiceException {
      return (it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse) channel.callBlockingMethod(
        getDescriptor().getMethods().get(5),
        controller,
        request,
        it.millsoft.users.api.interfaces.grpc.model.CheckUserExistenceResponse.getDefaultInstance());
    }

  }

  // @@protoc_insertion_point(class_scope:UsersGrpcApi)
}

