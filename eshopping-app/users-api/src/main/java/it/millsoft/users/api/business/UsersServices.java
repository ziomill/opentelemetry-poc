package it.millsoft.users.api.business;

import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.exceptions.UsersApiException;

import java.util.List;
import java.util.Set;

public interface UsersServices {

    UserDTO getUserByEmail(String email);

    Set<UserDTO> getUsers();

    String addUser(String name,
                    String surname,
                    String email);

    String updateUser(String email,
                      UserDTO user);

    String deleteUser(String email);

    void checkUserExistence(String email);

}
