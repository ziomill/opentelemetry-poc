package it.millsoft.users.api.config;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.common.AttributeKey;
import io.opentelemetry.api.common.Attributes;
import io.opentelemetry.api.trace.propagation.W3CTraceContextPropagator;
import io.opentelemetry.context.propagation.ContextPropagators;
import io.opentelemetry.exporter.otlp.metrics.OtlpGrpcMetricExporter;
import io.opentelemetry.exporter.otlp.trace.OtlpGrpcSpanExporter;
import io.opentelemetry.sdk.OpenTelemetrySdk;
import io.opentelemetry.sdk.metrics.SdkMeterProvider;
import io.opentelemetry.sdk.metrics.export.PeriodicMetricReader;
import io.opentelemetry.sdk.resources.Resource;
import io.opentelemetry.sdk.trace.SdkTracerProvider;
import io.opentelemetry.sdk.trace.export.BatchSpanProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenTelemetryConfig {

    @Value("${OTEL_EXPORTER_OTLP_ENDPOINT}")
    private String OTEL_EXPORTER_OTLP_ENDPOINT;

    @Value("${OTEL_SERVICE_NAME}")
    private String OTEL_SERVICE_NAME;

    @Bean
    public OpenTelemetry buildOpenTelemetry() {
        Resource serviceNameResource = Resource.create(
                Attributes.of(AttributeKey.stringKey("service.name"), OTEL_SERVICE_NAME)
        );

        SdkTracerProvider sdkTracerProvider = SdkTracerProvider.builder()
                                                               .setResource(serviceNameResource)
                                                               .addSpanProcessor(BatchSpanProcessor.builder(OtlpGrpcSpanExporter.builder()
                                                                        .setEndpoint(OTEL_EXPORTER_OTLP_ENDPOINT)
                                                                        .build())
                                                                        .build())
                                                               .build();

        SdkMeterProvider sdkMeterProvider = SdkMeterProvider.builder()
                                                            .setResource(serviceNameResource)
                                                            .registerMetricReader(PeriodicMetricReader.builder(OtlpGrpcMetricExporter.builder()
                                                                    .setEndpoint(OTEL_EXPORTER_OTLP_ENDPOINT)
                                                                    .build())
                                                                    .build())
                                                            .build();

        OpenTelemetry openTelemetry = OpenTelemetrySdk.builder()
                .setTracerProvider(sdkTracerProvider)
                .setMeterProvider(sdkMeterProvider)
                .setPropagators(ContextPropagators.create(W3CTraceContextPropagator.getInstance()))
                .build();

        return openTelemetry;
    }

}
