package it.millsoft.users.api.interfaces.rest.handlers;

import it.millsoft.users.api.commons.Utils;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

import static it.millsoft.users.api.commons.Utils.Constants.W3C_SERVER_TIMING_HEADER;

@Component
public class HttpTracesHandler extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException {
        // Add the "Access-Control-Expose-Headers" to enable clients to see the "Server-Timing" header
        response.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS,W3C_SERVER_TIMING_HEADER);
        // Add the "Server-Timing" header with the traceparent
        response.addHeader(W3C_SERVER_TIMING_HEADER,Utils.composeServerTimingHeader());
        filterChain.doFilter(request,response);
    }

}
