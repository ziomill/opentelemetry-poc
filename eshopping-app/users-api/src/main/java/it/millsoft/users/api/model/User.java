package it.millsoft.users.api.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Document("users")
public class User {

    @Id
    private String id;

    private String name;
    private String surname;
    @EqualsAndHashCode.Include
    private String email;

}
