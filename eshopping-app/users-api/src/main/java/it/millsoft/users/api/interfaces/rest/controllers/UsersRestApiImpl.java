package it.millsoft.users.api.interfaces.rest.controllers;

import it.millsoft.users.api.business.UsersServices;
import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.interfaces.rest.model.User;
import it.millsoft.users.api.interfaces.rest.model.UsersRestApi;
import it.millsoft.users.api.mappers.UserRestToFromDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
public class UsersRestApiImpl implements UsersRestApi {

    @Autowired
    private UsersServices usersServices;

    @Autowired
    private UserRestToFromDtoMapper mapper;

    @Override
    public ResponseEntity<User> getUserByEmail(String email) {
        UserDTO dUser = this.usersServices.getUserByEmail(email);
        User rUser = mapper.userDtoToRest(dUser);
        return new ResponseEntity(rUser, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Set<User>> getUsers() {
        Set<UserDTO> dUsers = this.usersServices.getUsers();
        Set<User> rUsers = mapper.usersDtoToRest(dUsers);
        return new ResponseEntity(rUsers, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> addUser(User user) {
        String createdUserId = this.usersServices.addUser(user.getName(), user.getSurname(), user.getEmail());
        return new ResponseEntity(createdUserId, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<String> updateUser(String email,
                                           User user) {
        UserDTO dUser = mapper.userRestToDto(user);
        String updatedUserId = this.usersServices.updateUser(email, dUser);
        return new ResponseEntity(updatedUserId, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> deleteUser(String email) {
        String deletedUserId = this.usersServices.deleteUser(email);
        return new ResponseEntity(deletedUserId, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> checkUserExistence(String email) {
        this.usersServices.checkUserExistence(email);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
