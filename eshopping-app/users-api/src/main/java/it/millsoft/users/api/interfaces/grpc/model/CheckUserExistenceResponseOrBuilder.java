// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: users_api__server_grpc_interfaces__v0.proto

package it.millsoft.users.api.interfaces.grpc.model;

public interface CheckUserExistenceResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:CheckUserExistenceResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>bool exists = 1;</code>
   * @return The exists.
   */
  boolean getExists();
}
