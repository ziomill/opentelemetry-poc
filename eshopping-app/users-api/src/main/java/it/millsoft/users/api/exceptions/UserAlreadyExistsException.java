package it.millsoft.users.api.exceptions;

import it.millsoft.users.api.dto.UsersApiErrorEnum;

import java.util.UUID;

public class UserAlreadyExistsException extends UsersApiException {
    public UserAlreadyExistsException(String message, UUID errorUUID) {
        super(message,
              UsersApiErrorEnum.USER_ALREADY_EXISTS,
              errorUUID);
    }
}

