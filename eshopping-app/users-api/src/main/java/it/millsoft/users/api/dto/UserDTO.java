package it.millsoft.users.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class UserDTO {

    private String name;
    private String surname;
    @EqualsAndHashCode.Include
    private String email;

}
