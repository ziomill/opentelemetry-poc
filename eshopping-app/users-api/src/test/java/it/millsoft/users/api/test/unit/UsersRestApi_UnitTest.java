package it.millsoft.users.api.test.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.millsoft.users.api.business.UsersServices;
import it.millsoft.users.api.commons.Utils;
import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.exceptions.UserAlreadyExistsException;
import it.millsoft.users.api.exceptions.UserNotFoundException;
import it.millsoft.users.api.interfaces.rest.model.UsersApiErrorResponse;
import it.millsoft.users.api.mappers.UserRestToFromDtoMapper;
import it.millsoft.users.api.model.User;
import it.millsoft.users.api.interfaces.rest.handlers.HttpExceptionsHandler;
import org.bson.types.ObjectId;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@WebMvcTest
@Import(HttpExceptionsHandler.class)
public class UsersRestApi_UnitTest {

    @TestConfiguration
    static class UsersRestApiConfiguration {

        @Bean
        UserRestToFromDtoMapper createUserRestToDtoMapper(){
            UserRestToFromDtoMapper mapper = Mappers.getMapper( UserRestToFromDtoMapper.class );
            return mapper;
        }

    }

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UsersServices usersService;

    @Test()
    public void getUserByEmail_UserExists_ShoudlReturnTheUserAndHttpCode200Ok() throws Exception {
        // Given
        String name = "Jhon";
        String surname = "Doe";
        String email = "jhon.doe@google.com";
        UserDTO mUser = new UserDTO(name, surname, email);
        Mockito.when(this.usersService.getUserByEmail(email)).thenReturn(mUser);

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        User responseBody = mapper.readValue(responseBodyAsJson, User.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(mUser.getName(), responseBody.getName()),
                () -> Assertions.assertEquals(mUser.getSurname(), responseBody.getSurname()),
                () -> Assertions.assertEquals(mUser.getEmail(), responseBody.getEmail())
        );
    }

    @Test
    public void getUserByEmail_UserDoesntExists_ShouldReturnHttpCode404NotFound() throws Exception {
        // Given
        String email = "jhon.doe@google.com";
        Mockito.when(this.usersService.getUserByEmail(email)).thenThrow(new UserNotFoundException("User having email: " + email + " not found", UUID.randomUUID()));

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        UsersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, UsersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorResponse.ApplicativeErrorEnum.USER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void getUsers_ThreeUsersExist_ShouldReturnThreeUsersAndHttpCode200Ok() throws Exception {
        // Given
        UserDTO mUser1 = new UserDTO("Jhon", "Doe", "jhon.doe@google.com");
        UserDTO mUser2 = new UserDTO("Andy", "Bee", "andy.bee@google.com");
        UserDTO mUser3 = new UserDTO("Carl", "Green", "carl.green@google.com");
        Set<UserDTO> mUsers = Set.of(mUser1, mUser2, mUser3);
        Mockito.when(this.usersService.getUsers()).thenReturn(mUsers);

        // When
        String uriTemplate = "/users";
        URI uri = Utils.toURI(uriTemplate,null,null);

        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Set<User> loadedUsers = mapper.readValue(responseBodyAsJson, Set.class);
        Assertions.assertEquals(3, loadedUsers.size());
    }

    @Test
    public void getUsers_NoUsersExist_ShouldReturnEmptyListAndHttpCode200Ok() throws Exception {
        // Given
        Set<UserDTO> mUsers = new HashSet();
        Mockito.when(this.usersService.getUsers()).thenReturn(mUsers);

        // When
        String uriTemplate = "/users";
        URI uri = Utils.toURI(uriTemplate,null,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Set<User> loadedUsers = mapper.readValue(responseBodyAsJson, Set.class);
        Assertions.assertEquals(0, loadedUsers.size());
    }

    @Test
    public void addUser_UsersDoesntExist_ShouldAddUserAndReturnIdAndHttpCode200Ok() throws Exception {
        // Given
        String name = "Jhon";
        String surname = "Doe";
        String email = "jhon.doe@google.com";
        String mockedCreatedUserId = (new ObjectId()).toString();
        Mockito.when(this.usersService.addUser(name, surname, email)).thenReturn(mockedCreatedUserId);

        // When
        ObjectMapper mapper = new ObjectMapper();
        User body = new User();
        body.setName(name);
        body.setSurname(surname);
        body.setEmail(email);
        String bodyAsJson = mapper.writeValueAsString(body);

        String uriTemplate = "/user";
        URI uri = Utils.toURI(uriTemplate,null,null);
        this.mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is(mockedCreatedUserId)));
    }

    @Test
    public void addUser_UserExists_ShouldReturnHttpCode409Conflict() throws Exception {
        // Given
        String name = "Jhon";
        String surname = "Doe";
        String email = "jhon.doe@google.com";
        UserAlreadyExistsException mockedThrownException = new UserAlreadyExistsException("User already exists", UUID.randomUUID());
        Mockito.when(this.usersService.addUser(name, surname, email)).thenThrow(mockedThrownException);

        // When
        ObjectMapper mapper = new ObjectMapper();
        User body = new User();
        body.setName(name);
        body.setSurname(surname);
        body.setEmail(email);
        String bodyAsJson = mapper.writeValueAsString(body);

        String uriTemplate = "/user";
        URI uri = Utils.toURI(uriTemplate,null,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(bodyAsJson).contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        UsersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, UsersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.CONFLICT.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorResponse.ApplicativeErrorEnum.USER_ALREADY_EXISTS, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateUser_UserExists_ShouldUpdateUserAndReturnIdAndHttpCode200Ok() throws Exception {
        // Given
        ObjectMapper mapper = new ObjectMapper();
        String name = "JhonJhon";
        String surname = "DonDon";
        String email = "jhon.doe@aol.com";
        UserDTO dUser = new UserDTO(name, surname, email);
        String emailOfUserToUpdate = "jhon.doe@google.com";
        String mockedUpdatedUserId = (new ObjectId()).toString();
        Mockito.when(this.usersService.updateUser(emailOfUserToUpdate, dUser)).thenReturn(mockedUpdatedUserId);

        // When
        User body = new User();
        body.setName(name);
        body.setSurname(surname);
        body.setEmail(email);
        String bodyAsJson = mapper.writeValueAsString(body);

        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",emailOfUserToUpdate);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        this.mockMvc.perform(MockMvcRequestBuilders.put(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is(mockedUpdatedUserId)));
    }

    @Test
    public void updateUser_UserDoesntExists_ShouldReturnHttpCode404NotFound() throws Exception {
        // Given
        String emailOfUserToUpdate = "jhon.doe@google.com";
        String name = "JhonJhon";
        String surname = "DonDon";
        String email = "jhon.doe@aol.com";
        UserDTO dUser = new UserDTO(name, surname, email);
        UserNotFoundException mockedThrownException = new UserNotFoundException("User not found", UUID.randomUUID());
        Mockito.when(this.usersService.updateUser(emailOfUserToUpdate, dUser)).thenThrow(new Throwable[]{mockedThrownException});

        // When
        User body = new User();
        body.setName(name);
        body.setSurname(surname);
        body.setEmail(email);
        ObjectMapper mapper = new ObjectMapper();
        String bodyAsJson = mapper.writeValueAsString(body);

        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",emailOfUserToUpdate);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.put(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        UsersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, UsersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorResponse.ApplicativeErrorEnum.USER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void deleteUser_UserExists_ShouldDeleteUserAndReturnIdAndHttpCode200Ok() throws Exception {
        // Given
        String emailOfUserToDelete = "jhon.doe@google.com";
        String mockedDeletedUserId = (new ObjectId()).toString();
        Mockito.when(this.usersService.deleteUser(emailOfUserToDelete)).thenReturn(mockedDeletedUserId);

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",emailOfUserToDelete);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        this.mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is(mockedDeletedUserId)));
    }

    @Test
    public void deleteUser_UserDoesntExists_ShouldReturnHttpCode404NotFound() throws Exception {

        // Given
        String emailOfUserToDelete = "jhon.doe@google.com";
        UserNotFoundException mockedThrownException = new UserNotFoundException("User not found", UUID.randomUUID());
        Mockito.when(this.usersService.deleteUser(emailOfUserToDelete)).thenThrow(new Throwable[]{mockedThrownException});

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",emailOfUserToDelete);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.delete(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        ObjectMapper mapper = new ObjectMapper();
        UsersApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, UsersApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorResponse.ApplicativeErrorEnum.USER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void checkUserExistence_UserExists_ShouldReturnHttpCode204NoContent() throws Exception {

        // Given
        String email = "jhon.doe@google.com";
        (Mockito.doNothing().when(this.usersService)).checkUserExistence(email);

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);

        this.mockMvc.perform(MockMvcRequestBuilders.head(uri))
                // Then
                .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void checkUserExistence_UserDoesntExists_ShouldReturnHttpCode404NotFound() throws Exception {

        // Given
        String email = "jhon.doe@google.com";
        UserNotFoundException mockedThrownException = new UserNotFoundException("User not found", UUID.randomUUID());
        (Mockito.doThrow(mockedThrownException).when(this.usersService)).checkUserExistence(email);

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        this.mockMvc.perform(MockMvcRequestBuilders.head(uri))
                // Then
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

}