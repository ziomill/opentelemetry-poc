package it.millsoft.users.api.test.unit;

import it.millsoft.users.api.business.UsersServices;
import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.dto.UsersApiErrorEnum;
import it.millsoft.users.api.exceptions.UserAlreadyExistsException;
import it.millsoft.users.api.exceptions.UserNotFoundException;
import it.millsoft.users.api.model.User;
import it.millsoft.users.api.repositories.UsersMongoRepository;
import it.millsoft.users.api.test.config.UnitTestConfig;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@SpringJUnitConfig(classes = UnitTestConfig.class)
public class UserServices_UnitTest {

    @Autowired
    private UsersServices usersServices;
    @MockBean
    private UsersMongoRepository usersMongoRepository;

    @Test
    public void getUserByEmail_UserExists_ShoudlReturnTheUser() {
        // Given
        String id = (new ObjectId()).toString();
        String name = "Jhon";
        String surname = "Doe";
        String email = "jhon.doe@google.com";
        User mUser = new User(id, name, surname, email);
        Optional<User> optUser = Optional.of(mUser);
        Mockito.when(this.usersMongoRepository.findUserByEmail(email)).thenReturn(optUser);

        // When
        UserDTO user = this.usersServices.getUserByEmail(email);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(mUser.getName(), user.getName()),
                () -> Assertions.assertEquals(mUser.getSurname(), user.getSurname()),
                () -> Assertions.assertEquals(mUser.getEmail(), user.getEmail())
        );
    }

    @Test
    public void getUserByEmail_UserDoesntExists_ShouldThrowAnUserNotFoundException() {
        // Given
        String email = "jhon.doe@google.com";
        Optional<User> optUser = Optional.empty();
        Mockito.when(this.usersMongoRepository.findUserByEmail(email)).thenReturn(optUser);

        // Then
        UserNotFoundException ex = Assertions.assertThrows(
                UserNotFoundException.class,
                () -> this.usersServices.getUserByEmail(email), // Then
                "It would have raised the " + UserNotFoundException.class.getName());
        Assertions.assertAll(
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, ex.getError()),
                () -> Assertions.assertNotNull(ex.getErrorUUID()),
                () -> Assertions.assertNotNull(ex.getMessage())
        );
    }

    @Test
    public void getUsers_ThreeUsersExist_ShouldReturnThreeUsers() {
        // Given
        User mUser1 = new User((new ObjectId()).toString(), "Jhon", "Doe", "jhon.doe@google.com");
        User mUser2 = new User((new ObjectId()).toString(), "Andy", "Bee", "andy.bee@google.com");
        User mUser3 = new User((new ObjectId()).toString(), "Carl", "Green", "carl.green@google.com");
        List<User> mUsers = List.of(mUser1, mUser2, mUser3);
        Mockito.when(this.usersMongoRepository.findAll()).thenReturn(mUsers);

        // When
        Set<UserDTO> loadedUsers = this.usersServices.getUsers();

        // Then
        Assertions.assertEquals(3, loadedUsers.size());
    }

    @Test
    public void getUsers_NoUsersExist_ShouldReturnAnEmptyList() {
        // Given
        List<User> mUsers = new ArrayList();
        Mockito.when(this.usersMongoRepository.findAll()).thenReturn(mUsers);

        // When
        Set<UserDTO> loadedUsers = this.usersServices.getUsers();

        // Then
        Assertions.assertEquals(0, loadedUsers.size());
    }

    @Test
    public void addUser_UsersDoesntExist_ShouldAddUserAndReturnItsId() {
        // Given
        String email = "jhon.doe@google.com";
        Mockito.when(this.usersMongoRepository.existByEmail(email)).thenReturn(false);
        User mUserInput = new User();
        mUserInput.setName("Jhon");
        mUserInput.setSurname("Doe");
        mUserInput.setEmail(email);
        String mockedCreatedUserId = (new ObjectId()).toString();
        User mUserOutput = new User();
        mUserOutput.setId(mockedCreatedUserId);
        mUserOutput.setName(mUserInput.getName());
        mUserOutput.setSurname(mUserInput.getSurname());
        mUserOutput.setEmail(mUserInput.getEmail());
        Mockito.when(this.usersMongoRepository.save(mUserInput)).thenReturn(mUserOutput);

        // When
        String userId = this.usersServices.addUser(mUserInput.getName(), mUserInput.getSurname(), mUserInput.getEmail());

        // Then
        Assertions.assertEquals(mockedCreatedUserId, userId);
    }

    @Test
    public void addUser_UserExists_ShouldThrowAnUserAlreadyExistsException() {
        // Given
        String email = "jhon.doe@google.com";
        Mockito.when(this.usersMongoRepository.existByEmail(email)).thenReturn(true);
        String name = "Jhon";
        String surname = "Doe";

        // Then
        UserAlreadyExistsException ex = Assertions.assertThrows(
                UserAlreadyExistsException.class,
                () -> this.usersServices.addUser(name, surname, email), // When
                "It would have thrown the " + UserAlreadyExistsException.class.getName()
        );
        Assertions.assertAll(
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_ALREADY_EXISTS, ex.getError()),
                () -> Assertions.assertNotNull(ex.getErrorUUID()),
                () -> Assertions.assertNotNull(ex.getMessage())
        );
    }

    @Test
    public void updateUser_UserExists_ShouldUpdateUserAndReturnId() {
        // Given
        String id = (new ObjectId()).toString();
        String name = "Jhon";
        String surname = "Doe";
        String email = "jhon.doe@google.com";
        User mUserInput = new User(id, name, surname, email);
        Optional<User> optUser = Optional.of(mUserInput);
        Mockito.when(this.usersMongoRepository.findUserByEmail(email)).thenReturn(optUser);
        String updatedName = "JhonJhon";
        String updatedSurname = "DoeDoe";
        String updatedEmail = "jhon.doe@aol.com";
        User mUserOutput = new User(id, updatedName, updatedSurname, updatedEmail);
        Mockito.when(this.usersMongoRepository.save(mUserInput)).thenReturn(mUserOutput);
        UserDTO updateData = new UserDTO(updatedName, updatedSurname, updatedEmail);

        // When
        String updatedUserId = this.usersServices.updateUser(email, updateData);

        // Then
        Assertions.assertEquals(id, updatedUserId);
    }

    @Test
    public void updateUser_UserDoesntExists_ShouldThrowAnUserNotFoundException() {
        // Given
        String emailOfUserToUpdate = "jhon.doe@google.com";
        Optional<User> optUser = Optional.empty();
        Mockito.when(this.usersMongoRepository.findUserByEmail(emailOfUserToUpdate)).thenReturn(optUser);
        String updatedName = "JhonJhon";
        String updatedSurname = "DoeDoe";
        String updatedEmail = "jhon.doe@aol.com";
        UserDTO updateData = new UserDTO(updatedName, updatedSurname, updatedEmail);

        // Then
        UserNotFoundException ex = Assertions.assertThrows(
                UserNotFoundException.class,
                () -> this.usersServices.updateUser(emailOfUserToUpdate, updateData), // When
                "It would have thrown the " + UserNotFoundException.class.getName()
        );
        Assertions.assertAll(
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, ex.getError()),
                () -> Assertions.assertNotNull(ex.getErrorUUID()),
                () -> Assertions.assertNotNull(ex.getMessage())
        );
    }

    @Test
    public void deleteUser_UserExists_ShouldDeleteUserAndReturnId() {
        // Given
        String id = (new ObjectId()).toString();
        String name = "Jhon";
        String surname = "Doe";
        String email = "jhon.doe@google.com";
        User mUser = new User(id, name, surname, email);
        Optional<User> optUser = Optional.of(mUser);
        Mockito.when(this.usersMongoRepository.findUserByEmail(email)).thenReturn(optUser);
        (Mockito.doNothing().when(this.usersMongoRepository)).delete(mUser);

        // When
        String deletedUserId = this.usersServices.deleteUser(email);
        Assertions.assertEquals(id, deletedUserId);
    }

    @Test
    public void deleteUser_UserDoesntExists_ShouldThrowAnUserNotFoundException() {
        // Given
        String emailUserToDelete = "jhon.doe@google.com";
        Optional<User> optUser = Optional.empty();
        Mockito.when(this.usersMongoRepository.findUserByEmail(emailUserToDelete)).thenReturn(optUser);

        // Then
        UserNotFoundException ex = Assertions.assertThrows(UserNotFoundException.class,
                () -> this.usersServices.deleteUser(emailUserToDelete),
                "It would have thrown the " + UserNotFoundException.class.getName()
        );

        Assertions.assertAll(
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, ex.getError()),
                () -> Assertions.assertNotNull(ex.getErrorUUID()),
                () -> Assertions.assertNotNull(ex.getMessage())
        );
    }

    @Test
    public void checkUserExistence_UserExists_ShouldEndNormally() {
        // Given
        String email = "jhon.doe@google.com";
        Mockito.when(this.usersMongoRepository.existByEmail(email)).thenReturn(true);

        // When
        this.usersServices.checkUserExistence(email);

        // Then
        Assertions.assertTrue(true);
    }

    @Test
    public void checkUserExistence_UserDoesntExists_ShouldThrowAnUserNotFoundException() {
        // Given
        String email = "jhon.doe@google.com";
        Mockito.when(this.usersMongoRepository.existByEmail(email)).thenReturn(false);

        // Then
        UserNotFoundException ex = Assertions.assertThrows(
                UserNotFoundException.class,
                () -> this.usersServices.checkUserExistence(email), // When
                "It would have thrown the " + UserNotFoundException.class.getName()
        );
        Assertions.assertAll(
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, ex.getError()),
                () -> Assertions.assertNotNull(ex.getErrorUUID()),
                () -> Assertions.assertNotNull(ex.getMessage())
        );
    }

}
