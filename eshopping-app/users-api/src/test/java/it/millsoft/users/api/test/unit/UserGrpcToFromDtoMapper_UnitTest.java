package it.millsoft.users.api.test.unit;

import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.interfaces.grpc.model.User;
import it.millsoft.users.api.mappers.UserGrpcToFromDtoMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.Optional;
import java.util.Set;

public class UserGrpcToFromDtoMapper_UnitTest {

    @Test
    public void userGrpcToDto_createTheUserDTO_ShouldReturnTheUserDTO() {
        // Given
        UserGrpcToFromDtoMapper mapper = Mappers.getMapper( UserGrpcToFromDtoMapper.class );
        User gUser = User.newBuilder()
                .setEmail("jhon.doe@aol.com")
                .setName("Jhon")
                .setSurname("Doe")
                .build();

        // When
        UserDTO dUser = mapper.userGrpcToDto(gUser);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(gUser.getName(),dUser.getName()),
                () -> Assertions.assertEquals(gUser.getSurname(),dUser.getSurname()),
                () -> Assertions.assertEquals(gUser.getEmail(),dUser.getEmail())
        );
    }

    @Test
    public void userDtoToGrpc_createTheUserGrpc_ShouldReturnTheUserGrpc() {
        // Given
        UserGrpcToFromDtoMapper mapper = Mappers.getMapper( UserGrpcToFromDtoMapper.class );
        UserDTO dUser = new UserDTO("Jhon","Doe","jhon.doe@aol.com");

        // When
        User gUser = mapper.userDtoToGrpc(dUser);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(dUser.getName(),gUser.getName()),
                () -> Assertions.assertEquals(dUser.getSurname(),gUser.getSurname()),
                () -> Assertions.assertEquals(dUser.getEmail(),gUser.getEmail())
        );
    }

    @Test
    public void usersDtoToGrpc_createAListOfUserGrpc_ShouldReturnTheListOfUserGrpc() {
        // Given
        UserGrpcToFromDtoMapper mapper = Mappers.getMapper( UserGrpcToFromDtoMapper.class );
        UserDTO dUser1 = new UserDTO("Jhon","Doe","jhon.doe@aol.com");
        UserDTO dUser2 = new UserDTO("Jack","Green","jack.green@aol.com");
        Set<UserDTO> dUsers = Set.of(dUser1,dUser2);

        // When
        Set<User> gUsers = mapper.usersDtoToGrpc(dUsers);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(dUsers.size(),gUsers.size()),
                () -> {
                    Optional<User> optUser1 = gUsers.stream().filter(gUser -> gUser.getEmail().equals(dUser1.getEmail())).findFirst();
                    Assertions.assertTrue(optUser1.isPresent());
                    User gUser1 = optUser1.get();
                    Assertions.assertEquals(dUser1.getName(),gUser1.getName());
                    Assertions.assertEquals(dUser1.getSurname(),gUser1.getSurname());
                    Assertions.assertEquals(dUser1.getEmail(),gUser1.getEmail());

                    Optional<User> optUser2 = gUsers.stream().filter(gUser -> gUser.getEmail().equals(dUser2.getEmail())).findFirst();
                    Assertions.assertTrue(optUser2.isPresent());
                    User gUser2 = optUser2.get();
                    Assertions.assertEquals(dUser2.getName(),gUser2.getName());
                    Assertions.assertEquals(dUser2.getSurname(),gUser2.getSurname());
                    Assertions.assertEquals(dUser2.getEmail(),gUser2.getEmail());
                }
        );
    }

    @Test
    public void usersGrpcToDto_createAListOfUserDto_ShouldReturnTheListOfUserDto() {
        // Given
        User gUser1 = User.newBuilder()
                .setEmail("jhon.doe@aol.com")
                .setName("Jhon")
                .setSurname("Doe")
                .build();
        User gUser2 = User.newBuilder()
                .setEmail("jack.green@aol.com")
                .setName("Jack")
                .setSurname("Green")
                .build();

        UserGrpcToFromDtoMapper mapper = Mappers.getMapper( UserGrpcToFromDtoMapper.class );
        Set<User> gUsers = Set.of(gUser1,gUser2);

        // When
        Set<UserDTO> dUsers = mapper.usersGrpcToDto(gUsers);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(gUsers.size(),dUsers.size()),
                () -> {
                    Optional<UserDTO> optUser1 = dUsers.stream().filter(dUser -> dUser.getEmail().equals(gUser1.getEmail())).findFirst();
                    Assertions.assertTrue(optUser1.isPresent());
                    UserDTO dUser1 = optUser1.get();
                    Assertions.assertEquals(gUser1.getName(),dUser1.getName());
                    Assertions.assertEquals(gUser1.getSurname(),dUser1.getSurname());
                    Assertions.assertEquals(gUser1.getEmail(),dUser1.getEmail());

                    Optional<UserDTO> optUser2 = dUsers.stream().filter(dUser -> dUser.getEmail().equals(gUser2.getEmail())).findFirst();
                    Assertions.assertTrue(optUser2.isPresent());
                    UserDTO dUser2 = optUser2.get();
                    Assertions.assertEquals(gUser2.getName(),dUser2.getName());
                    Assertions.assertEquals(gUser2.getSurname(),dUser2.getSurname());
                    Assertions.assertEquals(gUser2.getEmail(),dUser2.getEmail());
                }
        );
    }

}
