package it.millsoft.users.api.test.integration;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.users.api.dto.UsersApiErrorEnum;
import it.millsoft.users.api.interfaces.grpc.model.*;
import it.millsoft.users.api.test.config.GrpcIntegrationTestConfig;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.Objects;

@SpringBootTest(properties = {
        // Start an internal GRPC server
        "grpc.server.inProcessName=test",
        "grpc.server.port=-1",
        "grpc.client.inProcess.address=in-process:test",
        // OpenTelemetry config
        "OTEL_EXPORTER_OTLP_ENDPOINT=http://otel-collector:4317",
        "OTEL_SERVICE_NAME=users-api"
})
@SpringJUnitConfig(classes = GrpcIntegrationTestConfig.class)
@DirtiesContext // Properly shutdown the GRPC Server after each test
@Testcontainers
public class UsersGrpcApi_IntegrationTest {

    private static Logger LOGGER = LoggerFactory.getLogger(UsersGrpcApi_IntegrationTest.class);

    @Container
    private static MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:5.0.6"));

    @GrpcClient("inProcess")
    private UsersGrpcApiGrpc.UsersGrpcApiBlockingStub usersGrpcApi;

    @Autowired
    private MongoTemplate mongoTemplate;

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        Objects.requireNonNull(mongoDBContainer);
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    private it.millsoft.users.api.model.User user1;
    private it.millsoft.users.api.model.User user2;
    private it.millsoft.users.api.model.User user3;

    @BeforeEach
    void setupDatabase() {
        // Given
        user1 = new it.millsoft.users.api.model.User();
        user1.setName("Jhon");
        user1.setSurname("Doe");
        user1.setEmail("jhon.doe@google.com");
        user1 = mongoTemplate.save(user1);
        LOGGER.debug("User {} saved with success", user1);

        user2 = new it.millsoft.users.api.model.User();
        user2.setName("Andy");
        user2.setSurname("Bee");
        user2.setEmail("andy.bee@google.com");
        user2 = mongoTemplate.save(user2);
        LOGGER.debug("User {} saved with success", user2);

        user3 = new it.millsoft.users.api.model.User();
        user3.setName("Carl");
        user3.setSurname("Green");
        user3.setEmail("carl.green@google.com");
        user3 = mongoTemplate.save(user3);
        LOGGER.debug("User {} saved with success", user3);
    }

    @AfterEach
    void cleanupDatabase() {
        mongoTemplate.getDb().drop();
    }

    @Test
    public void getUserByEmail_UserExists_ShoudlReturnTheUser() {
        // Given
        String email = user1.getEmail();

        // When
        GetUserByEmailRequest request = GetUserByEmailRequest.newBuilder().setEmail(email).build();
        GetUserByEmailResponse response = this.usersGrpcApi.getUserByEmail(request);
        User user = response.getUser();

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(user1.getName(), user.getName()),
                () -> Assertions.assertEquals(user1.getSurname(), user.getSurname()),
                () -> Assertions.assertEquals(user1.getEmail(), user.getEmail())
        );
    }

    @Test
    public void getUserByEmail_UserDoesntExists_ShouldReturnGrpcCode5NotFound() {
        // Given
        String email = "jack.black@aol.com";

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    GetUserByEmailRequest request = GetUserByEmailRequest.newBuilder().setEmail(email).build();
                    this.usersGrpcApi.getUserByEmail(request);
                },
                "It would have been raised " + StatusRuntimeException.class.getName() + " with GRPC CODE: " + Status.NOT_FOUND
        );

        Metadata metadata = Status.trailersFromThrowable(ex);
        UsersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance()));
        String usersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, UsersApiErrorEnum.valueOf(usersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void getUsers_ThreeUsersExist_ShouldReturnThreeUsers() {

        // Given --> Three users in the collection (see the setup)

        // When
        GetUsersRequest request = GetUsersRequest.newBuilder().build();
        GetUsersResponse response = this.usersGrpcApi.getUsers(request);

        // Then
        Assertions.assertEquals(3, response.getUsersList().size());
    }

    @Test
    public void getUsers_NoUsersExist_ShouldReturnEmptyList() {
        // Given --> No record in the users collection
        mongoTemplate.remove(new Query(), it.millsoft.users.api.model.User.class);

        // When
        GetUsersRequest request = GetUsersRequest.newBuilder().build();
        GetUsersResponse response = this.usersGrpcApi.getUsers(request);

        // THen
        Assertions.assertEquals(0, response.getUsersList().size());
    }

    @Test
    public void addUser_UsersDoesntExist_ShouldAddUserAndReturnId() {
        // Given
        String name = "James";
        String surname = "Cross";
        String email = "james.cross@google.com";

        // When
        User gUser = User.newBuilder()
                .setName(name)
                .setSurname(surname)
                .setEmail(email)
                .build();
        AddUserRequest request = AddUserRequest.newBuilder().setUser(gUser).build();
        AddUserResponse response = this.usersGrpcApi.addUser(request);

        // Then
        Assertions.assertNotNull(response.getId());
    }

    @Test
    public void addUser_UserExists_ShouldReturnGrpcCode6AlreadyExists() {
        // Given --> an existent user (email already exists)
        String name = "Jhon";
        String surname = "Junior Doe";
        String email = user1.getEmail();

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    User gUser = User.newBuilder()
                            .setName(name)
                            .setSurname(surname)
                            .setEmail(email)
                            .build();
                    AddUserRequest request = AddUserRequest.newBuilder().setUser(gUser).build();
                    this.usersGrpcApi.addUser(request);
                },
                "It would have been raised the " + StatusRuntimeException.class.getName() + " with GRPC CODE: " + Status.ALREADY_EXISTS
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        UsersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance()));
        String usersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS, ex.getStatus()),
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_ALREADY_EXISTS, UsersApiErrorEnum.valueOf(usersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void updateUser_UserExists_ShouldUpdateUserAndReturnId() {
        // Given
        String emailOfUserToUpdate = user1.getEmail();
        String idOfUserToUpdate = mongoTemplate.findOne(Query.query(Criteria.where("email").is(emailOfUserToUpdate)), it.millsoft.users.api.model.User.class)
                .getId();

        // When
        User gUser = User.newBuilder()
                .setName("Jhon")
                .setSurname("Junior Don")
                .setEmail("jhon.junior.doe@aol.com")
                .build();
        UpdateUserRequest request = UpdateUserRequest.newBuilder().setEmail(emailOfUserToUpdate).setUser(gUser).build();
        UpdateUserResponse response = this.usersGrpcApi.updateUser(request);

        // Then
        Assertions.assertEquals(idOfUserToUpdate, response.getId());
    }

    @Test
    public void updateUser_UserDoesntExists_ShouldReturnGrpcCode5NotFound() {
        // Given --> not existent user
        String emailOfUserToUpdate = "jhon.junior.doe@google.com";

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    User gUser = User.newBuilder()
                            .setName("Jhon")
                            .setSurname("Junior Don")
                            .setEmail("jhon.junior.doe@google.com")
                            .build();
                    UpdateUserRequest request = UpdateUserRequest.newBuilder().setEmail(emailOfUserToUpdate).setUser(gUser).build();
                    this.usersGrpcApi.updateUser(request);
                },
                "It would have been raised " + StatusRuntimeException.class.getName() + " with GRPC CODE: " + Status.NOT_FOUND
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        UsersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance()));
        String usersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, UsersApiErrorEnum.valueOf(usersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void deleteUser_UserExists_ShouldDeleteUserAndReturnId() {
        // Given
        String emailOfUserToDelete = "jhon.doe@google.com";
        String idOfUserToDelete = mongoTemplate.findOne(Query.query(Criteria.where("email").is(emailOfUserToDelete)), it.millsoft.users.api.model.User.class)
                .getId();

        // When
        DeleteUserRequest request = DeleteUserRequest.newBuilder().setEmail(emailOfUserToDelete).build();
        DeleteUserResponse response = this.usersGrpcApi.deleteUser(request);

        // Then
        Assertions.assertEquals(idOfUserToDelete, response.getId());
    }

    @Test
    public void deleteUser_UserDoesntExists_ShouldReturnGrpcCode5NotFound() {
        // Given
        String emailOfUserToDelete = "jhon.junior.doe@google.com";

        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    DeleteUserRequest request = DeleteUserRequest.newBuilder()
                            .setEmail(emailOfUserToDelete)
                            .build();
                    this.usersGrpcApi.deleteUser(request);
                },
                "It would have been raised " + StatusRuntimeException.class.getName() + " with GRPC CODE: " + Status.NOT_FOUND
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        UsersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance()));
        String usersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, UsersApiErrorEnum.valueOf(usersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void checkUserExistence_UserExists_ShouldReturnTrue() {
        // Given
        String email = user1.getEmail();

        // When
        CheckUserExistenceRequest request = CheckUserExistenceRequest.newBuilder().setEmail(email).build();
        CheckUserExistenceResponse response = this.usersGrpcApi.checkUserExistence(request);

        // Then
        Assertions.assertTrue(response.getExists());
    }

    @Test
    public void checkUserExistence_UserDoesntExists_ShouldReturnGrpcCode5NotFound() {
        // Given --> Not existent user
        String email = "jhon.junior.doe@google.com";

        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    CheckUserExistenceRequest request = CheckUserExistenceRequest.newBuilder()
                            .setEmail(email)
                            .build();
                    this.usersGrpcApi.checkUserExistence(request);
                },
                "It would have been raised " + StatusRuntimeException.class.getName() + " with GRPC CODE: " + Status.NOT_FOUND
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        UsersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance()));
        String usersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, UsersApiErrorEnum.valueOf(usersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

}
