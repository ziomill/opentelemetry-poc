package it.millsoft.users.api.test.config;

import it.millsoft.users.api.mappers.UserGrpcToFromDtoMapper;
import it.millsoft.users.api.interfaces.grpc.handlers.GrpcExceptionsHandler;
import it.millsoft.users.api.interfaces.grpc.services.UsersGrpcApiImpl;
import net.devh.boot.grpc.client.autoconfigure.GrpcClientAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcAdviceAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcServerAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcServerFactoryAutoConfiguration;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@TestConfiguration
@ImportAutoConfiguration(value = {GrpcServerAutoConfiguration.class,
                          GrpcServerFactoryAutoConfiguration.class,
                          GrpcClientAutoConfiguration.class,
                          GrpcAdviceAutoConfiguration.class})
@Import(GrpcExceptionsHandler.class)
public class GrpcUnitTestConfig {

    @Bean
    public UsersGrpcApiImpl createGrpcService() {
        return new UsersGrpcApiImpl();
    }

    @Bean
    UserGrpcToFromDtoMapper createUserGrpcToDtoMapper(){
        UserGrpcToFromDtoMapper mapper = Mappers.getMapper( UserGrpcToFromDtoMapper.class );
        return mapper;
    }

}
