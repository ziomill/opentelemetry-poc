package it.millsoft.users.api.test.integration;

import com.jayway.jsonpath.JsonPath;
import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.interfaces.rest.model.UsersApiErrorResponse;
import it.millsoft.users.api.model.User;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.*;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.net.URI;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static it.millsoft.users.api.commons.Utils.toURI;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
                // OpenTelemetry config
                "OTEL_EXPORTER_OTLP_ENDPOINT=http://otel-collector:4317",
                "OTEL_SERVICE_NAME=users-api"
        })
@Testcontainers
public class UsersRestApi_IntegrationTest {

    private static Logger LOGGER = LoggerFactory.getLogger(UsersRestApi_IntegrationTest.class);

    @Container
    private static MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:5.0.6"));

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private TestRestTemplate restTemplate;

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        Objects.requireNonNull(mongoDBContainer);
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    private User user1;
    private User user2;
    private User user3;

    @BeforeEach
    void setupDatabase() {
        // Given
        user1 = new User();
        user1.setName("Jhon");
        user1.setSurname("Doe");
        user1.setEmail("jhon.doe@google.com");
        user1 = mongoTemplate.save(user1);
        LOGGER.debug("User {} saved with success", user1);

        user2 = new User();
        user2.setName("Andy");
        user2.setSurname("Bee");
        user2.setEmail("andy.bee@google.com");
        user2 = mongoTemplate.save(user2);
        LOGGER.debug("User {} saved with success", user2);

        user3 = new User();
        user3.setName("Carl");
        user3.setSurname("Green");
        user3.setEmail("carl.green@google.com");
        user3 = mongoTemplate.save(user3);
        LOGGER.debug("User {} saved with success", user3);
    }

    @AfterEach
    void cleanupDatabase() {
        mongoTemplate.getDb().drop();
    }

    @Test
    public void getUserByEmail_UserExists_ShoudlReturnTheUserAndHttpCode200Ok() {
        // Given
        String email = user1.getEmail();

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);

        ResponseEntity<User> response = this.restTemplate.exchange(uri, HttpMethod.GET, requestEntity, User.class);

        // Then
        User responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(user1.getName(), responseBody.getName()),
                () -> Assertions.assertEquals(user1.getSurname(), responseBody.getSurname()),
                () -> Assertions.assertEquals(user1.getEmail(), responseBody.getEmail())
        );
    }

    @Test
    public void getUserByEmail_UserDoesntExists_ShouldReturnHttpCode404NotFound() {
        // Given
        String email = "jack.black@aol.com";

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);

        ResponseEntity<UsersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.GET, requestEntity, UsersApiErrorResponse.class);

        // Then
        UsersApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorResponse.ApplicativeErrorEnum.USER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void getUsers_ThreeUsersExist_ShouldReturnThreeUsersAndHttpCode200Ok() {
        // Given --> Three users in the collection (see the setup)

        // When
        String uriTemplate = "/users";
        URI uri = toURI(uriTemplate,null,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);

        ResponseEntity<Set> response = this.restTemplate.exchange(uri, HttpMethod.GET, requestEntity, Set.class);

        // Then
        Set<UserDTO> loadedUsers = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(3, loadedUsers.size())
        );
    }

    @Test
    public void getUsers_NoUsersExist_ShouldReturnEmptyListAndHttpCode200Ok() {
        // Given --> No record in the users collection
        mongoTemplate.remove(new Query(), User.class);

        // When
        String uriTemplate = "/users";
        URI uri = toURI(uriTemplate,null,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);

        ResponseEntity<Set> response = this.restTemplate.exchange(uri, HttpMethod.GET, requestEntity, Set.class);

        // Then

        Set<UserDTO> loadedUsers = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(0, loadedUsers.size())
        );
    }

    @Test
    public void addUser_UsersDoesntExist_ShouldAddUserAndReturnIdAndHttpCode200Ok() {
        // Given
        User body = new User();
        body.setName("James");
        body.setSurname("Cross");
        body.setEmail("james.cross@google.com");

        // When
        String uriTemplate = "/user";
        URI uri = toURI(uriTemplate,null,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<User> requestEntity = new HttpEntity(body, headers);

        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.POST, requestEntity, String.class);

        // Then
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertNotNull(JsonPath.read(response.getBody(), "$"))
        );
    }

    @Test
    public void addUser_UserExists_ShouldReturnHttpCode409Conflict() {
        // Given --> an existent user (email already exists)
        User body = new User();
        body.setName("Jhon");
        body.setSurname("Junior Doe");
        body.setEmail(user1.getEmail());

        // When
        String uriTemplate = "/user";
        URI uri = toURI(uriTemplate,null,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<User> requestEntity = new HttpEntity(body, headers);

        ResponseEntity<UsersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.POST, requestEntity, UsersApiErrorResponse.class);
        UsersApiErrorResponse responseBody = response.getBody();

        // Then
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.CONFLICT, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(UsersApiErrorResponse.ApplicativeErrorEnum.USER_ALREADY_EXISTS, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateUser_UserExists_ShouldUpdateUserAndReturnIdAndHttpCode200Ok() {
        // Given
        String emailOfUserToUpdate = user1.getEmail();

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",emailOfUserToUpdate);
        URI uri = toURI(uriTemplate,uriParams,null);

        User body = new User();
        body.setName("Jhon");
        body.setSurname("Junior Doe");
        body.setEmail("jhon.junior.doe@aol.com");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<User> requestEntity = new HttpEntity(body, headers);
        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.PUT, requestEntity, String.class);

        // Then
        String responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(user1.getId(), responseBody)
        );
    }

    @Test
    public void updateUser_UserDoesntExists_ShouldReturnHttpCode404NotFound() {
        // Given --> Not existent user
        String emailOfUserToUpdate = "jhon.junior.doe@google.com";

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",emailOfUserToUpdate);
        URI uri = toURI(uriTemplate,uriParams,null);

//        UserDTO body = new UserDTO("Jhon", "Junior Don", "jhon.junior.doe@google.com");
        User body = new User();
        body.setName("Jhon");
        body.setSurname("Junior Doe");
        body.setEmail("jhon.junior.doe@google.com");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<UserDTO> requestEntity = new HttpEntity(body, headers);
        ResponseEntity<UsersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.PUT, requestEntity, UsersApiErrorResponse.class);

        // Then
        UsersApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorResponse.ApplicativeErrorEnum.USER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void deleteUser_UserExists_ShouldDeleteUserAndReturnIdAndHttpCode200Ok() {
        // Given
        String emailOfUserToDelete = user1.getEmail();

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",emailOfUserToDelete);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> requestEntity = new HttpEntity(headers);

        ResponseEntity<String> response = this.restTemplate.exchange(uri, HttpMethod.DELETE, requestEntity, String.class);

        // Then
        String responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(user1.getId(), responseBody)
        );
    }

    @Test
    public void deleteUser_UserDoesntExists_ShouldReturnHttpCode404NotFound() {
        // Given
        String emailOfUserToDelete = "jhon.junior.doe@google.com";

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",emailOfUserToDelete);
        URI uri = toURI(uriTemplate,uriParams,null);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> requestEntity = new HttpEntity(headers);

        ResponseEntity<UsersApiErrorResponse> response = this.restTemplate.exchange(uri, HttpMethod.DELETE, requestEntity, UsersApiErrorResponse.class);

        // Then
        UsersApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorResponse.ApplicativeErrorEnum.USER_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void checkUserExistence_UserExists_ShouldReturnHttpCode204NoContent() {
        // Given
        String email = user1.getEmail(); // An existent user

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = toURI(uriTemplate,uriParams,null);
        ResponseEntity<Void> response = this.restTemplate.exchange(uri, HttpMethod.HEAD, null, Void.class);

        // Then
        Assertions.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    public void checkUserExistence_UserDoesntExists_ShouldReturnHttpCode404NotFound() {
        // Given
        String email = "jhon.junior.doe@google.com"; // An unexistent user

        // When
        String uriTemplate = "/user/{user_email}";
        Map<String, String> uriParams = Map.of("user_email",email);
        URI uri = toURI(uriTemplate,uriParams,null);
        ResponseEntity<Void> response = this.restTemplate.exchange(uri, HttpMethod.HEAD, null, Void.class);

        // Then
        Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

}
