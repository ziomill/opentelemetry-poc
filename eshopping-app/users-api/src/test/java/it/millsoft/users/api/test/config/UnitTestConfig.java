package it.millsoft.users.api.test.config;

import io.opentelemetry.api.OpenTelemetry;
import io.opentelemetry.api.metrics.LongCounter;
import io.opentelemetry.api.metrics.LongCounterBuilder;
import io.opentelemetry.api.metrics.Meter;
import it.millsoft.users.api.business.UsersServices;
import it.millsoft.users.api.business.UsersServicesImpl;
import it.millsoft.users.api.mappers.UserDtoToFromModelMapper;
import org.mapstruct.factory.Mappers;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class UnitTestConfig {

    @Bean
    public OpenTelemetry createMockedOpenTelemetry() {
        OpenTelemetry mockedOpenTelemetry = Mockito.mock(OpenTelemetry.class);
        Meter mockedMeter = Mockito.mock(Meter.class);
        Mockito.when(mockedOpenTelemetry.getMeter(ArgumentMatchers.anyString())).thenReturn(mockedMeter);

        LongCounterBuilder mockedCounterBuilder = Mockito.mock(LongCounterBuilder.class);
        Mockito.when(mockedMeter.counterBuilder(ArgumentMatchers.anyString())).thenReturn(mockedCounterBuilder);
        Mockito.when(mockedCounterBuilder.setDescription(ArgumentMatchers.anyString())).thenReturn(mockedCounterBuilder);

        LongCounter mockedCounter = Mockito.mock(LongCounter.class);
        Mockito.when(mockedCounterBuilder.build()).thenReturn(mockedCounter);
        return mockedOpenTelemetry;
    }

    @Bean
    public UsersServices createUsersServices() {
        return new UsersServicesImpl();
    }

    @Bean
    UserDtoToFromModelMapper createUserDtoToFromModelMapper(){
        UserDtoToFromModelMapper mapper = Mappers.getMapper( UserDtoToFromModelMapper.class );
        return mapper;
    }

}
