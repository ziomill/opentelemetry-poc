
package it.millsoft.users.api.test.unit;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import it.millsoft.users.api.model.User;
import it.millsoft.users.api.repositories.UsersMongoRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.Objects;
import java.util.Optional;

@DataMongoTest
@DirtiesContext
@Testcontainers
public class UsersMongoRepository_UnitTest {

    @Container
    private static MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:5.0.6"));

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        Objects.requireNonNull(mongoDBContainer);
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    @Autowired
    private UsersMongoRepository usersMongoRepository;
    @Autowired
    private MongoTemplate mongoTemplate;

    @AfterEach
    void cleanUpDatabase() {
        mongoTemplate.getDb().drop();
    }

    @Test
    public void findUserByEmail_UserFound_ShouldReturnTheUser() {
        // Given
        String name = "Jhon";
        String surname = "Doe";
        String email = "jhon.doe@google.com";
        DBObject userToSave = BasicDBObjectBuilder.start()
                .add("name", name)
                .add("surname", surname)
                .add("email", email)
                .get();

        this.mongoTemplate.save(userToSave, "users");

        // When
        Optional<User> user = usersMongoRepository.findUserByEmail(email);

        // Then
        Assertions.assertTrue(user.isPresent());
    }

    @Test
    public void findUserByEmail_UserNotFound_ShouldReturnAnEmptyOptional() {
        // Given a User not exists

        // When
        String email = "jhon.doe@google.com";
        Optional<User> user = usersMongoRepository.findUserByEmail(email);

        // Then
        Assertions.assertTrue(user.isEmpty());
    }

    @Test
    public void existByEmail_UserExists_ShouldReturnTrue() {
        // Given
        String name = "Jhon";
        String surname = "Doe";
        String email = "jhon.doe@google.com";
        DBObject userToSave = BasicDBObjectBuilder.start()
                .add("name", name)
                .add("surname", surname)
                .add("email", email)
                .get();

        this.mongoTemplate.save(userToSave, "users");

        // When
        boolean exists = usersMongoRepository.existByEmail(email);

        // Then
        Assertions.assertTrue(exists);
    }

    @Test
    public void existByEmail_UserExists_ShouldReturnFalse() {
        // Given a User not exists

        // When
        String email = "jhon.doe@google.com";
        boolean exists = usersMongoRepository.existByEmail(email);

        // Then
        Assertions.assertFalse(exists);
    }

}
