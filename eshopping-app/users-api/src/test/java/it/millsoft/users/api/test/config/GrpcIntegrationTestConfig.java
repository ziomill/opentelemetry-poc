package it.millsoft.users.api.test.config;

import it.millsoft.users.api.interfaces.grpc.handlers.GrpcExceptionsHandler;
import net.devh.boot.grpc.client.autoconfigure.GrpcClientAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcAdviceAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcServerAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcServerFactoryAutoConfiguration;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Import;

@TestConfiguration
@ImportAutoConfiguration(value = {GrpcServerAutoConfiguration.class,
                          GrpcServerFactoryAutoConfiguration.class,
                          GrpcClientAutoConfiguration.class,
                          GrpcAdviceAutoConfiguration.class})
@Import(GrpcExceptionsHandler.class)
public class GrpcIntegrationTestConfig {

//    @Bean
//    public UsersGrpcApi createGrpcService() {
//        return new UsersGrpcApi();
//    }

}
