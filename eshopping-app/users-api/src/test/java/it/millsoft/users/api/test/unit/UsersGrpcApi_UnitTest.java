package it.millsoft.users.api.test.unit;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.users.api.business.UsersServices;
import it.millsoft.users.api.dto.UserDTO;
import it.millsoft.users.api.dto.UsersApiErrorEnum;
import it.millsoft.users.api.exceptions.UserAlreadyExistsException;
import it.millsoft.users.api.exceptions.UserNotFoundException;
import it.millsoft.users.api.interfaces.grpc.model.*;
import it.millsoft.users.api.test.config.GrpcUnitTestConfig;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@SpringJUnitConfig(classes = GrpcUnitTestConfig.class)
@TestPropertySource(
        properties = {
                "grpc.server.inProcessName=test",
                "grpc.server.port=-1",
                "grpc.client.inProcess.address=in-process:test"
        }
)
@DirtiesContext  // Properly shutdown the GRPC Server after each test
public class UsersGrpcApi_UnitTest {

    @GrpcClient("inProcess")
    private UsersGrpcApiGrpc.UsersGrpcApiBlockingStub usersGrpcApi;

    @MockBean
    private UsersServices usersService;

    @Test
    public void getUserByEmail_UserExists_ShoudlReturnTheUser() {
        // Given
        String name = "Jhon";
        String surname = "Doe";
        String email = "jhon.doe@google.com";
        UserDTO mUser = new UserDTO(name, surname, email);
        Mockito.when(this.usersService.getUserByEmail(email)).thenReturn(mUser);

        // When
        GetUserByEmailRequest request = GetUserByEmailRequest.newBuilder().setEmail(email).build();
        GetUserByEmailResponse response = this.usersGrpcApi.getUserByEmail(request);
        User user = response.getUser();

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals("Jhon", user.getName()),
                () -> Assertions.assertEquals("Doe", user.getSurname()),
                () -> Assertions.assertEquals("jhon.doe@google.com", user.getEmail())
        );
    }

    @Test
    public void getUserByEmail_UserDoesntExists_ShouldReturnGrpcCode5NotFound() {
        // Given
        String email = "jhon.doe@google.com";
        Mockito.when(this.usersService.getUserByEmail(email)).thenThrow(new UserNotFoundException("User having email: " + email + " not found", UUID.randomUUID()));

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    GetUserByEmailRequest request = GetUserByEmailRequest.newBuilder().setEmail(email).build();
                    this.usersGrpcApi.getUserByEmail(request);
                },
                "It should have raised the " + StatusRuntimeException.class.getName() + " with GRPC Code: " + Status.NOT_FOUND
        );

        Metadata metadata = Status.trailersFromThrowable(ex);
        UsersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance()));
        String usersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                // GRPC
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                // Applicative
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, UsersApiErrorEnum.valueOf(usersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void getUsers_ThreeUsersExist_ShouldReturnThreeUsers() {
        // Given
        UserDTO mUser1 = new UserDTO("Jhon", "Doe", "jhon.doe@google.com");
        UserDTO mUser2 = new UserDTO("Andy", "Bee", "andy.bee@google.com");
        UserDTO mUser3 = new UserDTO("Carl", "Green", "carl.green@google.com");
        Set<UserDTO> mUsers = Set.of(mUser1, mUser2, mUser3);
        Mockito.when(this.usersService.getUsers()).thenReturn(mUsers);

        // When
        GetUsersRequest request = GetUsersRequest.newBuilder().build();
        GetUsersResponse response = this.usersGrpcApi.getUsers(request);

        // Then
        Assertions.assertEquals(3, response.getUsersList().size());
    }

    @Test
    public void getUsers_NoUsersExist_ShouldReturnEmptyList() {
        // Given
        Set<UserDTO> mUsers = new HashSet();
        Mockito.when(this.usersService.getUsers()).thenReturn(mUsers);

        // When
        GetUsersRequest request = GetUsersRequest.newBuilder().build();
        GetUsersResponse response = this.usersGrpcApi.getUsers(request);

        // THen
        Assertions.assertEquals(0, response.getUsersList().size());
    }

    @Test
    public void addUser_UsersDoesntExist_ShouldAddUserAndReturnId() {
        // Given
        String name = "Jhon";
        String surname = "Doe";
        String email = "jhon.doe@google.com";
        String mockedCreatedUserId = (new ObjectId()).toString();
        Mockito.when(this.usersService.addUser(name, surname, email)).thenReturn(mockedCreatedUserId);

        // When
        User gUser = User.newBuilder()
                .setName(name)
                .setSurname(surname)
                .setEmail(email)
                .build();
        AddUserRequest request = AddUserRequest.newBuilder().setUser(gUser).build();
        AddUserResponse response = this.usersGrpcApi.addUser(request);

        // Then
        Assertions.assertEquals(mockedCreatedUserId, response.getId());
    }

    @Test
    public void addUser_UserExists_ShouldReturnGrpcCode6AlreadyExists() {
        // Given
        String name = "Jhon";
        String surname = "Doe";
        String email = "jhon.doe@google.com";
        UserAlreadyExistsException mockedThrownException = new UserAlreadyExistsException("User already exists", UUID.randomUUID());
        Mockito.when(this.usersService.addUser(name, surname, email)).thenThrow(mockedThrownException);

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    User gUser = User.newBuilder()
                            .setName(name)
                            .setSurname(surname)
                            .setEmail(email)
                            .build();
                    AddUserRequest request = AddUserRequest.newBuilder().setUser(gUser).build();

                    this.usersGrpcApi.addUser(request);
                },
                "It should have raised the " + StatusRuntimeException.class.getName() + " with GRPC Code: " + Status.ALREADY_EXISTS
        );

        Metadata metadata = Status.trailersFromThrowable(ex);
        UsersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance()));
        String usersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                // GRPC
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS, ex.getStatus()),
                // Applicative
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_ALREADY_EXISTS, UsersApiErrorEnum.valueOf(usersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void updateUser_UserExists_ShouldUpdateUserAndReturnId() {
        // Given
        String name = "JhonJhon";
        String surname = "DonDon";
        String email = "jhon.doe@aol.com";
        UserDTO body = new UserDTO(name, surname, email);
        String emailOfUserToUpdate = "jhon.doe@google.com";
        String mockedUpdatedUserId = (new ObjectId()).toString();
        Mockito.when(this.usersService.updateUser(emailOfUserToUpdate, body)).thenReturn(mockedUpdatedUserId);

        // When
        User gUser = User.newBuilder()
                .setName(name)
                .setSurname(surname)
                .setEmail(email)
                .build();
        UpdateUserRequest request = UpdateUserRequest.newBuilder().setEmail(emailOfUserToUpdate).setUser(gUser).build();
        UpdateUserResponse response = this.usersGrpcApi.updateUser(request);
        Assertions.assertEquals(mockedUpdatedUserId, response.getId());
    }

    @Test
    public void updateUser_UserDoesntExists_ShouldReturnGrpcCode5NotFound() {
        // Given
        String emailOfUserToUpdate = "jhon.doe@google.com";
        String name = "JhonJhon";
        String surname = "DonDon";
        String email = "jhon.doe@aol.com";
        UserDTO body = new UserDTO(name, surname, email);
        UserNotFoundException mockedThrownException = new UserNotFoundException("User not found", UUID.randomUUID());
        Mockito.when(this.usersService.updateUser(emailOfUserToUpdate, body)).thenThrow(mockedThrownException);

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    User gUser = User.newBuilder()
                            .setName(name)
                            .setSurname(surname)
                            .setEmail(email)
                            .build();
                    UpdateUserRequest request = UpdateUserRequest.newBuilder().setEmail(emailOfUserToUpdate).setUser(gUser).build();
                    this.usersGrpcApi.updateUser(request);
                },
                "It should have raised the " + StatusRuntimeException.class.getName() + " with GRPC Code: " + Status.NOT_FOUND
        );

        Metadata metadata = Status.trailersFromThrowable(ex);
        UsersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance()));
        String usersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, UsersApiErrorEnum.valueOf(usersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void deleteUser_UserExists_ShouldDeleteUserAndReturnId() {
        // Given
        String emailOfUserToDelete = "jhon.doe@google.com";
        String mockedDeletedUserId = (new ObjectId()).toString();
        Mockito.when(this.usersService.deleteUser(emailOfUserToDelete)).thenReturn(mockedDeletedUserId);

        // When
        DeleteUserRequest request = DeleteUserRequest.newBuilder().setEmail(emailOfUserToDelete).build();
        DeleteUserResponse response = this.usersGrpcApi.deleteUser(request);

        // Then
        Assertions.assertEquals(mockedDeletedUserId, response.getId());
    }

    @Test
    public void deleteUser_UserDoesntExists_ShouldReturnGrpcCode5NotFound() {
        // Given
        String emailOfUserToDelete = "jhon.doe@google.com";
        UserNotFoundException mockedThrownException = new UserNotFoundException("User not found", UUID.randomUUID());
        Mockito.when(this.usersService.deleteUser(emailOfUserToDelete)).thenThrow(mockedThrownException);

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    DeleteUserRequest request = DeleteUserRequest.newBuilder()
                            .setEmail(emailOfUserToDelete)
                            .build();
                    this.usersGrpcApi.deleteUser(request);
                },
                "It should have raised the " + StatusRuntimeException.class.getName() + " with GRPC Code: " + Status.NOT_FOUND
        );

        Metadata metadata = Status.trailersFromThrowable(ex);
        UsersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance()));
        String usersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, UsersApiErrorEnum.valueOf(usersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void checkUserExistence_UserExists_ShouldReturnTrue() {
        // Given
        String email = "jhon.doe@google.com";
        (Mockito.doNothing().when(this.usersService)).checkUserExistence(email);

        // When
        CheckUserExistenceRequest request = CheckUserExistenceRequest.newBuilder().setEmail(email).build();
        CheckUserExistenceResponse response = this.usersGrpcApi.checkUserExistence(request);

        // Then
        Assertions.assertTrue(response.getExists());
    }

    @Test
    public void checkUserExistence_UserDoesntExists_ShouldReturnGrpcCode5NotFound() {
        // Given
        String email = "jhon.doe@google.com";
        UserNotFoundException mockedThrownException = new UserNotFoundException("User not found", UUID.randomUUID());
        (Mockito.doThrow(mockedThrownException).when(this.usersService)).checkUserExistence(email);

        // Then
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> {
                    // When
                    CheckUserExistenceRequest request = CheckUserExistenceRequest.newBuilder().setEmail(email).build();
                    this.usersGrpcApi.checkUserExistence(request);
                },
                "It should have raised the " + StatusRuntimeException.class.getName() + " with GRPC Code: " + Status.NOT_FOUND
        );

        Metadata metadata = Status.trailersFromThrowable(ex);
        UsersApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(UsersApiErrorResponse.getDefaultInstance()));
        String usersApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(UsersApiErrorEnum.USER_NOT_FOUND, UsersApiErrorEnum.valueOf(usersApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

}
