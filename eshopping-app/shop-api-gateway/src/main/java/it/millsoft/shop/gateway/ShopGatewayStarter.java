package it.millsoft.shop.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ShopGatewayStarter {

    public static void main(String[] args) {
        SpringApplication.run(ShopGatewayStarter.class, args);
    }

}
