package it.millsoft.products.api.test.unit;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.products.api.business.ProductsServices;
import it.millsoft.products.api.dto.ProductDTO;
import it.millsoft.products.api.dto.ProductsApiErrorEnum;
import it.millsoft.products.api.exceptions.InsufficientStockException;
import it.millsoft.products.api.exceptions.InvalidInputException;
import it.millsoft.products.api.exceptions.ProductAlreadyExistsException;
import it.millsoft.products.api.exceptions.ProductNotFoundException;
import it.millsoft.products.api.interfaces.grpc.model.*;
import it.millsoft.products.api.test.config.GrpcUnitTestConfig;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringJUnitConfig(classes = GrpcUnitTestConfig.class)
@TestPropertySource(
        properties = {
                "grpc.server.inProcessName=test",
                "grpc.server.port=-1",
                "grpc.client.inProcess.address=in-process:test"
        }
)
@DirtiesContext  // Properly shutdown the GRPC Server after each test
public class ProductsGrpcApi_UnitTest {

    @GrpcClient("inProcess")
    private ProductsGrpcApiGrpc.ProductsGrpcApiBlockingStub productsGrpcApi;

    @MockBean
    private ProductsServices productsServices;

    @Test
    public void addProduct_ProductDoesntExist_ShouldAddProductAndReturnId() {
        // Given
        String mCreatedProductId = new ObjectId().toString();
        when(productsServices.addProduct(any(ProductDTO.class))).thenReturn(mCreatedProductId);

        // When
        Product gProduct = Product.newBuilder()
                .setCode("PRD0001")
                .setDescription("IPhone 11")
                .setCategory("Electronic")
                .setPrice(899.00)
                .setAvailableStock(13)
                .build();
        AddProductRequest request = AddProductRequest.newBuilder()
                .setProduct(gProduct)
                .build();
        AddProductResponse response = productsGrpcApi.addProduct(request);

        // Then
        Assertions.assertEquals(mCreatedProductId, response.getId());
    }

    @Test
    public void addProduct_ProductAlreadyExist_ShouldReturnGrpcCode6AlreadyExists() {
        // Given
        ProductAlreadyExistsException mThrownException = new ProductAlreadyExistsException("Product already exists", UUID.randomUUID());
        Mockito.when(this.productsServices.addProduct(any(ProductDTO.class))).thenThrow(mThrownException);

        // Then
        Product gProduct = Product.newBuilder()
                .setCode("PRD0001")
                .setDescription("IPhone 11")
                .setCategory("Electronic")
                .setPrice(899.00)
                .setAvailableStock(13)
                .build();
        AddProductRequest request = AddProductRequest.newBuilder()
                .setProduct(gProduct)
                .build();
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.addProduct(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS, ex.getStatus()),
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.PRODUCT_ALREADY_EXISTS, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void addProducts_TwoOfThreeProvidedProductsDoesntExist_ShouldAddNotExistentProducts() {
        // Given
        String notExistentProductCode1 = "PRD0001";
        String notExistentProductId1 = new ObjectId().toString();
        String notExistentProductCode2 = "PRD0002";
        String notExistentProductId2 = new ObjectId().toString();
        String existentProductCode = "PRD0003";
        Map<String, String> mCreatedProducts = Map.of(notExistentProductCode1, notExistentProductId1,
                notExistentProductCode2, notExistentProductId2);
        when(productsServices.addProducts(any(Set.class))).thenReturn(mCreatedProducts);

        // When
        Product gProduct1 = Product.newBuilder()
                .setCode(notExistentProductCode1)
                .setDescription("Lenovo Keyboard")
                .setCategory("Electronic")
                .setPrice(34.0)
                .setAvailableStock(13)
                .build();

        Product gProduct2 = Product.newBuilder()
                .setCode(notExistentProductCode2)
                .setDescription("IPhone 11")
                .setCategory("Electronic")
                .setPrice(899.0)
                .setAvailableStock(13)
                .build();

        Product gProduct3 = Product.newBuilder()
                .setCode(existentProductCode)
                .setDescription("Logitech Optical Mouse")
                .setCategory("Electronic")
                .setPrice(17.0)
                .setAvailableStock(3)
                .build();

        AddProductsRequest request = AddProductsRequest.newBuilder()
                .addProducts(gProduct1)
                .addProducts(gProduct2)
                .addProducts(gProduct3)
                .build();
        AddProductsResponse response = productsGrpcApi.addProducts(request);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(2, response.getCodesToIdsMapMap().size()),
                () -> {
                    String actualIdProduct1 = response.getCodesToIdsMapOrThrow(notExistentProductCode1);
                    Assertions.assertEquals(notExistentProductId1, actualIdProduct1);
                },
                () -> {
                    String actualIdProduct2 = response.getCodesToIdsMapOrThrow(notExistentProductCode2);
                    Assertions.assertEquals(notExistentProductId2, actualIdProduct2);
                }
        );
    }

    @Test
    public void addProducts_AllProvidedProductsAlreadyExist_ShouldReturnGrpcCode6AlreadyExists() {
        // Given
        String existentProductCode1 = "PRD0001";
        String existentProductCode2 = "PRD0002";
        String existentProductCode3 = "PRD0003";
        ProductAlreadyExistsException mProductAlreadyExistsException = new ProductAlreadyExistsException("Provided products already exist", UUID.randomUUID());
        when(productsServices.addProducts(any(Set.class))).thenThrow(mProductAlreadyExistsException);

        // When
        Product gProduct1 = Product.newBuilder()
                .setCode(existentProductCode1)
                .setDescription("Lenovo Keyboard")
                .setCategory("Electronic")
                .setPrice(34.0)
                .setAvailableStock(13)
                .build();

        Product gProduct2 = Product.newBuilder()
                .setCode(existentProductCode2)
                .setDescription("IPhone 11")
                .setCategory("Electronic")
                .setPrice(899.0)
                .setAvailableStock(13)
                .build();

        Product gProduct3 = Product.newBuilder()
                .setCode(existentProductCode3)
                .setDescription("Logitech Optical Mouse")
                .setCategory("Electronic")
                .setPrice(17.0)
                .setAvailableStock(3)
                .build();

        AddProductsRequest request = AddProductsRequest.newBuilder()
                .addProducts(gProduct1)
                .addProducts(gProduct2)
                .addProducts(gProduct3)
                .build();

        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.addProducts(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS, ex.getStatus()),
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.PRODUCT_ALREADY_EXISTS, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void addProducts_InvalidInputNoProductsProvided_ShouldReturnGrpcCode3InvalidArgument() {
        // Given
        InvalidInputException mInvalidInputException = new InvalidInputException("Invalid Input", UUID.randomUUID());
        when(productsServices.addProducts(null)).thenThrow(mInvalidInputException);
        when(productsServices.addProducts(new HashSet<>())).thenThrow(mInvalidInputException);

        // When
        AddProductsRequest request = AddProductsRequest.newBuilder().build();
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.addProducts(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT, ex.getStatus()),
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.INVALID_INPUT, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void getProductByCode_ProductFound_ShouldReturnTheProduct() {
        // Given
        String productCode = "PRD0001";
        ProductDTO mProduct = new ProductDTO(productCode, "Lenovo Keyboard", "Electronic", 27.0, 16);
        when(productsServices.getProductByCode(productCode)).thenReturn(mProduct);

        // When
        GetProductByCodeRequest request = GetProductByCodeRequest.newBuilder()
                .setCode(productCode)
                .build();
        GetProductByCodeResponse response = productsGrpcApi.getProductByCode(request);
        Product gProduct = response.getProduct();
        Assertions.assertAll(
                () -> Assertions.assertNotNull(gProduct),
                () -> Assertions.assertEquals(mProduct.getCode(), gProduct.getCode()),
                () -> Assertions.assertEquals(mProduct.getDescription(), gProduct.getDescription()),
                () -> Assertions.assertEquals(mProduct.getCategory(), gProduct.getCategory()),
                () -> Assertions.assertEquals(mProduct.getAvailableStock(), gProduct.getAvailableStock()),
                () -> Assertions.assertEquals(mProduct.getPrice(), gProduct.getPrice())
        );
    }

    @Test
    public void getProductByCode_ProductNotFound_ShouldReturnGrpcCode5NotFound() {
        // Given
        String productCode = "PRD0001";
        ProductNotFoundException mProductNotFoundException = new ProductNotFoundException("Product Not Found", UUID.randomUUID());
        when(productsServices.getProductByCode(productCode)).thenThrow(mProductNotFoundException);

        // Then
        GetProductByCodeRequest request = GetProductByCodeRequest.newBuilder()
                .setCode(productCode)
                .build();
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.getProductByCode(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.PRODUCT_NOT_FOUND, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void getProductsByCode_selectAllProducts_ShouldReturnAllProducts() {
        // Given
        ProductDTO mProduct1 = new ProductDTO("PRD00001", "Lenovo Keyboard", "Electronic", 27.0, 6);
        ProductDTO mProduct2 = new ProductDTO("PRD00002", "Logitech Optical Mouse", "Electronic", 12.0, 5);
        ProductDTO mProduct3 = new ProductDTO("PRD00003", "IPhone 11", "Electronic", 899.0, 5);
        Set<ProductDTO> mProducts = Set.of(mProduct1, mProduct2, mProduct3);
        when(productsServices.getProductsByCode(null)).thenReturn(mProducts);
        when(productsServices.getProductsByCode(new HashSet<>())).thenReturn(mProducts);

        // When
        GetProductsByCodeRequest request = GetProductsByCodeRequest.newBuilder().build();
        GetProductsByCodeResponse response = productsGrpcApi.getProductsByCode(request);

        // Then
        Assertions.assertEquals(mProducts.size(), response.getProductList().size());
    }

    @Test
    public void getProductsByCode_AllOfSelectedProductsExist_ShouldReturnThreeProducts() {
        // Given
        String mProductCode1 = "PRD00001";
        ProductDTO mProduct1 = new ProductDTO(mProductCode1, "Lenovo Keyboard", "Electronic", 27.0, 6);
        String mProductCode2 = "PRD00002";
        ProductDTO mProduct2 = new ProductDTO(mProductCode2, "Logitech Optical Mouse", "Electronic", 12.0, 5);
        String mProductCode3 = "PRD00003";
        ProductDTO mProduct3 = new ProductDTO(mProductCode3, "IPhone 11", "Electronic", 899.0, 5);
        Set<String> mProductsCode = Set.of(mProductCode1, mProductCode2, mProductCode3);
        Set<ProductDTO> mProducts = Set.of(mProduct1, mProduct2, mProduct3);
        when(productsServices.getProductsByCode(mProductsCode)).thenReturn(mProducts);

        // When
        GetProductsByCodeRequest request = GetProductsByCodeRequest.newBuilder()
                .addAllCodes(mProductsCode)
                .build();
        GetProductsByCodeResponse response = productsGrpcApi.getProductsByCode(request);

        // Then
        Assertions.assertEquals(mProducts.size(), response.getProductList().size());
    }

    @Test
    public void getProductsByCode_SomeOfSelectedProductsExist_ShouldReturnTwoProductsOfThreeRequested() {
        // Given
        String mProductCode1 = "PRD00001";
        ProductDTO mProduct1 = new ProductDTO(mProductCode1, "Lenovo Keyboard", "Electronic", 27.0, 6);
        String mProductCode2 = "PRD00002";
        ProductDTO mProduct2 = new ProductDTO(mProductCode2, "Logitech Optical Mouse", "Electronic", 12.0, 5);
        String notExistentProductCode = "PRD00003";

        Set<String> mProductsCode = Set.of(mProductCode1, mProductCode2, notExistentProductCode);
        Set<ProductDTO> mProducts = Set.of(mProduct1, mProduct2);
        when(productsServices.getProductsByCode(mProductsCode)).thenReturn(mProducts);

        // When
        GetProductsByCodeRequest request = GetProductsByCodeRequest.newBuilder()
                .addAllCodes(mProductsCode)
                .build();
        GetProductsByCodeResponse response = productsGrpcApi.getProductsByCode(request);

        // Then
        Assertions.assertEquals(mProducts.size(), response.getProductList().size());
    }

    @Test
    public void getProductsByCode_SelectedProductsDoesntExist_ShouldReturnAnEmptyListAndHttpCode200OK() {
        // Given
        String notExistentProductCode1 = "PRD00001";
        String notExistentProductCode2 = "PRD00002";
        String notExistentProductCode3 = "PRD00003";

        Set<String> mProductsCode = Set.of(notExistentProductCode1, notExistentProductCode2, notExistentProductCode3);
        Set<ProductDTO> mProducts = new HashSet<>();
        when(productsServices.getProductsByCode(mProductsCode)).thenReturn(mProducts);

        // When
        GetProductsByCodeRequest request = GetProductsByCodeRequest.newBuilder()
                .addAllCodes(mProductsCode)
                .build();
        GetProductsByCodeResponse response = productsGrpcApi.getProductsByCode(request);

        // Then
        Assertions.assertEquals(mProducts.size(), response.getProductList().size());
    }

    @Test
    public void updateStocks_ProductsAreAvailableInStock_ShouldReturnTheNewAvailableStock() {
        // Given
        String mProductCode1 = "PRD0001";
        String mProductCode2 = "PRD0002";

        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put(mProductCode1, 5);
        requestedStocks.put(mProductCode2, 3);

        Map<String, Integer> newAvailableStocks = new HashMap<>();
        newAvailableStocks.put(mProductCode1, 10);
        newAvailableStocks.put(mProductCode2, 8);

        when(productsServices.updateStocks(requestedStocks)).thenReturn(newAvailableStocks);

        // When
        UpdateStocksRequest request = UpdateStocksRequest.newBuilder()
                .putAllCodesToStocks(requestedStocks)
                .build();
        UpdateStocksResponse response = productsGrpcApi.updateStocks(request);
        Assertions.assertAll(
                () -> Assertions.assertEquals(requestedStocks.size(), response.getCodesToStocksCount()),
                () -> {
                    int expectedNewAvailableStockProduct1 = newAvailableStocks.get(mProductCode1);
                    int actualNewAvailableStockProduct1 = response.getCodesToStocksOrThrow(mProductCode1);
                    Assertions.assertEquals(expectedNewAvailableStockProduct1, actualNewAvailableStockProduct1);
                },
                () -> {
                    int expectedNewAvailableStockProduct2 = newAvailableStocks.get(mProductCode2);
                    int actualNewAvailableStockProduct2 = response.getCodesToStocksOrThrow(mProductCode2);
                    Assertions.assertEquals(expectedNewAvailableStockProduct2, actualNewAvailableStockProduct2);
                }
        );
    }

    @Test
    public void updateStocks_AtLeastOneProductsNotFound_ShouldReturnGrpcCode5NotFound() {
        // Given
        String mProductCode1 = "PRD0001";
        String mProductCode2 = "PRD0002";

        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put(mProductCode1, 5);
        requestedStocks.put(mProductCode2, 3);

        ProductNotFoundException mProductNotFoundException = new ProductNotFoundException("Product Not Found", UUID.randomUUID());
        when(productsServices.updateStocks(requestedStocks)).thenThrow(mProductNotFoundException);

        // When
        UpdateStocksRequest request = UpdateStocksRequest.newBuilder()
                .putAllCodesToStocks(requestedStocks)
                .build();
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.updateStocks(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, ex.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.PRODUCT_NOT_FOUND, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void updateStocks_InvalidStockAsInput_ShouldReturnGrpcCode3InvalidArgument() {
        // Given
        Map<String, Integer> requestedStocks = new HashMap<>();

        InvalidInputException mInvalidInputException = new InvalidInputException("Invalid Stock as input", UUID.randomUUID());
        when(productsServices.updateStocks(null)).thenThrow(mInvalidInputException);
        when(productsServices.updateStocks(requestedStocks)).thenThrow(mInvalidInputException);

        // When
        UpdateStocksRequest request = UpdateStocksRequest.newBuilder()
                .putAllCodesToStocks(requestedStocks)
                .build();
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.updateStocks(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT, ex.getStatus()),
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.INVALID_INPUT, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void updateStocks_AtLeastOneProductWithInsufficientStock_ShouldReturnGrpcCode7PermissionDenied() {
        // Given
        String mProductCode1 = "PRD0001";
        String mProductCode2 = "PRD0002";

        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put(mProductCode1, 5);
        requestedStocks.put(mProductCode2, 3);

        InsufficientStockException mInsufficientStockException = new InsufficientStockException("Insufficient Stock", UUID.randomUUID());
        when(productsServices.updateStocks(requestedStocks)).thenThrow(mInsufficientStockException);

        // When
        UpdateStocksRequest request = UpdateStocksRequest.newBuilder()
                .putAllCodesToStocks(requestedStocks)
                .build();
        StatusRuntimeException ex = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.updateStocks(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(ex);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.PERMISSION_DENIED, ex.getStatus()),
                () -> Assertions.assertEquals(Status.PERMISSION_DENIED.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.INSUFFICIENT_STOCK, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }


}
