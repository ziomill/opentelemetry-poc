package it.millsoft.products.api.test.config;

import it.millsoft.products.api.business.ProductsServices;
import it.millsoft.products.api.business.ProductsServicesImpl;
import it.millsoft.products.api.mappers.ProductDtoToFromModelMapper;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

@TestConfiguration
public class UnitTestConfig {

    @Bean
    public ProductsServices createUsersServices() {
        return new ProductsServicesImpl();
    }

    @Bean
    ProductDtoToFromModelMapper createProductDtoToFromModelMapper(){
        ProductDtoToFromModelMapper mapper = Mappers.getMapper( ProductDtoToFromModelMapper.class );
        return mapper;
    }
}
