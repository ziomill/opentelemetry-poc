package it.millsoft.products.api.test.config;

import it.millsoft.products.api.mappers.ProductGrpcToFromDtoMapper;
import it.millsoft.products.api.interfaces.grpc.handlers.GrpcExceptionsHandler;
import it.millsoft.products.api.interfaces.grpc.services.ProductsGrpcApiImpl;
import net.devh.boot.grpc.client.autoconfigure.GrpcClientAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcAdviceAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcServerAutoConfiguration;
import net.devh.boot.grpc.server.autoconfigure.GrpcServerFactoryAutoConfiguration;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;

@TestConfiguration
@ImportAutoConfiguration(value = {GrpcServerAutoConfiguration.class,
                          GrpcServerFactoryAutoConfiguration.class,
                          GrpcClientAutoConfiguration.class,
                          GrpcAdviceAutoConfiguration.class})
@Import(GrpcExceptionsHandler.class)
public class GrpcUnitTestConfig {

    @Bean
    public ProductsGrpcApiImpl createGrpcService() {
        return new ProductsGrpcApiImpl();
    }

    @Bean
    ProductGrpcToFromDtoMapper createUserGrpcToDtoMapper(){
        ProductGrpcToFromDtoMapper mapper = Mappers.getMapper( ProductGrpcToFromDtoMapper.class );
        return mapper;
    }

}
