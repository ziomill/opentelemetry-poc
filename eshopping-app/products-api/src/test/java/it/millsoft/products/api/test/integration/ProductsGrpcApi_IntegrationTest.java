package it.millsoft.products.api.test.integration;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.products.api.dto.ProductsApiErrorEnum;
import it.millsoft.products.api.interfaces.grpc.model.*;
import it.millsoft.products.api.test.config.GrpcIntegrationTestConfig;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@SpringBootTest(properties = {
        // Start an internal GRPC server
        "grpc.server.inProcessName=test",
        "grpc.server.port=-1",
        "grpc.client.inProcess.address=in-process:test"
})
@SpringJUnitConfig(classes = GrpcIntegrationTestConfig.class)
@DirtiesContext  // Properly shutdown the GRPC Server after each test
@Testcontainers
public class ProductsGrpcApi_IntegrationTest {

    private static Logger LOGGER = LoggerFactory.getLogger(ProductsGrpcApi_IntegrationTest.class);

    @Container
    private static MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:5.0.6"));

    @GrpcClient("inProcess")
    private ProductsGrpcApiGrpc.ProductsGrpcApiBlockingStub productsGrpcApi;

    @Autowired
    private MongoTemplate mongoTemplate;

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        Objects.requireNonNull(mongoDBContainer);
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    private it.millsoft.products.api.model.Product product1;
    private it.millsoft.products.api.model.Product product2;
    private it.millsoft.products.api.model.Product product3;

    @BeforeEach
    void setupDatabase() {
        // Given
        product1 = new it.millsoft.products.api.model.Product();
        product1.setCode("PRD00001");
        product1.setDescription("IPhone 11");
        product1.setCategory("Electronic");
        product1.setPrice(899.0);
        product1.setAvailableStock(15);
        this.product1 = mongoTemplate.save(product1);
        LOGGER.debug("Product {} saved with success", product1);

        product2 = new it.millsoft.products.api.model.Product();
        product2.setCode("PRD00002");
        product2.setDescription("Logitech Optical Mouse");
        product2.setCategory("Electronic");
        product2.setPrice(14.99);
        product2.setAvailableStock(32);
        product2 = mongoTemplate.save(product2);
        LOGGER.debug("Product {} saved with success", product2);

        product3 = new it.millsoft.products.api.model.Product();
        product3.setCode("PRD00003");
        product3.setDescription("Lenovo Keyboard");
        product3.setCategory("Electronic");
        product3.setPrice(34.0);
        product3.setAvailableStock(7);
        product3 = mongoTemplate.save(product3);
        LOGGER.debug("Product {} saved with success", product3);
    }

    @AfterEach
    void cleanupDatabase() {
        mongoTemplate.getDb().drop();
    }

    @Test
    public void addProduct_ProductDoesntExist_ShouldAddProductAndReturnId() {
        // Given
        Product notExistentProduct = Product.newBuilder()
                .setCode("PRD00004")
                .setDescription("Lenovo X1 Carbon Notebook")
                .setCategory("Electronic")
                .setPrice(1799.00)
                .setAvailableStock(5)
                .build();

        // When
        AddProductRequest request = AddProductRequest.newBuilder()
                .setProduct(notExistentProduct)
                .build();
        AddProductResponse response = productsGrpcApi.addProduct(request);

        // Then
        Assertions.assertNotNull(response.getId());
    }

    @Test
    public void addProduct_ProductAlreadyExist_ShouldReturnGrpcCode6AlreadyExists() {
        // Given
        Product alreadyExistentProduct = Product.newBuilder()
                .setCode(product1.getCode())
                .setDescription("IPhone 11")
                .setCategory("Electronic")
                .setPrice(899.0)
                .setAvailableStock(15)
                .build();

        // Then
        AddProductRequest request = AddProductRequest.newBuilder()
                .setProduct(alreadyExistentProduct)
                .build();
        StatusRuntimeException sre = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.addProduct(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(sre);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
//        String productsApiErrorCode = new String(errorResponse.getApplicativeError().getValue().toByteArray());
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS, sre.getStatus()),
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.PRODUCT_ALREADY_EXISTS, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void addProducts_TwoOfThreeProvidedProductsDoesntExist_ShouldAddNotExistentProducts() {
        // When
        String notExistentProductCode1 = "PRD00020";
        Product gProduct1 = Product.newBuilder()
                .setCode(notExistentProductCode1)
                .setDescription("Dell Monitor X11")
                .setCategory("Electronic")
                .setPrice(215.0)
                .setAvailableStock(10)
                .build();

        String notExistentProductCode2 = "PRD00021";
        Product gProduct2 = Product.newBuilder()
                .setCode(notExistentProductCode2)
                .setDescription("Ethernet cable")
                .setCategory("Electronic")
                .setPrice(4.99)
                .setAvailableStock(50)
                .build();

        String existentProductCode = product1.getCode();
        Product gProduct3 = Product.newBuilder()
                .setCode(existentProductCode)
                .setDescription("IPhone 11")
                .setCategory("Electronic")
                .setPrice(899.0)
                .setAvailableStock(15)
                .build();

        AddProductsRequest request = AddProductsRequest.newBuilder()
                .addProducts(gProduct1)
                .addProducts(gProduct2)
                .addProducts(gProduct3)
                .build();
        AddProductsResponse response = productsGrpcApi.addProducts(request);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(2, response.getCodesToIdsMapMap().size()),
                () -> {
                    String actualIdProduct1 = response.getCodesToIdsMapOrThrow(notExistentProductCode1);
                    Assertions.assertNotNull(actualIdProduct1);
                },
                () -> {
                    String actualIdProduct2 = response.getCodesToIdsMapOrThrow(notExistentProductCode2);
                    Assertions.assertNotNull(actualIdProduct2);
                }
        );
    }

    @Test
    public void addProducts_AllProvidedProductsAlreadyExist_ShouldReturnGrpcCode6AlreadyExists() {
        // Given
        String existentProductCode1 = product1.getCode();
        Product gProduct1 = Product.newBuilder()
                .setCode(existentProductCode1)
                .setDescription("IPhone 11")
                .setCategory("Electronic")
                .setPrice(899.0)
                .setAvailableStock(15)
                .build();

        String existentProductCode2 = product2.getCode();
        Product gProduct2 = Product.newBuilder()
                .setCode(existentProductCode2)
                .setDescription("Logitech Optical Mouse")
                .setCategory("Electronic")
                .setPrice(14.99)
                .setAvailableStock(32)
                .build();

        String existentProductCode3 = product3.getCode();
        Product gProduct3 = Product.newBuilder()
                .setCode(existentProductCode3)
                .setDescription("Lenovo Keyboard")
                .setCategory("Electronic")
                .setPrice(34.0)
                .setAvailableStock(7)
                .build();

        AddProductsRequest request = AddProductsRequest.newBuilder()
                .addProducts(gProduct1)
                .addProducts(gProduct2)
                .addProducts(gProduct3)
                .build();

        // Then
        StatusRuntimeException sre = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.addProducts(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(sre);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS, sre.getStatus()),
                () -> Assertions.assertEquals(Status.ALREADY_EXISTS.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.PRODUCT_ALREADY_EXISTS, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void addProducts_InvalidInputNoProductsProvided_ShouldReturnGrpcCode3InvalidArgument() {
        // Given --> An empty request
        AddProductsRequest request = AddProductsRequest.newBuilder().build();

        // When
        StatusRuntimeException sre = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.addProducts(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(sre);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT, sre.getStatus()),
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.INVALID_INPUT, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void getProductByCode_ProductFound_ShouldReturnTheProduct() {
        // Given
        String existentProductCode1 = product1.getCode();

        // When
        GetProductByCodeRequest request = GetProductByCodeRequest.newBuilder()
                .setCode(existentProductCode1)
                .build();
        GetProductByCodeResponse response = productsGrpcApi.getProductByCode(request);
        Product gProduct = response.getProduct();
        Assertions.assertAll(
                () -> Assertions.assertNotNull(gProduct),
                () -> Assertions.assertEquals(product1.getCode(), gProduct.getCode()),
                () -> Assertions.assertNotNull(product1.getDescription(), gProduct.getDescription()),
                () -> Assertions.assertEquals(product1.getCategory(), gProduct.getCategory()),
                () -> Assertions.assertEquals(product1.getAvailableStock(), gProduct.getAvailableStock()),
                () -> Assertions.assertEquals(product1.getPrice(), gProduct.getPrice())
        );
    }

    @Test
    public void getProductByCode_ProductNotFound_ShouldReturnGrpcCode5NotFound() {
        // Given
        String notExistentProductCode = "PRD00034";

        // Then
        GetProductByCodeRequest request = GetProductByCodeRequest.newBuilder()
                .setCode(notExistentProductCode)
                .build();
        StatusRuntimeException sre = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.getProductByCode(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(sre);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, sre.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.PRODUCT_NOT_FOUND, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void getProductsByCode_selectAllProducts_ShouldReturnAllProducts() {
        // Given --> En empty request (means FIND_ALL)
        GetProductsByCodeRequest request = GetProductsByCodeRequest.newBuilder().build();

        // When
        GetProductsByCodeResponse response = productsGrpcApi.getProductsByCode(request);

        // Then
        Assertions.assertEquals(3, response.getProductList().size());
    }

    @Test
    public void getProductsByCode_AllOfSelectedProductsExist_ShouldReturnThreeProducts() {
        // Given
        String existentProductCode1 = product1.getCode();
        String existentProductCode2 = product2.getCode();
        Set<String> requestedProductsCode = Set.of(existentProductCode1, existentProductCode2);

        // When
        GetProductsByCodeRequest request = GetProductsByCodeRequest.newBuilder()
                .addAllCodes(requestedProductsCode)
                .build();
        GetProductsByCodeResponse response = productsGrpcApi.getProductsByCode(request);

        // Then
        Assertions.assertEquals(2, response.getProductList().size());
    }

    @Test
    public void getProductsByCode_SomeOfSelectedProductsExist_ShouldReturnTwoProductsOfThreeRequested() {
        // Given
        String existentProductCode1 = product1.getCode();
        String existentProductCode2 = product2.getCode();
        String notExistentProductCode = "PRD00051";
        Set<String> requestedProductsCode = Set.of(existentProductCode1, existentProductCode2, notExistentProductCode);

        // When
        GetProductsByCodeRequest request = GetProductsByCodeRequest.newBuilder()
                .addAllCodes(requestedProductsCode)
                .build();
        GetProductsByCodeResponse response = productsGrpcApi.getProductsByCode(request);

        // Then
        Assertions.assertEquals(2, response.getProductList().size());
    }

    @Test
    public void getProductsByCode_SelectedProductsDoesntExist_ShouldReturnAnEmptyList() {
        // Given
        String notExistentProductCode1 = "PRD00051";
        String notExistentProductCode2 = "PRD00052";
        String notExistentProductCode3 = "PRD00053";
        Set<String> mProductsCode = Set.of(notExistentProductCode1, notExistentProductCode2, notExistentProductCode3);

        // When
        GetProductsByCodeRequest request = GetProductsByCodeRequest.newBuilder()
                .addAllCodes(mProductsCode)
                .build();
        GetProductsByCodeResponse response = productsGrpcApi.getProductsByCode(request);

        // Then
        Assertions.assertEquals(0, response.getProductList().size());
    }

    @Test
    public void updateStocks_ProductsAreAvailableInStock_ShouldReturnTheNewAvailableStock() {
        // Given
        String productCode1 = product1.getCode();
        Integer requestedStockProduct1 = 5;
        String productCode2 = product2.getCode();
        Integer requestedStockProduct2 = 4;

        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put(productCode1, requestedStockProduct1); // 15 Available
        requestedStocks.put(productCode2, requestedStockProduct2); // 32 Available

        // When
        UpdateStocksRequest request = UpdateStocksRequest.newBuilder()
                .putAllCodesToStocks(requestedStocks)
                .build();
        UpdateStocksResponse response = productsGrpcApi.updateStocks(request);
        Assertions.assertAll(
                () -> Assertions.assertEquals(requestedStocks.size(), response.getCodesToStocksCount()),
                () -> {
                    int expectedNewAvailableStockProduct1 = product1.getAvailableStock() - requestedStockProduct1;
                    int actualNewAvailableStockProduct1 = response.getCodesToStocksOrThrow(productCode1);
                    Assertions.assertEquals(expectedNewAvailableStockProduct1, actualNewAvailableStockProduct1);
                },
                () -> {
                    int expectedNewAvailableStockProduct2 = product2.getAvailableStock() - requestedStockProduct2;
                    int actualNewAvailableStockProduct2 = response.getCodesToStocksOrThrow(productCode2);
                    Assertions.assertEquals(expectedNewAvailableStockProduct2, actualNewAvailableStockProduct2);
                }
        );
    }

    @Test
    public void updateStocks_AtLeastOneProductsNotFound_ShouldReturnGrpcCode5NotFound() {
        // Given
        String existentProductCode1 = product1.getCode();
        Integer requestedStockProduct1 = 5;
        String notExistentProductCode2 = "PRD00053";
        Integer requestedStockProduct2 = 4;

        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put(existentProductCode1, requestedStockProduct1);
        requestedStocks.put(notExistentProductCode2, requestedStockProduct2);

        // When
        UpdateStocksRequest request = UpdateStocksRequest.newBuilder()
                .putAllCodesToStocks(requestedStocks)
                .build();
        StatusRuntimeException sre = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.updateStocks(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(sre);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.NOT_FOUND, sre.getStatus()),
                () -> Assertions.assertEquals(Status.NOT_FOUND.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.PRODUCT_NOT_FOUND, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void updateStocks_InvalidStockAsInput_ShouldReturnGrpcCode3InvalidArgument() {
        // Given --> Invalid empty request
        Map<String, Integer> requestedStocks = new HashMap<>();

        // When
        UpdateStocksRequest request = UpdateStocksRequest.newBuilder()
                .putAllCodesToStocks(requestedStocks)
                .build();
        StatusRuntimeException sre = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.updateStocks(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(sre);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT, sre.getStatus()),
                () -> Assertions.assertEquals(Status.INVALID_ARGUMENT.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.INVALID_INPUT, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }

    @Test
    public void updateStocks_AtLeastOneProductWithInsufficientStock_ShouldReturnGrpcCode7PermissionDenied() {
        // Given
        String productCode1 = product1.getCode();
        Integer requestedStockProduct1 = 16; // 15 Available!
        String productCode2 = product2.getCode();
        Integer requestedStockProduct2 = 4;

        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put(productCode1, requestedStockProduct1); // 15 Available
        requestedStocks.put(productCode2, requestedStockProduct2); // 32 Available

        // When
        UpdateStocksRequest request = UpdateStocksRequest.newBuilder()
                .putAllCodesToStocks(requestedStocks)
                .build();
        StatusRuntimeException sre = Assertions.assertThrows(
                StatusRuntimeException.class,
                () -> productsGrpcApi.updateStocks(request) // When
        );
        Metadata metadata = Status.trailersFromThrowable(sre);
        ProductsApiErrorResponse errorResponse = metadata.get(ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance()));
        String productsApiErrorCode = errorResponse.getApplicativeError().name();
        Assertions.assertAll(
                () -> Assertions.assertEquals(Status.PERMISSION_DENIED, sre.getStatus()),
                () -> Assertions.assertEquals(Status.PERMISSION_DENIED.getCode().value(), errorResponse.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorEnum.INSUFFICIENT_STOCK, ProductsApiErrorEnum.valueOf(productsApiErrorCode)),
                () -> Assertions.assertNotNull(errorResponse.getInstance()),
                () -> Assertions.assertNotNull(errorResponse.getTitle()),
                () -> Assertions.assertNotNull(errorResponse.getType())
        );
    }


}
