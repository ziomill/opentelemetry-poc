package it.millsoft.products.api.test.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import it.millsoft.products.api.business.ProductsServices;
import it.millsoft.products.api.commons.Utils;
import it.millsoft.products.api.dto.ProductDTO;
import it.millsoft.products.api.exceptions.InsufficientStockException;
import it.millsoft.products.api.exceptions.InvalidInputException;
import it.millsoft.products.api.exceptions.ProductAlreadyExistsException;
import it.millsoft.products.api.exceptions.ProductNotFoundException;
import it.millsoft.products.api.interfaces.rest.model.Product;
import it.millsoft.products.api.interfaces.rest.model.ProductsApiErrorResponse;
import it.millsoft.products.api.mappers.ProductRestToFromDtoMapper;
import it.millsoft.products.api.interfaces.rest.handlers.HttpExceptionsHandler;
import org.bson.types.ObjectId;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@WebMvcTest
@Import(HttpExceptionsHandler.class)
public class ProductsRestApi_UnitTest {

    @TestConfiguration
    static class ProductsRestApiConfiguration {

        @Bean
        ProductRestToFromDtoMapper createProductRestToDtoMapper(){
            ProductRestToFromDtoMapper mapper = Mappers.getMapper( ProductRestToFromDtoMapper.class );
            return mapper;
        }
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductsServices productsServices;

    @Test
    public void addProduct_ProductDoesntExist_ShouldAddProductAndReturnIdAndHttpCode200Ok() throws Exception {
        // Given
        String mCreatedProductId = new ObjectId().toString();
        when(productsServices.addProduct(any(ProductDTO.class))).thenReturn(mCreatedProductId);

        // When
        ObjectMapper mapper = new ObjectMapper();
        Product body = new Product("PRD0001", "IPhone 11", "Electronic", 899.00, 13);
        String bodyAsJson = mapper.writeValueAsString(body);

        String uriTemplate = "/product";
        URI uri = Utils.toURI(uriTemplate,null,null);
        this.mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.is(mCreatedProductId)));
    }

    @Test
    public void addProduct_ProductAlreadyExist_ShouldReturnHttpCode409Conflict() throws Exception {
        // Given
        ProductAlreadyExistsException mThrownException = new ProductAlreadyExistsException("Product already exists", UUID.randomUUID());
        Mockito.when(this.productsServices.addProduct(any(ProductDTO.class))).thenThrow(mThrownException);

        // When
        ObjectMapper mapper = new ObjectMapper();
        Product body = new Product("PRD0001", "IPhone 11", "Electronic", 899.00, 13);
        String bodyAsJson = mapper.writeValueAsString(body);

        String uriTemplate = "/product";
        URI uri = Utils.toURI(uriTemplate,null,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        ProductsApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, ProductsApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.CONFLICT.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.PRODUCT_ALREADY_EXISTS, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void addProducts_TwoOfThreeProvidedProductsDoesntExist_ShouldAddNotExistentProductsAndReturnHttpCode200OK() throws Exception {
        // Given
        String notExistentProductCode1 = "PRD0001";
        String notExistentProductId1 = new ObjectId().toString();
        String notExistentProductCode2 = "PRD0002";
        String notExistentProductId2 = new ObjectId().toString();
        String existentProductCode = "PRD0003";
        Map<String, String> mCreatedProducts = Map.of(notExistentProductCode1, notExistentProductId1,
                                                      notExistentProductCode2, notExistentProductId2);
        when(productsServices.addProducts(any(Set.class))).thenReturn(mCreatedProducts);

        // When
        Product notExistentProduct1 = new Product(notExistentProductCode1, "Lenovo Keyboard", "Electronic", 27.0, 16);
        Product notExistentProduct2 = new Product(notExistentProductCode2, "IPhone 8", "Electronic", 799.0, 5);
        Product existentProduct = new Product(existentProductCode, "Logitech Optical Mouse", "Electronic", 13.0, 5);
        Set<Product> body = Set.of(notExistentProduct1, notExistentProduct2, existentProduct);

        ObjectMapper mapper = new ObjectMapper();
        String bodyAsJson = mapper.writeValueAsString(body);

        String uriTemplate = "/products";
        URI uri = Utils.toURI(uriTemplate,null,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();
        String expectedResponseAsJson = mapper.writeValueAsString(mCreatedProducts);
        String actualResponseAsJson = mvcResult.getResponse().getContentAsString();
        JSONAssert.assertEquals(expectedResponseAsJson, actualResponseAsJson, false);
    }

    @Test
    public void addProducts_AllProvidedProductsAlreadyExist_ShouldReturnHttpCode409Conflict() throws Exception {
        // Given
        String existentProductCode1 = "PRD0001";
        String existentProductCode2 = "PRD0002";
        String existentProductCode3 = "PRD0003";
        ProductAlreadyExistsException mProductAlreadyExistsException = new ProductAlreadyExistsException("Provided products already exist", UUID.randomUUID());
        when(productsServices.addProducts(any(Set.class))).thenThrow(mProductAlreadyExistsException);

        // When
        Product notExistentProduct1 = new Product(existentProductCode1, "Lenovo Keyboard", "Electronic", 27.0, 16);
        Product notExistentProduct2 = new Product(existentProductCode2, "IPhone 8", "Electronic", 799.0, 5);
        Product existentProduct = new Product(existentProductCode3, "Logitech Optical Mouse", "Electronic", 13.0, 5);
        Set<Product> body = Set.of(notExistentProduct1, notExistentProduct2, existentProduct);

        ObjectMapper mapper = new ObjectMapper();
        String bodyAsJson = mapper.writeValueAsString(body);

        String uriTemplate = "/products";
        URI uri = Utils.toURI(uriTemplate,null,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isConflict())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        ProductsApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, ProductsApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.CONFLICT.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.PRODUCT_ALREADY_EXISTS, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void addProducts_InvalidInputNoProductsProvided_ShouldReturnHttpCode400BadRequest() throws Exception {
        // Given
        InvalidInputException mInvalidInputException = new InvalidInputException("Invalid Input", UUID.randomUUID());
        when(productsServices.addProducts(null)).thenThrow(mInvalidInputException);
        when(productsServices.addProducts(new HashSet<>())).thenThrow(mInvalidInputException);

        // When
        Set<Product> body = new HashSet<>();
        ObjectMapper mapper = new ObjectMapper();
        String bodyAsJson = mapper.writeValueAsString(body);

        String uriTemplate = "/products";
        URI uri = Utils.toURI(uriTemplate,null,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.post(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        ProductsApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, ProductsApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.INVALID_INPUT, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void getProductByCode_ProductFound_ShouldReturnTheProductAndHttpCode200OK() throws Exception {
        // Given
        String productCode = "PRD0001";
        ProductDTO mProduct = new ProductDTO(productCode, "Lenovo Keyboard", "Electronic", 27.0, 16);
        when(productsServices.getProductByCode(productCode)).thenReturn(mProduct);

        // When
        String uriTemplate = "/product/{product_code}";
        Map<String,String> uriParams = Map.of("product_code",productCode);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Product responseBody = mapper.readValue(responseBodyAsJson, Product.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(mProduct.getCode(), responseBody.getCode()),
                () -> Assertions.assertEquals(mProduct.getDescription(), responseBody.getDescription()),
                () -> Assertions.assertEquals(mProduct.getCategory(), responseBody.getCategory()),
                () -> Assertions.assertEquals(mProduct.getAvailableStock(), responseBody.getAvailableStock()),
                () -> Assertions.assertEquals(mProduct.getPrice(), responseBody.getPrice())
        );
    }

    @Test
    public void getProductByCode_ProductNotFound_ShouldReturnHttpCode404NotFound() throws Exception {
        // Given
        String productCode = "PRD0001";
        ProductNotFoundException mProductNotFoundException = new ProductNotFoundException("Product Not Found", UUID.randomUUID());
        when(productsServices.getProductByCode(productCode)).thenThrow(mProductNotFoundException);

        // When
        String uriTemplate = "/product/{product_code}";
        Map<String,String> uriParams = Map.of("product_code",productCode);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        ProductsApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, ProductsApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.PRODUCT_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void getProductsByCode_selectAllProducts_ShouldReturnAllProductsAndHttpCode200OK() throws Exception {
        // Given
        ProductDTO mProduct1 = new ProductDTO("PRD00001", "Lenovo Keyboard", "Electronic", 27.0, 6);
        ProductDTO mProduct2 = new ProductDTO("PRD00002", "Logitech Optical Mouse", "Electronic", 12.0, 5);
        ProductDTO mProduct3 = new ProductDTO("PRD00003", "IPhone 11", "Electronic", 899.0, 5);
        Set<ProductDTO> mProducts = Set.of(mProduct1, mProduct2, mProduct3);
        when(productsServices.getProductsByCode(null)).thenReturn(mProducts);
        when(productsServices.getProductsByCode(new HashSet<>())).thenReturn(mProducts);

        // When
        String uriTemplate = "/products/*";
        URI uri = Utils.toURI(uriTemplate,null,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Set<Product> responseBody = mapper.readValue(responseBodyAsJson, Set.class);
        Assertions.assertEquals(mProducts.size(), responseBody.size());
    }

    @Test
    public void getProductsByCode_AllOfSelectedProductsExist_ShouldReturnThreeProductsAndHttpCode200OK() throws Exception {
        // Given
        String mProductCode1 = "PRD00001";
        ProductDTO mProduct1 = new ProductDTO(mProductCode1, "Lenovo Keyboard", "Electronic", 27.0, 6);
        String mProductCode2 = "PRD00002";
        ProductDTO mProduct2 = new ProductDTO(mProductCode2, "Logitech Optical Mouse", "Electronic", 12.0, 5);
        String mProductCode3 = "PRD00003";
        ProductDTO mProduct3 = new ProductDTO(mProductCode3, "IPhone 11", "Electronic", 899.0, 5);
        Set<String> mProductsCode = Set.of(mProductCode1, mProductCode2, mProductCode3);
        Set<ProductDTO> mProducts = Set.of(mProduct1, mProduct2, mProduct3);
        when(productsServices.getProductsByCode(mProductsCode)).thenReturn(mProducts);

        // When
        String uriTemplate = "/products/{product_code_1},{product_code_2},{product_code_3}";
        Map<String,String> uriParams = Map.of(
                "product_code_1",mProductCode1,
                "product_code_2",mProductCode2,
                "product_code_3",mProductCode3);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Set<Product> responseBody = mapper.readValue(responseBodyAsJson, Set.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(mProducts.size(), responseBody.size())
        );
    }

    @Test
    public void getProductsByCode_SomeOfSelectedProductsExist_ShouldReturnTwoProductsOfThreeRequestedAndHttpCode200OK() throws Exception {
        // Given
        String mProductCode1 = "PRD00001";
        ProductDTO mProduct1 = new ProductDTO(mProductCode1, "Lenovo Keyboard", "Electronic", 27.0, 6);
        String mProductCode2 = "PRD00002";
        ProductDTO mProduct2 = new ProductDTO(mProductCode2, "Logitech Optical Mouse", "Electronic", 12.0, 5);
        String notExistentProductCode = "PRD00003";

        Set<String> mProductsCode = Set.of(mProductCode1, mProductCode2, notExistentProductCode);
        Set<ProductDTO> mProducts = Set.of(mProduct1, mProduct2);
        when(productsServices.getProductsByCode(mProductsCode)).thenReturn(mProducts);

        // When
        String uriTemplate = "/products/{product_code_1},{product_code_2},{ne_product_code}";
        Map<String,String> uriParams = Map.of(
                "product_code_1",mProductCode1,
                "product_code_2",mProductCode2,
                "ne_product_code",notExistentProductCode);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Set<Product> responseBody = mapper.readValue(responseBodyAsJson, Set.class);
        Assertions.assertEquals(mProducts.size(), responseBody.size());
    }

    @Test
    public void getProductsByCode_SelectedProductsDoesntExist_ShouldReturnAnEmptyListAndHttpCode200OK() throws Exception {
        // Given
        String notExistentProductCode1 = "PRD00001";
        String notExistentProductCode2 = "PRD00002";
        String notExistentProductCode3 = "PRD00003";

        Set<String> mProductsCode = Set.of(notExistentProductCode1, notExistentProductCode2, notExistentProductCode3);
        Set<ProductDTO> mProducts = new HashSet<>();
        when(productsServices.getProductsByCode(mProductsCode)).thenReturn(mProducts);

        // When
        String uriTemplate = "/products/{ne_product_code_1},{ne_product_code_2},{ne_product_code_3}";
        Map<String,String> uriParams = Map.of(
                "ne_product_code_1",notExistentProductCode1,
                "ne_product_code_2",notExistentProductCode2,
                "ne_product_code_3",notExistentProductCode3);
        URI uri = Utils.toURI(uriTemplate,uriParams,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.get(uri)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Set<Product> responseBody = mapper.readValue(responseBodyAsJson, Set.class);
        Assertions.assertEquals(mProducts.size(), responseBody.size());
    }

    @Test
    public void updateStocks_ProductsAreAvailableInStock_ShouldReturnTheNewAvailableStockAndHttpCode200OK() throws Exception {
        // Given
        String mProductCode1 = "PRD0001";
        String mProductCode2 = "PRD0002";

        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put(mProductCode1, 5);
        requestedStocks.put(mProductCode2, 3);

        Map<String, Integer> newAvailableStocks = new HashMap<>();
        newAvailableStocks.put(mProductCode1, 10);
        newAvailableStocks.put(mProductCode2, 8);

        when(productsServices.updateStocks(requestedStocks)).thenReturn(newAvailableStocks);

        // When
        ObjectMapper mapper = new ObjectMapper();
        String bodyAsJson = mapper.writeValueAsString(requestedStocks);

        String uriTemplate = "/products/stock";
        URI uri = Utils.toURI(uriTemplate,null,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.patch(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        Map<String, Integer> responseBody = mapper.readValue(responseBodyAsJson, Map.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(requestedStocks.size(), responseBody.size()),
                () -> {
                    int expectedNewAvailableStockProduct1 = newAvailableStocks.get(mProductCode1);
                    int actualNewAvailableStockProduct1 = responseBody.get(mProductCode1);
                    Assertions.assertEquals(expectedNewAvailableStockProduct1, actualNewAvailableStockProduct1);
                },
                () -> {
                    int expectedNewAvailableStockProduct2 = newAvailableStocks.get(mProductCode2);
                    int actualNewAvailableStockProduct2 = responseBody.get(mProductCode2);
                    Assertions.assertEquals(expectedNewAvailableStockProduct2, actualNewAvailableStockProduct2);
                }
        );
    }

    @Test
    public void updateStocks_AtLeastOneProductsNotFound_ShouldReturnHttpCode404NotFound() throws Exception {
        // Given
        String mProductCode1 = "PRD0001";
        String mProductCode2 = "PRD0002";

        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put(mProductCode1, 5);
        requestedStocks.put(mProductCode2, 3);

        ProductNotFoundException mProductNotFoundException = new ProductNotFoundException("Product Not Found", UUID.randomUUID());
        when(productsServices.updateStocks(requestedStocks)).thenThrow(mProductNotFoundException);

        // When
        ObjectMapper mapper = new ObjectMapper();
        String bodyAsJson = mapper.writeValueAsString(requestedStocks);

        String uriTemplate = "/products/stock";
        URI uri = Utils.toURI(uriTemplate,null,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.patch(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        ProductsApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, ProductsApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.PRODUCT_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateStocks_InvalidStockAsInput_ShouldReturnHttpCode400BadRequest() throws Exception {
        // Given
        Map<String, Integer> requestedStocks = new HashMap<>();

        InvalidInputException mInvalidInputException = new InvalidInputException("Invalid Stock as input", UUID.randomUUID());
        when(productsServices.updateStocks(null)).thenThrow(mInvalidInputException);
        when(productsServices.updateStocks(requestedStocks)).thenThrow(mInvalidInputException);

        // When
        ObjectMapper mapper = new ObjectMapper();
        String bodyAsJson = mapper.writeValueAsString(requestedStocks);

        String uriTemplate = "/products/stock";
        URI uri = Utils.toURI(uriTemplate,null,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.patch(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        ProductsApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, ProductsApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.INVALID_INPUT, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateStocks_AtLeastOneProductWithInsufficientStock_ShouldReturnHttpCode403Forbidden() throws Exception {
        // Given
        String mProductCode1 = "PRD0001";
        String mProductCode2 = "PRD0002";

        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put(mProductCode1, 5);
        requestedStocks.put(mProductCode2, 3);

        InsufficientStockException mInsufficientStockException = new InsufficientStockException("Insufficient Stock", UUID.randomUUID());
        when(productsServices.updateStocks(requestedStocks)).thenThrow(mInsufficientStockException);

        // When
        ObjectMapper mapper = new ObjectMapper();
        String bodyAsJson = mapper.writeValueAsString(requestedStocks);

        String uriTemplate = "/products/stock";
        URI uri = Utils.toURI(uriTemplate,null,null);
        MvcResult mvcResult = this.mockMvc.perform(MockMvcRequestBuilders.patch(uri)
                        .content(bodyAsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(MockMvcResultMatchers.status().isForbidden())
                .andExpect(MockMvcResultMatchers.content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andReturn();

        String responseBodyAsJson = mvcResult.getResponse().getContentAsString();
        ProductsApiErrorResponse responseBody = mapper.readValue(responseBodyAsJson, ProductsApiErrorResponse.class);
        Assertions.assertAll(
                () -> Assertions.assertEquals(HttpStatus.FORBIDDEN.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.INSUFFICIENT_STOCK, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }


}
