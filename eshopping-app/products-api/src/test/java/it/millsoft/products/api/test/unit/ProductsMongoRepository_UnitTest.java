package it.millsoft.products.api.test.unit;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import it.millsoft.products.api.model.Product;
import it.millsoft.products.api.repositories.ProductsMongoRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;

@DataMongoTest
@DirtiesContext
@Testcontainers
public class ProductsMongoRepository_UnitTest {

    @Container
    private static MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:5.0.6"));

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        Objects.requireNonNull(mongoDBContainer);
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    @Autowired
    private ProductsMongoRepository productsMongoRepository;
    @Autowired
    private MongoTemplate mongoTemplate;

    @AfterEach
    void cleanUpDatabase() {
        mongoTemplate.getDb().drop();
    }

    @Test
    public void findProductByCode_ProductFound_ShouldReturnTheProduct() {
        // Given
        String code = "PRD0001";
        DBObject productToSave = BasicDBObjectBuilder.start()
                .add("code", code)
                .add("description", "Lenovo Keyboard")
                .add("category", "Electronic")
                .add("price", 27.0)
                .add("stock", 16)
                .get();

        this.mongoTemplate.save(productToSave, "products");

        // When
        Optional<Product> optProduct = productsMongoRepository.findProductByCode(code);

        // Then
        Assertions.assertTrue(optProduct.isPresent());
    }

    @Test
    public void findProductByCode_ProductNotFound_ShouldReturnAnEmptyOptional() {
        // Given --> not existent product
        String code = "PRD0002";

        // When
        Optional<Product> optProduct = productsMongoRepository.findProductByCode(code);

        // Then
        Assertions.assertTrue(optProduct.isEmpty());
    }

    @Test
    public void findProductsByCodes_TwoOfTheThreeRequestedProductsExist_ShouldReturnTwoProducts() {
        // Given
        String productCode1 = "PRD0001";
        String productCode2 = "PRD0002";
        String productCode3 = "PRD0003";

        DBObject productToSave1 = BasicDBObjectBuilder.start()
                .add("code", productCode1)
                .add("description", "Lenovo Keyboard")
                .add("category", "Electronic")
                .add("price", 27.0)
                .add("stock", 16)
                .get();
        this.mongoTemplate.save(productToSave1, "products");

        DBObject productToSave2 = BasicDBObjectBuilder.start()
                .add("code", productCode2)
                .add("description", "Logitech Optical Mouse")
                .add("category", "Electronic")
                .add("price", 14.0)
                .add("stock", 24)
                .get();
        this.mongoTemplate.save(productToSave2, "products");

        DBObject productToSave3 = BasicDBObjectBuilder.start()
                .add("code", productCode3)
                .add("description", "IPhone 8")
                .add("category", "Electronic")
                .add("price", 799.0)
                .add("stock", 5)
                .get();
        this.mongoTemplate.save(productToSave3, "products");

        // When
        String notExistentProductCode = "PRD0005";
        Set<String> codes = Set.of(productCode1,productCode2,notExistentProductCode);
        Set<Product> eProducts = productsMongoRepository.findProductsByCodes(codes);

        // Then
        Assertions.assertEquals(2,eProducts.size());
    }

    @Test
    public void findProductsByCodes_RequestedProductsDoesntExist_ShouldReturnAnEmptyList() {
        // Given
        String productCode = "PRD0001";
        DBObject productToSave1 = BasicDBObjectBuilder.start()
                .add("code", productCode)
                .add("description", "Lenovo Keyboard")
                .add("category", "Electronic")
                .add("price", 27.0)
                .add("stock", 16)
                .get();
        this.mongoTemplate.save(productToSave1, "products");

        // When
        String notExistentProductCode1 = "PRD0002";
        String notExistentProductCode2 = "PRD0003";
        Set<String> codes = Set.of(notExistentProductCode1,notExistentProductCode2);
        Set<Product> eProducts = productsMongoRepository.findProductsByCodes(codes);

        // Then
        Assertions.assertEquals(0,eProducts.size());
    }

    @Test
    public void existsByCode_ProductExists_ShouldReturnTrue() {
        // Given
        String code = "PRD0001";
        DBObject productToSave = BasicDBObjectBuilder.start()
                .add("code", code)
                .add("description", "Lenovo Keyboard")
                .add("category", "Electronic")
                .add("price", 27.0)
                .add("stock", 16)
                .get();

        this.mongoTemplate.save(productToSave, "products");

        // When
        boolean exists = productsMongoRepository.existsByCode(code);

        // Then
        Assertions.assertTrue(exists);
    }

    @Test
    public void existsByCode_ProductExists_ShouldReturnFalse() {
        // Given --> Not existent product
        String code = "PRD0006";

        // When
        boolean exists = productsMongoRepository.existsByCode(code);

        // Then
        Assertions.assertFalse(exists);
    }

}
