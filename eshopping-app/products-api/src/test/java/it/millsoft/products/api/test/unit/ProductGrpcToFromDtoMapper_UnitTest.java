package it.millsoft.products.api.test.unit;

import it.millsoft.products.api.dto.ProductDTO;
import it.millsoft.products.api.interfaces.grpc.model.Product;
import it.millsoft.products.api.mappers.ProductGrpcToFromDtoMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import java.util.Optional;
import java.util.Set;

public class ProductGrpcToFromDtoMapper_UnitTest {

    @Test
    public void ProductGrpcToDto_createTheProductDTO_ShouldReturnTheProductDTO() {
        // Given
        ProductGrpcToFromDtoMapper mapper = Mappers.getMapper( ProductGrpcToFromDtoMapper.class );
        Product gProduct = Product.newBuilder()
                .setCode("PRD000001")
                .setDescription("Lenovo X1 Carbon")
                .setCategory("Electronic")
                .setPrice(1899.00)
                .setAvailableStock(2)
                .build();

        // When
        ProductDTO dProduct = mapper.productGrpcToDto(gProduct);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(gProduct.getCode(),dProduct.getCode()),
                () -> Assertions.assertEquals(gProduct.getDescription(),dProduct.getDescription()),
                () -> Assertions.assertEquals(gProduct.getCategory(),dProduct.getCategory()),
                () -> Assertions.assertEquals(gProduct.getPrice(),dProduct.getPrice()),
                () -> Assertions.assertEquals(gProduct.getAvailableStock(),dProduct.getAvailableStock())
        );
    }

    @Test
    public void ProductDtoToGrpc_createTheProductGrpc_ShouldReturnTheProductGrpc() {
        // Given
        ProductGrpcToFromDtoMapper mapper = Mappers.getMapper( ProductGrpcToFromDtoMapper.class );
        ProductDTO dProduct = new ProductDTO("PRD000001","Lenovo X1 Carbon","Electronic",1899.00,2);

        // When
        Product gProduct = mapper.productDtoToGrpc(dProduct);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(dProduct.getCode(),gProduct.getCode()),
                () -> Assertions.assertEquals(dProduct.getDescription(),gProduct.getDescription()),
                () -> Assertions.assertEquals(dProduct.getCategory(),gProduct.getCategory()),
                () -> Assertions.assertEquals(dProduct.getPrice(),gProduct.getPrice()),
                () -> Assertions.assertEquals(dProduct.getAvailableStock(),gProduct.getAvailableStock())
        );
    }

    @Test
    public void ProductsDtoToGrpc_createAListOfProductGrpc_ShouldReturnTheListOfProductGrpc() {
        // Given
        ProductGrpcToFromDtoMapper mapper = Mappers.getMapper( ProductGrpcToFromDtoMapper.class );
        ProductDTO dProduct1 = new ProductDTO("PRD000001","Lenovo X1 Carbon","Electronic",1899.00,2);
        ProductDTO dProduct2 = new ProductDTO("PRD000002","IPhone 8","Electronic",899.00,1);
        Set<ProductDTO> dProducts = Set.of(dProduct1,dProduct2);

        // When
        Set<Product> gProducts = mapper.productsDtoToGrpc(dProducts);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(dProducts.size(),gProducts.size()),
                () -> {
                    Optional<Product> optProduct1 = gProducts.stream().filter(gProduct -> gProduct.getCode().equals(dProduct1.getCode())).findFirst();
                    Assertions.assertTrue(optProduct1.isPresent());
                    Product gProduct1 = optProduct1.get();
                    Assertions.assertEquals(dProduct1.getCode(),gProduct1.getCode());
                    Assertions.assertEquals(dProduct1.getDescription(),gProduct1.getDescription());
                    Assertions.assertEquals(dProduct1.getCategory(),gProduct1.getCategory());
                    Assertions.assertEquals(dProduct1.getPrice(),gProduct1.getPrice());
                    Assertions.assertEquals(dProduct1.getAvailableStock(),gProduct1.getAvailableStock());

                    Optional<Product> optProduct2 = gProducts.stream().filter(gProduct -> gProduct.getCode().equals(dProduct2.getCode())).findFirst();
                    Assertions.assertTrue(optProduct2.isPresent());
                    Product gProduct2 = optProduct2.get();
                    Assertions.assertEquals(dProduct2.getCode(),gProduct2.getCode());
                    Assertions.assertEquals(dProduct2.getDescription(),gProduct2.getDescription());
                    Assertions.assertEquals(dProduct2.getCategory(),gProduct2.getCategory());
                    Assertions.assertEquals(dProduct2.getPrice(),gProduct2.getPrice());
                    Assertions.assertEquals(dProduct2.getAvailableStock(),gProduct2.getAvailableStock());
                }
        );
    }

    @Test
    public void ProductsGrpcToDto_createAListOfProductDto_ShouldReturnTheListOfProductDto() {
        // Given
        Product gProduct1 = Product.newBuilder()
                .setCode("PRD000001")
                .setDescription("Lenovo X1 Carbon")
                .setCategory("Electronic")
                .setPrice(1899.00)
                .setAvailableStock(2)
                .build();
        Product gProduct2 = Product.newBuilder()
                .setCode("PRD000002")
                .setDescription("IPhone 8")
                .setCategory("Electronic")
                .setPrice(899.00)
                .setAvailableStock(1)
                .build();

        ProductGrpcToFromDtoMapper mapper = Mappers.getMapper( ProductGrpcToFromDtoMapper.class );
        Set<Product> gProducts = Set.of(gProduct1,gProduct2);

        // When
        Set<ProductDTO> dProducts = mapper.productsGrpcToDto(gProducts);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(gProducts.size(),dProducts.size()),
                () -> {
                    Optional<ProductDTO> optProduct1 = dProducts.stream().filter(dProduct -> dProduct.getCode().equals(gProduct1.getCode())).findFirst();
                    Assertions.assertTrue(optProduct1.isPresent());
                    ProductDTO dProduct1 = optProduct1.get();
                    Assertions.assertEquals(gProduct1.getCode(),dProduct1.getCode());
                    Assertions.assertEquals(gProduct1.getDescription(),dProduct1.getDescription());
                    Assertions.assertEquals(gProduct1.getCategory(),dProduct1.getCategory());
                    Assertions.assertEquals(gProduct1.getPrice(),dProduct1.getPrice());
                    Assertions.assertEquals(gProduct1.getAvailableStock(),dProduct1.getAvailableStock());

                    Optional<ProductDTO> optProduct2 = dProducts.stream().filter(dProduct -> dProduct.getCode().equals(gProduct2.getCode())).findFirst();
                    Assertions.assertTrue(optProduct2.isPresent());
                    ProductDTO dProduct2 = optProduct2.get();
                    Assertions.assertEquals(gProduct2.getCode(),dProduct2.getCode());
                    Assertions.assertEquals(gProduct2.getDescription(),dProduct2.getDescription());
                    Assertions.assertEquals(gProduct2.getCategory(),dProduct2.getCategory());
                    Assertions.assertEquals(gProduct2.getPrice(),dProduct2.getPrice());
                    Assertions.assertEquals(gProduct2.getAvailableStock(),dProduct2.getAvailableStock());
                }
        );
    }

}
