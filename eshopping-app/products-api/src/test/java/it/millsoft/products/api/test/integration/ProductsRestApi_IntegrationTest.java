package it.millsoft.products.api.test.integration;

import it.millsoft.products.api.interfaces.rest.model.Product;
import it.millsoft.products.api.interfaces.rest.model.ProductsApiErrorResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.*;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.net.URI;
import java.util.*;

import static it.millsoft.products.api.commons.Utils.toURI;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Testcontainers
public class ProductsRestApi_IntegrationTest {

    private static Logger LOGGER = LoggerFactory.getLogger(ProductsRestApi_IntegrationTest.class);

    @Container
    private static MongoDBContainer mongoDBContainer = new MongoDBContainer(DockerImageName.parse("mongo:5.0.6"));

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @DynamicPropertySource
    static void setProperties(DynamicPropertyRegistry registry) {
        Objects.requireNonNull(mongoDBContainer);
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    private it.millsoft.products.api.model.Product product1;
    private it.millsoft.products.api.model.Product product2;
    private it.millsoft.products.api.model.Product product3;

    @BeforeEach
    void setupDatabase() {
        // Given
        product1 = new it.millsoft.products.api.model.Product();
        product1.setCode("PRD00001");
        product1.setDescription("IPhone 11");
        product1.setCategory("Electronic");
        product1.setPrice(899.0);
        product1.setAvailableStock(15);
        this.product1 = mongoTemplate.save(product1);
        LOGGER.debug("Product {} saved with success", product1);

        product2 = new it.millsoft.products.api.model.Product();
        product2.setCode("PRD00002");
        product2.setDescription("Logitech Optical Mouse");
        product2.setCategory("Electronic");
        product2.setPrice(14.99);
        product2.setAvailableStock(32);
        product2 = mongoTemplate.save(product2);
        LOGGER.debug("Product {} saved with success", product2);

        product3 = new it.millsoft.products.api.model.Product();
        product3.setCode("PRD00003");
        product3.setDescription("Lenovo Keyboard");
        product3.setCategory("Electronic");
        product3.setPrice(34.0);
        product3.setAvailableStock(7);
        product3 = mongoTemplate.save(product3);
        LOGGER.debug("Product {} saved with success", product3);
    }

    @AfterEach
    void cleanupDatabase() {
        mongoTemplate.getDb().drop();
    }

    @Test
    public void addProduct_ProductDoesntExist_ShouldAddProductAndReturnIdAndHttpCode200Ok() {
        // Given
        Product product = new Product("PRD00004", "Lenovo X1 Carbon Notebook", "Electronic", 1799.00, 5);

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Product> requestEntity = new HttpEntity(product, headers);

        String uriTemplate = "/product";
        URI uri = toURI(uriTemplate,null,null);
        ResponseEntity<String> response = this.testRestTemplate.exchange(uri, HttpMethod.POST, requestEntity, String.class);

        // Then
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> {
                    Assertions.assertNotNull(response.getBody());
                }
        );
    }

    @Test
    public void addProduct_ProductAlreadyExist_ShouldReturnHttpCode409Conflict() {
        // Given
        Product product = new Product(product1.getCode(), "Lenovo X1 Carbon Notebook", "Electronic", 1799.00, 5); // CODE used by an existent product

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Product> requestEntity = new HttpEntity(product, headers);

        String uriTemplate = "/product";
        URI uri = toURI(uriTemplate,null,null);
        ResponseEntity<ProductsApiErrorResponse> response = this.testRestTemplate.exchange(uri, HttpMethod.POST, requestEntity, ProductsApiErrorResponse.class);

        // Then
        ProductsApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.CONFLICT, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.PRODUCT_ALREADY_EXISTS, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void addProducts_TwoOfThreeProvidedProductsDoesntExist_ShouldAddNotExistentProductsAndReturnHttpCode200OK() {
        // Given
        String notExistentProductCode1 = "PRD00020";
        Product notExistentProduct1 = new Product(notExistentProductCode1, "Dell Monitor X11", "Electronic", 215.0, 10);
        String notExistentProductCode2 = "PRD00021";
        Product notExistentProduct2 = new Product(notExistentProductCode2, "Ethernet cable", "Electronic", 4.99, 50);
        String existentProductCode = product1.getCode();
        Product existentProduct = new Product(existentProductCode, "IPhone 11", "Electronic", 899.00, 15);

        Set<Product> productsToAdd = Set.of(notExistentProduct1, notExistentProduct2, existentProduct);

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Product> requestEntity = new HttpEntity(productsToAdd, headers);

        String urlTemplate = "/products";
        ResponseEntity<Map> response = this.testRestTemplate.exchange(urlTemplate, HttpMethod.POST, requestEntity, Map.class);

        // Then
        Map<String, String> responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.CREATED, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> {
                    Assertions.assertEquals(2, responseBody.size()); // Two added products
                    Assertions.assertTrue(responseBody.containsKey(notExistentProduct1.getCode())); // The Product 1 id exists
                    Assertions.assertTrue(responseBody.containsKey(notExistentProduct2.getCode())); // The Product 2 is exists
                }
        );
    }

    @Test
    public void addProducts_AllProvidedProductsAlreadyExist_ShouldReturnHttpCode409Conflict() {
        // Given
        Product existentProduct1 = new Product(product1.getCode(), "Dell Monitor X11", "Electronic", 215.0, 10);  // CODE used by an existent product
        Product existentProduct2 = new Product(product2.getCode(), "Ethernet cable", "Electronic", 4.99, 50);     // CODE used by an existent product
        Product existentProduct3 = new Product(product3.getCode(), "IPhone 11", "Electronic", 899.00, 15);        // CODE used by an existent product
        Set<Product> productsToAdd = Set.of(existentProduct1, existentProduct2, existentProduct3);

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Product> requestEntity = new HttpEntity(productsToAdd, headers);

        String uriTemplate = "/products";
        URI uri = toURI(uriTemplate,null,null);
        ResponseEntity<ProductsApiErrorResponse> response = this.testRestTemplate.exchange(uri, HttpMethod.POST, requestEntity, ProductsApiErrorResponse.class);

        // Then
        ProductsApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.CONFLICT, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.PRODUCT_ALREADY_EXISTS, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void addProducts_InvalidInputNoProductsProvided_ShouldReturnHttpCode400BadRequest() {
        // Given
        Set<Product> productsToAdd = new HashSet<>(); // Empty input

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Product> requestEntity = new HttpEntity(productsToAdd, headers);

        String uriTemplate = "/products";
        URI uri = toURI(uriTemplate,null,null);
        ResponseEntity<ProductsApiErrorResponse> response = this.testRestTemplate.exchange(uri, HttpMethod.POST, requestEntity, ProductsApiErrorResponse.class);

        // Then
        ProductsApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.INVALID_INPUT, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void getProductByCode_ProductFound_ShouldReturnTheProductAndHttpCode200OK() {
        // Given
         String productCode = product1.getCode();

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);

        String uriTemplate = "/product/{product_code}";
        Map<String,String> uriParams = Map.of("product_code",productCode);
        URI uri = toURI(uriTemplate,uriParams,null);
        ResponseEntity<Product> response = this.testRestTemplate.exchange(uri, HttpMethod.GET, requestEntity, Product.class);

        Product responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(product1.getCode(), responseBody.getCode()),
                () -> Assertions.assertEquals(product1.getDescription(), responseBody.getDescription()),
                () -> Assertions.assertEquals(product1.getCategory(), responseBody.getCategory()),
                () -> Assertions.assertEquals(product1.getAvailableStock(), responseBody.getAvailableStock()),
                () -> Assertions.assertEquals(product1.getPrice(), responseBody.getPrice())
        );
    }

    @Test
    public void getProductByCode_ProductNotFound_ShouldReturnHttpCode404NotFound() {
        // Given
        String productCode = "PRD00021";

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);

        String uriTemplate = "/product/{product_code}";
        Map<String,String> uriParams = Map.of("product_code",productCode);
        URI uri = toURI(uriTemplate,uriParams,null);
        ResponseEntity<ProductsApiErrorResponse> response = this.testRestTemplate.exchange(uri, HttpMethod.GET, requestEntity, ProductsApiErrorResponse.class);

        // Then
        ProductsApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.PRODUCT_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void getProductsByCode_selectAllProducts_ShouldReturnAllProductsAndHttpCode200OK() {
        // Given
        String allProductsCode = "*";

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);

        String uriTemplate = "/products/{product_code}";
        Map<String,String> uriParams = Map.of("product_code",allProductsCode);
        URI uri = toURI(uriTemplate,uriParams,null);
        ResponseEntity<Set> response = this.testRestTemplate.exchange(uri, HttpMethod.GET, requestEntity, Set.class);

        // Then
        Set<Product> responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(3, responseBody.size())
        );
    }

    @Test
    public void getProductsByCode_AllOfSelectedProductsExist_ShouldReturnThreeProductsAndHttpCode200OK() {
        // Given
        String requestedProduct1 = product1.getCode();
        String requestedProduct2 = product2.getCode();
        String requestedProduct3 = product3.getCode();

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);

        String uriTemplate = "/products/{product_code_1},{product_code_2},{product_code_3}";
        Map<String,String> uriParams = Map.of(
                "product_code_1",requestedProduct1,
                "product_code_2",requestedProduct2,
                "product_code_3",requestedProduct3);
        URI uri = toURI(uriTemplate,uriParams,null);
        ResponseEntity<Set> response = this.testRestTemplate.exchange(uri, HttpMethod.GET, requestEntity, Set.class);

        // Then
        Set<Product> responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(3, responseBody.size())
        );
    }

    @Test
    public void getProductsByCode_SomeOfSelectedProductsExist_ShouldReturnTwoProductsOfThreeRequestedAndHttpCode200OK() {
        // Given
        String existentProductCode1 = product1.getCode();
        String existentProductCode2 = product2.getCode();
        String notExistentProductCode = "PRD000023";

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);

        String uriTemplate = "/products/{product_code_1},{product_code_2},{ne_product_code}";
        Map<String,String> uriParams = Map.of(
                "product_code_1",existentProductCode1,
                "product_code_2",existentProductCode2,
                "ne_product_code",notExistentProductCode);
        URI uri = toURI(uriTemplate,uriParams,null);
        ResponseEntity<Set> response = this.testRestTemplate.exchange(uri, HttpMethod.GET, requestEntity, Set.class);

        // Then
        Set<Product> responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(2, responseBody.size())
        );
    }

    @Test
    public void getProductsByCode_SelectedProductsDoesntExist_ShouldReturnAnEmptyListAndHttpCode200OK() {
        // Given
        String notExistentProductCode1 = "PRD000021";
        String notExistentProductCode2 = "PRD000022";
        String notExistentProductCode3 = "PRD000023";

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> requestEntity = new HttpEntity(headers);

        String uriTemplate = "/products/{ne_product_code_1},{ne_product_code_2},{ne_product_code_3}";
        Map<String,String> uriParams = Map.of(
                "ne_product_code_1",notExistentProductCode1,
                "ne_product_code_2",notExistentProductCode2,
                "ne_product_code_3",notExistentProductCode3);
        URI uri = toURI(uriTemplate,uriParams,null);
        ResponseEntity<Set> response = this.testRestTemplate.exchange(uri, HttpMethod.GET, requestEntity, Set.class);

        // Then
        Set<Product> responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertTrue(responseBody.isEmpty())
        );
    }

    @Test
    public void updateStocks_ProductsAreAvailableInStock_ShouldReturnTheNewAvailableStockAndHttpCode200OK() {
        // Given
        Integer requestedStockProduct1 = 5;
        Integer requestedStockProduct2 = 4;
        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put(product1.getCode(), requestedStockProduct1);
        requestedStocks.put(product2.getCode(), requestedStockProduct2);

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Map> requestEntity = new HttpEntity(requestedStocks, headers);

        String uriTemplate = "/products/stock";
        URI uri = toURI(uriTemplate,null,null);
        ResponseEntity<Map> response = this.testRestTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, Map.class);

        // Then
        Map<String, Integer> responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.OK, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(requestedStocks.size(), responseBody.size()),
                () -> {
                    int expectedNewAvailableStockProduct1 = product1.getAvailableStock() - requestedStockProduct1;
                    int actualNewAvailableStockProduct1 = responseBody.get(product1.getCode());
                    Assertions.assertEquals(expectedNewAvailableStockProduct1, actualNewAvailableStockProduct1);
                },
                () -> {
                    int expectedNewAvailableStockProduct2 = product2.getAvailableStock() - requestedStockProduct2;
                    int actualNewAvailableStockProduct2 = responseBody.get(product2.getCode());
                    Assertions.assertEquals(expectedNewAvailableStockProduct2, actualNewAvailableStockProduct2);
                }
        );
    }

    @Test
    public void updateStocks_AtLeastOneProductsNotFound_ShouldReturnHttpCode404NotFound() {
        // Given
        Integer requestedStockProduct1 = 5;
        Integer requestedStockProduct2 = 4;
        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put("PRD000023", requestedStockProduct1); // Product doesn't exist
        requestedStocks.put(product2.getCode(), requestedStockProduct2);

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Map> requestEntity = new HttpEntity(requestedStocks, headers);

        String uriTemplate = "/products/stock";
        URI uri = toURI(uriTemplate,null,null);
        ResponseEntity<ProductsApiErrorResponse> response = this.testRestTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, ProductsApiErrorResponse.class);

        ProductsApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(HttpStatus.NOT_FOUND.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.PRODUCT_NOT_FOUND, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateStocks_InvalidStockAsInput_ShouldReturnHttpCode400BadRequest() {
        // Given
        Map<String, Integer> requestedStocks = new HashMap<>(); // Invalid Empty stocks

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Map> requestEntity = new HttpEntity(requestedStocks, headers);

        String uriTemplate = "/products/stock";
        URI uri = toURI(uriTemplate,null,null);
        ResponseEntity<ProductsApiErrorResponse> response = this.testRestTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, ProductsApiErrorResponse.class);

        // Then
        ProductsApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(HttpStatus.BAD_REQUEST.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.INVALID_INPUT, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }

    @Test
    public void updateStocks_AtLeastOneProductWithInsufficientStock_ShouldReturnHttpCode403Forbidden() {
        // Given
        Integer requestedStockProduct1 = 5;
        Integer requestedStockProduct2 = 33; // Insufficient stock
        Map<String, Integer> requestedStocks = new HashMap<>();
        requestedStocks.put(product1.getCode(), requestedStockProduct1); // 15 Available
        requestedStocks.put(product2.getCode(), requestedStockProduct2); // 32 Available

        // When
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<Map> requestEntity = new HttpEntity(requestedStocks, headers);

        String uriTemplate = "/products/stock";
        URI uri = toURI(uriTemplate,null,null);
        ResponseEntity<ProductsApiErrorResponse> response = this.testRestTemplate.exchange(uri, HttpMethod.PATCH, requestEntity, ProductsApiErrorResponse.class);

        // Then
        ProductsApiErrorResponse responseBody = response.getBody();
        Assertions.assertAll(
                // Http
                () -> Assertions.assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode()),
                () -> Assertions.assertEquals(MediaType.APPLICATION_JSON, response.getHeaders().getContentType()),
                // Applicative
                () -> Assertions.assertEquals(HttpStatus.FORBIDDEN.value(), responseBody.getStatus()),
                () -> Assertions.assertEquals(ProductsApiErrorResponse.ApplicativeErrorEnum.INSUFFICIENT_STOCK, responseBody.getApplicativeError()),
                () -> Assertions.assertNotNull(responseBody.getInstance()),
                () -> Assertions.assertNotNull(responseBody.getTitle()),
                () -> Assertions.assertNotNull(responseBody.getType())
        );
    }


}
