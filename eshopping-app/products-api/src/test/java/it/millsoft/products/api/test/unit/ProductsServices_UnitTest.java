package it.millsoft.products.api.test.unit;

import it.millsoft.products.api.business.ProductsServices;
import it.millsoft.products.api.dto.ProductDTO;
import it.millsoft.products.api.exceptions.InsufficientStockException;
import it.millsoft.products.api.exceptions.InvalidInputException;
import it.millsoft.products.api.exceptions.ProductAlreadyExistsException;
import it.millsoft.products.api.exceptions.ProductNotFoundException;
import it.millsoft.products.api.model.Product;
import it.millsoft.products.api.repositories.ProductsMongoRepository;
import it.millsoft.products.api.test.config.UnitTestConfig;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringJUnitConfig(classes = UnitTestConfig.class)
public class ProductsServices_UnitTest {

    @Autowired
    private ProductsServices productsServices;

    @MockBean
    private ProductsMongoRepository productsMongoRepository;

    @Test
    public void addProduct_ProductDoesntExist_ShouldAddTheProductAndReturntheId() {
        // Given
        String newProductCode = "PRD0001";
        when(productsMongoRepository.existsByCode(newProductCode)).thenReturn(false);

        String mockedCreatedUserId = (new ObjectId()).toString();
        Product eProduct = new Product();
        eProduct.setId(mockedCreatedUserId);
        when(productsMongoRepository.save(any(Product.class))).thenReturn(eProduct);

        // When
        ProductDTO product = new ProductDTO(newProductCode,
                "Lenovo Keyboard",
                "Electronic",
                27.0,
                16);
        String productId = productsServices.addProduct(product);

        // Then
        Assertions.assertEquals(productId, mockedCreatedUserId);
    }

    @Test
    public void addProduct_ProductAlreadyExist_ShouldThrowAProductAlreadyExistsException() {

        // Given
        String newProductCode = "PRD0001";
        when(productsMongoRepository.existsByCode(newProductCode)).thenReturn(true);

        ProductDTO product = new ProductDTO(newProductCode,
                "Lenovo Keyboard",
                "Electronic",
                27.0,
                16);

        // Then
        Assertions.assertThrows(
                ProductAlreadyExistsException.class,
                () -> productsServices.addProduct(product), // When
                "It should have raised the " + ProductAlreadyExistsException.class.getName()
        );
    }

    @Test
    public void addProducts_TwoOfThreeProvidedProductsDoesntExist_AddNotExistentProductsAndReturnIds() {
        // Given
        String neProductCode1 = "PRD0001";
        when(productsMongoRepository.existsByCode(neProductCode1)).thenReturn(false);
        String neProductCode2 = "PRD0002";
        when(productsMongoRepository.existsByCode(neProductCode2)).thenReturn(false);
        String existentProductCode = "PRD0003";
        when(productsMongoRepository.existsByCode(existentProductCode)).thenReturn(true);

        Product mockedProduct1 = new Product();
        mockedProduct1.setId(new ObjectId().toString());
        mockedProduct1.setCode(neProductCode1);
        Product mockedProduct2 = new Product();
        mockedProduct2.setId(new ObjectId().toString());
        mockedProduct2.setCode(neProductCode2);
        List<Product> savedProduct = List.of(mockedProduct1, mockedProduct2);
        when(productsMongoRepository.saveAll(any(List.class))).thenReturn(savedProduct);

        // When
        ProductDTO notExistentProduct1 = new ProductDTO(neProductCode1, "Lenovo Keyboard", "Electronic", 27.0, 16);
        ProductDTO notExistentProduct2 = new ProductDTO(neProductCode2, "IPhone 8", "Electronic", 799.0, 5);
        ProductDTO existentProduct3 = new ProductDTO(existentProductCode, "Logitech Optical Mouse", "Electronic", 13.0, 5);
        Set<ProductDTO> products = Set.of(notExistentProduct1, notExistentProduct2, existentProduct3);
        Map<String, String> addedProducts = productsServices.addProducts(products);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(2, addedProducts.size()),
                () -> Assertions.assertEquals(mockedProduct1.getId(), addedProducts.get(neProductCode1)),
                () -> Assertions.assertEquals(mockedProduct2.getId(), addedProducts.get(neProductCode2))
        );
    }

    @Test
    public void addProducts_AllProductsAlreadyExist_ShouldThrowAProductAlreadyExistsException() {
        // Given
        String existentProductCode1 = "PRD0001";
        when(productsMongoRepository.existsByCode(existentProductCode1)).thenReturn(true);
        String existentProductCode2 = "PRD0002";
        when(productsMongoRepository.existsByCode(existentProductCode2)).thenReturn(true);
        String existentProductCode3 = "PRD0003";
        when(productsMongoRepository.existsByCode(existentProductCode3)).thenReturn(true);

        // Then
        ProductDTO existentProduct1 = new ProductDTO(existentProductCode1, "Lenovo Keyboard", "Electronic", 27.0, 16);
        ProductDTO existentProduct2 = new ProductDTO(existentProductCode2, "IPhone 8", "Electronic", 799.0, 5);
        ProductDTO existentProduct3 = new ProductDTO(existentProductCode3, "Logitech Optical Mouse", "Electronic", 13.0, 5);
        Set<ProductDTO> products = Set.of(existentProduct1, existentProduct2, existentProduct3);
        // Then
        Assertions.assertThrows(
                ProductAlreadyExistsException.class,
                () -> productsServices.addProducts(products), // When
                "It should have raised the " + ProductAlreadyExistsException.class.getName()
        );
    }

    @Test
    public void addProducts_InvalidInputNoProductsProvided_ShouldThrowAnInvalidInputException() {

        // Given --> An Empty input
        Set<ProductDTO> products = new HashSet<>();

        // Then
        Assertions.assertThrows(
                InvalidInputException.class,
                () -> productsServices.addProducts(products), // When
                "It should have raised the " + InvalidInputException.class.getName()
        );
    }

    @Test
    public void getProductByCode_ProductFound_ShouldReturnTheProduct() {
        // Given
        String productCode = "PRD0001";

        Product mockedProduct = new Product();
        mockedProduct.setId(new ObjectId().toString());
        mockedProduct.setCode(productCode);
        mockedProduct.setDescription("Lenovo Keyboard");
        mockedProduct.setCategory("Electronic");
        mockedProduct.setPrice(35.0);
        mockedProduct.setAvailableStock(11);
        Optional<Product> optProduct = Optional.of(mockedProduct);
        when(productsMongoRepository.findProductByCode(productCode)).thenReturn(optProduct);

        // When
        ProductDTO foundProduct = productsServices.getProductByCode(productCode);

        // Then
        Assertions.assertAll(
                () -> Assertions.assertEquals(mockedProduct.getCode(), foundProduct.getCode()),
                () -> Assertions.assertEquals(mockedProduct.getDescription(), foundProduct.getDescription()),
                () -> Assertions.assertEquals(mockedProduct.getCategory(), foundProduct.getCategory()),
                () -> Assertions.assertEquals(mockedProduct.getPrice(), foundProduct.getPrice()),
                () -> Assertions.assertEquals(mockedProduct.getAvailableStock(), foundProduct.getAvailableStock())
        );
    }

    @Test
    public void getProductByCode_ProductNotFound_ShouldThrowAProductNotFoundException() {
        // Given
        String productCode = "PRD0001";
        Optional<Product> optProduct = Optional.empty();
        when(productsMongoRepository.findProductByCode(productCode)).thenReturn(optProduct);

        // Then
        Assertions.assertThrows(
                ProductNotFoundException.class,
                () -> productsServices.getProductByCode(productCode), // When
                "It should have raised the " + ProductNotFoundException.class.getName()
        );
    }

    @Test
    public void getProductsByCode_selectAllProducts_ShouldReturnAllProducts() {
        // Given
        Product mockedProduct1 = new Product();
        mockedProduct1.setId(new ObjectId().toString());
        mockedProduct1.setCode("PRD0001");
        mockedProduct1.setDescription("Lenovo Keyboard");
        mockedProduct1.setCategory("Electronic");
        mockedProduct1.setPrice(35.0);
        mockedProduct1.setAvailableStock(11);

        Product mockedProduct2 = new Product();
        mockedProduct2.setId(new ObjectId().toString());
        mockedProduct2.setCode("PRD0002");
        mockedProduct2.setDescription("Logitech Optical Mouse");
        mockedProduct2.setCategory("Electronic");
        mockedProduct2.setPrice(13.0);
        mockedProduct2.setAvailableStock(5);

        Product mockedProduct3 = new Product();
        mockedProduct3.setId(new ObjectId().toString());
        mockedProduct3.setCode("PRD0003");
        mockedProduct3.setDescription("IPhone 11");
        mockedProduct3.setCategory("Electronic");
        mockedProduct3.setPrice(899.0);
        mockedProduct3.setAvailableStock(5);

        List<Product> availableProducts = List.of(mockedProduct1, mockedProduct2, mockedProduct3);
        when(productsMongoRepository.findAll()).thenReturn(availableProducts);

        // When
        Set<String> codes = new HashSet<>(); // Empty list as input means findAll
        Set<ProductDTO> foundProducts = productsServices.getProductsByCode(codes);

        // Then
        Assertions.assertEquals(3, foundProducts.size());
    }

    @Test
    public void getProductsByCode_AllOfSelectedProductsExist_ShouldReturnThreeProducts() {
        // Given
        Product mockedProduct1 = new Product();
        mockedProduct1.setId(new ObjectId().toString());
        mockedProduct1.setCode("PRD0001");
        mockedProduct1.setDescription("Lenovo Keyboard");
        mockedProduct1.setCategory("Electronic");
        mockedProduct1.setPrice(35.0);
        mockedProduct1.setAvailableStock(11);

        Product mockedProduct2 = new Product();
        mockedProduct2.setId(new ObjectId().toString());
        mockedProduct2.setCode("PRD0002");
        mockedProduct2.setDescription("Logitech Optical Mouse");
        mockedProduct2.setCategory("Electronic");
        mockedProduct2.setPrice(13.0);
        mockedProduct2.setAvailableStock(5);

        Set<String> requestedProductsCode = Set.of(mockedProduct1.getCode(), mockedProduct2.getCode());
        Set<Product> requestedProducts = Set.of(mockedProduct1, mockedProduct2);
        when(productsMongoRepository.findProductsByCodes(requestedProductsCode)).thenReturn(requestedProducts);

        // When
        Set<ProductDTO> foundProducts = productsServices.getProductsByCode(requestedProductsCode);

        // Then
        Assertions.assertEquals(2, foundProducts.size());
    }

    @Test
    public void getProductsByCode_SomeOfSelectedProductsExist_ShouldReturnTwoProductsOfThreeRequested() {
        // Given
        Product mockedProduct1 = new Product();
        mockedProduct1.setId(new ObjectId().toString());
        mockedProduct1.setCode("PRD0001");
        mockedProduct1.setDescription("Lenovo Keyboard");
        mockedProduct1.setCategory("Electronic");
        mockedProduct1.setPrice(35.0);
        mockedProduct1.setAvailableStock(11);

        Product mockedProduct2 = new Product();
        mockedProduct2.setId(new ObjectId().toString());
        mockedProduct2.setCode("PRD0002");
        mockedProduct2.setDescription("Logitech Optical Mouse");
        mockedProduct2.setCategory("Electronic");
        mockedProduct2.setPrice(13.0);
        mockedProduct2.setAvailableStock(5);

        String notExistendProductCode = "PRD0003";

        Set<String> requestedProductsCode = Set.of(mockedProduct1.getCode(), mockedProduct2.getCode(), notExistendProductCode);
        Set<Product> requestedProducts = Set.of(mockedProduct1, mockedProduct2);
        when(productsMongoRepository.findProductsByCodes(requestedProductsCode)).thenReturn(requestedProducts);

        // When
        Set<ProductDTO> foundProducts = productsServices.getProductsByCode(requestedProductsCode);

        // Then
        Assertions.assertEquals(2, foundProducts.size());
    }

    @Test
    public void getProductsByCode_SelectedProductsDoesntExist_ShouldReturnAnEmptyList() {
        // Given
        String notExistendProductCode1 = "PRD0001";
        String notExistendProductCode2 = "PRD0002";

        Set<String> requestedProductsCode = Set.of(notExistendProductCode1, notExistendProductCode2);
        Set<Product> emptyResult = Set.of();
        when(productsMongoRepository.findProductsByCodes(requestedProductsCode)).thenReturn(emptyResult);

        // When
        Set<ProductDTO> foundProducts = productsServices.getProductsByCode(requestedProductsCode);

        // Then
        Assertions.assertEquals(0, foundProducts.size());
    }

    @Test
    public void updateStocks_ProductsAreAvailableInStock_ShouldReturnTheNewAvailableStock() {
        // Given
        Integer product1availableStock = 11;
        Product mockedProduct1 = new Product();
        mockedProduct1.setId(new ObjectId().toString());
        mockedProduct1.setCode("PRD0001");
        mockedProduct1.setDescription("Lenovo Keyboard");
        mockedProduct1.setCategory("Electronic");
        mockedProduct1.setPrice(35.0);
        mockedProduct1.setAvailableStock(product1availableStock);

        Integer product2availableStock = 5;
        Product mockedProduct2 = new Product();
        mockedProduct2.setId(new ObjectId().toString());
        mockedProduct2.setCode("PRD0002");
        mockedProduct2.setDescription("Logitech Optical Mouse");
        mockedProduct2.setCategory("Electronic");
        mockedProduct2.setPrice(13.0);
        mockedProduct2.setAvailableStock(product2availableStock);

        Set<String> foundProductsCode = Set.of(mockedProduct1.getCode(), mockedProduct2.getCode());
        Set<Product> foundProducts = Set.of(mockedProduct1, mockedProduct2);
        when(productsMongoRepository.findProductsByCodes(foundProductsCode)).thenReturn(foundProducts);

        // When
        Integer requestedStockForProduct1 = 5;
        Integer requestedStockForProduct2 = 3;
        Map<String, Integer> requestedStocks = Map.of(mockedProduct1.getCode(), requestedStockForProduct1,
                mockedProduct2.getCode(), requestedStockForProduct2);
        Map<String, Integer> newAvailableStock = productsServices.updateStocks(requestedStocks);

        // Then
        int expectedNewAvailableStockForProduct1 = product1availableStock - requestedStockForProduct1;
        int expectedNewAvailableStockForProduct2 = product2availableStock - requestedStockForProduct2;
        int actualNewAvailableStockForProduct1 = newAvailableStock.get(mockedProduct1.getCode());
        int actualNewAvailableStockForProduct2 = newAvailableStock.get(mockedProduct2.getCode());
        Assertions.assertAll(
                () -> Assertions.assertEquals(expectedNewAvailableStockForProduct1, actualNewAvailableStockForProduct1),
                () -> Assertions.assertEquals(expectedNewAvailableStockForProduct2, actualNewAvailableStockForProduct2)
        );
    }

    @Test
    public void updateStocks_AtLeastOneProductsNotFound_ShouldThrowAnProductNotFoundException() {
        // Given
        Integer product1availableStock = 11;
        Product mockedProduct1 = new Product();
        mockedProduct1.setId(new ObjectId().toString());
        mockedProduct1.setCode("PRD0001");
        mockedProduct1.setDescription("Lenovo Keyboard");
        mockedProduct1.setCategory("Electronic");
        mockedProduct1.setPrice(35.0);
        mockedProduct1.setAvailableStock(product1availableStock);

        String notExistentProductCode = "PRD0002";

        Set<String> foundProductsCode = Set.of(mockedProduct1.getCode(), notExistentProductCode);
        Set<Product> foundProducts = Set.of(mockedProduct1);
        when(productsMongoRepository.findProductsByCodes(foundProductsCode)).thenReturn(foundProducts);

        // Then
        Assertions.assertThrows(
                ProductNotFoundException.class,
                () -> {
                    // When
                    Integer requestedStockForProduct1 = 5;
                    Integer requestedStockForProduct2 = 3;
                    Map<String, Integer> requestedStocks = Map.of(mockedProduct1.getCode(), requestedStockForProduct1,
                            notExistentProductCode, requestedStockForProduct2);
                    productsServices.updateStocks(requestedStocks);
                },
                "It should have raised the " + ProductNotFoundException.class.getName()
        );
    }

    @Test
    public void updateStocks_InvalidStockAsInput_ShouldThrowAnInvalidInputException() {
        // Then
        Assertions.assertThrows(
                InvalidInputException.class,
                () -> {
                    // Given --> Null input
                    Map<String, Integer> requestedStocks = null;
                    // When
                    productsServices.updateStocks(requestedStocks);
                },
                "It should have raised the " + InvalidInputException.class.getName()
        );

        // Then
        Assertions.assertThrows(
                InvalidInputException.class,
                () -> {
                    // Given --> Empty input
                    Map<String, Integer> requestedStocks = new HashMap<>();
                    // When
                    productsServices.updateStocks(requestedStocks);
                },
                "It should have raised the " + InvalidInputException.class.getName()
        );
    }

    @Test
    public void updateStocks_AtLeastOneProductWithInsufficientStock_ShouldThrowAnInsufficientStockException() {
        // Given
        Integer product1availableStock = 11;
        Product mockedProduct1 = new Product();
        mockedProduct1.setId(new ObjectId().toString());
        mockedProduct1.setCode("PRD0001");
        mockedProduct1.setDescription("Lenovo Keyboard");
        mockedProduct1.setCategory("Electronic");
        mockedProduct1.setPrice(35.0);
        mockedProduct1.setAvailableStock(product1availableStock);

        Integer product2availableStock = 5;
        Product mockedProduct2 = new Product();
        mockedProduct2.setId(new ObjectId().toString());
        mockedProduct2.setCode("PRD0002");
        mockedProduct2.setDescription("Logitech Optical Mouse");
        mockedProduct2.setCategory("Electronic");
        mockedProduct2.setPrice(13.0);
        mockedProduct2.setAvailableStock(product2availableStock);

        Set<String> foundProductsCode = Set.of(mockedProduct1.getCode(), mockedProduct2.getCode());
        Set<Product> foundProducts = Set.of(mockedProduct1, mockedProduct2);
        when(productsMongoRepository.findProductsByCodes(foundProductsCode)).thenReturn(foundProducts);

        // Then
        Assertions.assertThrows(
                InsufficientStockException.class,
                () -> {
                    // When
                    Integer requestedStockForProduct1 = 5;
                    Integer requestedStockForProduct2 = 6; // --> Insuffient Stock (Available 5, Requested 6)
                    Map<String, Integer> requestedStocks = Map.of(mockedProduct1.getCode(), requestedStockForProduct1,
                                                                  mockedProduct2.getCode(), requestedStockForProduct2);
                    productsServices.updateStocks(requestedStocks);
                },
                "It should have raised the " + InsufficientStockException.class.getName()
        );
    }


}
