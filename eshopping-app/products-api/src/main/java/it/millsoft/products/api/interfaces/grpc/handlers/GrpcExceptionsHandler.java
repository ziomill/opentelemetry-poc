package it.millsoft.products.api.interfaces.grpc.handlers;

import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusException;
import io.grpc.protobuf.ProtoUtils;
import it.millsoft.products.api.commons.Utils;
import it.millsoft.products.api.dto.ProductsApiErrorEnum;
import it.millsoft.products.api.exceptions.*;
import it.millsoft.products.api.interfaces.grpc.model.ProductsApiErrorResponse;
import net.devh.boot.grpc.server.advice.GrpcAdvice;
import net.devh.boot.grpc.server.advice.GrpcExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

@GrpcAdvice
public class GrpcExceptionsHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(GrpcExceptionsHandler.class);

    @GrpcExceptionHandler(Exception.class)
    public StatusException handleGenericError(Exception ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", ProductsApiErrorEnum.GENERIC_FAILURE.name());
        return this.buildGenericStatusException(ex);
    }

    @GrpcExceptionHandler(ProductAlreadyExistsException.class)
    public StatusException handleApplicativeError(ProductAlreadyExistsException ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", ex.getError().name());
        return this.buildApplicativeStatusException(ex, Status.ALREADY_EXISTS);
    }

    @GrpcExceptionHandler(ProductNotFoundException.class)
    public StatusException handleApplicativeError(ProductNotFoundException ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", ex.getError().name());
        return this.buildApplicativeStatusException(ex, Status.NOT_FOUND);
    }

    @GrpcExceptionHandler(InvalidInputException.class)
    public StatusException handleApplicativeError(InvalidInputException ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", ex.getError().name());
        return this.buildApplicativeStatusException(ex, Status.INVALID_ARGUMENT);
    }

    @GrpcExceptionHandler(InsufficientStockException.class)
    public StatusException handleApplicativeError(InsufficientStockException ex) {
        LOGGER.debug("GRPC - Intercepted error --> {}", ex.getError().name());
        return this.buildApplicativeStatusException(ex, Status.PERMISSION_DENIED);
    }

    private StatusException buildApplicativeStatusException(ProductsApiException ex, Status status) {
        String type = ex.getError().getErrorDesc();
        String instance = Utils.errorInstanceAsURN(ex.getErrorUUID());
        String title = ex.getError().name();
        String details = ex.getMessage();
        ProductsApiErrorEnum error = ex.getError();

        ProductsApiErrorResponse.ApplicativeError applicativeError = ProductsApiErrorResponse.ApplicativeError.valueOf(error.name());
        ProductsApiErrorResponse errorResponse = ProductsApiErrorResponse.newBuilder()
                .setType(type)
                .setInstance(instance)
                .setTitle(title)
                .setDetails(details)
                .setApplicativeError(applicativeError)
                .setStatus(status.getCode().value())
                .build();
        Metadata.Key<ProductsApiErrorResponse> shopErrorResponseKey = ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance());
        Metadata metadata = new Metadata();
        metadata.put(shopErrorResponseKey, errorResponse);
        return status.asException(metadata);
    }

    private StatusException buildGenericStatusException(Exception ex) {
        UUID errorUUID = UUID.randomUUID();
        String type = ProductsApiErrorEnum.GENERIC_FAILURE.getErrorDesc();
        String instance = Utils.errorInstanceAsURN(errorUUID);
        String title = ProductsApiErrorEnum.GENERIC_FAILURE.name();
        String details = ex.getMessage();
        ProductsApiErrorEnum error = ProductsApiErrorEnum.GENERIC_FAILURE;
        Status status = Status.INTERNAL;

        ProductsApiErrorResponse.ApplicativeError applicativeError =ProductsApiErrorResponse.ApplicativeError.valueOf(error.name());
        String msg = String.format(ex.getMessage() + " - Error UUID: %s", errorUUID);
        LOGGER.error(msg);
        ProductsApiErrorResponse errorResponse = ProductsApiErrorResponse.newBuilder()
                .setType(type)
                .setInstance(instance)
                .setTitle(title)
                .setDetails(details)
                .setApplicativeError(applicativeError)
                .setStatus(status.getCode().value())
                .build();
        Metadata.Key<ProductsApiErrorResponse> shopErrorResponseKey = ProtoUtils.keyForProto(ProductsApiErrorResponse.getDefaultInstance());
        Metadata metadata = new Metadata();
        metadata.put(shopErrorResponseKey, errorResponse);
        return status.asException(metadata);
    }

}
