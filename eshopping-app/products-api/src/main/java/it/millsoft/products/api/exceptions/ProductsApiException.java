package it.millsoft.products.api.exceptions;

import it.millsoft.products.api.dto.ProductsApiErrorEnum;
import lombok.Getter;

import java.util.UUID;

@Getter
public abstract class ProductsApiException extends RuntimeException {
    private ProductsApiErrorEnum error;
    private UUID errorUUID;

    public ProductsApiException(String message, ProductsApiErrorEnum error, UUID errorUUID) {
        super(message);
        this.error = error;
        this.errorUUID = errorUUID;
    }
}
