package it.millsoft.products.api.mappers;

import it.millsoft.products.api.dto.ProductDTO;
import it.millsoft.products.api.interfaces.rest.model.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.Set;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface ProductRestToFromDtoMapper {

    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "availableStock", source = "availableStock")
    ProductDTO productRestToDto(Product product);

    Set<ProductDTO> productsRestToDto(Set<Product> products);

    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "availableStock", source = "availableStock")
    Product productDtoToRest(ProductDTO product);

    Set<Product> productsDtoToRest(Set<ProductDTO> producs);



}
