package it.millsoft.products.api.business;

import it.millsoft.products.api.dto.ProductDTO;
import it.millsoft.products.api.exceptions.ProductsApiException;

import java.util.Map;
import java.util.Set;

public interface ProductsServices {
    String addProduct(ProductDTO product);

    Map<String, String> addProducts(Set<ProductDTO> products);

    ProductDTO getProductByCode(String code);

    Set<ProductDTO> getProductsByCode(Set<String> codes);

    Map<String, Integer> updateStocks(Map<String, Integer> stock);
}

