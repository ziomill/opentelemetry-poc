package it.millsoft.products.api.repositories;

import it.millsoft.products.api.model.Product;
import org.springframework.data.mongodb.repository.ExistsQuery;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
public interface ProductsMongoRepository  extends MongoRepository<Product,String> {

    @Query("{ 'code' : ?0}")
    Optional<Product> findProductByCode(String code);

    @Query("{ 'code' : { $in : ?0} }")
    Set<Product> findProductsByCodes(Set<String> codes);

    @ExistsQuery("{ 'code': ?0}")
    boolean existsByCode(String code);

}
