package it.millsoft.products.api.dto;

import lombok.Getter;

@Getter
public enum ProductsApiErrorEnum {

    PRODUCT_ALREADY_EXISTS("/error/product-already-exists"),
    INSUFFICIENT_STOCK("/error/insufficient-stock"),
    PRODUCT_NOT_FOUND("/error/product-not-found"),
    INVALID_INPUT("/error/invalid-input"),
    GENERIC_FAILURE("/failure/generic-failure");
    private String errorDesc;

    ProductsApiErrorEnum(String errorDesc) {
        this.errorDesc = errorDesc;
    }

}
