package it.millsoft.products.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class ProductsApiStarter {

    public static void main(String[] args)
    {
        SpringApplication.run(ProductsApiStarter.class, args);
    }

}
