package it.millsoft.products.api.interfaces.rest.controllers;

import it.millsoft.products.api.business.ProductsServices;
import it.millsoft.products.api.dto.ProductDTO;
import it.millsoft.products.api.interfaces.rest.model.Product;
import it.millsoft.products.api.interfaces.rest.model.ProductsRestApi;
import it.millsoft.products.api.mappers.ProductRestToFromDtoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@RestController
public class ProductsRestApiImpl implements ProductsRestApi {

    @Autowired
    private ProductsServices productsServices;

    @Autowired
    private ProductRestToFromDtoMapper mapper;

    @Override
    public ResponseEntity<String> addProduct(Product product) {
        ProductDTO dProduct = mapper.productRestToDto(product);
        String createdProductId = productsServices.addProduct(dProduct);
        return new ResponseEntity(createdProductId,HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Map<String,String>> addProducts(Set<Product> products) {
        Set<ProductDTO> dProducts = mapper.productsRestToDto(products);
        Map<String,String> result = productsServices.addProducts(dProducts);
        return new ResponseEntity(result,HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<Product> getProductByCode(String code) {
        ProductDTO dProduct = this.productsServices.getProductByCode(code);
        Product rProduct = mapper.productDtoToRest(dProduct);
        return new ResponseEntity(rProduct, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Product> getProductsByCode(String codes) {
        Set<String> codesAsMap = (!codes.equals("*")) ? Set.of(codes.split(",")) : null;
        Set<ProductDTO> dProducts = productsServices.getProductsByCode(codesAsMap);
        Set<Product> rProducts = mapper.productsDtoToRest(dProducts);
        return new ResponseEntity(rProducts,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Map<String, Integer>> updateStocks(Map<String, Integer> stock) {
        Map<String, Integer> result = productsServices.updateStocks(stock);
        ResponseEntity response = new ResponseEntity(result,HttpStatus.OK);
        return response;
    }

}
