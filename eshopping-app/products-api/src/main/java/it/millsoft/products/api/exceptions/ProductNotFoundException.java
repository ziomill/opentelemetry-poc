package it.millsoft.products.api.exceptions;

import it.millsoft.products.api.dto.ProductsApiErrorEnum;
import java.util.UUID;

public class ProductNotFoundException extends ProductsApiException {

    public ProductNotFoundException(String message, UUID errorUUID) {
        super(message,
              ProductsApiErrorEnum.PRODUCT_NOT_FOUND,
              errorUUID);

    }
}
