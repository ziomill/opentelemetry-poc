package it.millsoft.products.api.exceptions;

import it.millsoft.products.api.dto.ProductsApiErrorEnum;
import java.util.UUID;

public class ProductAlreadyExistsException extends ProductsApiException {
    public ProductAlreadyExistsException(String message, UUID errorUUID) {
        super(message,
              ProductsApiErrorEnum.PRODUCT_ALREADY_EXISTS,
              errorUUID);
    }

}
