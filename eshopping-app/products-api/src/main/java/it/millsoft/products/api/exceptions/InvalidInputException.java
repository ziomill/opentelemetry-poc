package it.millsoft.products.api.exceptions;

import it.millsoft.products.api.dto.ProductsApiErrorEnum;

import java.util.UUID;

public class InvalidInputException extends ProductsApiException {

    public InvalidInputException(String message,
                                 UUID errorUUID) {
        super(message,
              ProductsApiErrorEnum.INVALID_INPUT,
              errorUUID);
    }

}
