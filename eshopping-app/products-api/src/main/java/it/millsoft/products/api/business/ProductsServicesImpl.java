package it.millsoft.products.api.business;

import it.millsoft.products.api.dto.ProductDTO;
import it.millsoft.products.api.exceptions.InsufficientStockException;
import it.millsoft.products.api.exceptions.InvalidInputException;
import it.millsoft.products.api.exceptions.ProductAlreadyExistsException;
import it.millsoft.products.api.exceptions.ProductNotFoundException;
import it.millsoft.products.api.mappers.ProductDtoToFromModelMapper;
import it.millsoft.products.api.model.Product;
import it.millsoft.products.api.repositories.ProductsMongoRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class ProductsServicesImpl implements ProductsServices {

    private static Logger LOGGER = LoggerFactory.getLogger(ProductsServicesImpl.class);

    @Autowired
    private ProductsMongoRepository productsMongoRepository;

    @Autowired
    private ProductDtoToFromModelMapper mapper;

    @Override
    public String addProduct(ProductDTO product) {
        // Check if Product exists
        if(productsMongoRepository.existsByCode(product.getCode())) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("The Product having CODE %s already exists in the Inventory - Error UUID: %s",product.getCode(),errorUUID);
            LOGGER.warn(msg);
            throw new ProductAlreadyExistsException(msg,errorUUID);
        }

        // Save Product
        Product mProduct = mapper.productDtoToModel(product);
        mProduct = productsMongoRepository.save(mProduct);
        LOGGER.info("The Product having CODE {} has been added to the Inventory with ID {}",mProduct.getCode(),mProduct.getId());
        return mProduct.getId();
    }

    @Override
    public Map<String,String> addProducts(Set<ProductDTO> products) {
        // Check the input
        if(CollectionUtils.isEmpty(products)) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("No one product has been provided - Error UUID: %s", errorUUID);
            LOGGER.info(msg);
            throw new InvalidInputException(msg,errorUUID);
        }

        // The built products to add to the inventory
        List<Product> eProducts = new ArrayList<>();

        // Check and build products
        for(ProductDTO dProduct : products) {
            // Check if current Product exists
            if(productsMongoRepository.existsByCode(dProduct.getCode())) {
                LOGGER.warn("The Product having CODE {} already exists in the Inventory",dProduct.getCode());
                continue;
            }

            // Build current Product
            Product mProduct = mapper.productDtoToModel(dProduct);
            LOGGER.info("The Product having CODE {} will be added to the Inventory",mProduct.getCode(),mProduct.getId());
            eProducts.add(mProduct);
        }

        // Check if almost a product is available to be saved
        if(eProducts.size() == 0) {
            String notAddedProducts = products.stream().map(eProduct -> eProduct.getCode()).collect(Collectors.joining(","));
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("The products with codes {} has not been added because they already exists in the Inventory - Error UUID: %s",notAddedProducts, errorUUID);
            LOGGER.info(msg);
            throw new ProductAlreadyExistsException(msg,errorUUID);
        }

        // Bulk products save
        eProducts = productsMongoRepository.saveAll(eProducts);

        // Generates a String in the format: PRODUCT_ID - PRODUCT_CODE for logging
        String addedProducts = eProducts.stream().map(eProduct -> eProduct.getId() + " - " + eProduct.getCode()).collect(Collectors.joining(","));
        LOGGER.info("Added to the Inventory {} products on {}. Generated Ids are: ",eProducts.size(),products.size(),addedProducts);

        // Generates the result
        Map<String,String> result = eProducts.stream()
                                             .collect(Collectors.toMap(Product::getCode, Product::getId));
        return result;
    }

    public ProductDTO getProductByCode(String code) {
        LOGGER.debug("Sarching for PRODUCT having code {}", code);
        Optional<Product> optProduct = this.productsMongoRepository.findProductByCode(code);
        if (optProduct.isEmpty()) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("PRODUCT with code %s doesn't exist - Error UUID: %s", code, errorUUID);
            LOGGER.warn(msg);
            throw new ProductNotFoundException(msg, errorUUID);
        } else {
            Product mProduct = optProduct.get();
            LOGGER.info("The PRODUCT having code {} has been found",mProduct.getCode());
            return mapper.productModelToDto(mProduct);
        }
    }

    public Set<ProductDTO> getProductsByCode(Set<String> codes) {
        LOGGER.info("Listing products having codes: {}", (CollectionUtils.isNotEmpty(codes)) ? codes.stream().map(Object::toString).collect(Collectors.joining(",")) : "ALL_PRODUCTS");

        // If codes is empty, returns all products
        Set<Product> mProducts;
        if(CollectionUtils.isNotEmpty(codes)) {
            mProducts = productsMongoRepository.findProductsByCodes(codes);
        } else {
            mProducts = new HashSet<>(productsMongoRepository.findAll());
        }

        LOGGER.info("{} products has been found",mProducts.size());
        Set<ProductDTO> result = mapper.productsModelToDto(mProducts);
        return result;
    }

    @Override
    public Map<String, Integer> updateStocks(Map<String, Integer> requestedStocks) {
        if(MapUtils.isEmpty(requestedStocks)) {
            UUID errorUUID = UUID.randomUUID();
            String msg = String.format("No PRODUCT has been provided - Error UUID: %s",errorUUID);
            LOGGER.warn(msg);
            throw new InvalidInputException(msg,errorUUID);
        }

        // Get products quantity
        Set<Product> mProducts = productsMongoRepository.findProductsByCodes(requestedStocks.keySet());

        // Check constraints and calculate new products quantity
        Map<String,Integer> newAvailableStocks =  new HashMap<>();
        for(Map.Entry<String,Integer> entry : requestedStocks.entrySet()) {

            String productCode = entry.getKey();

            // Check product existence
//            Product mProduct = new Product(productCode);
//            if(!mProducts.contains(mProduct)) {
//                UUID errorUUID = UUID.randomUUID();
//                String msg = String.format("PRODUCT with code %s doesn't exist - Error UUID: %s", productCode,errorUUID);
//                LOGGER.warn(msg);
//                throw new ProductNotFoundException(msg,errorUUID);
//            }

            // Check requested vs available products quantity
//            product = products.get(products.indexOf(product));
            Optional<Product> optProduct = mProducts.stream().filter(mProduct -> mProduct.getCode().equals(productCode)).findFirst();
            if(optProduct.isEmpty()) {
                UUID errorUUID = UUID.randomUUID();
                String msg = String.format("PRODUCT with code %s doesn't exist - Error UUID: %s", productCode,errorUUID);
                LOGGER.warn(msg);
                throw new ProductNotFoundException(msg,errorUUID);
            }
            Product mProduct = optProduct.get();
            int availableStock = mProduct.getAvailableStock();
            int desiredStock = entry.getValue();
            if(desiredStock > availableStock) {
                UUID errorUUID = UUID.randomUUID();
                String msg = String.format("The product with code %s is not available for the requested requestedStocks %d. The available requestedStocks is %d - Error UUID: %s",mProduct.getCode(),
                                                                                                                                                                                  desiredStock,
                                                                                                                                                                                  availableStock,
                                                                                                                                                                                  errorUUID);
                LOGGER.warn(msg);
                throw new InsufficientStockException(msg,errorUUID);
            }

            // Update product's quantity
            int newStock = availableStock - desiredStock;
            mProduct.setAvailableStock(newStock);

            // Add the new available stock to the result
            newAvailableStocks.put(productCode,newStock);

            LOGGER.info("The new calculated requestedStocks for the PRODUCT with code {} is {}",productCode,newStock);
        }

        // Update products with new calculated requestedStocks
        productsMongoRepository.saveAll(mProducts);
        LOGGER.info("PRODUCTS requestedStocks having codes {} has been updated",requestedStocks.keySet());

        return newAvailableStocks;
    }

}
