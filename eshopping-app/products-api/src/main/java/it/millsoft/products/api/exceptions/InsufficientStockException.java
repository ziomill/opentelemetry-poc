package it.millsoft.products.api.exceptions;

import it.millsoft.products.api.dto.ProductsApiErrorEnum;
import java.util.UUID;

public class InsufficientStockException extends ProductsApiException {

    public InsufficientStockException(String message, UUID errorUUID) {
        super(message,
              ProductsApiErrorEnum.INSUFFICIENT_STOCK,
              errorUUID);
    }

}
