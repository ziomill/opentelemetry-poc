package it.millsoft.products.api.interfaces.rest.handlers;


import it.millsoft.products.api.commons.Utils;
import it.millsoft.products.api.dto.ProductsApiErrorEnum;
import it.millsoft.products.api.exceptions.*;
import it.millsoft.products.api.interfaces.rest.model.ProductsApiErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.UUID;

@ControllerAdvice
public class HttpExceptionsHandler extends ResponseEntityExceptionHandler {

    private static Logger LOGGER = LoggerFactory.getLogger(HttpExceptionsHandler.class);

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleGenericError(Exception ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", ProductsApiErrorEnum.GENERIC_FAILURE.name());
        ProductsApiErrorResponse errorBody = this.buildGenericError(ex);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    @ExceptionHandler(ProductAlreadyExistsException.class)
    public ResponseEntity<Object> handleApplicativeError(ProductAlreadyExistsException ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", ex.getError().name());
        ProductsApiErrorResponse errorBody = this.buildApplicativeError(ex,HttpStatus.CONFLICT);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity<Object> handleApplicativeError(ProductNotFoundException ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", ex.getError().name());
        ProductsApiErrorResponse errorBody = this.buildApplicativeError(ex,HttpStatus.NOT_FOUND);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    @ExceptionHandler(InvalidInputException.class)
    public ResponseEntity<Object> handleApplicativeError(InvalidInputException ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", ex.getError().name());
        ProductsApiErrorResponse errorBody = this.buildApplicativeError(ex,HttpStatus.BAD_REQUEST);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    @ExceptionHandler(InsufficientStockException.class)
    public ResponseEntity<Object> handleApplicativeError(InsufficientStockException ex, WebRequest request) {
        LOGGER.debug("REST - Intercepted error --> {}", ex.getError().name());
        ProductsApiErrorResponse errorBody = this.buildApplicativeError(ex,HttpStatus.FORBIDDEN);
        return this.handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(errorBody.getStatus()), request);
    }

    private ProductsApiErrorResponse buildApplicativeError(ProductsApiException ex, HttpStatus status) {
        String type = ex.getError().getErrorDesc();
        String instance = Utils.errorInstanceAsURN(ex.getErrorUUID());
        String title = ex.getError().name();
        String details = ex.getMessage();
        ProductsApiErrorEnum error = ex.getError();
        ProductsApiErrorResponse result = new ProductsApiErrorResponse(type,
                instance,
                title,
                details,
                ProductsApiErrorResponse.ApplicativeErrorEnum.fromValue(error.name()),
                status.value());
        return result;
    }

    private ProductsApiErrorResponse buildGenericError(Exception ex) {
        UUID errorUUID = UUID.randomUUID();
        String instance = Utils.errorInstanceAsURN(errorUUID);
        String type = ProductsApiErrorEnum.GENERIC_FAILURE.getErrorDesc();
        String title = ProductsApiErrorEnum.GENERIC_FAILURE.name();
        String details = ex.getMessage();
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
        ProductsApiErrorResponse result = new ProductsApiErrorResponse(type,
                instance,
                title,
                details,
                ProductsApiErrorResponse.ApplicativeErrorEnum.GENERIC_FAILURE,
                status.value());
        String msg = String.format(ex.getMessage() + " - Error UUID: %s", errorUUID);
        LOGGER.error(msg);
        return result;
    }

}
