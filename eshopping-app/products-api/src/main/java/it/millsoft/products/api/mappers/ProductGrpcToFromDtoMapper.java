package it.millsoft.products.api.mappers;

import it.millsoft.products.api.dto.ProductDTO;
import it.millsoft.products.api.interfaces.grpc.model.Product;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;

import java.util.Set;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING,
        collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED)
public interface ProductGrpcToFromDtoMapper {

    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "availableStock", source = "availableStock")
    ProductDTO productGrpcToDto(Product product);

    Set<ProductDTO> productsGrpcToDto(Set<Product> products);

    @Mapping(target = "code", source = "code")
    @Mapping(target = "description", source = "description")
    @Mapping(target = "category", source = "category")
    @Mapping(target = "price", source = "price")
    @Mapping(target = "availableStock", source = "availableStock")
    Product productDtoToGrpc(ProductDTO product);

    Set<Product> productsDtoToGrpc(Set<ProductDTO> products);



}
