package it.millsoft.products.api.interfaces.grpc.services;

import io.grpc.stub.StreamObserver;
import it.millsoft.products.api.business.ProductsServices;
import it.millsoft.products.api.business.ProductsServicesImpl;
import it.millsoft.products.api.dto.ProductDTO;
import it.millsoft.products.api.interfaces.grpc.model.*;
import it.millsoft.products.api.mappers.ProductGrpcToFromDtoMapper;
import net.devh.boot.grpc.server.service.GrpcService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@GrpcService
public class ProductsGrpcApiImpl extends ProductsGrpcApiGrpc.ProductsGrpcApiImplBase {

    private static Logger LOGGER = LoggerFactory.getLogger(ProductsServicesImpl.class);

    @Autowired
    private ProductsServices productsServices;

    @Autowired
    private ProductGrpcToFromDtoMapper mapper;

    @Override
    public void getProductByCode(GetProductByCodeRequest request, StreamObserver<GetProductByCodeResponse> responseObserver) {
        // Call Business
        ProductDTO dProduct = this.productsServices.getProductByCode(request.getCode());

        // Build GRPC Result
        Product gProduct = this.mapper.productDtoToGrpc(dProduct);
        GetProductByCodeResponse response = GetProductByCodeResponse.newBuilder()
                .setProduct(gProduct)
                .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void getProductsByCode(GetProductsByCodeRequest request, StreamObserver<GetProductsByCodeResponse> responseObserver) {
        // Call Business
        Set<String> codes = request.getCodesList().stream().collect(Collectors.toSet());
        Set<ProductDTO> dProducts = productsServices.getProductsByCode(codes);

        // Build GRPC Result
        Set<Product> gProducts = mapper.productsDtoToGrpc(dProducts);

        // Build GRPC Response
        GetProductsByCodeResponse response = GetProductsByCodeResponse.newBuilder()
                                                                    .addAllProduct(gProducts)
                                                                    .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void addProduct(AddProductRequest request, StreamObserver<AddProductResponse> responseObserver) {
        // Call Business
        ProductDTO dProduct = this.mapper.productGrpcToDto(request.getProduct());
        String bResult = productsServices.addProduct(dProduct);

        // Build GRPC Response
        AddProductResponse response = AddProductResponse.newBuilder()
                                                        .setId(bResult)
                                                        .build();
        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void addProducts(AddProductsRequest request, StreamObserver<AddProductsResponse> responseObserver) {
        // Call Business
        Set<ProductDTO> dProducts = this.mapper.productsGrpcToDto(new HashSet<>(request.getProductsList()));
        Map<String,String> createdProducts = productsServices.addProducts(dProducts);

        // Build GRPC Response
        AddProductsResponse response = AddProductsResponse.newBuilder()
                                                          .putAllCodesToIdsMap(createdProducts)
                                                          .build();
        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public void updateStocks(UpdateStocksRequest request, StreamObserver<UpdateStocksResponse> responseObserver) {
        // Call Business
        Map<String,Integer> stock = request.getCodesToStocksMap();
        Map<String,Integer> result = productsServices.updateStocks(stock);

        // Build GRPC Response
        UpdateStocksResponse response = UpdateStocksResponse.newBuilder()
                .putAllCodesToStocks(result)
                .build();

        // Send Response
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

}
