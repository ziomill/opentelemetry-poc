package it.millsoft.products.api.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@Data
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ProductDTO {

    @NonNull
    @EqualsAndHashCode.Include
    private String code;
    @NonNull
    private String description;
    @NonNull
    private String category;
    @NonNull
    private Double price;

    private Integer availableStock = 0;

}
