// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: products_api__server_grpc_interfaces__v0.proto

package it.millsoft.products.api.interfaces.grpc.model;

public interface GetProductsByCodeResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:GetProductsByCodeResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>repeated .Product product = 1;</code>
   */
  java.util.List<it.millsoft.products.api.interfaces.grpc.model.Product> 
      getProductList();
  /**
   * <code>repeated .Product product = 1;</code>
   */
  it.millsoft.products.api.interfaces.grpc.model.Product getProduct(int index);
  /**
   * <code>repeated .Product product = 1;</code>
   */
  int getProductCount();
  /**
   * <code>repeated .Product product = 1;</code>
   */
  java.util.List<? extends it.millsoft.products.api.interfaces.grpc.model.ProductOrBuilder> 
      getProductOrBuilderList();
  /**
   * <code>repeated .Product product = 1;</code>
   */
  it.millsoft.products.api.interfaces.grpc.model.ProductOrBuilder getProductOrBuilder(
      int index);
}
