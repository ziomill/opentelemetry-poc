package it.millsoft.products.api.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Document("products")
public class Product {

    @Id
    private String id;

    @EqualsAndHashCode.Include
    private String code;

    private String description;
    private String category;
    private double price;
    private int availableStock;

    public Product(String code) {
        this.code = code;
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        Product product = (Product) o;
//        return getCode().equals(product.getCode());
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(getCode());
//    }
}
